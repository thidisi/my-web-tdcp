<?php

namespace App\Events\Apply;

use App\Models\Candidate;
use Illuminate\Queue\SerializesModels;

class JobCVMail
{
    use SerializesModels;

    /**
     * @var Candidate
     */
    public $candidate;


    /**
     * @var array
     */
    public $data;

    /**
     * JobCVMail constructor.
     * @param Candidate $candidate
     */
    public function __construct(Candidate $candidate, $data)
    {
        $this->candidate = $candidate;
        $this->data = $data;
    }
}
