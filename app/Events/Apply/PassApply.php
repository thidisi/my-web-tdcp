<?php

namespace App\Events\Apply;

use App\Models\Candidate;
use Illuminate\Queue\SerializesModels;

class PassApply
{
    use SerializesModels;

    /**
     * @var Candidate
     */
    public $candidate;


    /**
     * @var array
     */
    public $data;

    /**
     * PassApply constructor.
     * @param Candidate $candidate
     */
    public function __construct(Candidate $candidate, $data)
    {
        $this->candidate = $candidate;
        $this->data = $data;
    }
}
