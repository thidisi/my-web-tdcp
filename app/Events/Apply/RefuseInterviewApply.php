<?php

namespace App\Events\Apply;

use App\Models\Candidate;
use Illuminate\Queue\SerializesModels;

class RefuseInterviewApply
{
    use SerializesModels;

    /**
     * @var Candidate
     */
    public $candidate;


    /**
     * @var array
     */
    public $data;

    /**
     * RefuseInterviewApply constructor.
     * @param Candidate $candidate
     */
    public function __construct(Candidate $candidate, $data)
    {
        $this->candidate = $candidate;
        $this->data = $data;
    }
}
