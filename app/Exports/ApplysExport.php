<?php

namespace App\Exports;

use App\Models\Apply;
use App\Models\JobDetail;
use App\Models\SettingJob;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ApplysExport implements FromView
{
    public function __construct(array $data = array())
    {
        $this->data = $data;
    }

    public function query()
    {
        $monthSearch = $this->data['month'];
        $yearSearch = $this->data['year'];
        $user = User::query()->find($this->data['id']);
        $object = null;
        if($user !== null) {
            if ($user->role_id != 0 && $user->role_id != '1') {
                $object = Apply::query()
                    ->whereMonth('created_at', $monthSearch)
                    ->whereYear('created_at', $yearSearch)
                    ->where(function ($query) use ($user) {
                        $query->where('user_id', $user->id)->orWhere('user_id', null);
                    })->whereHas('jobs', fn ($query) =>
                    $query->where('assignee', "0")->orWhere('assignee', $user->role_id))
                    ->latest('code');
            } else {
                $object = Apply::query()
                    ->with('candidates', 'jobs', 'customers')
                    ->whereMonth('created_at', $monthSearch)
                    ->whereYear('created_at', $yearSearch)
                    ->latest('code');
            }
        }
        return $object;
    }

    public function view(): View
    {
        $infos = $this->query()->get();
        $infos = $infos->map(function ($query) {
            $query->format_created_at = $query->created_at->format('H:i d-m-Y');
            $query->format_name = $query->candidates->name;
            $query->format_email = $query->candidates->email;
            $query->format_skill = SettingJob::query()->find($query->skills) ? ucfirst(SettingJob::query()->find($query->skills)->name) : null;
            $query->format_address = JobDetail::query()->where('job_id', $query->job_id)->first() ? ucfirst(JobDetail::query()->where('job_id', $query->job_id)->first()->address) : null;
            $query->format_user = User::query()->find($query->user_id) ? User::query()->find($query->user_id)->name : null;
            $query->format_phone =  $query->candidates->phone;
            return $query;
        });
        return view('admin.exports.applies', [
            'data' => $infos,
        ]);
    }
}
