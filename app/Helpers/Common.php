<?php

use App\Models\Permission;

//Check permission to redirect
if (!function_exists('checkPermissionToRedirect')) {
    function checkPermissionToRedirect($permission = 0)
    {
        $arrSessionPermission = session('sessionPermission');
        if (!empty($arrSessionPermission)) {
            if (in_array($permission, $arrSessionPermission)) {
                return true;
            }
            return false;
        }
        return true;
    }
}

if (!function_exists('getWebURL')) {
    /**
     * Build web url with query params
     *
     * @param string $segment
     * @param array $queryParams
     * @return string
     */
    function getWebURL(string $segment, array $queryParams = [])
    {
        $queryString = http_build_query($queryParams);
        return config('app.url') . $segment . ($queryString ? '?' . http_build_query($queryParams) : '');
    }
}
