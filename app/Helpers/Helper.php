<?php

function h_paginate($jobs)
{
    $html = '';
    if ($jobs->hasPages()) {
        $html .= '<div class="data-table-toolbar">';
        $html .= '<ul class="pagination">';
        if ($jobs->onFirstPage()) {
            $html .= '<li class="disabled"><a>&laquo;</a></li>';
        } else {
            $html .= '<li><a href="' . $jobs->previousPageUrl() . '" rel="prev">&laquo;</a></li>';
        }
        for ($i = 1; $i <= $jobs->lastPage(); $i++) {
            $class = ($jobs->currentPage() == $i) ? ' active' : '';
            $html .= '<li class="' . $class . '">';
            $html .= '<a href="' . $jobs->url($i) . '">' . $i . '</a>';
            $html .= '</li>';
        }
        if ($jobs->hasMorePages()) {
            $html .= '<li><a href="' . $jobs->nextPageUrl() . '" rel="next">&raquo;</a></li>';
        } else {
            $html .= '<li class="disabled"><a>&raquo;</a></li>';
        }
        $html .= '</ul>';
        $html .= '</div>';
    }
    return $html;
}

function h_option_job()
{
    $job = \DB::table('options')->where('key', 'job')->value('value');
    return json_decode($job, true);
}

function h_job_hots()
{
    return \DB::table('jobs')->where('status', 1)->pluck('position');
}

function h_svg_hide()
{
    return '<svg style="width: 10px;" data-v-38af99a7="" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="parking-circle-slash" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" class="svg-inline--fa fa-parking-circle-slash fa-w-16 fa-lg"><path data-v-38af99a7="" fill="currentColor" d="M248 8C111.03 8 0 119.03 0 256s111.03 248 248 248 248-111.03 248-248S384.97 8 248 8zm0 432c-101.46 0-184-82.54-184-184 0-30.83 7.71-59.86 21.17-85.41l286.39 221.35C338.86 421.69 295.58 440 248 440zm-9.77-232H280c8.82 0 16 7.18 16 16s-7.18 16-16 16h-.37l-41.4-32zm172.6 133.41l-76.42-59.06C350.07 267.74 360 247.1 360 224c0-44.18-35.82-80-80-80h-96c-6.91 0-12.67 4.43-14.9 10.57l-44.66-34.51C157.14 90.31 200.42 72 248 72c101.46 0 184 82.54 184 184 0 30.83-7.71 59.86-21.17 85.41zM168 352c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-19.48l-64-49.46V352z"></path></svg> Ẩn';
}

function h_svg_show()
{
    return '<svg style="width: 10px;" data-v-38af99a7="" aria-hidden="true" focusable="false" data-prefix="far" data-icon="parking-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" class="svg-inline--fa fa-parking-circle fa-w-16 fa-lg"><path data-v-38af99a7="" fill="currentColor" d="M256.09 135.91h-80c-8.84 0-16 7.16-16 16v216c0 8.84 7.16 16 16 16h16c8.84 0 16-7.16 16-16v-56h48c48.53 0 88-39.47 88-88s-39.47-88-88-88zm0 128h-48v-80h48c22.06 0 40 17.94 40 40s-17.94 40-40 40zM248 8C111.03 8 0 119.03 0 256s111.03 248 248 248 248-111.03 248-248S384.97 8 248 8zm0 448c-110.28 0-200-89.72-200-200S137.72 56 248 56s200 89.72 200 200-89.72 200-200 200z"></path></svg> Công khai';
}

function h_action_admin($row, $change_status = true, $edit = true, $delete = true)
{

    if($row->status === 'active') {
        $status = h_svg_hide();
    }else {
        $status = h_svg_show();
    }
    $routeDelete = url('/' . 'admin/jobs/' . $row->id);
    $routeStatus = url('/' . 'admin/jobs/status/' . $row->id);

    if ($edit) {
        $edit = '<a href="/admin/jobs/' . $row->id . '/edit" data-toggle="tooltip" title="Edit" class="action-custom"><button type="button" class="btn-custom-action"><i class="fa fa-edit"></i>Sửa</button></a><br />';
    }

    if ($change_status) {
        $change_status = '<button data-route="' . $routeStatus . '" title="Change status Job"  type="button" class="btn-custom-action change_status">' . $status . '</button><br />';
    }

    if ($delete) {
        $delete = '<button type="button" data-route="' . $routeDelete . '" data-toggle="tooltip"
        title="Destroy Job" class="btn-custom-action button-delete"><i class="fa fa-trash-o"></i>Xóa</button>';
    }
    $actionBtn = '
                <div class="btn-group btn-group-xs">
                    ' . $edit . '
                    ' . $change_status . '
                    ' . $delete . '
                </div>';
    return $actionBtn;
}

function blog_action_admin($row, $show = true, $edit = true, $delete = true)
{
    $status = ($row->status == 'active') ? h_svg_hide() : h_svg_show();
    $name_status = ($row->status) ? 'Private Job' : 'Pulic Job';

    if ($show) {
        $show = '<a href="/admin/blog/update/' . $row->id . '" data-toggle="tooltip" title="Edit" class="action-custom"><button type="button" class="btn-custom-action"><i class="fa fa-edit"></i>&nbsp;&nbsp;Sửa</button></a><br />';
    }

    if ($edit) {
        $edit = '<a href="/admin/jobs/change-status/' . $row->id . '"  data-toggle="tooltip" title="' . $name_status . '" class="action-custom"><button type="button" class="btn-custom-action">' . $status . '</button></a>';
    }

    if ($delete) {
        $delete = '<a href="/admin/blog/delete/' . $row->id . '" data-toggle="tooltip" title="Delete" class="action-custom"><button type="button" class="btn-custom-action"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Xóa</button></a>';
    }
    $actionBtn = '
                <div class="btn-group btn-group-xs">
                    ' . $show . '
                    ' . $edit . '
                    ' . $delete . '
                </div>';
    return $actionBtn;
}

function h_paginate_client($jobs)
{
    $html = '';
    if ($jobs->hasPages()) {
        $html .= '<div class="data-table-toolbar">';
        $html .= '<ul class="pagination modal-4">';
        if ($jobs->onFirstPage()) {
            $html .=    '<li style="width: 100px;"><a href="#" class="prev"><i class="fa fa-chevron-left"></i> Previous</a></li>';
        } else {
            $html .= '<li><a href="' . $jobs->previousPageUrl() . '" class="prev"><i class="fa fa-chevron-left"></i> Previous</a></li>';
        }
        for ($i = 1; $i <= $jobs->lastPage(); $i++) {
            if ($i > 5) {
                $html .= '...';
                break;
            }
            $class = ($jobs->currentPage() == $i) ? ' active' : '';
            $html .= '<li>';
            $html .= '<a class="' . $class . '" href="' . $jobs->url($i) . '">' . $i . '</a>';
            $html .= '</li>';
        }
        if ($jobs->hasMorePages()) {
            $html .= '<li style="width: 100px;"><a href="' . $jobs->nextPageUrl() . '" class="next"> Next  <i class="fa fa-chevron-right"></i></a></li>';
        } else {
            $html .= '<li class="disabled"><a>&raquo;</a></li>';
        }
        $html .= '</ul>';
        $html .= '</div>';
    }
    return $html;
}

function account_action_admin($row, $change_status = true, $edit = true, $delete = true)
{

    if ($row->status === 'active') {
        $status = h_svg_hide();
    } elseif($row->status === 'pending') {
        $status = h_svg_show();
    }else {
        $status = h_svg_show();
    }
    $routeDelete = url('/' . 'admin/jobs/' . $row->id);
    $routeStatus = url(route('account.status', $row->id));

    if ($edit) {
        $edit = '<button data-id="' . $row->id . '" title="Thay đổi thông tin người dùng"  type="button" class="btn-custom-action update_information"><i class="fa fa-edit"></i>Sửa</button><br />';
    }

    if ($change_status) {
        $change_status = '<button data-route="' . $routeStatus . '" title="Thay đổi trạng thái"  type="button" class="btn-custom-action change_status">' . $status . '</button><br />';
    }

    if ($delete) {
        $delete = '<button type="button" data-route="' . $routeDelete . '" data-toggle="tooltip"
        title="Xóa" class="btn-custom-action button-delete"><i class="fa fa-trash-o"></i>Xóa</button>';
    }
    $actionBtn = '
                <div class="btn-group btn-group-xs">
                    ' . $edit . '
                    ' . $change_status . '
                    ' . $delete . '
                </div>';
    return $actionBtn;
}
