<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->roles = $role;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->role_id == null) {
            $roles = $this->roles->get();
            return view('admin.accounts.index', ['roles' => $roles]);
        } else {
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = $this->user->findOrFail($id);
            $user->parent = $this->user->find($user->parent_id);
            return response()->json($user, 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 405);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = $this->user->findOrFail($id);
            $user->name = $request->name;
            $user->role_id = $request->role_id;
            $user->parent_id = $request->parent_id;
            $user->save();
            return response()->json($user, 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 405);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_api(Request $request)
    {
        if ($request->ajax()) {
            $accounts =  $this->user->with('roles')->whereNotNull('password')->whereNotNull('role_id')->latest('code')->get();
        }
        return DataTables::of($accounts)
            ->addIndexColumn()
            ->addColumn('action', function ($object) {
                return account_action_admin($object, true, true, false);
            })
            ->addColumn('role', function ($object) {
                return $object->roles->name;
            })
            ->addColumn('status', function ($object) {
                if ($object->status == User::USER_STATUS['ACTIVE']) {
                    return '<span class="label label-success">Đang hoạt động</span>';
                } elseif($object->status == User::USER_STATUS['PENDING']) {
                    return '<span class="label label-info">Đợi xác thực</span>';
                }
                return '<span class="label label-danger">Vô hiệu hóa</span>';
            })
            ->rawColumns(['action', 'role','status'])
            ->make(true);
    }

    /**
     * Change status of user
     *
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($id)
    {
        try {
            $user = $this->user->findOrFail($id);
            if ($user->status == User::USER_STATUS['ACTIVE']) {
                $user->status = User::USER_STATUS['INACTIVE'];
            } else {
                $user->status = User::USER_STATUS['ACTIVE'];
            }
            $user->save();
            return response()->json($user, 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 405);
        }
    }

    /**
     * Get list of user.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_user_api(Request $request)
    {
        try {
            $users = $this->user->where('status', User::USER_STATUS['ACTIVE'])
                ->where('role_id', '!=', NULL)
                ->where('name', 'LIKE', "%$request->search%")
                ->where('name', '!=', $request->member_name)->get();
            return response()->json($users, 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 405);
        }
    }
}
