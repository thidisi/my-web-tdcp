<?php

namespace App\Http\Controllers\Admin;

use App\Events\Apply\FailApply;
use App\Events\Apply\InterviewApply;
use App\Events\Apply\JobCVMail;
use App\Events\Apply\PassApply;
use App\Events\Apply\RefuseInterviewApply;
use App\Exports\ApplysExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\CandidateRequest;
use App\Models\Apply;
use App\Models\Blog;
use App\Models\Bonus;
use App\Models\Candidate;
use App\Models\CandidatesCustomer;
use App\Models\Category;
use App\Models\Job;
use App\Models\JobDetail;
use App\Models\Notify;
use App\Models\Role;
use App\Models\Source;
use App\Models\SettingJob;
use App\Models\SettingKpi;
use App\Models\User;
use App\Models\UserBonus;
use App\Services\ApplyService;
use App\Services\BonusService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ApplyController extends Controller
{
    public function __construct(CandidatesCustomer $candidatesCustomer, ApplyService $applyService, Candidate $candidate, JobDetail $jobDetail, Job $job, Apply $apply, Source $source, SettingJob $settingJob, UserBonus $userBonus, Bonus $bonus, User $user, SettingKpi $settingKpi, BonusService $bonusService, Notify $notify)
    {
        $this->applyService = $applyService;
        $this->settingJob = $settingJob;
        $this->candidate = $candidate;
        $this->jobDetail = $jobDetail;
        $this->apply = $apply;
        $this->job = $job;
        $this->source = $source;
        $this->candidatesCustomer = $candidatesCustomer;
        $this->userBonus = $userBonus;
        $this->bonus = $bonus;
        $this->user = $user;
        $this->notify = $notify;
        $this->kpiMonth = $settingKpi->select(['month', 'role_id'])->get();
        $this->bonusService = $bonusService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CandidateRequest $request)
    {
        try {
            $data = $request->all();
            if (!empty($data['_token'])) {
                unset($data['_token']);
            }
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $path = Storage::disk('public')->put('applyCv', $file);
                $data['file'] = $path;
            }
            $candi = $this->candidate->where('email', $request->email)->where('phone', $request->phone)->first();

            if ($candi) {
                $app = $this->apply->where('job_id', $request->job_id)->where('candidate_id', $candi->id)->first();
                if ($app) {
                    $this->apply->where('id', $app->id)->update(['file' => $data['file']]);
                } else {
                    if (!isset($request->user_id)) {
                        $apply = $this->apply->create([
                            "candidate_id" => $candi->id,
                            "job_id" => $data['job_id'],
                            "file" => $data['file'],
                            "skills" => $data['skills']
                        ]);
                    } else {
                        if (isset($data['source'])) {
                            $source_id = $this->source->where('slug', $data['source'])->firstOrFail()->id;
                        } else {
                            $source_id = isset($data['source_id']) ? $data['source_id'] : null;
                        }
                        $apply = $this->apply->create([
                            "candidate_id" => $candi->id,
                            "job_id" => $data['job_id'],
                            "file" => $data['file'],
                            "source_id" => $source_id,
                            "user_id" => $data['user_id'],
                            "skills" => $data['skills'],
                        ]);
                    }
                }
            } else {
                $checkEmail = $this->candidate->where('email', $request->email)->first();
                $checkPhone = $this->candidate->where('phone', $request->phone)->first();
                if ($checkEmail) {
                    $mess = "Email đã tồn tại";
                    return response()->json(['mess' => $mess, 'email' => true], 406);
                }
                if ($checkPhone) {
                    $mess = "Số điện thoại đã tồn tại";
                    return response()->json(['mess' => $mess, 'phone' => true], 406);
                }
                if (!isset($request->user_id)) {
                    $candidate = $this->candidate->create([
                        "name" => $data['name'],
                        "email" => $data['email'],
                        "phone" => $data['phone'],
                    ]);
                    $apply = $this->apply->create([
                        "candidate_id" => $candidate->id,
                        "job_id" => $data['job_id'],
                        "file" => $data['file'],
                        "skills" => $data['skills']
                    ]);
                } else {
                    $candidate = $this->candidate->create([
                        "name" => $data['name'],
                        "email" => $data['email'],
                        "phone" => $data['phone'],
                    ]);
                    if (isset($data['source'])) {
                        $source_id = $this->source->where('slug', $data['source'])->firstOrFail()->id;
                    } else {
                        $source_id = isset($data['source_id']) ? $data['source_id'] : null;
                    }
                    $apply = $this->apply->create([
                        "candidate_id" => $candidate->id,
                        "job_id" => $data['job_id'],
                        "file" => $data['file'],
                        "source_id" => $source_id,
                        "user_id" => $data['user_id'],
                        "skills" => $data['skills'],
                    ]);
                }
            }

            if (!empty($apply)) {
                // Gửi mail ứng tuyển của ứng viên
                $job_mail = $this->job->find($data['job_id']);
                $candidate_mail = $this->candidate->where('email', $data['email'])->first();
                $params['title'] = $job_mail->title;
                event(new JobCVMail($candidate_mail, $params));
                if ($apply->user_id && $this->user->find($apply->user_id)->role_id != null) {
                    $bonus = $this->bonusService->createBonusByUserIdAndApplyId($apply->user_id, $apply->id);
                }
                $notify['user_id'] = $apply->user_id;
                $notify['type'] = Notify::NOTIFY_TYPE['APPLY'];
                $notify['title'] = 'Đã có cv ứng tuyển mới';
                $notify['content'] = 'CV ' . $this->job->find($apply->job_id)->title . ' vừa thêm chưa có HR PIC.';
                if ($apply->user_id != null) {
                    $notify['priority'] = Notify::NOTIFY_STATUS['HIGH'];
                    $notify['content'] = $this->user->find($apply->user_id)->name . ' đã có ứng viên ứng tuyển vị trí ' . $this->job->find($apply->job_id)->title . '.';
                }
                $notify['type_id'] = $apply->id;
                $this->notify->create($notify);
            }
            $countApply = $this->apply->where('job_id', $data['job_id'])->count();
            return response()->json($countApply);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    public function getCvJob($id)
    {
        $showCv = $this->apply->with('candidates', 'users', 'jobs')->where('job_id', $id)->orderBy('code', 'DESC')->get();
        return DataTables::of($showCv)
            ->addIndexColumn()
            ->addColumn('name', function ($object) {
                return $object->candidates->name;
            })
            ->addColumn('hrpic', function ($object) {
                return $object->users != null ? $object->users->name : '-';
            })
            ->addColumn('status', function ($object) {
                $status = $object->status;
                switch ($status) {
                    case 'new':
                        $html = '<span class="label label-info">Mới</span>';
                        break;
                    case 'interview':
                        $html = '<span class="label label-warning">Xác nhận</span>';
                        break;
                    case 'rejected':
                        $html = '<span class="label label-danger">Từ chối</span>';
                        break;
                    case 'passed':
                        $html = '<span class="label label-success">Thành công</span>';
                        break;
                    case 'failed':
                        $html = '<span class="label label-danger">Trượt</span>';
                        break;
                    default:
                        $html = "-";
                        break;
                }
                return $html;
            })
            ->addColumn('date', function ($object) {
                return $object->candidates->created_at;
            })
            ->addColumn('file', function ($object) {
                $show = null;
                if ($object->file) {
                    $show = Storage::url($object->file);
                    if (!Storage::disk('public')->exists($object->file)) {
                        $show = null;
                    }
                }
                $download = url('/' . 'storage/' . $object->file);
                $url = route('api.jobs.show', $object->id);
                return '<button type="button" data-pdf="' . $show . '"
                            data-route="' . $url . '"
                            class="btn-custom-action show-filepdf-btn">
                            <i class="fa fa-eye"></i>&nbsp;&nbsp;Xem Cv</button>
                            <a href="' . $download . '" download><button
                             type="button" class="btn-custom-action"><i
                            class="fa fa-download"></i>&nbsp;&nbsp;Tải về</button></a>';
            })
            ->rawColumns(['name', 'email', 'phone', 'status', 'date', 'file'])
            ->make(true);
    }

    public function get_show_job(Request $request, $id)
    {
        try {
            $apply = $this->apply->with('candidates', 'users', 'sources')->findOrFail($id);
            $apply->created_at->format('h:s d/m/Y');
            $job = $this->job->with('job_details')->findOrFail($apply->job_id);
            $cadidate = $apply->candidates;
            $arrStatus = [];
            if ($apply->status == Apply::APPLY_STATUS['NEW']) {
                $arrStatus = Apply::APPLY_STATUS;
                unset($arrStatus['NEW'], $arrStatus['PASSED'], $arrStatus['FAILED']);
            }
            if ($apply->status == Apply::APPLY_STATUS['INTERVIEW']) {
                $arrStatus = Apply::APPLY_STATUS;
                if ($request->role != null) {
                    $arrStatus = [];
                } else {
                    unset($arrStatus['NEW'], $arrStatus['INTERVIEW'], $arrStatus['REJECTED']);
                }
            }
            $dataStatus = [];
            if (count($arrStatus) > 0) {
                foreach ($arrStatus as $value) {
                    if ($value == 'interview') {
                        $dataStatus[$value] = "Xác nhận phỏng vấn";
                    }
                    if ($value == 'rejected') {
                        $dataStatus[$value] = "Từ chối";
                    }
                    if ($value == 'failed') {
                        $dataStatus[$value] = "Phỏng vấn thất bại";
                    }
                    if ($value == 'passed') {
                        $dataStatus[$value] = "Phỏng vấn thành công";
                    }
                }
            }
            $apply->skills = $this->settingJob->find($apply->skills)->name;
            $job->job_details->level =  $this->settingJob->find($job->job_details->level)->name;
            $job->job_details->salary =  $this->settingJob->find($job->job_details->salary)->name;
            $currentDate = (Carbon::now());
            $dateline = (Carbon::parse($job->job_details->deadline));
            $diffInDays = $currentDate->diffInDays($dateline);
            if ($diffInDays > 0) {
                $job->job_details->deadline = 'Sau ' . $diffInDays . ' ngày';
            } else {
                $job->job_details->deadline = 'Việc làm hết hạn';
            }
            $apply->route = route('api.apply.status', $apply->id);

            $customer = $this->candidatesCustomer->where('apply_id', $id)->first();
            if ($customer) {
                $customer->time_actual = $customer->time_actual != null ? $customer->time_actual : 'Chưa xác định';
                $customer->start_date = $customer->start_date != null ? $customer->start_date : 'Chưa xác định';
                $customer->end_date = $customer->end_date != null ? $customer->end_date : 'Chưa xác định';
            }
            return response()->json([
                'job' => $job,
                'apply' => $apply,
                'cadidate' => $cadidate,
                'status' => $dataStatus,
                'customer' => $customer,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    public function change_status(Request $request, $id)
    {
        try {
            $status = $request->status;
            $apply = $this->apply->with('jobs')->findOrFail($id);
            $job = $this->jobDetail->where('job_id', $apply->job_id)->firstOrFail();

            $customer = '';
            if ($status != $apply->status) {
                $candidate = $this->candidate->find($apply->candidate_id);
                $params['title'] = $apply->jobs->title;
                if ($status == Apply::APPLY_STATUS['REJECTED'] || $status == Apply::APPLY_STATUS['FAILED']) {
                    $reason = $request->reason;
                    $apply->reason = $reason;
                    $params['reason'] = $reason;
                    if ($status == Apply::APPLY_STATUS['REJECTED']) {
                        // Gửi mail từ chối phỏng vấn ứng viên
                        event(new RefuseInterviewApply($candidate, $params));
                    } else {
                        // Gửi mail phỏng vấn thất bại
                        event(new FailApply($candidate, $params));
                    }
                } else if ($status == Apply::APPLY_STATUS['PASSED']) {
                    $user_list = $this->user->where('role_id', '!=', null)->get();
                    foreach ($user_list as $user) {
                        if ($user->id == $apply->user_id) {
                            $this->bonusService->updateBonusByUserIdAndApplyId($user->id, $apply->id, $status, true, $job->level);
                        } else {
                            $this->bonusService->updateBonusByUserIdAndApplyId($user->id, $apply->id, $status, false, $job->level);
                        }
                    }
                    // Gửi mail phỏng vấn thành công
                    // $params['start_date'] = date("d/m/Y", strtotime($request->start_date));
                    // event(new PassApply($candidate, $params));

                    $customer = $this->candidatesCustomer->create([
                        'apply_id' => $apply->id,
                        'level_id' => (int)$job->level,
                        'customer_onsite' => null,
                        'time_onboard' => $request->time_onboard,
                        'time_actual' => $request->time_actual ? $request->time_actual : null,
                        'start_date' => $request->start_date ? $request->start_date : null,
                        'end_date' => $request->end_date ? $request->end_date : null,
                        'salary' => $request->salary ? $request->salary : 0,
                        'recruitment_money' => $request->recruitment_money ? $request->recruitment_money : 0,
                        'sale_money' => $request->sale_money ? $request->sale_money : 0,
                        'salary_13' => $request->salary_13 ? $request->salary_13 : 0,
                        'bonus_onsite' => $request->bonus_onsite ? $request->bonus_onsite : 0,
                        'insurance' => $request->insurance ? $request->insurance : 0,
                        'team_building' => $request->team_building ? $request->team_building : 0,
                        'income' => $request->income ? $request->income : 0,
                    ]);
                    if ($customer) {
                        $customer->time_actual = $customer->time_actual != null ? $customer->time_actual : 'Chưa xác định';
                        $customer->start_date = $customer->start_date != null ? $customer->start_date : 'Chưa xác định';
                        $customer->end_date = $customer->end_date != null ? $customer->end_date : 'Chưa xác định';
                    }
                } else if ($status == Apply::APPLY_STATUS['INTERVIEW']) {
                    $apply->interview_date = $request->interview_date;
                    if ($this->user->find($apply->user_id)->role_id != null) {
                        $this->bonusService->updateBonusByUserIdAndApplyId($apply->user_id, $apply->id, $status, true, $job->level);
                    }
                    // Gửi mail cho ứng viên phỏng vấn
                    $params['date'] = date("H:i d/m/Y", strtotime($request->interview_date));
                    $params['mail_user'] = $this->user->find($apply->user_id)->email;
                    event(new InterviewApply($candidate, $params));
                }
            }
            $apply->status = $status;
            $apply->save();
            if ($request->role == null) {
                $arrStatus = [];
                if ($apply->status == Apply::APPLY_STATUS['NEW']) {
                    $arrStatus = Apply::APPLY_STATUS;
                    unset($arrStatus['NEW'], $arrStatus['PASSED'], $arrStatus['FAILED']);
                }
                if ($apply->status == Apply::APPLY_STATUS['INTERVIEW']) {
                    $arrStatus = Apply::APPLY_STATUS;
                    unset($arrStatus['NEW'], $arrStatus['INTERVIEW'], $arrStatus['REJECTED']);
                }
                $dataStatus = [];
                if (count($arrStatus) > 0) {
                    foreach ($arrStatus as $value) {
                        if ($value == 'interview') {
                            $dataStatus[$value] = "Xác nhận phỏng vấn";
                        }
                        if ($value == 'rejected') {
                            $dataStatus[$value] = "Từ chối";
                        }
                        if ($value == 'failed') {
                            $dataStatus[$value] = "Phỏng vấn thất bại";
                        }
                        if ($value == 'passed') {
                            $dataStatus[$value] = "Phỏng vấn thành công";
                        }
                    }
                }
            }
            $notify['type'] = Notify::NOTIFY_TYPE['APPLY'];
            $notify['title'] = 'Đã thay đổi cv cập nhật mới';
            $notify['type_id'] = $apply->id;
            $notify['user_id'] = $request->user_id;
            $notify['content'] = $request->user_name . ' đã thay đổi trạng thái cv ứng viên ' . $candidate->name . ' thành ' . $apply->status . '.';
            if ($request->role == '' && $request->role == 1) {
                $notify['priority'] = Notify::NOTIFY_STATUS['HIGH'];
            }
            $this->notify->create($notify);
            return response()->json([
                'apply' => $apply,
                'status' => $dataStatus ? $dataStatus  : null,
                'customer' => $customer ? $customer : null,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }
    public function change_date(Request $request)
    {
        try {
            $apl = $this->apply->findOrFail($request->id);
            $apply = $this->candidatesCustomer->where('apply_id', $request->id)->firstOrFail();
            if ($apl->status == Apply::APPLY_STATUS['PASSED']) {
                $apply->time_onboard = $request->time_onboard;
                $apply->time_actual = $request->time_actual;
                $apply->start_date = $request->start_date;
                $apply->end_date = $request->end_date;
                $apply->salary = $request->salary;
                $apply->recruitment_money = $request->recruitment_money;
                $apply->sale_money = $request->sale_money;
                $apply->salary_13 = $request->salary_13;
                $apply->bonus_onsite = $request->bonus_onsite;
                $apply->insurance = $request->insurance;
                $apply->team_building = $request->team_building;
                $apply->income = $request->income;
                $apply->save();
            }
            $apply->time_actual = $apply->time_actual != null ? $apply->time_actual : 'Chưa xác định';
            $apply->start_date = $apply->start_date != null ? $apply->start_date : 'Chưa xác định';
            $apply->end_date = $apply->end_date != null ? $apply->end_date : 'Chưa xác định';
            return response()->json([
                'apply' => $apply,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    public function export(Request $request)
    {
        $params = [];
        $splitDate = explode('/', $request->date);
        $params['id'] = $request->id;
        $params['month'] = $splitDate[0];
        $params['year'] = $splitDate[1];
        $file = Excel::download(new ApplysExport($params), 'danh-sach-ung-vien-' . $params['month'] . '-' . $params['year'] . '.xlsx');
        return $file;
    }
}
