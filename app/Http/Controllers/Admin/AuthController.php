<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\Role;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    /**
     * User model
     *
     * @param User
     */
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }
    public function login()
    {

        // // If user is already logged in, redirect to dashboard
        if (auth()->check()) {
            if (auth()->user()->status == User::USER_STATUS['INACTIVE']) {
                auth()->logout();
                return view('admin.auth.login');
            }
            if (auth()->user()->password != null) {
                if (auth()->user()->status == User::USER_STATUS['PENDING']) {
                    return view('admin.auth.login', [
                        'checkStatus' => true,
                    ]);
                }
                return redirect()->route('dashboard.index');
            }
        }
        return view('admin.auth.login');
    }

    /**
     * Login
     *
     * @param Request $request
     * @return void
     */
    public function handleLogin(Request $request)
    {
        if (auth()->attempt(['email' => $request->email, 'password' => $request->password], $request->remember ? true : false)) {
            if (auth()->user()->status == User::USER_STATUS['ACTIVE']) {
                return redirect()->route('dashboard.index');
            } elseif (auth()->user()->status == User::USER_STATUS['PENDING']) {
                return redirect()->route('admin.login');
            }
            auth()->logout();
            return back()->withErrors([
                'error' => 'Tài khoản đã bị vô hiệu hóa.',
            ]);
        }
        return back()->withErrors([
            'error' => 'Tài khoản hoặc mật khẩu không đúng.',
        ]);
    }

    public function callback($provider)
    {
        try {
            $data = Socialite::driver($provider)->user();
            $user = User::query()
                ->where('email', $data->email)
                ->whereNotNull('password')
                ->first();
            if (is_null($user)) {
                User::query()
                    ->where('email', $data->email)
                    ->delete();
                $user = new User();
                $user->email = $data->email;
                $user->role_id = '3';
            }
            $user->name   = $data->name;
            $user->avatar = $data->avatar;
            $user->save();
            Auth::login($user, true);
            if (auth()->user()->status == User::USER_STATUS['INACTIVE']) {
                auth()->logout();
                return redirect()->route('admin.login')->withErrors([
                    'error' => 'Tài khoản đã bị vô hiệu hóa.',
                ]);
            }
            return redirect()->route('admin.login');
        } catch (\Exception $e) {
            return redirect()->route('admin.login');
        }
    }

    public function registering(RegisterRequest $request)
    {
        $password = Hash::make($request->password);
        if (auth()->check()) {
            User::where('id', auth()->user()->id)
                ->update([
                    'password' => $password,
                ]);
        }
        if (auth()->user()->status == User::USER_STATUS['INACTIVE']) {
            return redirect()->route('admin.login');
        }
        return redirect()->route('dashboard.index');
    }

    /**
     * Logout
     *
     * @return void
     */
    public function logout()
    {
        auth()->logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        return redirect()->route('admin.login');
    }
}
