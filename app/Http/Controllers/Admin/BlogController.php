<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use App\Models\Category;
use App\Models\SettingJob;
use App\Services\BlogService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    public function __construct(Blog $blog, BlogService $blogService, Category $category, SettingJob $settingJob)
    {
        $this->blog = $blog;
        $this->blogService = $blogService;
        $this->category = $category;
        $this->settingJob = $settingJob;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = $this->blog->with('categories')->get();
        return view('admin.blogs.index', [
            'blog' => $blog,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cate = $this->category->all();
        return view('admin.blogs.create', [
            'category' => $cate
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        try {
            $data = $request->all();
            unset($data['_token']);
            if ($request->hasFile('banner')) {
                $images = $request->file('banner');
                $path = Storage::disk('public')->put('imageblogs', $images);
                $data['banner'] = $path;
            }
            $blogs = $this->blogService->create($data);
            return redirect()->route('blogs.index')->with('success', 'Đã thêm bài viết thành công');
        } catch (\Throwable $th) {
            return abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $viewer = $this->job->find($id)->viewer;
        return view('admin.blogs.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $blog = $this->blog->findOrFail($id);
            $cate = $this->category->all();

            if ($blog->banner != null) {
                $blog->banner = url('/' . 'storage/' . $blog->banner);
            }
            return view('admin.blogs.edit', [
                'blog' => $blog,
                'category' => $cate
            ]);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            $data['id'] = $id;
            $data['banner'] = $request->file('banner');
            $data['checkFile'] = $request->hasFile('banner');
            unset($data['_method'], $data['_token']);
            $blog = $this->blogService->update($data);

            return redirect()->route('blogs.index')->with('success', 'Đã sửa bài viết thành công');
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $blog = Blog::findOrFail($id);
            if ($blog->banner != null) {
                if (Storage::disk('public')->exists($blog->banner)) {
                    Storage::disk('public')->delete($blog->banner);
                }
            }
            $blog->delete($id);
            return redirect()->route('blogs.index')->with('success', 'Đã xóa bài viết thành công');
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }
    public function getAll()
    {

        $categories = $this->category->with('blogs')->get();
        $newpaper = $this->blog->with('categories')->latest()->paginate(9);
        $newpaper_view = $this->blog->orderby('viewer', 'DESC')->latest()->limit(4)->get();
        $skills = $this->settingJob->where('type', 'skills')->select(['id', 'name', 'slug'])->paginate(12);
        return view('client.abouts.index', [
            'categories' => $categories,
            'newpaper' => $newpaper,
            'newpaper_view' => $newpaper_view,
            'skills' => $skills,
        ]);
    }
    public function showBlogDetailClient($slug)
    {
        try {
            $blog = $this->blog->with('categories')->where('slug', $slug)->firstOrFail();
            $image = null;
            if ($blog->banner != null) {
                $image = url('/' . 'storage/' . $blog->banner);
            }
            //đếm lượt xem blog
            if (Cookie::has('blog' . $blog->id) == false) {
                $blog->viewer += 1;
                $blog->save();
                Cookie::queue(Cookie::make('blog' . $blog->id, $blog->id, 0.5));
            }
            //end đếm lượt xem blog
            // công việc liên quan
            $categories = $this->blog->with('categories')->where('category_id', $blog->category_id)->where('id', '!=', $blog->id)->limit(4)->get();
            return view('client.abouts.detail', [
                'blog' => $blog,
                'categories' => $categories,
                'image' => $image
            ]);
        } catch (\Throwable $th) {
            return redirect()->route('404');
        }
    }

    public function getCategoryBlog($slug)
    {

        try {
            $categories = $this->category->with('blogs')->get();
            $category = $this->category->where('slug', $slug)->firstOrFail();
            $newpaper = $this->blog->where('category_id', $category->id)->latest()->paginate(4);
            $newpaper_view = $this->blog->where('category_id', $category)->orderby('viewer', 'DESC')->latest()->limit(4)->get();
            $skills = $this->settingJob->where('type', 'skills')->select(['id', 'name', 'slug'])->paginate(12);
            // dd($category);
            return view('client.abouts.index', [
                'categories' => $categories,
                'newpaper' => $newpaper,
                'newpaper_view' => $newpaper_view,
                'skills' => $skills,
            ]);
        } catch (\Throwable $th) {
            return redirect()->route('404');
        }
    }
    public function filterBlogsList(Request $request)
    {
        $query = $this->blog->with('categories')->whereRelation('categories', 'id', $request->id)->get();
        foreach ($query as $key => $value) {
            $value->banner = asset('storage/'.$value->banner);
            // $value->created_at =  date('d/m/Y', strtotime($value->created_at));

        }
        return response()->json(['data' => $query], 200);
    }



}
