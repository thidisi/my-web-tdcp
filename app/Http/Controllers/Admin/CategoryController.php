<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{

    public function __construct(Category $category, CategoryService $categoryService)
    {
        $this->category = $category;
        $this->categoryService = $categoryService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::get();
        return view('admin.categories.index', [
            'categories' => $categories,
        ]);
    }

    public function list()
    {
        try {
            $categories = $this->category->get();
            foreach ($categories as $each) {
                if ($each->image_path != null) {
                    $each->image_path = url('/' . 'storage/' . $each->image_path);
                }
            }
            return response()->json(['data' => $categories], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        try {
            $data = $request->all();
            unset($data['_token']);
            if ($request->hasFile('fileImage')) {
                $images = $request->file('fileImage');
                $path = Storage::disk('public')->put('imageCategories', $images);
                $data['image_path'] = $path;
            }
            $categories = $this->categoryService->create($data);
            return response()->json([
                'category' => $categories,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.categories.index', [
            'count' => $this->category->count()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $categories = $this->category->findOrFail($id);
            if ($categories->image_path != null) {
                $categories->image_path = url('/' . 'storage/' . $categories->image_path);
            }
            return response()->json([
                'category' => $categories
            ], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            $data['id'] = $id;
            $data['fileImage'] = $request->file('fileImage');
            $data['checkFile'] = $request->hasFile('fileImage');
            unset($data['_method'], $data['_token']);
            $categories = $this->categoryService->update($data);
            return response()->json([
                'category' => $categories,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $categories = Category::findOrFail($id);
            if ($categories->image_path != null) {
                if (Storage::disk('public')->exists($categories->image_path)) {
                    Storage::disk('public')->delete($categories->image_path);
                }
            }
            $categories->delete($id);
            return response()->json([], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }
}
