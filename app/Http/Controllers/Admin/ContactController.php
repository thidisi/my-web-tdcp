<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactEmailRequest;
use App\Http\Requests\ContactRequest;
use App\Http\Requests\SubscribeRequest;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }
    public function index()
    {
        $contacts = $this->contact->all();
        return response()->json($contacts);
    }
    public function store_email(ContactEmailRequest $request)
    {
        try {
            $contact = $this->contact->create([
                'name' => $request->name,
                'email' => $request->email ? $request->email : Null,
                'content' => $request->content ? $request->content : Null,
            ]);
            return response()->json(['mess' => 'Gửi liên hệ thành công!']);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }
    public function store(ContactRequest $request)
    {
        try {
            $contact = $this->contact->create([
                'name' => $request->name,
                'email' => $request->email ? $request->email : Null,
                'content' => $request->content ? $request->content : Null,
            ]);
            return response()->json(['mess' => 'Gửi liên hệ thành công!']);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }
}
