<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Apply;
use App\Models\Role;
use App\Models\SettingJob;
use App\Models\User;
use App\Services\BonusService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class CvController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Apply $apply, SettingJob $settingJob, User $user, BonusService $bonusService)
    {
        $this->apply = $apply;
        $this->settingJob = $settingJob;
        $this->user = $user;
        $this->bonusService = $bonusService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $modalId = $request->modal;
        $info = [];
        $data = [];
        $user = $this->user->find(\Auth()->user()->id);
        if ($user->role_id != 0 && $user->role_id != '1') {
            $object = $this->apply
                ->where(function ($query) use ($user) {
                    $query->where('user_id', $user->id)->orWhere('user_id', null);
                })->whereHas('jobs', fn ($query) =>
                $query->where('assignee', "0")->orWhere('assignee', $user->role_id))
                ->get();
        } else {
            $object = $this->apply
                ->with('candidates', 'jobs', 'customers')
                ->get();
        }
        foreach($object as $each) {
            if($modalId == $each->code) {
                $info = $each->toArray();
            }
        }
        $data['data_pdf'] = null;
        $data['data_route'] = null;
        $data['role_id'] = null;
        if(count($info) > 0) {
            $data['data_pdf'] = url(Storage::url($info['file']));
            $data['data_route'] = route('api.jobs.show', $info['id']);
            $data['role_id'] = $user->role_id;
        }
        return view('admin.cv.index',[
            'info' => $data,
        ]);
    }

    public function list_by_status(Request $request)
    {
        if ($request->ajax()) {
            $status = str_replace('table_', '', $request->status);
            if ($request->date != '') {
                $splitDate = explode('/', $request->date);
                $monthSearch = $splitDate[0];
                $yearSearch = $splitDate[1];
            } else {
                $monthSearch = now()->month;
                $yearSearch = now()->year;
            }
            $user = $this->user->where('id', $request->user_id)->first();
            if ($user->role_id != 0 && $user->role_id != '1') {
                $object = $this->apply
                    ->whereMonth('created_at', $monthSearch)
                    ->whereYear('created_at', $yearSearch)
                    ->where(function ($query) use ($user) {
                        $query->where('user_id', $user->id)->orWhere('user_id', null);
                    })->whereHas('jobs', fn ($query) =>
                    $query->where('assignee', "0")->orWhere('assignee', $user->role_id))
                    ->latest('code');
            } else {
                $object = $this->apply
                    ->with('candidates', 'jobs', 'customers')
                    ->whereMonth('created_at', $monthSearch)
                    ->whereYear('created_at', $yearSearch)
                    ->latest('code');
            }

            return DataTables::of($object)
                ->addIndexColumn()
                ->addColumn('candidate_name', function ($object) {
                    $show = null;
                    // if ($object->file) {
                    //     $show = Storage::url($object->file);
                    //     if (!Storage::disk('public')->exists($object->file)) {
                    //         $show = null;
                    //     }
                    // }
                    // $url = route('api.jobs.show', $object->id);
                    $html = '<span>' . $object->candidates->name . '</span>';
                    return $html;
                })
                ->addColumn('hr_pic', function ($object) {
                    if ($object->users != null) {
                        return $object->users->name;
                    } else {
                        $html = '<button data-id="' . $object->candidates->id . '" data-apply="' . $object->id . '" title="Nhận hỗ trợ ứng viên" type="button" class="btn-custom-actions assign_to_me">Nhận ứng viên</button>';
                        return $html;
                    }
                })
                ->addColumn('customer', function ($object) {
                    return $object->customers ? $object->customers->customer_onsite : "-";
                })
                ->addColumn('level', function ($object) {
                    $level_id = $object->jobs->job_details->level;
                    $level = $this->settingJob->where('type', SettingJob::JOB_TYPE['LEVELS'])->findOrFail($level_id);
                    return $level->name;
                })
                ->addColumn('onboard', function ($object) {
                    $onboard = $object->customers != null ? $object->customers->time_onboard : '';
                    $actual = $object->customers != null ? $object->customers->time_actual : '';
                    $html = 'Dự kiến: - ' . $onboard . '<br>Thực tế: - ' . $actual . '';
                    return $html;
                })
                ->addColumn('time_start_end', function ($object) {
                    $start = $object->customers != null ? $object->customers->start_date : '';
                    $end = $object->customers != null ? $object->customers->end_date : '';
                    $html = 'Bắt đầu: - ' . $start . '<br>Kết thúc: - ' . $end . '';
                    // $html = $start . ' ~ ' . $end;
                    return $html;
                })
                ->addColumn('status', function ($object) {
                    $status = $object->status;
                    switch ($status) {
                        case 'new':
                            $html = '<span class="label label-info">Mới</span>';
                            break;
                        case 'interview':
                            $html = '<span class="label label-warning">Xác nhận</span>';
                            break;
                        case 'rejected':
                            $html = '<span class="label label-danger">Từ chối</span>';
                            break;
                        case 'passed':
                            $html = '<span class="label label-success">Thành công</span>';
                            break;
                        case 'failed':
                            $html = '<span class="label label-danger">Trượt</span>';
                            break;
                        default:
                            $html = "-";
                            break;
                    }
                    return $html;
                })
                ->addColumn('action', function ($object) {
                    $cancelAssign = '';
                    if ($object->users != null) {
                        $cancelAssign = '<a data-toggle="tooltip" data-toggle="tooltip" title="Hủy nhận ứng viên""
                        data-route=""
                        class="cancel_assign" data-id="' . $object->candidates->id . '" data-apply="' . $object->id . '" title="Hủy Nhận ứng viên"><button type="button" class="btn-custom-action d-inline-block"  ><i class="fa fa-ban"></i></button></a>';
                    }
                    $show = null;
                    if ($object->file) {
                        $show = Storage::url($object->file);
                        if (!Storage::disk('public')->exists($object->file)) {
                            $show = null;
                        }
                    }
                    $url = route('api.jobs.show', $object->id);
                    return '<div class="d-flex">
                    <a style="margin-right: 2px;" href="' . route('jobs.show', '') . '/' . $object->job_id . '" data-toggle="tooltip" title="Xem công việc"><button type="button" class="btn-custom-action d-inline-block"><i class="fa fa-list"></i></button></a>
                    <a data-toggle="tooltip" data-pdf="' . $show . '" data-toggle="tooltip" title="Xem CV của ứng viên""
                    data-route="' . $url . '"
                    class="show-filepdf-btn" title="Xem công việc"><button type="button" class="btn-custom-action d-inline-block"><i class="fa fa-eye"></i></button></a>

                    ' . $cancelAssign . '
                    </div>';
                })
                ->filter(function ($instance) use ($request, $status) {
                    if ($status != 'all') {
                        $instance->where('status', $status);
                    }
                })
                ->rawColumns(['candidate_name', 'hr_pic', 'customer', 'level', 'onboard', 'time_start_end', 'status', 'action'])
                ->make(true);
        }
    }

    function assignToMe(Request $request)
    {
        try {

            $role_id = $this->user->find($request->user_id)->role_id;
            if (empty($role_id)) {
                return response()->json([
                    'mess' => 'ADMIN không thể nhận ứng viên.'
                ], 406);
            }
            $apply = $this->apply->find($request->apply_id);
            $apply->user_id = $request->user_id;
            $apply->update();
            if (isset($apply)) {
                $bonus = $this->bonusService->createBonusByUserIdAndApplyId($apply->user_id, $apply->id);
            }
            return response()->json($request->candidate_id, 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 405);
        }
    }
    function cancelAssign(Request $request)
    {
        try {

            // $role_id = $this->user->find($request->user_id)->role_id;
            // if (empty($role_id)) {
            //     return response()->json([
            //         'mess' => 'ADMIN không thể nhận ứng viên.'
            //     ], 406);
            // }
            $apply = $this->apply->find($request->apply_id);
            $apply->user_id = null;
            $apply->update();
            // if (isset($apply)) {
            //     $bonus = $this->bonusService->createBonusByUserIdAndApplyId($apply->user_id, $apply->id);
            // }
            return response()->json($request->candidate_id, 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 405);
        }
    }
}
