<?php

namespace App\Http\Controllers\Admin;

use App\Events\Apply\InterviewApply;
use App\Http\Controllers\Controller;
use App\Models\Apply;
use App\Models\Candidate;
use App\Models\CandidatesCustomer;
use App\Models\Notify;
use App\Models\Role;
use App\Models\SettingKpi;
use App\Models\Source;
use App\Models\User;
use App\Models\UserBonus;
use App\Services\BonusService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserBonus $userBonus, SettingKpi $settingKpi, User $user, BonusService $bonusService, Source $source, Apply $apply, CandidatesCustomer $candidatesCustomer, Role $role)
    {
        $this->userBonus = $userBonus;
        $this->kpiMonth = $settingKpi->select(['month', 'role_id'])->get();
        $this->user = $user;
        $this->bonusService = $bonusService;
        $this->source = $source;
        $this->apply = $apply;
        $this->candidatesCustomer = $candidatesCustomer;
        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $currentMonth = now()->month;
            $currentYear = now()->year;
            $sum_bonus_monthly = 0;
            $sum_bonus_input = 0;
            $sum_profit = 0;
            $sum_kpi = 0;
            $kpi_team = null;
            $user_list = null;
            if (auth()->user()->role_id == null) {
                $user_list = $this->user->where('role_id', '!=', null)->get();
                $index = 1;
                foreach ($user_list as $key => $user) {
                    for ($month = 1; $month < 13; $month++) {
                        $user_bonus = $this->bonusService->getBonusInfo($user->id, $month, $currentYear);
                        if ($user_bonus) {
                            $sum_bonus_monthly += $user_bonus['role_bonus'];
                            $sum_bonus_input += $user_bonus['level_bonus'];
                            if ($month == $currentMonth) {
                                $user->bonus_tt = number_format($user_bonus['role_bonus'] + $user_bonus['level_bonus']);
                                $user->bonus = $user_bonus;
                                $user->index = $index++;
                                // $sum_kpi += number_format(explode("/", $user_bonus['candidate'])[0]);
                            }
                        } else {
                            if ($month == $currentMonth) {
                                unset($user_list[$key]);
                            }
                        }
                    }
                }

                $candidate_user_list = $this->candidatesCustomer->whereYear('created_at', $currentYear)->get();
                $cost = 0;
                if (count($candidate_user_list) > 0) {
                    foreach ($candidate_user_list as $apply) {
                        $salary = str_replace(',', '', $apply->salary);
                        $recruitment_money = str_replace(',', '', $apply->recruitment_money);
                        $sale_money = str_replace(',', '', $apply->sale_money);
                        $salary_13 = str_replace(',', '', $apply->salary_13);
                        $bonus_onsite = str_replace(',', '', $apply->bonus_onsite);
                        $insurance = str_replace(',', '', $apply->insurance);
                        $team_building = str_replace(',', '', $apply->team_building);
                        $income = str_replace(',', '', $apply->income);
                        $cost += $income - ($salary + $recruitment_money + $sale_money +  $salary_13 +  $bonus_onsite +  $team_building + $insurance);
                    }
                }
                $sum_profit = $cost - $sum_bonus_monthly - $sum_bonus_input;

                $kpi_team = $this->role->get();
                foreach ($kpi_team as $role) {
                    $count = 0;
                    foreach ($user_list as $user) {
                        if ($user->role_id == $role->id) {
                            $count += number_format(explode("/", $user->bonus['candidate'])[0]);
                        }
                    }
                    $role->kpi = $count;
                }
                $user_list = $user_list->take(5);
            } else {
                $user_list = $this->bonusService->getBonusInfo(auth()->user()->id, $currentMonth, $currentYear);
                if ($user_list) {
                    $sum_bonus_monthly = $user_list['role_bonus'];
                    $sum_bonus_input = $user_list['level_bonus'];
                }
                $cost = 0;
                // $sum_kpi = number_format(explode("/", $user_list['candidate'])[0]);
                $apply_list = $this->apply->where('user_id', auth()->user()->id)->get();
                if (count($apply_list) > 0) {
                    foreach ($apply_list as $apply) {
                        $candidate_user = $this->candidatesCustomer->whereYear('created_at', $currentYear)->where('apply_id', $apply->id)->get();
                        if (count($candidate_user) > 0) {
                            foreach ($candidate_user as $apply) {
                                $salary = str_replace(',', '', $apply->salary);
                                $recruitment_money = str_replace(',', '', $apply->recruitment_money);
                                $sale_money = str_replace(',', '', $apply->sale_money);
                                $salary_13 = str_replace(',', '', $apply->salary_13);
                                $bonus_onsite = str_replace(',', '', $apply->bonus_onsite);
                                $insurance = str_replace(',', '', $apply->insurance);
                                $team_building = str_replace(',', '', $apply->team_building);
                                $income = str_replace(',', '', $apply->income);
                                $cost += $income - ($salary + $recruitment_money + $sale_money +  $salary_13 +  $bonus_onsite +  $team_building + $insurance);
                            }
                        }
                    }
                }
                $sum_profit = $cost - $sum_bonus_monthly - $sum_bonus_input;
            }

            return view('admin.dashboard.index', [
                'bonus_monthly' => number_format($sum_bonus_monthly / 1000000, 2),
                'bonus_input' => number_format($sum_bonus_input / 1000000, 2),
                'profit' => number_format($sum_profit / 1000000, 2),
                'kpi' => $sum_kpi,
                'kpi_employee' => $user_list,
                'kpi_team' => $kpi_team,
                'currentMonth' => $currentMonth,
                'currentYear' => $currentYear,
            ]);
        } catch (\Throwable $th) {
            response()->json(['message' => config('const.not_content')], 405);
        }
    }

    public function getDataToDrawChart(Request $request)
    {
        try {
            $year = now()->year;
            $data_bonus_monthly = array();

            for ($month = 1; $month < 13; $month++) {
                //Bonus Monthly
                $bonus_month = 0;
                $bonus_input = 0;
                $user = $this->user->find($request->user_id);
                if ($user) {
                    $kpi_of_user = $this->bonusService->getBonusInfo($user->id, $month, $year);
                    if ($kpi_of_user) {
                        $bonus_month = $kpi_of_user['role_bonus'];
                        $bonus_input = $kpi_of_user['level_bonus'];
                    }
                    $cost = 0;
                    $apply_list = $this->apply->where('user_id', $request->user_id)->get();
                    if (count($apply_list) > 0) {
                        foreach ($apply_list as $apply) {
                            $candidate_user = $this->candidatesCustomer->whereMonth('created_at', $month)->whereYear('created_at', $year)->where('apply_id', $apply->id)->get();
                            if (count($candidate_user) > 0) {
                                foreach ($candidate_user as $apply) {
                                    $salary = str_replace(',', '', $apply->salary);
                                    $recruitment_money = str_replace(',', '', $apply->recruitment_money);
                                    $sale_money = str_replace(',', '', $apply->sale_money);
                                    $salary_13 = str_replace(',', '', $apply->salary_13);
                                    $bonus_onsite = str_replace(',', '', $apply->bonus_onsite);
                                    $insurance = str_replace(',', '', $apply->insurance);
                                    $team_building = str_replace(',', '', $apply->team_building);
                                    $income = str_replace(',', '', $apply->income);
                                    $cost += $income - ($salary + $recruitment_money + $sale_money +  $salary_13 +  $bonus_onsite +  $team_building + $insurance);
                                }
                            }
                        }
                    }
                    $sum_profit = $cost - $bonus_month - $bonus_input;
                    // $sum_profit = $cost - $sum_bonus_monthly - $sum_bonus_input;
                } else {
                    $user_list = $this->user->where('role_id', '!=', null)->get();
                    foreach ($user_list as $key => $user) {
                        $user_bonus = $this->bonusService->getBonusInfo($user->id, $month, $year);
                        if ($user_bonus) {
                            $bonus_month += $user_bonus['role_bonus'];
                            $bonus_input += $user_bonus['level_bonus'];
                        } else {
                            unset($user_list[$key]);
                        }
                    }
                    $candidate_user_list = $this->candidatesCustomer
                        ->whereYear('created_at', $year)->whereMonth('created_at', $month)->get();
                    $cost = 0;
                    if (count($candidate_user_list) > 0) {
                        foreach ($candidate_user_list as $apply) {
                            $salary = str_replace(',', '', $apply->salary);
                            $recruitment_money = str_replace(',', '', $apply->recruitment_money);
                            $sale_money = str_replace(',', '', $apply->sale_money);
                            $salary_13 = str_replace(',', '', $apply->salary_13);
                            $bonus_onsite = str_replace(',', '', $apply->bonus_onsite);
                            $insurance = str_replace(',', '', $apply->insurance);
                            $team_building = str_replace(',', '', $apply->team_building);
                            $income = str_replace(',', '', $apply->income);
                            $cost += $income - ($salary + $recruitment_money + $sale_money +  $salary_13 +  $bonus_onsite +  $team_building + $insurance);
                        }
                    }
                    $sum_profit = $cost - $bonus_month - $bonus_input;
                }
                $data_bonus_monthly[] = array(
                    "month" => "Tháng " . $month,
                    "value" => number_format($bonus_month / 1000000, 2),
                );
                //Bonus Input
                $data_bonus_input[] = array(
                    "month" => "Tháng " . $month,
                    "value" => number_format($bonus_input / 1000000, 2),
                );
                //Profit
                $data_profit[] = array(
                    "month" => "Tháng " . $month,
                    "value" => number_format($sum_profit / 1000000, 2),
                );
            }
            return response()->json([
                'bonus_monthly' => $data_bonus_monthly,
                'bonus_input' => $data_bonus_input,
                'profit' => $data_profit,

            ], 200);
        } catch (\Throwable $th) {
            response()->json(['message' => config('const.not_content')], 403);
        }
    }

    public function getDataSource(Request $request)
    {
        try {
            if ($request->role_id == null) {
                $users = $this->user->get(['id']);
            } else {
                $users = $this->user->where('role_id', $request->role_id)->get(['id']);
            }
            $sources = $this->source->get();
            $nameSources = [];
            $id = [];
            foreach ($sources as $item) {
                $nameSources[] = $item->name;
                $id[] = $item->id;
            }
            $arrCVPass = [];
            $arrCV = [];
            if (count($id) > 0 && count($users) > 0) {
                $applies = $this->apply->whereIn('user_id', $users)->whereIn('source_id', $id)->whereYear('created_at', now()->year)->get();
                foreach ($applies as $value) {
                    if ($value->status == Apply::APPLY_STATUS['PASSED']) {
                        $arrCVPass[$this->source->find($value->source_id)->name][] = 1;
                    }
                    $arrCV[$this->source->find($value->source_id)->name][] = 1;
                }
            }
            foreach ($nameSources as $each) {
                $totalCV[] = array_key_exists($each, $arrCV) ? count($arrCV[$each]) : 0;
                $totalCVPass[] = array_key_exists($each, $arrCVPass) ? count($arrCVPass[$each]) : 0;
            }
            return response()->json([
                'sources' => $nameSources,
                'totalCV' => $totalCV,
                'totalCVPass' => $totalCVPass,
            ], 200);
        } catch (\Throwable $th) {
            response()->json(['message' => config('const.not_content')], 403);
        }
    }
}
