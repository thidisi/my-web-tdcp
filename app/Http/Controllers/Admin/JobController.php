<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\JobRequest;
use App\Models\Apply;
use App\Models\Job;
use App\Models\JobDetail;
use App\Models\Permission;
use App\Models\Province;
use App\Models\Role;
use App\Models\SettingJob;
use App\Models\ShortUrl;
use App\Models\Source;
use App\Models\User;
use App\Services\JobService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class JobController extends Controller
{
    public function __construct(Job $job, User $user, JobDetail $jobDetail, SettingJob $settingJob, JobService $jobService, Role $role, Apply $apply, ShortUrl $shortUrl, Source $source, Province $province)
    {
        $this->job = $job;
        $this->jobService = $jobService;
        $this->user = $user;
        $this->jobDetail = $jobDetail;
        $this->settingJob = $settingJob;
        $this->role = $role;
        $this->apply = $apply;
        $this->shortUrl = $shortUrl;
        $this->level = $this->settingJob->where('type', SettingJob::JOB_TYPE['LEVELS']);
        $this->skill = $this->settingJob->where('type', SettingJob::JOB_TYPE['SKILLS']);
        $this->source = $source;
        $this->province = $province;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $skills = $this->settingJob->where('type', SettingJob::JOB_TYPE['SKILLS'])->get(['id', 'name']);
        $users = $this->role->where('status', Role::ROLE_STATUS['ACTIVE'])->get(['id', 'name']);
        $users[] = (object) ['id' => 0, 'name' => 'Tất cả'];
        return view('admin.jobs.index-data', [
            'skills' => $skills,
            'users' => $users,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list_jobs['salary'] = $this->settingJob->where('type', SettingJob::JOB_TYPE['SALARIES'])->get(['id', 'name']);
        $list_jobs['skill'] = $this->settingJob->where('type', SettingJob::JOB_TYPE['SKILLS'])->get(['id', 'name']);
        $list_jobs['level'] = $this->settingJob->where('type', SettingJob::JOB_TYPE['LEVELS'])->get(['id', 'name']);
        $list_jobs['exp'] = config('const.exp');
        $list_jobs['educate'] = config('const.educate');
        $list_jobs['work_time'] = config('const.work_time');
        $list_jobs['assignee'] = $this->role->where('status', Role::ROLE_STATUS['ACTIVE'])->get();
        $list_jobs['category_job'] = config('const.category_job');
        $list_jobs['province'] = $this->province->get();
        return view('admin.jobs.create', [
            'list_jobs' => $list_jobs
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\JobRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobRequest $request)
    {
        $data = $request->all();
        $data['job_type'] = $data['work_time'];
        $data['detail'] = [
            ['start_date' => $request->start_date],
            ['end_date' => $request->end_date],
        ];
        unset($data['_token'], $data['work_time'], $data['start_date'], $data['end_date']);
        $this->jobService->create($data);
        return redirect()->route('jobs.index')->with('success', 'Đã thêm job mới thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $countApply = $this->apply->where('job_id', $id)->count();
            $countView = $this->job->find($id)->view_count;
            $job = $this->job->with('job_details')->find($id);
            $currentDate =  Carbon::now();
            $deadline = Carbon::parse($job->job_details->deadline);
            $diffInDays = $currentDate->diffInDays($deadline, false);
            $jobDetail = $job->job_details;
            foreach ($jobDetail->skills as $each) {
                $skills[] = $this->settingJob->find($each)->name;
                $img[] = $this->settingJob->find($each)->image_path;
            }
            $image = url('/' . 'storage/' . $img[0]);
            $jobDetail->skills = $skills;
            $jobDetail->level = $this->settingJob->find($jobDetail->level)->name;
            $jobDetail->salary = $this->settingJob->find($jobDetail->salary)->name;
            $jobDetail->start_date = json_decode($jobDetail->detail)['0']->start_date;
            $jobDetail->end_date = json_decode($jobDetail->detail)['1']->end_date;
            //show all source
            $sources = $this->source->all();
            $skillAll = $this->settingJob->where('type', SettingJob::JOB_TYPE['SKILLS'])->get(['id', 'name']);
            $getEducate = config('const.educate');
            $getType = config('const.work_time');
            if (Auth::user()->role_id == null || Auth::user()->role_id == 1) {
                return view('admin.jobs.show', [
                    'job' => $job,
                    'image' => $image,
                    'jobDetail' => $jobDetail,
                    'countApply' => $countApply,
                    'countView' => $countView,
                    'sources' => $sources,
                    'skillAll' => $skillAll,
                    'diffInDays' => $diffInDays,
                    'getEducate' => $getEducate,
                    'getType' => $getType,
                ]);
            } elseif ($job->assignee == '0' | Auth::user()->role_id == $job->assignee) {
                return view('admin.jobs.show', [
                    'job' => $job,
                    'image' => $image,
                    'jobDetail' => $jobDetail,
                    'countApply' => $countApply,
                    'countView' => $countView,
                    'sources' => $sources,
                    'skillAll' => $skillAll,
                    'diffInDays' => $diffInDays,
                    'getEducate' => $getEducate,
                    'getType' => $getType,
                ]);
            } else {
                return abort('403');
            }
        } catch (\Throwable $th) {
            return abort('404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (checkPermissionToRedirect(Permission::PERMISSION_ID['CRUD-JOB'])) {
            $list_jobs['salary'] = $this->settingJob->where('type', SettingJob::JOB_TYPE['SALARIES'])->get(['id', 'name']);
            $list_jobs['skill'] = $this->settingJob->where('type', SettingJob::JOB_TYPE['SKILLS'])->get(['id', 'name']);
            $list_jobs['level'] = $this->settingJob->where('type', SettingJob::JOB_TYPE['LEVELS'])->get(['id', 'name']);
            $list_jobs['exp'] = config('const.exp');
            $list_jobs['educate'] = config('const.educate');
            $list_jobs['work_time'] = config('const.work_time');
            $list_jobs['assignees'] = $this->role->where('status', Role::ROLE_STATUS['ACTIVE'])->get();
            $list_jobs['assignees'][] = (object) ['id' => 0, 'name' => 'Tất cả'];
            $list_jobs['category_job'] = config('const.category_job');
            $list_jobs['province'] = $this->province->get();
            $job = $this->job->with('job_details')->findOrFail($id);
            $showJob['title'] = $job->title;
            $showJob['id'] = $job->id;
            $showJob['assignee'] = $job->assignee;
            $showJob['job_details'] = $job->job_details;
            $showJob['category_job'] = $job->category_job;
            $showJob = (object) $showJob;
            $showJob->start_date = json_decode($showJob->job_details->detail)['0']->start_date;
            $showJob->end_date = json_decode($showJob->job_details->detail)['1']->end_date;
            foreach ($showJob->job_details->skills as $value) {
                $showJob->skills[] = (object)['id' => $value];
            }
            if (Auth::user()->role_id == null) {
                return view('admin.jobs.edit', [
                    'list_jobs' => $list_jobs,
                    'showJob' => $showJob,
                ]);
            } elseif ($showJob->assignee == '0' | Auth::user()->role_id == $showJob->assignee) {
                return view('admin.jobs.edit', [
                    'list_jobs' => $list_jobs,
                    'showJob' => $showJob,
                ]);
            } else {
                return abort('403');
            }
        } else {
            return abort('403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['job_type'] = $data['work_time'];
        $data['id'] = $id;
        $data['detail'] = [
            ['start_date' => $request->start_date],
            ['end_date' => $request->end_date],
        ];
        $data['detail'] = json_encode($data['detail']);
        unset($data['_method'], $data['_token'], $data['work_time'], $data['start_date'], $data['end_date']);
        $this->jobService->update($data);
        return redirect()->route('jobs.index')->with('success', 'Đã sửa job thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $countApply = $this->apply->where('job_id', $id)->count();
            if ($countApply > 0) {
                return response()->json([], 422);
            }
            $job = $this->job->findOrFail($id);
            $this->shortUrl->where('destination_url', 'LIKE', "%{$job->slug}%")->delete();
            $job->delete($id);
            return response()->json($job, 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    public function changeStatus($id)
    {
        try {
            $job = $this->job->findOrFail($id);
            if ($job->status == 'active') {
                $job->status = 'inactive';
            } else {
                $job->status = 'active';
            }
            $job->save();
            return response()->json($job, 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_api(Request $request)
    {
        if ($request->user_role != '0' && $request->user_role != '1') {
            $request->merge(['user' => $request->user_role]);
        }

        if ($request->ajax()) {
            $jobs = $this->job->with(['users', 'job_details'])->latest('created_at');
            $jobIds = [];
            $jobDetails = $this->jobDetail->get();
            if (count($jobDetails) > 0) {
                foreach ($jobDetails as $each) {
                    if (in_array($request->skill, $each->skills)) {
                        $jobIds[] = $each->job_id;
                    }
                }
                if (!empty($request->skill)) {
                    $jobs->whereIn('id', $jobIds);
                }
            }
            return DataTables::of($jobs)
                ->addIndexColumn()
                ->addColumn('action', function ($object) {
                    return h_action_admin($object, true, true, true);
                })
                ->addColumn('name', function ($object) {
                    $inactive = '';
                    if ($object->status == 'inactive') {
                        $inactive = 'require';
                    }
                    $actionBtn = '<a data-toggle="tooltip" title="Xem danh sách các ứng viên đã ứng tuyển" class="title-datatable ' . $inactive . '" href="/admin/jobs/' . $object->id . '">' . $object->title . '</a> <p class="fz-12"><i data-v-38af99a7="" class="fa fa-clock-o m-r-5"></i>&nbsp;&nbsp;&nbsp;' . $object->created_at . '</p><p class="fz-12 mgt-10">Người tạo: ' . @$object->users->name . '</p>';
                    return $actionBtn;
                })
                ->addColumn('statistical', function ($object) {
                    $countApply = $this->apply->where('job_id', $object->id)->count();
                    $countViewJob = $this->job->find($object->id)->view_count;
                    $html = '
                    <div class="statical">
                    <p class="statical_view">' . $countViewJob . '</p>
                        <p>Lượt xem</p>
                    </div>
                    <div class="statical">
                    <p class="statical_view">' . $countApply . '</p>
                        <p>Ứng tuyển</p>
                    </div>';
                    return $html;
                })
                ->addColumn('deadline', function ($object) {
                    $currentDate =  Carbon::now();
                    $deadline = Carbon::parse($object->job_details->deadline);
                    $diffInDays = $currentDate->diffInDays($deadline, false);
                    if ($diffInDays < 0) {
                        $deadline = '<span class="require">• Việc làm hết hạn</span>';
                    } else {
                        $deadline = $object->job_details->deadline;
                    }
                    return $deadline;
                })
                ->addColumn('linkJd', function ($object) {
                    $disabled = '';
                    if ($object->status == 'inactive') {
                        $disabled = 'disabled';
                    }
                    $html = '
                    <button ' . $disabled . ' class="btn apply-cv-btn" data-route="' . route('store.shortUrl') . '" data-slug="' . $object->slug . '">Link JD</button>';
                    return $html;
                })
                ->filter(function ($instance) use ($request, $jobs) {
                    if ($request->get('status')) {
                        $instance->where('status', $request->get('status'));
                    }
                    if ($request->get('user')) {
                        $instance->whereIn('assignee', [$request->get('user'), '0']);
                    }
                    if (!empty($request->get('name'))) {
                        $instance->where(function ($w) use ($request) {
                            $search = $request->get('name');
                            $w->where('title', 'LIKE', "%$search%");
                        });
                    }
                })
                ->rawColumns(['action', 'deadline', 'statistical', 'linkJd', 'name'])
                ->make(true);
        }
    }

    // Client

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {

        $levels = $this->level->get(['id', 'name', 'slug']);
        $skills = $this->skill->get(['id', 'name', 'slug']);
        $jobs = $this->jobService->getListJobClient();
        $count_job = $this->job->count();
        foreach ($jobs as $each) {
            $each->skills = $this->settingJob->whereIn('id', $each->job_details->skills)->get(['id', 'name', 'image_path', 'slug']);
            $each->level =  $this->settingJob->select('id', 'name', 'slug')->find($each->job_details->level);
            $each->salary =  $this->settingJob->select('id', 'name', 'slug')->find($each->job_details->salary);
        }
        $category_jobs = config('const.category_job');
        foreach ($category_jobs as $key => $item) {
            $category_count[$key] = $this->job->with('job_details')->whereRelation('job_details', 'deadline', '>=',Carbon::now()->toDateString())->where('category_job', $key)->count();
        }
        $blogs = $this->jobService->getListBlog();
        $skillAll = $this->settingJob->where('type', SettingJob::JOB_TYPE['SKILLS'])->get(['id', 'name']);
        $address = $this->province->get();
        $jobDetails =  $this->jobDetail->get();
        foreach ($jobDetails as $key => $item) {
            foreach ($this->settingJob->where('type', SettingJob::JOB_TYPE['SKILLS'])->whereIn('id', $item->skills)->get(['name'])->toArray() as $val) {
                $skillJobs[$val['name']][] = $val['name'];
            }
        }
        foreach ($skillJobs as $key => $each) {
            if (count($each) < 10) {
                $countSkillJobs[ucfirst($key)] = '0' . count($each);
            } else {
                $countSkillJobs[ucfirst($key)] = count($each);
            }
        }
        arsort($countSkillJobs);
        $countSkillJobs = array_slice($countSkillJobs, 0, 5);
        return view('client.jobs.index', [
            'levels' => $levels,
            'skills' => $skills,
            'jobs' => $jobs,
            'blogs' => $blogs,
            'skillAll' => $skillAll,
            'category_job' => $category_jobs,
            'category_count' => $category_count,
            'count_job' => $count_job,
            'count_skill_jobs' => array_slice($countSkillJobs, 0, 5),
            'address' => $address
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function showJobDetailClient($slug, Request $request)
    {
        try {
            $checkUser = null;
            $data = null;
            if (count($request->all()) > 0) {
                $data['id'] = $request->id;
                $data['source'] = $request->source;
                $checkUser = $this->shortUrl->where('destination_url', url('/' . 'tuyen-dung/' . $slug . '?source=' . $data['source'] . '&id=' . $data['id']))->first();
                if ($checkUser == null) {
                    $data = null;
                }
            }
            $skillAll = $this->settingJob->where('type', SettingJob::JOB_TYPE['SKILLS'])->get(['id', 'name']);
            $job = $this->job->with('job_details')->where('slug', $slug)->firstOrFail();
            if ($job->status == 'active') {
                $currentDate =  Carbon::now();
                $deadline = Carbon::parse($job->job_details->deadline);
                $diffInDays = $currentDate->diffInDays($deadline, false);
                if ($diffInDays < 0) {
                    return redirect()->route('404');
                } else {
                    //đếm lượt xem job
                    if (Cookie::has('job' . $job->id) == false) {
                        $job->view_count += 1;
                        $job->save();
                        Cookie::queue(Cookie::make('job' . $job->id, $job->id, 0.5));
                    }
                    //end đếm lượt xem job
                    $jobDetail = $job->job_details;
                    // dd($diffInDays);
                    if ($diffInDays < 7) {
                        $jobDetail->deadline = 'nộp còn ' . $diffInDays + 1 . ' ngày';
                    } else {
                        $jobDetail->deadline = date('d/m/Y', strtotime($jobDetail->deadline));
                    }
                    foreach ($jobDetail->skills as $each) {
                        $skills[] = $this->settingJob->select('id', 'name', 'slug')->find($each);
                        $img[] = $this->settingJob->find($each)->image_path;
                    }
                    $image = null;
                    if ($img[0] != null) {
                        $image = url('/' . 'storage/' . $img[0]);
                    }else {
                        $image = asset('/assets/img/hachinet-viet-nam-logo.png');
                    }
                    $skillJob = $jobDetail->skills[0];
                    $jobDetail->skills = $skills;
                    $jobDetail->exp = config('const.exp.'.$jobDetail->exp);
                    $jobDetail->level = $this->settingJob->find($jobDetail->level)->name;
                    $jobDetail->salary = $this->settingJob->find($jobDetail->salary)->name;
                    $jobDetail->start_date = json_decode($jobDetail->detail)['0']->start_date;
                    $jobDetail->end_date = json_decode($jobDetail->detail)['1']->end_date;
                    $jobDetail->province_name = $this->province->find($jobDetail->province_id)->name;
                    // công việc liên quan
                    $jobDetailskill = $this->jobDetail->get();
                    if (count($jobDetailskill) > 0) {
                        foreach ($jobDetailskill as $each) {
                            if (in_array($skillJob, $each->skills)) {
                                $jobIds[] = $each->job_id;
                            }
                        }
                    }
                    $jobRelated = $this->job->with('job_details')->whereRelation('job_details', 'deadline', '>=', date('Y-m-d'))->whereIn('id', $jobIds)->where('id', '!=', $job->id)->get();
                    foreach ($jobRelated  as $each) {
                        $each->skills = $this->settingJob->whereIn('id', $each->job_details->skills)->get(['id', 'name', 'image_path', 'slug']);
                        $each->level =  $this->settingJob->select('id', 'name', 'slug')->find($each->job_details->level);
                        $each->salary =  $this->settingJob->select('id', 'name', 'slug')->find($each->job_details->salary);
                    }
                }
                $category_jobs = config('const.category_job');
                $getType = config('const.work_time');

                return view('client.jobs.detail', [
                    'job' => $job,
                    'image' => $image,
                    'jobDetail' => $jobDetail,
                    'jobRelated' => $jobRelated,
                    'dataInfo' => json_encode($data),
                    'skillAll' => $skillAll,
                    'category_jobs' => $category_jobs,
                    'getType' => $getType
                ]);
            } else {
                return redirect()->route('404');
            }
        } catch (\Throwable $th) {
            return redirect()->route('404');
        }
    }
}
