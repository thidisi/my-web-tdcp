<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Apply;
use App\Models\SettingJob;
use App\Models\SettingKpi;
use App\Models\User;
use App\Models\UserBonus;
use App\Services\BonusService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class KpiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Apply $apply, SettingKpi $settingKpi, User $user, UserBonus $userBonus, BonusService $bonusService)
    {
        $this->apply = $apply;
        $this->settingKpi = $settingKpi;
        $this->user = $user;
        $this->userBonus = $userBonus;
        $this->bonusService = $bonusService;
    }

    public function index()
    {
        return view('admin.list-kpi.index');
    }

    public function api_list_kpi(Request $request)
    {
        if ($request->user_id == null) {
            $getUserChild = $this->user->whereNotNull('role_id')->get(['id']);
        } else {
            $getUserChild = $this->user->where('parent_id', $request->user_id)->orWhere('id', $request->user_id)->get(['id']);
        }
        $dataMonth = [];
        $dateBetween = [];

        if ($request->date) {
            $dateBetween = explode(' -', $request->date);
            foreach ($dateBetween as $each) {
                $date[] = date('Y-m-d', strtotime($each));
            }
        }
        foreach ($getUserChild as $value) {
            if (is_array($dateBetween) && $request->date != null) {
                if ($this->userBonus->where('user_id', $value->id)->whereBetween('created_at', [$date['0'], $date['1']])->latest('created_at')->first() != null) {
                    $dataMonth[] = $this->userBonus->where('user_id', $value->id)->whereBetween('created_at', [$date['0'], $date['1']])->latest('created_at')->first()->id;
                }
            } elseif ($this->userBonus->where('user_id', $value->id)->latest('created_at')->first() != null) {
                $dataMonth[] = $this->userBonus->where('user_id', $value->id)->latest('created_at')->first()->id;
            }
        }
        // Begin Total Bonus KPI TT
        if (is_array($dateBetween) && $request->date != null) {
            $data = $this->userBonus->whereIn('user_id', $getUserChild)->whereBetween('created_at', [$date['0'], $date['1']])->latest('created_at')->get();
        } else {
            $data = $this->userBonus->whereIn('user_id', $getUserChild)->latest('created_at')->get();
        }
        $totals = $this->bonusService->getBonusInforTotal($data);
        // End Total Bonus KPI TT
        $object = $this->userBonus->with(['users', 'applies'])->whereRelation('users', 'status', '=', 'active')->whereIn('id', $dataMonth)->latest('created_at');
        return DataTables::of($object)
            ->addIndexColumn()
            ->addColumn('name_user', function ($object) {
                return $object->users->name;
            })
            ->addColumn('cv_customer', function ($object) {
                return json_decode($object->month)->cv_customer;
            })
            ->addColumn('cv_interview', function ($object) {
                return json_decode($object->month)->cv_interview;
            })
            ->addColumn('cv_candidate', function ($object) {
                return json_decode($object->month)->candidates;
            })
            ->addColumn('kpi', function ($object) {
                $kpi = json_decode($object->month)->candidates;
                $kpis = explode("/", $kpi);
                if (intval($kpis['0']) > 0 && intval($kpis['1']) != 0) {
                    $class = 'require';
                    if((intval($kpis['0']) / intval($kpis['1']) * 100) > 100) {
                        $class = 'success';
                    }
                    return '<span class="'.$class.'">'. number_format(intval($kpis['0']) / intval($kpis['1']) * 100, 2) . '%' .'</span>';
                } else {
                    return '-';
                }
            })
            ->addColumn('total_bonus', function ($object) use ($totals) {
                if (count($totals) > 0) {
                    $kpis = $totals['kpis'];
                    $level_bonus = $totals['level_bonus'];
                    $role_bonus = $totals['role_bonus'];
                    $kpi = 0;
                    $total_level_bonus = 0;
                    $total_role_bonus = 0;
                    if (array_key_exists($object->user_id, $kpis)) {
                        if (array_key_exists($object->id, $kpis[$object->user_id]))
                            $kpi = $kpis[$object->user_id][$object->id];
                    }
                    if (array_key_exists($object->user_id, $level_bonus)) {
                        $total_level_bonus = $level_bonus[$object->user_id];
                    }
                    if (array_key_exists($object->user_id, $role_bonus)) {
                        $total_role_bonus = $role_bonus[$object->user_id];
                    }
                    return number_format(($kpi * $total_level_bonus) + $total_role_bonus) . ' đ';
                }
                return '-';
            })
            ->rawColumns(['name_user', 'cv_customer', 'cv_interview', 'cv_candidate', 'kpi', 'total_bonus'])
            ->make(true);
    }
}
