<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Apply;
use App\Models\Notify;
use App\Models\NotifyActision;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NotifyController extends Controller
{
    public function actision(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->id;
            $priority = true;
            if ($request->role_id == '' || $request->role_id == 1) {
                $priority = false;
            }
            $resuft = Notify::notifyLists($priority, $request->role_id, $request->user_id);
            foreach ($resuft['data'] as $each) {
                NotifyActision::updateOrCreate(
                    [
                        'user_id' => $request->user_id,
                        'notify_id' => $each->id,
                    ],
                    [
                        'actision' => $each->actision,
                    ]
                );
            }
            $notify_actision = NotifyActision::where('user_id', $request->user_id)->where('notify_id', $id)->first();
            $notify_actision->actision = Notify::NOTIFY_STATUS['ACTIVE'];
            $notify_actision->save();
        }
    }

    public function bubble_notify(Request $request)
    {
        if ($request->ajax()) {
            $ListTimeLimitNotify = NotifyActision::where('user_id', $request->user_id)->where('actision', '=', Notify::NOTIFY_STATUS['ACTIVE'])->where('updated_at', '<', Carbon::now()->addDays(-3)->toDateString())->get();
            $ListIdTimeLimitNotify = $ListTimeLimitNotify->pluck('notify_id')->toArray();
            $priority = true;
            if ($request->role_id == '' || $request->role_id == 1) {
                $priority = false;
            }
            $resuft = Notify::notifyLists($priority, $request->role_id, $request->user_id, $ListIdTimeLimitNotify);
            $notifications = $resuft['data'];
            $count_notify = $resuft['count_data'];
            foreach ($notifications as $each) {
                if ($each->type == Notify::NOTIFY_TYPE['JOB']) {
                    $each->image_path = url('/' . 'storage/' . $each->image_path);
                    $each->redirect = route('jobs.show', $each->type_id);
                }

                if ($each->type == Notify::NOTIFY_TYPE['BLOG']) {
                    $each->redirect = route('blogs.index');
                }

                if ($each->type == Notify::NOTIFY_TYPE['APPLY']) {
                    $each->redirect = route('cv.index') . '?modal=' . Apply::find($each->type_id)->code;
                }
                $each->route = route('api.notify.actision', $each->id);
                $each->date = $each->created_at->format('H:i d/m/Y');
            }
            return response()->json([
                "count_notify" => $count_notify,
                "notifications" => $notifications
            ], 200);
        }
    }
}
