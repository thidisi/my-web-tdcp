<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Job;
use App\Models\JobDetail;
use App\Models\Province;
use App\Models\SettingJob;
use App\Models\User;
use App\Services\JobService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function __construct(Job $job, JobDetail $jobDetail, SettingJob $settingJob, JobService $jobService, Province $provinces)
    {
        $this->job = $job;
        $this->jobService = $jobService;
        $this->jobDetail = $jobDetail;
        $this->settingJob = $settingJob;
        $this->provinces = $provinces;
        $this->level = $this->settingJob->where('type', SettingJob::JOB_TYPE['LEVELS']);
        $this->skill = $this->settingJob->where('type', SettingJob::JOB_TYPE['SKILLS']);
        $this->salary = $this->settingJob->where('type', SettingJob::JOB_TYPE['SALARIES']);
    }

    public function search(Request $request)
    {
        try {
            $skill = $request->skill ? $request->skill : null;
            $address = null;
            $category_job = $request->category_job ? $request->category_job : null;
            if ($request->address) {
                $address = $this->provinces->where('slug', "$request->address")->firstOrFail()->slug;
            }
            if ($address == null) {
                return redirect()->route('client.findjob.skill', [$skill, 'category_job' => $category_job]);
            }
            if ($skill == null) {
                return redirect()->route('client.findjob.all', [$address, 'category_job' => $category_job]);
            }
            if ($category_job == null) {
                return redirect()->route('client.findjob.all', [$address, $skill]);
            }
            return redirect()->route('client.findjob.all', [$address, $skill, 'category_job' => $category_job]);
        } catch (\Throwable $th) {
            return redirect()->route('client.findjob.skill', $skill);
        }
    }

    public function searchAll(Request $request, $address = null, $skill = null)
    {
        try {
            $job['address'] = $address;
            $job['skill'] = $skill;
            $job['category_job'] = $request->category_job ? $request->category_job : null;
            $query = $this->jobService->search($job);
            $jobAll = $query->get();
            $jobs = $query->paginate(9);
            foreach ($jobs as $each) {
                $each->skills = $this->settingJob->whereIn('id', $each->job_details->skills)->get(['id', 'name', 'image_path', 'slug']);
                $each->level =  $this->settingJob->select('id', 'name', 'slug')->find($each->job_details->level);
                $each->salary =  $this->settingJob->select('id', 'name', 'slug')->find($each->job_details->salary);
                $each->job_details->deadline = date('d/m/Y', strtotime($each->job_details->deadline));
            }
            $category_job = config('const.category_job');
            $findJob['address'] = $address;
            $findJob['skill'] = $skill;
            $findJob['category_job'] = $request->category_job;

            $getType = config('const.work_time');
            $getExp = config('const.exp');
            $getSalary = $this->salary->orderBy('created_at', 'ASC')->get(['id', 'name', 'slug']);
            $getSkill = $this->skill->orderBy('created_at', 'ASC')->get(['id', 'name', 'slug']);
            $getCount['job'] = $jobAll->count();
            $job_id = [];
            foreach ($jobAll as $item) {
                $job_id[] = $item->id;
            }

            $query = $this->jobDetail->whereIn('job_id', $job_id)->where('deadline', '>=', now())->get();

            foreach ($getType as $key => $each) {
                $getCount['type'][$key] = $query->where('job_type', $key)->count();
            }

            foreach ($getSalary as $each) {
                $getCount['salary'][$each->slug] = $query->where('salary', $each->id)->count();
            }

            foreach ($getExp as $key => $each) {
                $getCount['exp'][$key] = $query->where('exp', $key)->count();
            }

            $str_jobId = implode(",", $job_id);
            $address = $this->provinces->get();
            return view('client.jobs.findjob', [
                'category_job' => $category_job,
                'jobs' => $jobs,
                'getSkill' => $getSkill,
                'getSalary' => $getSalary,
                'getExp' => $getExp,
                'getType' => $getType,
                'getCount' => $getCount,
                'findJob' => $findJob,
                'jobID' => $str_jobId,
                'address' => $address,
            ]);
        } catch (\Throwable $th) {
            return redirect()->route('404');
        }
    }

    public function searchSkill(Request $request, $skill = null)
    {
        try {
            $job['skill'] = $skill;
            $job['category_job'] = $request->category_job ? $request->category_job : null;
            $query = $this->jobService->search($job);
            $jobAll = $query->get();
            $jobs = $query->paginate(9);
            foreach ($jobs as $each) {
                $each->skills = $this->settingJob->whereIn('id', $each->job_details->skills)->get(['id', 'name', 'image_path', 'slug']);
                $each->level =  $this->settingJob->select('id', 'name', 'slug')->find($each->job_details->level);
                $each->salary =  $this->settingJob->select('id', 'name', 'slug')->find($each->job_details->salary);
                $each->job_details->deadline = date('d/m/Y', strtotime($each->job_details->deadline));
            }
            $category_job = config('const.category_job');
            $findJob['skill'] = $skill;
            $findJob['category_job'] = $request->category_job;

            $getType = config('const.work_time');
            $getExp = config('const.exp');
            $getSalary = $this->salary->orderBy('created_at', 'ASC')->get(['id', 'name', 'slug']);
            $getSkill = $this->skill->orderBy('created_at', 'ASC')->get(['id', 'name', 'slug']);

            $getCount['job'] = $jobAll->count();
            $job_id = [];
            foreach ($jobAll as $item) {
                $job_id[] = $item->id;
            }
            $query = $this->jobDetail->whereIn('job_id', $job_id)->where('deadline', '>=', now())->get();

            foreach ($getType as $key => $each) {
                $getCount['type'][$key] = $query->where('job_type', $key)->count();
            }

            foreach ($getSalary as $each) {
                $getCount['salary'][$each->slug] = $query->where('salary', $each->id)->count();
            }

            foreach ($getExp as $key => $each) {
                $getCount['exp'][$key] = $query->where('exp', $key)->count();
            }
            $str_jobId = implode(",", $job_id);
            $address = $this->provinces->get();
            return view('client.jobs.findjob', [
                'category_job' => $category_job,
                'jobs' => $jobs,
                'getSkill' => $getSkill,
                'getSalary' => $getSalary,
                'getExp' => $getExp,
                'getType' => $getType,
                'getCount' => $getCount,
                'findJob' => $findJob,
                'jobID' => $str_jobId,
                'address' => $address,
            ]);
        } catch (\Throwable $th) {
            return redirect()->route('404');
        }
    }

    public function filterJobsList(Request $request)
    {
        $jobIDs = explode(',', $request->str_jobId);
        if (count($jobIDs) > 0) {
            $query = $this->jobDetail->with('jobs')->whereIn('job_id', $jobIDs);
        } else {
            $query = $this->jobDetail->with('jobs');
        }
        if ($request->type) {
            $query->whereIn('job_type', $request->type)->get();
        }
        if ($request->exp) {
            $query->whereIn('exp', $request->exp);
        }
        if ($request->salary) {
            $query->whereIn('salary', $request->salary);
        }
        if ($request->timeSearch) {
            if ($request->timeSearch == 1) {
                $query->orderBy('created_at', 'DESC');
            }
            if ($request->timeSearch == 2) {
                $query->orderBy('created_at', 'ASC');
            }
        }
        $query = $query->whereRelation('jobs', 'status', Job::JOB_STATUS['ACTIVE'])->where('deadline', '>=', Carbon::now())->paginate(9);
        $category_jobs = config('const.category_job');
        $getType = config('const.work_time');
        foreach ($category_jobs as $key =>  $item) {
            if ($key == 'business-development') {
                $category_job['icon'][$key] = 'fa-pie-chart';
            } elseif ($key == 'marketing-communication') {
                $category_job['icon'][$key] = 'fa-bullhorn';
            } elseif ($key == 'customer-service') {
                $category_job['icon'][$key] = 'fa-comments-o';
            } elseif ($key == 'human-resources') {
                $category_job['icon'][$key] = 'fa-address-card-o';
            } elseif ($key == 'project-management') {
                $category_job['icon'][$key] = 'fa-calendar-o';
            } elseif ($key == 'software-engineering') {
                $category_job['icon'][$key] = 'fa-terminal';
            };
        }
        foreach ($query as $each) {
            $each->deadline = date('d/m/Y', strtotime($each->deadline));
            $each->skills = $this->settingJob->whereIn('id', $each->skills)->get(['id', 'name', 'image_path', 'slug']);
            $each->level =  $this->settingJob->select('id', 'name', 'slug')->find($each->level);
            $each->salary =  $this->settingJob->select('id', 'name', 'slug')->find($each->salary);
            $each->job_type = $getType[$each->job_type];
        }
        return response()->json(['jobs' => $query, 'category_job' => $category_job, 'category_jobs' => $category_jobs, 'url' => config('app.url')], 200);
    }
}
