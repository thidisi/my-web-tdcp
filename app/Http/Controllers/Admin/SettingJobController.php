<?php

namespace App\Http\Controllers\Admin;

use App\Models\SettingJob;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SettingJobRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SettingJobController extends Controller
{

    public function __construct(SettingJob $settingJob)
    {
        $this->settingJob = $settingJob;
        $this->skill = $this->settingJob->where('type', SettingJob::JOB_TYPE['SKILLS']);
        $this->level = $this->settingJob->where('type', SettingJob::JOB_TYPE['LEVELS']);
        
        $this->salary = $this->settingJob->where('type', SettingJob::JOB_TYPE['SALARIES']);
    }

    public function showJobType($type)
    {
        try {
            switch ($type) {
                case SettingJob::JOB_TYPE['SKILLS']:
                    $data = $this->skill->get();
                    foreach ($data as $each) {
                        if ($each->image_path != null) {
                            $each->image_path = url('/' . 'storage/' . $each->image_path);
                        }
                    }
                    return response()->json(['data' => $data], 200);
                    break;
                case SettingJob::JOB_TYPE['LEVELS']:
                    $data = $this->level->get();
                    return response()->json(['data' => $data], 200);
                    break;
                case SettingJob::JOB_TYPE['SALARIES']:
                    $data = $this->salary->get();
                    return response()->json(['data' => $data], 200);
                    break;
                default:
                    return response()->json(['message' => config('const.not_content')], 403);
                    break;
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showSkill()
    {
        return view('admin.skills.index', [
            'type' => SettingJob::JOB_TYPE['SKILLS'],
            'count' => $this->skill->count()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLevel()
    {
        return view('admin.levels.index', [
            'type' => SettingJob::JOB_TYPE['LEVELS'],
            'count' => $this->level->count()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showSalary()
    {
        return view('admin.salaries.index', [
            'type' => SettingJob::JOB_TYPE['SALARIES'],
            'count' => $this->salary->count()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\SettingJobRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingJobRequest $request)
    {
        try {
            $data = $request->all();
            unset($data['_token']);
            if ($request->hasFile('fileImage')) {
                $images = $request->file('fileImage');
                $path = Storage::disk('public')->put('imageSkill', $images);
                $data['image_path'] = $path;
            }
            $settingJob = $this->settingJob->create($data);
            return response()->json([
                'settingJob' => $settingJob,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $settingJob = $this->settingJob->findOrFail($id);
            if ($settingJob->image_path != null) {
                $settingJob->image_path = url('/' . 'storage/' . $settingJob->image_path);
            }
            return response()->json([
                'settingJob' => $settingJob
            ], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\SettingJobRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingJobRequest $request, $id)
    {
        try {
            $settingJob = $this->settingJob->findOrFail($id);
            $data['name'] = $request->name ? $request->name : $settingJob->name;
            $data['description'] = $request->description ? $request->description : $settingJob->description;
            if ($request->hasFile('fileImage')) {
                $images = $request->file('fileImage');
                if ($settingJob->image_path != null) {
                    if (Storage::disk('public')->exists($settingJob->image_path)) {
                        Storage::disk('public')->delete($settingJob->image_path);
                    }
                }
                $path = Storage::disk('public')->put('imageSkill', $images);
                $data['image_path'] = $path ? $path : $settingJob->image_path;
            }
            $settingJob->slug = null;
            $settingJob->update($data);
            return response()->json([
                'settingJob' => $settingJob,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $settingJob = SettingJob::findOrFail($id);
            if ($settingJob->image_path != null) {
                if (Storage::disk('public')->exists($settingJob->image_path)) {
                    Storage::disk('public')->delete($settingJob->image_path);
                }
            }
            $settingJob->delete($id);
            return response()->json([], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 403);
        }
    }
}
