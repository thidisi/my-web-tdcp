<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bonus;
use App\Models\Permission;
use App\Models\Role;
use App\Models\SettingJob;
use App\Models\SettingKpi;
use Illuminate\Http\Request;

class SettingRoleController extends Controller
{
    public function __construct(Role $role, SettingKpi $kpi, Permission $permission, SettingJob $settingJob, Bonus $bonuses)
    {
        $this->role = $role;
        $this->kpi = $kpi;
        $this->permission = $permission;
        $this->settingJob = $settingJob;
        $this->level = $this->settingJob->where('type', SettingJob::JOB_TYPE['LEVELS']);
        $this->bonuses = $bonuses;
        $this->bonusRole = $this->bonuses->where('type', Bonus::BONUS_TYPE['ROLE']);
        $this->bonusLevel = $this->bonuses->where('type', Bonus::BONUS_TYPE['LEVEL']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $roles = $this->role->get();
        $roleDefault = $this->role->where('name', 'HR')->first();
        $bonusRoleDefault = $this->bonusRole->where('key', $roleDefault->id)->first();
        $permissionDefault = $this->permission->get();

        if (!empty($roleDefault->kpis)) {
            $roleDefault->kpis->month = json_decode($roleDefault->kpis->month);
            $roleDefault->kpis->week = json_decode($roleDefault->kpis->week);
        }

        $levels  = $this->level->get();
        $bonusLevel = $this->bonusLevel->get();
        $levelBonus = [];
        foreach ($levels as $levelElement) {
            $isExistBonus = false;
            foreach ($bonusLevel as $bonusElement) {
                if ($levelElement->id == $bonusElement->key) {
                    $levelBonus[] = [
                        'id' => $levelElement->id,
                        'key' => $levelElement->name,
                        'value' => $bonusElement->value,
                    ];
                    $isExistBonus = true;
                }
            }
            if(!$isExistBonus){
                $levelBonus[] = [
                    'id' => $levelElement->id,
                    'key' => $levelElement->name,
                    'value' => 0,
                ];
            }
        }

        return view('admin.settings.role-kpi-bonus.index', [
            'roles' => $roles,
            'roleDefault' => $roleDefault,
            'permissionDefault' => $permissionDefault,
            'bonusDefault' => $bonusRoleDefault,
            'levels' => $levelBonus
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = $this->role->with(['permissions', 'kpis'])->find($id);
        $bonus = $this->bonuses->where('type', Bonus::BONUS_TYPE['ROLE'])->where('key', $id)->first();

        return response()->json([
            'role' => $role,
            'bonus' => $bonus,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            //Update data setting by role
            if ($request->submit_type == 'by_role') {
                $kpi_week = [
                    'candidates' => $request->candidates_of_week,
                    'cv_interview' => $request->cv_interview_of_week,
                    'cv_customer' => $request->cv_customer_of_week
                ];

                $kpi_month = [
                    'candidates' => $request->candidates_of_month,
                    'cv_interview' => $request->cv_interview_of_month,
                    'cv_customer' => $request->cv_customer_of_month
                ];
                $role = $this->role->with(['permissions', 'kpis'])->find($id);

                //Update permission
                $permission = $request->permissions;
                $role->permissions()->sync($permission);

                //Update KPI
                $kpi = $role->kpis()->updateOrCreate(
                    [
                        'role_id' => $id,
                    ],
                    [
                        'week' => json_encode($kpi_week),
                        'month' => json_encode($kpi_month)
                    ]
                );

                //Update Bonus
                $bonuses = $this->bonuses->updateOrCreate(
                    [
                        'type' => Bonus::BONUS_TYPE['ROLE'],
                        'key' => $id,
                    ],
                    [
                        'value' => $request->bonus_of_month,
                    ]
                );
            }
            //Update data setting by level
            if ($request->submit_type == 'by_level') {
                foreach ($request->bonus as $bonus) {
                    $bonuses = $this->bonuses->updateOrCreate(
                        [
                            'type' => Bonus::BONUS_TYPE['LEVEL'],
                            'key' => $bonus[0],
                        ],
                        [
                            'value' => $bonus[1],
                        ]
                    );
                }
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => "Cập nhật không thành công"], 500);
        }
        return response()->json(['message' => "Cập nhật thành công!"], 200);
    }
}
