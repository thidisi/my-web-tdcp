<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Source;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SettingSourceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Source $source)
    {
        $this->source = $source;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.settings.sources.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $checkIsExist = $this->source->where('name', $request->name)->first();
            if ($checkIsExist) {
                return response()->json(['message' => "Thông tin đã bị trùng!"], 405);
            }
            if (isset($request->image) == false) {
                return response()->json(['message' => "Vui lòng chọn ảnh source!"], 405);
            }
            if($request->hasFile('image')) {
                $images = $request->file('image');
                $path = Storage::disk('public')->put('imageblogs', $images);
                $data['image'] = $path;
            }
            $source = $this->source->create([
                'name' => $request->name,
                'url' => $request->url,
                'image' => $data['image'],
            ]);
            // $source->save();
            return response()->json(['message' => "Thêm nguồn JD thành công"], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => "Thêm nguồn JD không thành công"], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            $checkIsExist = $this->source->where('id', '!=', $id)->where('name', $request->name)->first();
            if ($checkIsExist) {
                return response()->json(['message' => "Thông tin đang bị trùng!"], 405);
            }
            if($request->hasFile('image')) {
                $images = $request->file('image');
                $path = Storage::disk('public')->put('imageblogs', $images);
                $data['image'] = $path;
            }
            $source = $this->source->where('id', $id)->firstOrFail();
            $source->name = $request->name;
            $source->url = $request->url;
            $source->slug = null;
            if(isset($request->image)) {
                $source->image = $data['image'];
            }
            $source->update();
            return response()->json(['message' => "Cập nhật thông tin nguồn JD thành công"], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => "Thêm nguồn JD không thành công"], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $source = $this->source->findOrFail($id);
            $source->delete($id);
            return response()->json(['message' => 'Xóa nguồn JD thành công!'], 200);
        } catch (\Throwable $th) {
            return response()->json(['message' => config('const.not_content')], 500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_api(Request $request)
    {
        $sourceList = $this->source->get();
        return response()->json([
            'data' => $sourceList,
        ], 200);
    }
}
