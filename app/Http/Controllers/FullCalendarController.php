<?php

namespace App\Http\Controllers;

use App\Models\Apply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FullCalendarController extends Controller
{

    public function index()
    {
        // if (request()->ajax()) {

        //     $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
        //     $end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');

        //     $data = Event::whereDate('start', '>=', $start)->whereDate('end',   '<=', $end)->get(['id', 'title', 'start', 'end']);
        //     return Response::json($data);
        // }
        // $data = Apply::where('status', Apply::APPLY_STATUS['INTERVIEW'])->get();
        return view('admin.calender.fullcalendar');
    }

    public function showCalendar(Request $request)
    {
        if ($request->role == Null || $request->role == 1) {
            $data = Apply::where('status', Apply::APPLY_STATUS['INTERVIEW'])->with('candidates')->get();
        } else {
            $data = Apply::where('status', Apply::APPLY_STATUS['INTERVIEW'])->where('user_id', Auth::user()->id)->with('candidates')->get();
        }
        if(count($data) > 0) {
            foreach($data as $each) {
                if($each->file != null) {
                    $each->pdf = url(Storage::url($each->file));
                }
                $each->route = route('api.jobs.show', $each->id);
            }
        }
        return response()->json($data);
    }
}
