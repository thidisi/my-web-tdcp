<?php

namespace App\Http\Middleware;

use App\Models\Role;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class CheckAuthUser
{
    public function __construct(Role $role)
    {
        $this->role = $role;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $role = $this->role->with('permissions')->find(auth()->user()->role_id);
        if (!empty($role) && count($role->permissions) > 0) {
            $request->session()->forget('sessionPermission');
            foreach ($role->permissions as $each) {
                $arrPermission[] = $each->id;
            }

            $request->session()->put('sessionPermission', $arrPermission);
        }
        $arrPermission = $request->session()->get('sessionPermission') ? $request->session()->get('sessionPermission') : [];

        if (Auth::check() && Auth::user()->password != null && (auth()->user()->status == User::USER_STATUS['ACTIVE'])) {
            return $next($request);
        }
        return redirect()->route('admin.login');
    }
}
