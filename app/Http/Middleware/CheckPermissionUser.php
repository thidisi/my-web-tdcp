<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckPermissionUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $permissions)
    {
        $arrPermission = $request->session()->get('sessionPermission') ? $request->session()->get('sessionPermission') : [];

        if(!empty($permissions) && count($arrPermission) > 0){
            $checkPermission = explode("|", $permissions);
                if (!count(array_intersect($checkPermission, $arrPermission)) > 0) {
                    return abort('403');
                }
        }
        return $next($request);
    }
}
