<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'highlight' => 'required|max:255',
            'title' => 'required',
            'banner' => 'required|mimes:jpeg,png,jpg,gif|max:1024',
            'content' => 'required',
            'category_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'highlight.required' => 'không được để trống',
            'highlight.max' => 'Nội dung quá dài',
            'title.required' => 'Không được để trống',
            'banner.required' => 'Không được để trống',
            "banner.mimes" => "file phải đúng định dạng image!",
            "banner.max" => "Kích thước ảnh không được lớn hơn 1MB!",
            'content.required' => 'Không được để trống',
            'category_id.required' => 'Không được để trống',
        ];
    }
}
