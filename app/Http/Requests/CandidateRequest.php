<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CandidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "name" => "required",
            "skills" => "required",
            "phone" => "required|regex:/^([0-9\s\-\+\(\)]*)$/|digits_between:10,11",
            "email" => "required|email",
            "file" => "required|mimes:pdf|max:1024",
        ];
    }
    public function messages()
    {
       return [
            "name.required" => "Tên ứng viên không để trống!",
            "skills.required" => "Kỹ năng của ứng viên không để trống!",

            "phone.required" => "Số điện thoại không để trống!",
            "phone.regex" => "Số điện thoại phải là số!",
            "phone.digits_between" => "Số điện thoại có độ dài từ 10 đến 11 kí tự!",

            "email.required" => "Email không để trống!",
            "email.email" => "Email phải đúng định dạng!",
            
            "file.required" => "File không để trống!",
            "file.mimes" => "file phải đúng định dạng pdf!",
            "file.max" => "Kích thước file không được lớn hơn 1MB!",
       ];
    }
}
