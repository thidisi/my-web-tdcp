<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:jobs,title',
            'slot' => 'required|numeric|min:1',
            'start_date' => 'required',
            'end_date' => 'required',
            'level' => 'required',
            'exp' => 'required',
            'work_time' => 'required',
            'education' => 'required',
            'deadline' => 'required',
            'address' => 'required',
            'salary' => 'required',
            'description' => 'required',
            'benefit' => 'required',
            'require' => 'required',
            'assignee' => 'required',
            'category_job' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Không được để trống',
            'title.unique' => 'Tiêu đề không được trùng',
            'slot.required' => 'Không được để trống',
            'start_date.required' => 'Không được để trống',
            'end_date.required' => 'Không được để trống',
            'level.required' => 'Không được để trống',
            'exp.required' => 'Không được để trống',
            'work_time.required' => 'Không được để trống',
            'education.required' => 'Không được để trống',
            'deadline.required' => 'Không được để trống',
            'address.required' => 'Không được để trống',
            'salary.required' => 'Không được để trống',
            'description.required' => 'Không được để trống',
            'benefit.required' => 'Không được để trống',
            'require.required' => 'Không được để trống',
            'assignee.required' => 'Không được để trống',
            'category_job.required' => 'Không được để trống',
        ];
    }
}
