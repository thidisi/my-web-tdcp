<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'password' => 'required|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'password_confirmation' => 'required|same:password'
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'Mật khẩu không được để trống',
            'password.regex' => 'Tối thiểu 6 ký tự, ít nhất 1 chữ cái viết hoa, 1 kí tự đặc biệt, 1 chữ cái viết thường và 1 chữ số',
            'password_confirmation.required' => ' Xác nhận mật khẩu không được để trống',
            'password_confirmation.same' => 'Mật khẩu và xác nhận mật khẩu không khớp',
        ];
    }
}
