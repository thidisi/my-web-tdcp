<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'Không được để trống',
            'name.required' => 'Không được để trống',
        ];
    }
}
