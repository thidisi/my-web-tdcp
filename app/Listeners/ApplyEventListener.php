<?php

namespace App\Listeners;

use App\Events\Apply\FailApply;
use App\Events\Apply\InterviewApply;
use App\Events\Apply\JobCVMail;
use App\Events\Apply\PassApply;
use App\Events\Apply\RefuseInterviewApply;
use App\Notifications\Apply\VerifyApplyCVMail;
use App\Notifications\Apply\VerifyFailApply;
use App\Notifications\Apply\VerifyInterviewApply;
use App\Notifications\Apply\VerifyPassApply;
use App\Notifications\Apply\VerifyRefuseApply;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ApplyEventListener
{
    /**
     * @param JobCVMail $event
     */
    public function onApplyJobCVMail(JobCVMail $event)
    {
        $candidate = $event->candidate;
        $data = $event->data;
        return $candidate->notify(new VerifyApplyCVMail($data));
    }

    /**
     * @param InterviewApply $event
     */
    public function onInterviewApply(InterviewApply $event)
    {
        $candidate = $event->candidate;
        $data = $event->data;
        return $candidate->notify(new VerifyInterviewApply($data));
    }

    /**
     * @param RefuseInterviewApply $event
     */
    public function onRefuseInterviewApply(RefuseInterviewApply $event)
    {
        $candidate = $event->candidate;
        $data = $event->data;
        return $candidate->notify(new VerifyRefuseApply($data));
    }

    /**
     * @param PassApply $event
     */
    public function onPassApply(PassApply $event)
    {
        $candidate = $event->candidate;
        $data = $event->data;
        return $candidate->notify(new VerifyPassApply($data));
    }

    /**
     * @param FailApply $event
     */
    public function onFailApply(FailApply $event)
    {
        $candidate = $event->candidate;
        $data = $event->data;
        return $candidate->notify(new VerifyFailApply($data));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen(
            JobCVMail::class,
            'App\Listeners\ApplyEventListener@onApplyJobCVMail',
        );
        $events->listen(
            InterviewApply::class,
            'App\Listeners\ApplyEventListener@onInterviewApply',
        );
        $events->listen(
            RefuseInterviewApply::class,
            'App\Listeners\ApplyEventListener@onRefuseInterviewApply',
        );
        $events->listen(
            PassApply::class,
            'App\Listeners\ApplyEventListener@onPassApply',
        );
        $events->listen(
            FailApply::class,
            'App\Listeners\ApplyEventListener@onFailApply',
        );
    }
}
