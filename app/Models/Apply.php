<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apply extends Model
{
    use HasFactory, Uuid;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'candidate_id',
        'job_id',
        'status',
        'file',
        'source_id',
        'user_id',
        'skills',
        'reason',
        'interview_date'

    ];

    const APPLY_STATUS = [
        'NEW' => 'new',
        'INTERVIEW' => 'interview',
        'REJECTED' => 'rejected',
        'PASSED' => 'passed',
        'FAILED' => 'failed',
    ];
    public $timestamps = true;

     /**
    * Return the created_at configuration array for this model.
    *
    * @return array
    */
    protected $casts = [
        'created_at' => 'date:d-m-Y',
        'updated_at' => 'date:d-m-Y',
        'skills' => 'array',
    ];

    public function candidates()
    {
        return $this->belongsTo(Candidate::class, 'candidate_id');
    }

    public function jobs()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function sources()
    {
        return $this->belongsTo(Source::class, 'source_id');
    }

    public function customers()
    {
        return $this->hasOne(CandidatesCustomer::class, 'apply_id');
    }
}
