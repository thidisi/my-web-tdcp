<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    use HasFactory, Notifiable, Uuid;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'status'
    ];

    const CANDIDATE_STATUS = [
        'ACTIVE' => 'active',
        'INACTIVE' => 'inactive',
    ];
    public $timestamps = true;

     /**
    * Return the created_at configuration array for this model.
    *
    * @return array
    */
    protected $casts = [
        'created_at' => 'date:d-m-Y',
        'updated_at' => 'date:d-m-Y',
    ];

    public function applies()
    {
        return $this->hasMany(Candidate::class, 'apply_id');
    }
}
