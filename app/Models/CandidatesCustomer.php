<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CandidatesCustomer extends Model
{
    use HasFactory;
    protected $table = 'candidates_customers';
    protected $fillable = [
        'apply_id',
        'level_id',
        'customer_onsite',
        'time_onboard',
        'time_actual',
        'start_date',
        'end_date',
        'salary',
        'recruitment_money',
        'sale_money',
        'salary_13',
        'bonus_onsite',
        'insurance',
        'team_building',
        'income'
    ];
}
