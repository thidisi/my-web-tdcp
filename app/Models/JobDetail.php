<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobDetail extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'job_id',
        'job_type',
        'salary',
        'skills',
        'deadline',
        'level',
        'exp',
        'education',
        'slot',
        'benefit',
        'description',
        'require',
        'address',
        'detail',
        'province_id',
    ];

    public $timestamps = true;

     /**
    * Return the created_at configuration array for this model.
    *
    * @return array
    */
    protected $casts = [
        'created_at' => 'date:d-m-Y',
        'updated_at' => 'date:d-m-Y',
        'skills' => 'array',
        'detail' => 'array'
    ];

    public function jobs()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }

    public function provinces()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
}
