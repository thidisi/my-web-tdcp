<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'type', // job, apply, blog
        'user_id',
        'type_id', // job_id, apply_id, blog_id
        'priority',
        'content',
        'actision'
    ];

    const NOTIFY_STATUS = [
        'ACTIVE' => 'active',
        'INACTIVE' => 'inactive',
        'NORMAL' => 'normal',
        'HIGH' => 'high',
    ];

    const NOTIFY_TYPE = [
        'JOB' => 'job',
        'APPLY' => 'apply',
        'BLOG' => 'blog',
    ];


    public $timestamps = true;

    /**
     * Return the created_at configuration array for this model.
     *
     * @return array
     */
    protected $casts = [
        'created_at' => 'date:d-m-Y',
        'updated_at' => 'date:d-m-Y'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function notify_actisions()
    {
        return $this->hasMany(NotifyActision::class, 'notify_id');
    }

    public static function notifyLists($action, $role_id = null, $user_id = null, $ListIdTimeLimitNotify = [])
    {
        $ids = [];
        if ($role_id == '' || $role_id == 1) {
            $role_id = null;
        }
        $type_job = self::query()->where('type', 'job')->get();
        $idJobs = [];
        $idTypeJobs = [];
        $idNotJobNotifies = [];
        if (count($type_job) > 0) {
            foreach ($type_job as $each) {
                $idJobs[] = $each->type_id;
            }
            $jobs = Job::query()->whereIn('assignee', ['0', '1', $role_id])->whereIn('id', $idJobs)->get();
            if (count($jobs) > 0) {
                foreach ($jobs as $job) {
                    $idTypeJobs[] = $job->id;
                }
            }
            $getNotJobNotifies = self::query()->where('type', 'job')->whereNotIn('type_id', $idTypeJobs)->get();
            if (count($getNotJobNotifies) > 0) {
                foreach ($getNotJobNotifies as $getNotJobNotify) {
                    $idNotJobNotifies[] = $getNotJobNotify->id;
                }
            }
        }
        $type_chang_applies = self::query()->where('title', 'Đã thay đổi cv cập nhật mới')->get();
        $idTypeApplies = [];
        $idApplyNotifies = [];
        $idNotApplyNotifies = [];

        if (count($type_chang_applies) > 0) {
            foreach ($type_chang_applies as $type_chang_apply) {
                $idTypeApplies[] = $type_chang_apply->type_id;
            }
            $idTypeApplies = array_unique($idTypeApplies);
            if ($user_id != null) {
                $applies = Apply::query()->whereIn('id', $idTypeApplies)->where('user_id', $user_id)->get();
                if (count($applies) > 0) {
                    foreach ($applies as $apply) {
                        $idApplyNotifies[] = $apply->id;
                    }
                }
                $getNotApyNotifies = self::query()->where('title', 'Đã thay đổi cv cập nhật mới')->whereNotIn('type_id', $idApplyNotifies)->get();
                if (count($getNotApyNotifies) > 0) {
                    foreach ($getNotApyNotifies as $getNotApyNotify) {
                        $idNotApplyNotifies[] = $getNotApyNotify->id;
                    }
                }
            }
        }

        $ids = array_unique(array_merge($idNotJobNotifies, $idNotApplyNotifies, $ListIdTimeLimitNotify));
        if ($action) {
            $data = self::query()->with(['users', 'notify_actisions'])->where('priority', '!=', self::NOTIFY_STATUS['HIGH'])->whereNotIn('id', $ids)->latest('id')->get();
        } else {
            $data = self::query()->with(['users', 'notify_actisions'])->whereNotIn('id', $ListIdTimeLimitNotify)->latest('id')->get();
        }
        foreach ($data as $value) {
            if ($value->type == self::NOTIFY_TYPE['JOB']) {
                $value->job = Job::query()->where('id', $value->type_id)->first();
                $value->image_path = SettingJob::query()->where('type', SettingJob::JOB_TYPE['SKILLS'])->whereIn('id', JobDetail::query()->where('job_id', $value->type_id)->first()->skills)->first()->image_path;
            }
            if ($value->type == self::NOTIFY_TYPE['BLOG']) {
                $value->blog = Blog::query()->where('id', $value->type_id)->first();
                $value->image_path = asset('assets/img/avatar/blog.jpg');
            }
            if ($value->type == self::NOTIFY_TYPE['APPLY']) {
                $value->apply = Apply::query()->where('id', $value->type_id)->first();
                $value->image_path = asset('assets/img/avatar/cv.avif');
                if ($value->title == 'Đã có cv ứng tuyển mới' && $value->priority == self::NOTIFY_STATUS['NORMAL']) {
                    $value->image_path = asset('assets/img/avatar/not-hr-pic.png');
                }
            }
            if (count($value->notify_actisions) > 0) {
                $value->actision = 'inactive';
                foreach ($value->notify_actisions as $item) {
                    if ($item->user_id == $user_id) {
                        $value->actision = $item->actision;
                    }
                }
            } else {
                $value->actision = 'inactive';
            }
        }
        return [
            'data' => $data,
            'count_data' => $data->where('actision', self::NOTIFY_STATUS['INACTIVE'])->count(),
        ];
    }
}
