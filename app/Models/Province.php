<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;

    protected $table = 'provinces';
    protected $fillable = [
        'id',
        'name',
        'slug'
    ];

    public $timestamps = false;

    public function job_details()
    {
        return $this->hasMany(JobDetail::class, 'province_id');
    }
}
