<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBonus extends Model
{
    use HasFactory;
    protected $table = 'users_bonus';
    protected $fillable = [
        'user_id',
        'apply_id',
        'level_bonus',
        'role_bonus',
        'month'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function applies()
    {
        return $this->belongsTo(Apply::class, 'apply_id');
    }

    public function total_bonus()
    {
        return $this->getBonusInfo();
    }
}
