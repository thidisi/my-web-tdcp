<?php

namespace App\Notifications\Apply;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class VerifyApplyCVMail extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var array
     */
    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data = null)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $title = $this->data['title'];
        $company = config('app.name');
        return (new MailMessage)
            ->greeting(new HtmlString("<span class='text_title ml-0'>Kính gửi</span>: $notifiable->name,"))
            ->subject("[$company] Thông báo hồ sơ ứng tuyển - $title")
            ->line(new HtmlString("$company chân thành cảm ơn bạn đã nộp hồ sơ ứng tuyển vị trí <b class='text-black fw-500'>$title</b> của công ty."))
            ->line("Chúng tôi chắc rằng bạn đã dành thời gian tìm hiểu công ty và tự tin với hồ sơ được chuẩn bị rất tốt của mình. Tuy nhiên để có thể đánh giá hồ sơ của bạn thật kỹ lưỡng, chúng tôi sẽ cân nhắc và sớm phản hồi lại cho bạn.")
            ->line("Cam kết giữa bạn và công ty về việc không hủy bỏ ứng tuyển trong vòng 10 ngày và cam kết onboard sau khi pass phỏng vấn sẽ có hiệu lực kể từ ngày bạn nhận được email này.")
            ->line(new HtmlString('Chúc bạn một ngày tốt lành và đừng quên kiểm tra email thường xuyên nhé!'))
            ->line('Chúng tôi rất mong có cơ hội gặp bạn.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function getRedirectURL($notifiable): string
    {
        return getWebURL('');
    }
}
