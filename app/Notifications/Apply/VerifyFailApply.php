<?php

namespace App\Notifications\Apply;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class VerifyFailApply extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var array
     */
    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data = null)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $title = $this->data['title'];
        $company = config('app.name');
        $reason = $this->data['reason'];
        return (new MailMessage)
            ->greeting(new HtmlString("<span class='text_title ml-0'>Kính gửi</span>: $notifiable->name,"))
            ->subject("[$company] Phỏng vấn thất bại - $title")
            ->line(new HtmlString("Qua hồ sơ của bạn, chúng tôi đánh giá cao những kiến thức và kinh nghiệm đã có của bạn. Song thật tiếc vì bạn thực sự phù hợp với công việc mà chúng tôi cần."))
            ->line("Và chúng tôi sẽ giữ lại hồ sơ của bạn để có thể tiếp tục liên hệ lại với bạn nếu công ty có nhu cầu tuyển dụng mới và phù hợp trong thời gian tới.")
            ->line("Trân trọng và chúc bạn luôn gặp nhiều may mắn và thành công trong quá trình tìm việc.");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function getRedirectURL($notifiable): string
    {
        return getWebURL('');
    }
}
