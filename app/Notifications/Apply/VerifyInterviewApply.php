<?php

namespace App\Notifications\Apply;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class VerifyInterviewApply extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var array
     */
    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data = null)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $title = $this->data['title'];
        $dateInterview = $this->data['date'];
        $company = config('app.name');
        $address_company = config('const.address_company');
        $mail_user = $this->data['mail_user'];
        return (new MailMessage)
            ->greeting(new HtmlString("<span class='text_title ml-0'>Kính gửi</span>: $notifiable->name,"))
            ->subject("[$company] Thông báo lịch phỏng vấn - $title")
            ->line(new HtmlString("Lời đầu tiên, hachinet xin cảm ơn bạn vì đã quan tâm đến vị trí <b class='text-black fw-500'>$title</b> của công ty. Thông qua hồ sơ mà bạn đã gửi về, chúng tôi nhận thấy bạn có kiến thức chuyên môn phù hợp với vị trí mà chúng tôi đang tuyển."))
            ->line(new HtmlString('Chúng tôi trân trọng kính mời bạn đến tham gia buổi phỏng vấn của công ty chúng tôi tại:'))
            ->line(new HtmlString("<ul><li><span>Địa chỉ: <b class='text-black fw-500'>$address_company</b></span></li></ul>"))
            ->line(new HtmlString("<ul><li><span>Thời gian: <b class='text-black fw-500'>$dateInterview</b></span></li></ul>"))
            ->line('Để buổi phỏng vấn được diễn ra thuận lợi, bạn vui lòng phản hồi lại email này ngay khi nhận được. Mọi thắc mắc khác, bạn vui lòng liên hệ với chúng tôi qua:')
            ->line(new HtmlString("- Liên hệ qua mail: <a href='mailto:$mail_user'>$mail_user</a>"));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function getRedirectURL($notifiable): string
    {
        return getWebURL('');
    }
}
