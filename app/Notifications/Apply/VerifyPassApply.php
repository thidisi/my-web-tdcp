<?php

namespace App\Notifications\Apply;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class VerifyPassApply extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var array
     */
    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data = null)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $title = $this->data['title'];
        $company = config('app.name');
        $start_date = $this->data['start_date'];
        $address_company = config('const.address_company');
        $phone_company_tel = config('const.phone_company')['tel'];
        $phone_company_text = config('const.phone_company')['text'];
        return (new MailMessage)
            ->greeting(new HtmlString("<span class='text_title ml-0'>Kính gửi</span>: $notifiable->name,"))
            ->subject("[$company] Thư mời làm việc - $title")
            ->line(new HtmlString("Đây là thư thông báo trúng tuyển vị trí <b class='text-black fw-500'>$title</b> thuộc bộ phận công ty $company. Xin chúc mừng Anh/Chị/Bạn đã vượt qua vòng phỏng vấn và chính thức trở thành một một thành viên của công ty chúng tôi."))
            ->line(new HtmlString('Chúng tôi đã sắp xếp và bố trí mọi thứ cần thiết phục vụ cho công việc của Anh/Chị/Bạn. Bởi vậy, chúng tôi rất mong Anh/Chị/Bạn sẽ có mặt đúng giờ vào ngày đầu làm việc để chúng tôi có thể chào đón bạn một cách nồng nhiệt nhất.'))
            ->line(new HtmlString("Thời gian làm việc cụ thể bắt đầu từ ngày <b class='text-black fw-500'>$start_date</b> tại <b class='text-black fw-500'>$address_company</b>"))
            ->line(new HtmlString("Nếu bạn còn bất kỳ thắc mắc gì, hãy liên lạc với chúng tôi qua số điện thoại: <a href='tel:$phone_company_tel'>$phone_company_text</a>."))
            ->line('Hy vọng nhận được phản hồi sớm từ bạn!.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function getRedirectURL($notifiable): string
    {
        return getWebURL('');
    }
}
