<?php

namespace App\Notifications\Apply;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class VerifyRefuseApply extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var array
     */
    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data = null)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $title = $this->data['title'];
        $company = config('app.name');
        return (new MailMessage)
            ->greeting(new HtmlString("<span class='text_title ml-0'>Kính gửi</span>: $notifiable->name,"))
            ->subject("[$company] Thông báo đánh giá ứng tuyển - $title")
            ->line(new HtmlString("Chúng tôi thuộc bộ phận nhân sự thuộc công ty $company nhận được hồ sơ ứng tuyển của bạn. Rất cám ơn vì sự quan tâm của bạn đối với vị trí việc làm <b class='text-black fw-500'>$title</b>."))
            ->line("Tuy nhiên, sau khi xem xét các hồ sơ, chúng tôi nhận thấy bạn chưa phù hợp để chọn vào vòng phỏng vấn.")
            ->line("Chúc bạn may mắn trong quá trình tìm việc sau đó và hi vọng sẽ có cơ hội được hợp tác với bạn trong tương lai.");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function getRedirectURL($notifiable): string
    {
        return getWebURL('');
    }
}
