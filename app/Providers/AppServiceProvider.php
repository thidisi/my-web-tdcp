<?php

namespace App\Providers;

use App\Models\Notify;
use Illuminate\Support\ServiceProvider;
use Laravel\Socialite\Contracts\Factory;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $socialite = $this->app->make(Factory::class);

        $socialite->extend('git-hachinet', function () use ($socialite) {
            $config = config('services.git-hachinet');
            return $socialite->buildProvider(SocialiteGitHachinetProvider::class, $config);
        });
    }
}
