<?php

namespace App\Providers;

use Laravel\Socialite\Two\AbstractProvider;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Laravel\Socialite\Two\User;

class SocialiteGitHachinetProvider extends AbstractProvider
{
    /**
     * The scopes being requested.
     *
     * @var array
     */
    protected $scopes = [
        'openid',
        'read_user'
    ];

    /**
     * The separating character for the requested scopes.
     *
     * @var string
     */
    protected $scopeSeparator = ' ';

    /**
     * @return  string
     */
    public function getGitHachinetUrl()
    {
        return config('services.git-hachinet.instance_uri');
    }

    /**
     * @param  string $state
     *
     * @return  string
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase($this->getGitHachinetUrl() . '/oauth/authorize', $state);
    }

    /**
     * @return  string
     */
    protected function getTokenUrl()
    {
        return 'https://git.hachinet.com/oauth/token';
    }

    /**
     * @param  string $token
     *
     * @throws  GuzzleException
     *
     * @return  array|mixed
     */
    protected function getUserByToken($token)
    {
        $response = $this->getHttpClient()->get('http://git.hachinet.com/api/v4/user', [
            RequestOptions::QUERY => ['access_token' => $token],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * @return  User
     */
    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user)->map([
            'id' => $user['id'],
            'nickname' => $user['username'],
            'name' => $user['name'],
            'email' => $user['email'],
            'avatar' => $user['avatar_url'],
        ]);
    }
}
