<?php

namespace App\Services;

use App\Models\Apply;
use App\Models\Candidate;
use Illuminate\Support\Facades\DB;

/**
 * Class ApplyService
 * @package App\Services
 */
class ApplyService
{
    /**
     * UserService constructor.
     * @param Apply $apply
     */
    public function __construct(Apply $apply, Candidate $candidate)
    {
        $this->apply = $apply;
        $this->$candidate = $candidate;
    }

    /**
     * @param array $params
     * @return Apply
     * @throws Throwable
     */
    public function create(array $params = []): Apply
    {
        DB::beginTransaction();
        try {
            $apply = $this->createByParams($params);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $apply;
    }

    /**
     * @param $params
     * @return mixed
     */
    private function createByParams($params): Apply
    {
        $apply = $this->candidate->create([
            'id' => (string)\Str::uuid(),
            'name' => $params['name'],
            'email' => $params['email'],
            'phone' => $params['phone'],
            'file' => $params['file'],
            'skill' => 'php',
            'status' => 'active',
        ]);
        // $this->jobDetail->create($params);
        // $params['detail'] = json_encode($params['detail']);
        return $apply;
    }
}
