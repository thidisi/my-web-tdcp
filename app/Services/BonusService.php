<?php

namespace App\Services;

use App\Models\Apply;
use App\Models\Bonus;
use App\Models\JobDetail;
use App\Models\Role;
use App\Models\SettingKpi;
use App\Models\User;
use App\Models\UserBonus;

/**
 * Class JobService
 * @package App\Services
 */
class BonusService
{
    /**
     * UserService constructor.
     * @param Job $job
     */
    public function __construct(UserBonus $userBonus, SettingKpi $settingKpi, Bonus $bonus, User $user, JobDetail $jobDetail, Role $role)
    {
        $this->userBonus = $userBonus;
        $this->kpiMonth = $settingKpi->select(['month', 'role_id'])->get();
        $this->bonus = $bonus;
        $this->user = $user;
        $this->job_detail = $jobDetail;
        $this->role = $role;
    }

    /**
     * Create User bonus by User ID and Apply ID
     * @param uuid $user_id
     * @param uuid $apply_id
     * @return bonus_info_of_user
     */
    public function createBonusByUserIdAndApplyId($user_id, $apply_id)
    {

        $userBonus = $this->userBonus->where('user_id', $user_id)
            ->latest('updated_at')->whereMonth('created_at', now()->month)->first();
        $user_role = $this->user->find($user_id)->role_id;

        $set_kpis = $this->getSettingKPI($user_role);

        if ($userBonus) {
            $userBonusMonth = json_decode($userBonus->month);
            $month['candidates'] = $userBonusMonth->candidates;
            $month['cv_interview'] = $userBonusMonth->cv_interview;
            $cv_customer =  explode('/', $userBonusMonth->candidates)[0];
            $month['cv_customer'] = ($cv_customer + 1) . "/" . $set_kpis->cv_customer;
            $userBonusNew['month'] = json_encode($month);
            $userBonusNew['user_id'] = $user_id;
            $userBonusNew['apply_id'] = $apply_id;
            $userBonusNew['level_bonus'] = '0';
            $userBonusNew['role_bonus'] = '0';
            $this->userBonus->create($userBonusNew);
        } else {
            $userBonusMonth['candidates'] = '0/' . $set_kpis->candidates;
            $userBonusMonth['cv_interview'] = '0/' . $set_kpis->cv_interview;
            $userBonusMonth['cv_customer'] = '1/' . $set_kpis->cv_customer;
            $userBonusNew['month'] = json_encode($userBonusMonth);
            $userBonusNew['user_id'] = $user_id;
            $userBonusNew['apply_id'] = $apply_id;
            $userBonusNew['level_bonus'] = '0';
            $userBonusNew['role_bonus'] = '0';
            $this->userBonus->create($userBonusNew);
        }
        return $userBonusNew;
    }

    /**
     * Create User bonus by User ID and Apply ID
     * @param uuid $user_id
     * @param uuid $apply_id
     * @param enum $status
     * @param bool $isUser
     * @return bonus_info_of_user
     */
    public function updateBonusByUserIdAndApplyId($user_id, $apply_id, $status, $isUser, $level)
    {
        $user_role = $this->user->find($user_id)->role_id;
        $set_kpis = $this->getSettingKPI($user_role);

        //if user following this candidate
        if ($isUser) {
            $userBonus = $this->userBonus->where('user_id', $user_id)
                ->where('apply_id', $apply_id)->latest('updated_at')->first();
        } else {
            //If user not following this candidate -> get the last record
            $userBonusOld = $this->userBonus->where('user_id', $user_id)
                ->latest('updated_at')->whereMonth('created_at', now()->month)->first();
            //If not have record -> create
            if ($userBonusOld == null) {
                $userBonusNew['user_id'] = $user_id;
                $userBonusNew['apply_id'] = $apply_id;
                $userBonusNew['level_bonus'] = '0';
                $userBonusNew['role_bonus'] = '0';
                $userBonusMonth['candidates'] = '0/' . $set_kpis->candidates;
                $userBonusMonth['cv_interview'] = '0/' . $set_kpis->cv_interview;
                $userBonusMonth['cv_customer'] = '0/' . $set_kpis->cv_customer;
                $userBonusNew['month'] = json_encode($userBonusMonth);
                $userBonus = $this->userBonus->create($userBonusNew);
            } else {
                //if have, get info 'month' of the last record and create new record
                $userBonusNew['user_id'] = $user_id;
                $userBonusNew['apply_id'] = $apply_id;
                $userBonusNew['level_bonus'] = '0';
                $userBonusNew['role_bonus'] = '0';
                $userBonusNew['month'] = $userBonusOld->month;
                $userBonus = $this->userBonus->create($userBonusNew);
            }
        }

        $kpi_month = json_decode($userBonus->month);

        if ($status == Apply::APPLY_STATUS['INTERVIEW']) {
            //Increment KPI of cv_interview (INTERVIEW)
            $kpi_cv_interview_before =  explode('/', $kpi_month->cv_interview)[0];
            $kpi_month->cv_interview = ($kpi_cv_interview_before + 1) . "/" . $set_kpis->cv_interview;
            $userBonus->month = json_encode($kpi_month);
        } else if ($status == Apply::APPLY_STATUS['PASSED']) {
            //Save role_bonus
            $role_bonus = $this->bonus->where('type', 'role')->where('key', $user_role)->first();
            if ($role_bonus) {
                $role_bonus = $role_bonus->value;
            } else {
                $role_bonus = 0;
            }
            $userBonus->role_bonus = $role_bonus;
            //Save level_bonus
            if ($isUser) {
                //UPDATE KPI
                //Increment KPI of candidates (PASS)
                $kpi_candidates_before =  explode('/', $kpi_month->candidates)[0];
                $kpi_month->candidates = ($kpi_candidates_before + 1) . "/" . $set_kpis->candidates;
                $userBonus->month = json_encode($kpi_month);
                //If role_id = 4 => Role['SALE']
                if ($user_role == 4) {
                    //If case 1->10 : bonus + 1,5M
                    if ($kpi_candidates_before < 10) {
                        $level_bonus = config('const.bonus_input_sale')[0];
                    } else {
                        //If case 11 : bonus + 2M
                        $level_bonus = config('const.bonus_input_sale')[1];
                    }
                } else {
                    //Else get Bonus value in "bonuses" table
                    $level_bonus = $this->bonus->where('type', 'level')->where('key', $level)->first();
                    if ($level_bonus) {
                        $level_bonus = $level_bonus->value;
                    } else {
                        $level_bonus = 0;
                    }
                }
            } else {
                //If isUser = false => Not following this candidate and this job
                // => Not have bonus
                $level_bonus = 0;
            }
            $userBonus->level_bonus = $level_bonus;
        }
        return $userBonus->update();
    }

    /**
     * Get bonus information from User ID
     * @param uuid $user_id
     * @param int $month
     * @param int $year
     * @return bonus_info_of_user
     */
    public function getBonusInfo($user_id, $month, $year)
    {
        try {
            $bonus_list = $this->userBonus->where('user_id', $user_id)
                ->whereMonth('updated_at', $month)
                ->whereYear('updated_at', $year)->get();
            $date = date(2000);
            $role_bonus = 0;
            $level_bonus = 0;
            if (count($bonus_list) == 0) {
                return null;
            }
            foreach ($bonus_list as $user_bonus) {
                if ($user_bonus->updated_at > $date) {
                    $date = $user_bonus->updated_at;
                    $user_candidate = json_decode($user_bonus->month)->candidates;
                }
                $role_bonus += str_replace(',', '', $user_bonus->role_bonus);
                $level_bonus += str_replace(',', '', $user_bonus->level_bonus);
            }
            $split = explode("/", $user_candidate);
            if ($split[1] == 0) {
                return null;
            } else {
                $kpi = $split[0] / $split[1];
                $user['candidate'] = $user_candidate;
                $user['kpi'] = number_format($kpi * 100, 2);
                $user['role_bonus'] = $role_bonus;
                $user['level_bonus'] = $level_bonus * $kpi;
            }
            return $user;
        } catch (\Throwable $th) {
            return false;
        }
    }

    /**
     * Get bonus information from User ID
     * @param array $data
     * @throws Throwable
     */
    public function getBonusInforTotal($data)
    {
        try {
            $totals = [];
            foreach ($data as $each) {
                $totals["role_bonus"][$each->user_id][$each->id] = str_replace(',', '', $each->role_bonus);
                $totals["level_bonus"][$each->user_id][$each->id] = str_replace(',', '', $each->level_bonus);
                if (explode('/', json_decode($each->month)->candidates)['1'] != '0') {
                    $totals["kpi"][$each->user_id][$each->id] = (explode('/', json_decode($each->month)->candidates)['0'] / explode('/', json_decode($each->month)->candidates)['1']);
                }
            }
            $role_bonus = [];
            $level_bonus = [];
            $kpis = [];
            if (count($totals) > 0) {
                $kpis = $totals["kpi"];
                foreach ($totals['role_bonus'] as $key => $items) {
                    $role_bonus["$key"] = 0;
                    foreach ($items as $value) {
                        $role_bonus["$key"] += $value;
                    }
                }
                foreach ($totals['level_bonus'] as $key => $items) {
                    $level_bonus["$key"] = 0;
                    foreach ($items as $value) {
                        $level_bonus["$key"] += $value;
                    }
                }
            }
            return [
                'role_bonus' => $role_bonus,
                'level_bonus' => $level_bonus,
                'kpis' => $kpis,
            ];
        } catch (\Throwable $th) {
            return [];
        }
    }

    private function getSettingKPI($user_role)
    {
        $set_kpis = $this->kpiMonth->where('role_id', $user_role)->first();
        if ($set_kpis) {
            return json_decode($set_kpis->month);
        } else {
            //Create KPI = 0
            $kpi_week = [
                'candidates' => '0',
                'cv_interview' => '0',
                'cv_customer' => '0'
            ];

            $kpi_month = [
                'candidates' => '0',
                'cv_interview' => '0',
                'cv_customer' => '0'
            ];

            $role = $this->role->with('kpis')->find($user_role);

            //Update KPI
            $kpi = $role->kpis()->updateOrCreate(
                [
                    'role_id' => $user_role,
                ],
                [
                    'week' => json_encode($kpi_week),
                    'month' => json_encode($kpi_month)
                ]
            );
            return json_decode($kpi->month);
        }
    }
}
