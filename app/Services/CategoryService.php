<?php

namespace App\Services;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class JobService
 * @package App\Services
 */
class CategoryService
{

    public function __construct(Category $category,)
    {
        $this->category = $category;
    }

    /**
     * @param array $params
     * @return Category
     * @throws Throwable
     */
    public function create(array $params = []): Category
    {
        DB::beginTransaction();
        try {
            $categories = $this->createByParams($params);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $categories;
    }

    /**
     * @param $params
     * @return mixed
     */
    private function createByParams($params): Category
    {
        $categories = $this->category->create($params);
        return $categories;
    }


    /**
     * @param array $params
     * @return Category
     * @throws Throwable
     */
    public function update(array $params = []): Category
    {
        DB::beginTransaction();
        try {
            $categories = $this->updateByParams($params);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $categories;
    }

    /**
     * @param $params
     * @return mixed
     */
    private function updateByParams($params): Category
    {

        $categories = $this->category->findOrFail($params['id']);
        if ($params['checkFile']) {
            $images = $params['fileImage'];
            if ($categories->image_path != null) {
                if (Storage::disk('public')->exists($categories->image_path)) {
                    Storage::disk('public')->delete($categories->image_path);
                }
            }
            $path = Storage::disk('public')->put('imageCategory', $images);
            $data['image_path'] = $path ? $path : $categories->image_path;

        }
        $data['name'] = $params['name'];
        $data['description'] = $params['description'];
        $categories->slug = null;
        $categories->update($data);
        return $categories;
    }
}
