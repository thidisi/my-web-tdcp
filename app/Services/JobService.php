<?php

namespace App\Services;

use App\Models\Blog;
use App\Models\Job;
use App\Models\JobDetail;
use App\Models\Notify;
use App\Models\Province;
use App\Models\SettingJob;
use App\Models\ShortUrl;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class JobService
 * @package App\Services
 */
class JobService
{
    /**
     * UserService constructor.
     * @param Job $job
     */
    public function __construct(Job $job, JobDetail $jobDetail, Blog $blog, ShortUrl $shortUrl, SettingJob $settingJob, Province $province, Notify $notify)
    {
        $this->job = $job;
        $this->jobDetail = $jobDetail;
        $this->blog = $blog;
        $this->shortUrl = $shortUrl;
        $this->settingJob = $settingJob;
        $this->province = $province;
        $this->notify = $notify;
    }

    /**
     * @param array $params
     * @return Job
     * @throws Throwable
     */
    public function create(array $params = []): Job
    {
        DB::beginTransaction();
        try {
            $job = $this->createByParams($params);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $job;
    }

    /**
     * @param $params
     * @return mixed
     */
    private function createByParams($params): Job
    {
        $job = $this->job->create([
            'id' => (string)\Str::uuid(),
            'title' => $params['title'],
            'user_id' => \Auth::user()->id,
            'assignee' => $params['assignee'],
            'category_job' => $params['category_job'],
        ]);
        unset($params['title'], $params['assignee']);
        $params['job_id'] = $job->id;
        $params['detail'] = json_encode($params['detail']);
        $this->jobDetail->create($params);
        $notify['type'] = Notify::NOTIFY_TYPE['JOB'];
        $notify['title'] = 'Đã thêm một job mới';
        $notify['content'] = 'Công việc '. $job->title . ' đã được ' . \Auth::user()->name .  ' thêm.';
        $notify['type_id'] = $job->id;
        $notify['user_id'] = \Auth::user()->id;
        $this->notify->create($notify);
        return $job;
    }

    /**
     * @param array $params
     * @return Job
     * @throws Throwable
     */

    public function update(array $params = [])
    {
        DB::beginTransaction();
        try {
            $this->updateByParams($params);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        // return $job;
    }

    /**
     * @param $params
     * @return mixed
     */
    private function updateByParams($params)
    {
        $params['skills'] = json_encode($params['skills']);
        $job = $this->job->findOrFail($params['id']);
        // xóa short url
        if ($job->title != $params['title']) {
            $this->shortUrl->where('destination_url', 'LIKE', "%{$job->slug}%")->delete();
        }
        $job->slug = null;
        $job->update([
            'title' => $params['title'],
            'assignee' => $params['assignee'],
            'category_job' => $params['category_job']
        ]);
        unset($params['id'], $params['title'], $params['assignee'], $params['category_job']);
        $params['detail'] = json_encode($params['detail']);
        $jobDetail = $this->jobDetail->where('job_id', $job->id);
        $jobDetail->update($params);

        return $job;
    }
    public function getListJobClient()
    {
        return $this->job->whereRelation('job_details', 'deadline', '>=', date('Y-m-d'))->where('status', JOB::JOB_STATUS['ACTIVE'])->latest('created_at')->paginate(8);
    }

    public function getListBlog()
    {
        return $this->blog->with("categories")->latest('created_at')->limit(4)->get();
    }
    public function search($job)
    {
        $query = $this->job->whereRelation('job_details', 'deadline', '>=', date('Y-m-d'))->where('status', Job::JOB_STATUS['ACTIVE'])->orderBy('created_at', 'desc');
        if (!empty($job['name'])) {
            $name = $job['name'];
            $query = $query->where(function ($query) use ($name) {
                $query = $query->orWhere('title', 'like', "%$name%");
            });
        }
        if (!empty($job['category_job'])) {
            $category_job = $job['category_job'];
            $query = $query->where(function ($query) use ($category_job) {
                $query = $query->Where('category_job', 'like', "%$category_job%");
            });
        }
        if (!empty($job['level'])) {
            $level = $this->settingJob->where('slug', $job['level'])->first();
            $level = $level->id;

            $query = $query->whereHas('job_details', fn ($query) =>
            $query->where('level', "$level"));
        }
        if (!empty($job['address'])) {
            $province_id = $this->province->where('slug', $job['address'])->first();
            if(!empty($province_id)){
                $query = $query->whereHas('job_details', fn ($query) =>
                $query->where('province_id', "$province_id->id"));
            }
        }
        if (!empty($job['skill'])) {
            $skill = $this->settingJob->where('slug', $job['skill'])->first();
            $jobDetails = $this->jobDetail->get();
            $jobIds = [];
            if (!empty($skill)) {
                if (count($jobDetails) > 0) {
                    foreach ($jobDetails as $each) {
                        if (in_array($skill->id, $each->skills)) {
                            $jobIds[] = $each->job_id;
                        }
                    }
                }
            }
            $query->whereIn('id', $jobIds);
        }
        return $query;
    }
}
