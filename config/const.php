<?php

return [
    'not_content' => 'Không tìm thấy nội dung xử lý',
    'educate' => [
        "cao-dang" => "Cao đẳng",
        "dai-hoc" => "Đại học",
        "khac" => "Khác",
        "tat-cat-trinh-do" => "Tất cả trình độ",
        "tren-dai-hoc" => "Trên đại học",
        "trung-hoc" => "Trung học"
    ],
    'exp' => [
        "no-exp" => "Không yêu cầu",
        "duoi-1-nam" => "Dưới 1 năm",
        "1-nam" => "1 năm",
        "2-nam" => "2 năm",
        "3-nam" => "3 năm",
        "4-nam" => "4 năm",
        "5-nam" => "5 năm trở lên"
    ],
    'category_job' => [
        "software-engineering" => "Software Engineering",
        "human-resources" => "Human Resources",
        "project-management" => "Project Management",
        "business-development" => "Business Development",
        "marketing-communication" => "Marketing & Communication",
        "customer-service" => "Customer Service",


    ],
    'work_time' => [
        "part-time" => "Part-time",
        "full-time" => "Full-time",
        "freelancer" => "Freelancer",
        "thuc-tap" => "Thực tập",
        "khac" => "Khác"
    ],
    'source' => [
        "facebook",
        "google",
        "twitter",
        "instagram",
    ],
    'bonus_input_sale' => [
        "1,500,000",
        "2,000,000",
    ],
    'address_company' => '2F - 21B6, 234 Phạm Văn Đồng, Cổ Nhuế, Từ Liêm, Hà Nội',
    'phone_company' => [
        'tel' => '02462900388',
        'text' => '(+84) 24-6290-0388'
    ],
];
