<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary()->index()->comment('uuid gen auto in code');
            $table->string('email', 100)->unique();
            $table->string('password', 100);
            $table->string('name', 200);
            $table->string('phone', 15)->unique();
            $table->text('avatar', 255)->nullable();
            $table->unsignedBigInteger('role_id');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->enum('status', [
                'active',
                'inactive',
            ])->default(App\Models\User::USER_STATUS['ACTIVE']);
            $table->rememberToken();
            $table->timestamps();
        });
        \DB::statement('ALTER Table users add code INTEGER NOT NULL UNIQUE AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
