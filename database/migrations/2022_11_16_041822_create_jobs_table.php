<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->uuid('id')->primary()->index()->comment('uuid gen auto in code');
            $table->string('title', 255);
            $table->string('slug', 100);
            $table->foreignUuid('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('assignee');
            $table->enum('status', [
                'active',
                'inactive',
            ])->default(App\Models\Job::JOB_STATUS['ACTIVE']);
            $table->timestamps();
        });
        \DB::statement('ALTER Table jobs add code INTEGER NOT NULL UNIQUE AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
};
