<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_details', function (Blueprint $table) {
            $table->id();
            $table->foreignUuid('job_id')->references('id')->on('jobs')->onDelete('cascade');
            $table->string('job_type', 255);
            $table->integer('salary')->default(0);
            $table->longText('skills');
            $table->date('deadline');
            $table->string('level', 255);
            $table->string('exp', 255);
            $table->string('education', 255);
            $table->integer('slot');
            $table->text('benefit');
            $table->text('description');
            $table->text('require');
            $table->string('address', 255);
            $table->longText('detail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_details');
    }
};
