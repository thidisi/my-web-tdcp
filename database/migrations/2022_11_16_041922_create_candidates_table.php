<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->uuid('id')->primary()->index()->comment('uuid gen auto in code');
            $table->string('name', 200);
            $table->string('email', 100)->unique();
            $table->string('phone', 15)->unique();
            $table->longText('file');
            $table->longText('skills');
            $table->enum('status', [
                'active',
                'inactive',
            ])->default(App\Models\Candidate::CANDIDATE_STATUS['ACTIVE']);
            $table->timestamps();
        });
        \DB::statement('ALTER Table candidates add code INTEGER NOT NULL UNIQUE AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
};
