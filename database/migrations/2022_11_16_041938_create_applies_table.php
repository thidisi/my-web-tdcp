<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applies', function (Blueprint $table) {
            $table->uuid('id')->primary()->index()->comment('uuid gen auto in code');
            $table->foreignUuid('candidate_id')->references('id')->on('candidates')->onDelete('cascade');
            $table->foreignUuid('job_id')->references('id')->on('jobs')->onDelete('cascade');
            $table->enum('status', [
                'active',
                'inactive',
            ]);
            $table->timestamps();

        });
        \DB::statement('ALTER Table applies add code INTEGER NOT NULL UNIQUE AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applies');
    }
};
