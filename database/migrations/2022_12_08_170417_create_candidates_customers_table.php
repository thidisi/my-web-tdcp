<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates_customers', function (Blueprint $table) {
            $table->id();
            $table->integer('apply_id');
            $table->integer('level_id');
            $table->text('customer_onsite')->nullable();
            $table->date('time_onboard');
            $table->date('time_actual');
            $table->date('start_date');
            $table->date('end_date');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates_customers');
    }
};
