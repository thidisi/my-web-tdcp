<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('blogs');
        Schema::create('blogs', function (Blueprint $table) {
            $table->uuid('id')->primary()->index()->comment('uuid gen auto in code');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->string('highlight',255);
            $table->string('title',255)->unique();
            $table->string('slug',255)->nullable();
            $table->longText('content');
            $table->text('banner');
            $table->integer('viewer')->default(0);
            $table->timestamps();
        });
        \DB::statement('ALTER Table blogs add code INTEGER NOT NULL UNIQUE AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
};
