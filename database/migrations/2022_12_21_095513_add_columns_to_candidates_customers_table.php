<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidates_customers', function (Blueprint $table) {
            $table->text('salary')->nullable();
            $table->text('recruitment_money')->nullable();
            $table->text('sale_money')->nullable();
            $table->text('salary_13')->nullable();
            $table->text('bonus_onsite')->nullable();
            $table->text('insurance')->nullable();
            $table->text('team_building')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidates_customers', function (Blueprint $table) {
            //
        });
    }
};
