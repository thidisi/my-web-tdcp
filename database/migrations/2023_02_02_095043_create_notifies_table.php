<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifies', function (Blueprint $table) {
            $table->id();
            $table->string('title', 255);
            $table->enum('type', [
                'job',
                'apply',
                'blog'
            ]);
            $table->foreignUuid('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
            $table->string('type_id',255)->nullable();
            $table->enum('priority', [
                'normal',
                'high',
            ])->default(App\Models\Notify::NOTIFY_STATUS['NORMAL']);
            $table->enum('actision', [
                'active',
                'inactive',
            ])->default(App\Models\Notify::NOTIFY_STATUS['INACTIVE']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifies');
    }
};
