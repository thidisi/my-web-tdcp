<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Permission;
use App\Models\Province;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->roleDefault();
        $this->userDefault();
        $this->permissionDefault();
        $this->provinceDefault();
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }

    public function userDefault()
    {
        User::insert([
            [
                'id' => (string)\Str::uuid(),
                'name' => "Admin",
                'phone' => "0942389598",
                'email' => "admin@takumi.com",
                'role_id' => null,
                'status' => User::USER_STATUS['ACTIVE'],
                'password' => Hash::make('takumi123'),
                'created_at' => now()
            ],
            [
                'id' => (string)\Str::uuid(),
                'name' => "Leader Division A",
                'phone' => "0942389599",
                'email' => "leaderdivision@takumi.com",
                'role_id' => '1',
                'status' => User::USER_STATUS['ACTIVE'],
                'password' => Hash::make('takumi123'),
                'created_at' => now()
            ],
            [
                'id' => (string)\Str::uuid(),
                'name' => "Leader B",
                'phone' => "0942389597",
                'email' => "leader@takumi.com",
                'role_id' => '2',
                'status' => User::USER_STATUS['ACTIVE'],
                'password' => Hash::make('takumi123'),
                'created_at' => now()
            ],
            [
                'id' => (string)\Str::uuid(),
                'name' => "HR C",
                'phone' => "0942389596",
                'email' => "hr@takumi.com",
                'role_id' => '3',
                'status' => User::USER_STATUS['ACTIVE'],
                'password' => Hash::make('takumi123'),
                'created_at' => now()
            ],
            [
                'id' => (string)\Str::uuid(),
                'name' => "Sale D",
                'phone' => "0942389595",
                'email' => "sale@takumi.com",
                'role_id' => '4',
                'status' => User::USER_STATUS['ACTIVE'],
                'password' => Hash::make('takumi123'),
                'created_at' => now()
            ],
            [
                'id' => (string)\Str::uuid(),
                'name' => "Khác",
                'phone' => "0942389594",
                'email' => "khac@takumi.com",
                'role_id' => '5',
                'status' => User::USER_STATUS['ACTIVE'],
                'password' => Hash::make('takumi123'),
                'created_at' => now()
            ],
        ]);


    }

    public function roleDefault()
    {
        Role::insert([
            [
                'name' => "Lead Division",
                'status' => User::USER_STATUS['ACTIVE'],
                'created_at' => now()
            ],
            [
                'name' => "Leader",
                'status' => User::USER_STATUS['ACTIVE'],
                'created_at' => now()
            ],
            [
                'name' => "HR",
                'status' => User::USER_STATUS['ACTIVE'],
                'created_at' => now()
            ],
            [
                'name' => "Sale",
                'status' => User::USER_STATUS['ACTIVE'],
                'created_at' => now()
            ],
            [
                'name' => "Khác",
                'status' => User::USER_STATUS['ACTIVE'],
                'created_at' => now()
            ],
        ]);
    }

    public function permissionDefault()
    {
        Permission::insert([
            [
                'name' => "Phân quyền",
                'slug' => "phan-quyen",
                'description' => "Có chức năng Phân quyền cho các vai trò.",
                'status' => Permission::PERMISSION_STATUS['ACTIVE'],
                'created_at' => now()
            ],
            [
                'name' => "Cấu hình KPI",
                'slug' => "cau-hinh-kpi",
                'description' => "Có chức năng cấu hình thông số KPI cho các vai trò.",
                'status' => Permission::PERMISSION_STATUS['ACTIVE'],
                'created_at' => now()
            ],
            [
                'name' => "Bonus",
                'slug' => "bonus",
                'description' => "Có chức năng Cấu hình thông số Bonus cho các vai trò.",
                'status' => Permission::PERMISSION_STATUS['ACTIVE'],
                'created_at' => now()
            ],
            [
                'name' => "Cấu hình Công việc",
                'slug' => "cau-hinh-cong-viec",
                'description' => "Có chức năng Cấu hình thông số cho công việc",
                'status' => Permission::PERMISSION_STATUS['ACTIVE'],
                'created_at' => now()
            ],
            [
                'name' => "Thêm/Sửa/Xóa Công việc",
                'slug' => "them-sua-xoa-cong-viec",
                'description' => "Có chức năng Thêm/Sửa/Xóa công việc.",
                'status' => Permission::PERMISSION_STATUS['ACTIVE'],
                'created_at' => now()
            ],
            [
                'name' => "Xem danh sách Công việc",
                'slug' => "xem-danh-sach-cong-viec",
                'description' => "Có chức Xem danh sách công việc.",
                'status' => Permission::PERMISSION_STATUS['ACTIVE'],
                'created_at' => now()
            ],
        ]);
    }

    public function provinceDefault()
    {
        Province::insert([
            [
                "name" => "Hà Nội",
                "slug" => "ha-noi",
                "id" => "01"
            ],
            [
                "name" => "Hà Giang",
                "slug" => "ha-giang",
                "id" => "02"
            ],
            [
                "name" => "Cao Bằng",
                "slug" => "cao-bang",
                "id" => "04"
            ],
            [
                "name" => "Bắc Kạn",
                "slug" => "bac-kan",
                "id" => "06"
            ],
            [
                "name" => "Tuyên Quang",
                "slug" => "tuyen-quang",
                "id" => "08"
            ],
            [
                "name" => "Lào Cai",
                "slug" => "lao-cai",
                "id" => "10"
            ],
            [
                "name" => "Điện Biên",
                "slug" => "dien-bien",
                "id" => "11"
            ],
            [
                "name" => "Lai Châu",
                "slug" => "lai-chau",
                "id" => "12"
            ],
            [
                "name" => "Sơn La",
                "slug" => "son-la",
                "id" => "14"
            ],
            [
                "name" => "Yên Bái",
                "slug" => "yen-bai",
                "id" => "15"
            ],
            [
                "name" => "Hoà Bình",
                "slug" => "hoa-binh",
                "id" => "17"
            ],
            [
                "name" => "Thái Nguyên",
                "slug" => "thai-nguyen",
                "id" => "19"
            ],
            [
                "name" => "Lạng Sơn",
                "slug" => "lang-son",
                "id" => "20"
            ],
            [
                "name" => "Quảng Ninh",
                "slug" => "quang-ninh",
                "id" => "22"
            ],
            [
                "name" => "Bắc Giang",
                "slug" => "bac-giang",
                "id" => "24"
            ],
            [
                "name" => "Phú Thọ",
                "slug" => "phu-tho",
                "id" => "25"
            ],
            [
                "name" => "Vĩnh Phúc",
                "slug" => "vinh-phuc",
                "id" => "26"
            ],
            [
                "name" => "Bắc Ninh",
                "slug" => "bac-ninh",
                "id" => "27"
            ],
            [
                "name" => "Hải Dương",
                "slug" => "hai-duong",
                "id" => "30"
            ],
            [
                "name" => "Hải Phòng",
                "slug" => "hai-phong",
                "id" => "31"
            ],
            [
                "name" => "Hưng Yên",
                "slug" => "hung-yen",
                "id" => "33"
            ],
            [
                "name" => "Thái Bình",
                "slug" => "thai-binh",
                "id" => "34"
            ],
            [
                "name" => "Hà Nam",
                "slug" => "ha-nam",
                "id" => "35"
            ],
            [
                "name" => "Nam Định",
                "slug" => "nam-dinh",
                "id" => "36"
            ],
            [
                "name" => "Ninh Bình",
                "slug" => "ninh-binh",
                "id" => "37"
            ],
            [
                "name" => "Thanh Hóa",
                "slug" => "thanh-hoa",
                "id" => "38"
            ],
            [
                "name" => "Nghệ An",
                "slug" => "nghe-an",
                "id" => "40"
            ],
            [
                "name" => "Hà Tĩnh",
                "slug" => "ha-tinh",
                "id" => "42"
            ],
            [
                "name" => "Quảng Bình",
                "slug" => "quang-binh",
                "id" => "44"
            ],
            [
                "name" => "Quảng Trị",
                "slug" => "quang-tri",
                "id" => "45"
            ],
            [
                "name" => "Thừa Thiên Huế",
                "slug" => "thua-thien-hue",
                "id" => "46"
            ],
            [
                "name" => "Đà Nẵng",
                "slug" => "da-nang",
                "id" => "48"
            ],
            [
                "name" => "Quảng Nam",
                "slug" => "quang-nam",
                "id" => "49"
            ],
            [
                "name" => "Quảng Ngãi",
                "slug" => "quang-ngai",
                "id" => "51"
            ],
            [
                "name" => "Bình Định",
                "slug" => "binh-dinh",
                "id" => "52"
            ],
            [
                "name" => "Phú Yên",
                "slug" => "phu-yen",
                "id" => "54"
            ],
            [
                "name" => "Khánh Hòa",
                "slug" => "khanh-hoa",
                "id" => "56"
            ],
            [
                "name" => "Ninh Thuận",
                "slug" => "ninh-thuan",
                "id" => "58"
            ],
            [
                "name" => "Bình Thuận",
                "slug" => "binh-thuan",
                "id" => "60"
            ],
            [
                "name" => "Kon Tum",
                "slug" => "kon-tum",
                "id" => "62"
            ],
            [
                "name" => "Gia Lai",
                "slug" => "gia-lai",
                "id" => "64"
            ],
            [
                "name" => "Đắk Lắk",
                "slug" => "dak-lak",
                "id" => "66"
            ],
            [
                "name" => "Đắk Nông",
                "slug" => "dak-nong",
                "id" => "67"
            ],
            [
                "name" => "Lâm Đồng",
                "slug" => "lam-dong",
                "id" => "68"
            ],
            [
                "name" => "Bình Phước",
                "slug" => "binh-phuoc",
                "id" => "70"
            ],
            [
                "name" => "Tây Ninh",
                "slug" => "tay-ninh",
                "id" => "72"
            ],
            [
                "name" => "Bình Dương",
                "slug" => "binh-duong",
                "id" => "74"
            ],
            [
                "name" => "Đồng Nai",
                "slug" => "dong-nai",
                "id" => "75"
            ],
            [
                "name" => "Bà Rịa - Vũng Tàu",
                "slug" => "ba-ria-vung-tau",
                "id" => "77"
            ],
            [
                "name" => "Hồ Chí Minh",
                "slug" => "ho-chi-minh",
                "id" => "79"
            ],
            [
                "name" => "Long An",
                "slug" => "long-an",
                "id" => "80"
            ],
            [
                "name" => "Tiền Giang",
                "slug" => "tien-giang",
                "id" => "82"
            ],
            [
                "name" => "Bến Tre",
                "slug" => "ben-tre",
                "id" => "83"
            ],
            [
                "name" => "Trà Vinh",
                "slug" => "tra-vinh",
                "id" => "84"
            ],
            [
                "name" => "Vĩnh Long",
                "slug" => "vinh-long",
                "id" => "86"
            ],
            [
                "name" => "Đồng Tháp",
                "slug" => "dong-thap",
                "id" => "87"
            ],
            [
                "name" => "An Giang",
                "slug" => "an-giang",
                "id" => "89"
            ],
            [
                "name" => "Kiên Giang",
                "slug" => "kien-giang",
                "id" => "91"
            ],
            [
                "name" => "Cần Thơ",
                "slug" => "can-tho",
                "id" => "92"
            ],
            [
                "name" => "Hậu Giang",
                "slug" => "hau-giang",
                "id" => "93"
            ],
            [
                "name" => "Sóc Trăng",
                "slug" => "soc-trang",
                "id" => "94"
            ],
            [
                "name" => "Bạc Liêu",
                "slug" => "bac-lieu",
                "id" => "95"
            ],
            [
                "name" => "Cà Mau",
                "slug" => "ca-mau",
                "id" => "96"
            ],
        ]);
    }

}
