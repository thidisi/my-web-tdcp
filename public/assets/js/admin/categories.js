//show modal add
$("#button-add").click(function () {
    $("#modal-form").toggle();
    $("#modal-form-overlay").toggle();
    $(".btn.btn-primary.button").text("Thêm mới");
    $("#edit_id").remove();
    $("#formValidate")[0].reset();
    $(".btn.btn-primary.button").attr("id", "storeAdd");
    $("#preview-image").attr('src', '');
    $("#button-add").css('display', 'none');
});
$("#fileImage").change(function () {
    let reader = new FileReader();
    reader.onload = (e) => {
        $("#preview-image").attr("src", e.target.result);
    };
    reader.readAsDataURL(this.files[0]);
});
//show modal edit
$("body").on("click", ".button-edit", function () {
    var category_id = $(this).data("id");
    $.ajax({
        type: "GET",
        url: "/api/categoryEdit/" + category_id,
        dataType: "JSON",
        success: function (data) {
            $("#edit_id").remove();
            $("#formValidate").append(
                '<input type="hidden" id="edit_id" value="' +
                data.category.id +
                '">'
            );
            $("#name").val(data.category.name);
            $("#description").val(data.category.description);
            $("#preview-image").attr("src", data.category.image_path);
            $("#storeAdd").text("Sửa danh mục");
            $("#storeAdd").attr("id", "storeEdit");
            $("#button-add").css('display', 'block');
        },
    });
});
//xử lý thêm
$("body").on("click", "#storeAdd", function (e) {
    e.preventDefault();
    var url = $('#formValidate').data('route');
    var form_data = new FormData(document.getElementById("formValidate"));
    $.ajax({
        url: url,
        type: "post",
        dataType: "json",
        async: false,
        processData: false,
        contentType: false,
        data: form_data,
        success: function (response) {
            $.toast({
                heading: 'Thông báo thành công!',
                text: 'Thêm thành công!',
                showHideTransition: 'slide',
                position: 'top-right',
                icon: 'success'
            });
            $("#formValidate")[0].reset();
            $(".btn.btn-primary.button").text("Thêm mới");
            $(".btn.btn-primary.button").attr("id", "storeAdd");
            table.ajax.reload();
            let total = Number($("#total_number").text());
            $("#total_number").text(total + 1);
        },
        error: function (response) {
            $.toast({
                heading: 'Thông báo lỗi!',
                text: response.responseJSON.message,
                showHideTransition: 'slide',
                position: 'top-right',
                icon: 'error'
            });
        },
    });
});
// // xử lý update
$("body").on("click", "#storeEdit", function (e) {
    e.preventDefault();
    var form_data = new FormData(document.getElementById("formValidate"));
    form_data.append("_method", "put");
    var id = $("#edit_id").val();
    if (confirm("Bạn có muốn sửa danh mục này?")) {
        $.ajax({
            url: "/api/categoryUpdate/" + id,
            type: "post",
            dataType: "json",
            async: false,
            processData: false,
            contentType: false,
            data: form_data,
            success: function (response) {
                $.toast({
                    heading: 'Thông báo thành công!',
                    text: 'Cập nhật thành công!',
                    showHideTransition: 'slide',
                    position: 'top-right',
                    icon: 'success'
                });
                $("#formValidate")[0].reset();
                $(".btn.btn-primary.button").text("Thêm mới");
                $(".btn.btn-primary.button").attr("id", "storeAdd");
                $("#edit_id").remove();
                table.ajax.reload();
            $("#button-add").css('display', 'none');

            },
            error: function (response) {
                $(".input-name").find(".text-danger").remove();
                $(".input-name").append(
                    '<p class="text-danger ml-2 mt-2"></p>'
                );
                $(".input-name")
                    .find(".text-danger")
                    .text(response.responseJSON.message)
                    .show()
                    .fadeOut(4000);
            },
        });
    }
});
delete
$(document).on("click", "#delete", function () {
    if (confirm("Bạn có chắc chắn xóa?")) {
        $.ajax({
            url: "/api/categoryDelete/" + $(this).data("id"),
            type: "delete",
            dataType: "json",
            data: {
                id: $(this).data("id"),
            },
            success: function (response) {
                $.toast({
                    heading: 'Thông báo thành công!',
                    text: 'Xóa thành công!',
                    showHideTransition: 'slide',
                    position: 'top-right',
                    icon: 'success'
                });
                table.ajax.reload();
                let total = Number($("#total_number").text());
                $("#total_number").text(total - 1);
            },
        });
    }
});
