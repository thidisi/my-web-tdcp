var dataTable, role;
function setDataTable(table) {
    dataTable = table;
}

function setRole(role) {
    role = role;
}

// confirm lí do interview/failed
$(document).on("change", "input[name=confimApply]", function () {
    $("#btn-next").hide();
    $("#btn-change").show();
    if (
        $(this).data("status") == "failed" ||
        $(this).data("status") == "rejected"
    ) {
        $(".interview_date").css("display", "none");
        $(".time_pass_work").css("display", "none");
        $(".reason").css("display", "block");
    } else if ($(this).data("status") == "interview") {
        $(".interview_date").css("display", "block");
        $(".reason").css("display", "none");
    } else {
        $(".interview_date").css("display", "none");
        $(".reason").css("display", "none");
        $(".reason").removeClass("text-error");
    }
    if ($(this).data("status") == "passed") {
        $(".time_pass_work").css("display", "block");
        $("#btn-next").show();
    } else {
        $(".time_pass_work").css("display", "none");
    }
});
// end confirm lí do interview/failed

function showModalCV(role_id, data_pdf, data_route) {
    $(".footer-apply").show();
    $("#btn-next").hide();
    $("#btn-previous").hide();
    $("#btn-edit").hide();
    $("#btn-change").hide();

    var id_role = role_id;
    $(".modal-candidate").toggleClass("show-candidate");
    $(".image-pdf").removeClass("change");
    $("#showFilePdf").show();
    $("#showFilePdf").attr("src", data_pdf);
    if (!data_pdf) {
        $("#showFilePdf").hide();
        $(".image-pdf").addClass("change");
    }
    ChangeTextCV(true);
    $.ajax({
        type: "get",
        url: data_route,
        data: {
            role: role,
        },
        success: function (response) {
            $(".confim-apply").append(
                `<div id='idApply' data-id='${response.apply.id}'></div>`
            );
            $("#title-job").text(response.job.title);
            $("#date-apply").text(response.apply.created_at);
            $("#name-cadidate").text(response.cadidate.name);
            if (response.apply.user_id != null) {
                $(".confim-apply").show();
            } else {
                $(".confim-apply").hide();
            }
            if (response.apply.status == "interview") {
                $("#status-apply").removeClass("label label-info");
                $("#status-apply").addClass("label label-warning");
                if (id_role == "" && response.apply.user_id == null) {
                    $("#btn-change").show();
                    $("#confim-profile").show();
                    $(".confim-apply")
                        .find("#confim-profile")
                        .text("Xác nhận phỏng vấn:");
                }
                if (id_role != "" && id_role != 1) {
                    $("#confim-profile").show();
                    $(".confim-apply")
                        .find("#confim-profile")
                        .html(
                            '<p style="color: red;">Vui lòng chờ xác nhận từ phía quản trị!</p>'
                        );
                }
            }
            if (
                response.apply.status == "rejected" ||
                response.apply.status == "failed"
            ) {
                $("#status-apply").removeClass("label label-info");
                $("#status-apply").addClass("label label-danger");
                $(".confim-apply").find("#confim-profile").show();
                $(".confim-apply").find("#confim-profile").text("Lý do:");
                $(".confim-box").text(response.apply.reason);
            }
            if (response.apply.status == "passed") {
                $(".confim-apply").show();
                $("#status-apply").removeClass("label label-info");
                $("#status-apply").addClass("label label-success");
                $("#confim-profile").hide();

                $("#btn-edit").show();
                // $('.confim-apply').find('#confim-profile').show();

                $(".confim-box").html(`
                    <div class="row" style="margin-top: 8px;">
                        <div class="col-md-6 pull-left">
                            <span class="font-12">Thời gian dự kiến: <b class="time_onboard">${response.customer.time_onboard}</b></span>
                        </div>
                        <div class="col-md-6 pull-left">
                            <span class="font-12">Thời gian thực tế: <b class="time_actual">${response.customer.time_actual}</b></span>
                        </div>
                        <div class="col-md-6 pull-left">
                            <span class="font-12">Ngày bắt đầu làm việc: <b class="start_date">${response.customer.start_date}</b></span>
                        </div>
                        <div class="col-md-6 pull-left">
                            <span class="font-12">Ngày kết thúc làm việc: <b class="end_date">${response.customer.end_date}</b></span>
                        </div>
                    </div>`);
                $("input[name=time_onboard]").val(
                    response.customer.time_onboard
                );
                $("input[name=time_actual]").val(response.customer.time_actual);
                $("input[name=start_date]").val(response.customer.start_date);
                $("input[name=end_date]").val(response.customer.end_date);
                $("input[id=salary]").val(response.customer.salary);
                $("input[id=recruitment_money]").val(
                    response.customer.recruitment_money
                );
                $("input[id=sale_money]").val(response.customer.sale_money);
                $("input[id=salary_13]").val(response.customer.salary_13);
                $("input[id=bonus_onsite]").val(response.customer.bonus_onsite);
                $("input[id=insurance]").val(response.customer.insurance);
                $("input[id=team_building]").val(
                    response.customer.team_building
                );
                $("input[id=income]").val(response.customer.income);
                // $('.confim-apply').find('#confim-profile').show();
                $("#btn-change").data("route", response.apply.route);
            }
            $("#modal-form-overlay").toggle();
            $("#status-apply").text(changeTextStatus(response.apply.status));
            $("#skill-apply").append($("<span>").append(response.apply.skills));
            $("#email-candidate").text(response.cadidate.email);
            $("#phone-candidate").text(response.cadidate.phone);
            if (response.apply.users) {
                $("#hr-apply").text(response.apply.users.name);
                if (response.apply.sources) {
                    $("#source").text(response.apply.sources.name);
                }
            }
            if (
                ((id_role != "" || id_role != 1) &&
                    response.apply.status != "interview") ||
                id_role == "" ||
                id_role == 1
            ) {
                if (response.apply.user_id != null) {
                    $.each(response.status, function (key, value) {
                        let confimData = `<div><input type="radio" data-status="${key}" name="confimApply" id="${key}"><label for="${key}">${value}</label></div>`;
                        $(".confim-box").append(confimData);
                    }); // TOD
                    if (Object.keys(response.status).length > 0) {
                        $(".confim-apply").find("#confim-profile").show();
                        $(".footer-apply").show();
                        $("#btn-change").data("route", response.apply.route);
                    }
                }
            }
        },
    });
}

$(document).on("click", ".show-filepdf-btn", function (e) {
    var role_id = $("#id_role").data("id");
    var pdf = $(this).data("pdf");
    var route = $(this).data("route");
    showModalCV(role_id, pdf, route);
});
$(document).on("click", ".show-candidate-btn-close", function (e) {
    e.preventDefault();
    $("#btn-change").data("route", null);
    $("#modal-form-overlay").toggle();
    $(".modal-candidate").toggleClass("show-candidate");
    $("#showFilePdf").attr("src", "");
    $(".reason").css("display", "none");
    $(".time_pass_work").css("display", "none");
    $("#confim-profile").hide();
    $(".confim-box").show();
    $("#editDate").remove();
    $("#idApply").remove();
    $(".contract-info").hide();
    $("#btn-change").hide();

    $("input[name=time_onboard]").val(0);
    $("input[name=time_actual]").val(0);
    $("input[name=start_date]").val(0);
    $("input[name=end_date]").val(0);
    $("input[id=salary]").val(0);
    $("input[id=recruitment_money]").val(0);
    $("input[id=sale_money]").val(0);
    $("input[id=salary_13]").val(0);
    $("input[id=bonus_onsite]").val(0);
    $("input[id=insurance]").val(0);
    $("input[id=team_building]").val(0);
    $("input[id=income]").val(0);
    $("#btn-change").removeClass("edit_info");
    $(".time_onboard").removeClass("text-error");
    $(".contract-info").find(".text-error").removeClass("text-error");
});
// update status cv
$("#btn-change").on("click", function (e) {
    if (!$("#btn-change").hasClass("edit_info")) {
        var id_role = $("#id_role").data("id");
        e.preventDefault();
        if (!$("input[name='confimApply']:checked").data("status")) {
            $.toast({
                heading: "Thông báo thất bại!",
                text: "Vui lòng xác nhận lại trạng thái",
                showHideTransition: "slide",
                position: "top-right",
                icon: "error",
            });
        } else {
            var dataStatus = $("input[name='confimApply']:checked").data(
                "status"
            );
            var reason = $("textarea[name=reason]").val();
            var time_onboard = $("input[name=time_onboard]").val();
            var time_actual = $("input[name=time_actual]").val();
            var start_date = $("input[name=start_date]").val();
            var end_date = $("input[name=end_date]").val();
            var salary = $("input[id=salary]").val();
            var recruitment_money = $("input[id=recruitment_money]").val();
            var sale_money = $("input[id=sale_money]").val();
            var salary_13 = $("input[id=salary_13]").val();
            var bonus_onsite = $("input[id=bonus_onsite]").val();
            var insurance = $("input[id=insurance]").val();
            var team_building = $("input[id=team_building]").val();
            var income = $("input[id=income]").val();
            var interview_date = $("input[name=interview_date]").val();
            var validate = true;
            if (
                (dataStatus == "rejected" && reason == "") ||
                (dataStatus == "failed" && reason == "")
            ) {
                $(".reason").addClass("text-error");
                validate = false;
            } else if (dataStatus == "passed") {
                if (time_onboard == "") {
                    $(".time_onboard").addClass("text-error");
                    $.toast({
                        heading: "Thông báo lỗi!",
                        text: "Vui lòng nhập thời gian dự kiến",
                        showHideTransition: "slide",
                        position: "top-right",
                        icon: "error",
                    });
                    validate = false;
                }
                var regex = /^\d{1,3}(,\d{3})*?$/;

                if (salary == "" || !regex.test(salary)) {
                    $("#salary").addClass("text-error");
                    $.toast({
                        heading: "Thông báo lỗi!",
                        text: "Vui lòng nhập chi phí lương",
                        showHideTransition: "slide",
                        position: "top-right",
                        icon: "error",
                    });
                    validate = false;
                }
                if (recruitment_money == "" || !regex.test(recruitment_money)) {
                    $("#recruitment_money").addClass("text-error");
                    $.toast({
                        heading: "Thông báo lỗi!",
                        text: "Vui lòng nhập Chi phí tuyển dụng",
                        showHideTransition: "slide",
                        position: "top-right",
                        icon: "error",
                    });
                    validate = false;
                }
                if (sale_money == "" || !regex.test(sale_money)) {
                    $("#sale_money").addClass("text-error");
                    $.toast({
                        heading: "Thông báo lỗi!",
                        text: "Vui lòng nhập Chi phí Sale",
                        showHideTransition: "slide",
                        position: "top-right",
                        icon: "error",
                    });
                    validate = false;
                }
                if (salary_13 == "" || !regex.test(salary_13)) {
                    $("#salary_13").addClass("text-error");
                    $.toast({
                        heading: "Thông báo lỗi!",
                        text: "Vui lòng nhập Lương T13",
                        showHideTransition: "slide",
                        position: "top-right",
                        icon: "error",
                    });
                    validate = false;
                }
                if (bonus_onsite == "" || !regex.test(bonus_onsite)) {
                    $("#bonus_onsite").addClass("text-error");
                    $.toast({
                        heading: "Thông báo lỗi!",
                        text: "Vui lòng nhập Bonus onsite",
                        showHideTransition: "slide",
                        position: "top-right",
                        icon: "error",
                    });
                    validate = false;
                }
                if (insurance == "" || !regex.test(insurance)) {
                    $("#insurance").addClass("text-error");
                    $.toast({
                        heading: "Thông báo lỗi!",
                        text: "Vui lòng nhập Bảo hiểm",
                        showHideTransition: "slide",
                        position: "top-right",
                        icon: "error",
                    });
                    validate = false;
                }
                if (team_building == "" || !regex.test(team_building)) {
                    $("#team_building").addClass("text-error");
                    $.toast({
                        heading: "Thông báo lỗi!",
                        text: "Vui lòng nhập Team Building",
                        showHideTransition: "slide",
                        position: "top-right",
                        icon: "error",
                    });
                    validate = false;
                }
                if (income == "" || !regex.test(income)) {
                    $("#income").addClass("text-error");
                    $.toast({
                        heading: "Thông báo lỗi!",
                        text: "Vui lòng nhập Đơn giá",
                        showHideTransition: "slide",
                        position: "top-right",
                        icon: "error",
                    });
                    validate = false;
                }
            } else if (dataStatus == "interview" && interview_date == "") {
                $("input[name=interview_date]").addClass("text-error");
                $.toast({
                    heading: "Thông báo lỗi!",
                    text: "Vui lòng nhập thời gian phỏng vấn?",
                    showHideTransition: "slide",
                    position: "top-right",
                    icon: "error",
                });
                validate = false;
            }
            if (validate) {
                $.ajax({
                    type: "post",
                    url: $("#btn-change").data("route"),
                    beforeSend: function () {
                        $(".lds-ellipsis").show();
                    },
                    complete: function () {
                        $(".lds-ellipsis").hide();
                    },
                    async: true,
                    data: {
                        status: dataStatus,
                        role: role,
                        reason: reason,
                        time_onboard: time_onboard,
                        time_actual: time_actual,
                        start_date: start_date,
                        end_date: end_date,
                        salary: salary,
                        recruitment_money: recruitment_money,
                        sale_money: sale_money,
                        salary_13: salary_13,
                        bonus_onsite: bonus_onsite,
                        insurance: insurance,
                        team_building: team_building,
                        income: income,
                        interview_date: interview_date,
                        user_name: $("#user_name").data("name"),
                        user_id: $("#user_id").data("id"),
                    },
                    success: function (response) {
                        $.toast({
                            heading: "Thông báo thành công!",
                            text: "Cập nhật trạng thái thành công",
                            showHideTransition: "slide",
                            position: "top-right",
                            icon: "success",
                        });
                        if (dataTable != undefined) {
                            dataTable.ajax.reload();
                        }
                        ChangeTextCV();
                        $("textarea[name=reason]").val("");
                        $(".reason").css("display", "none");
                        $(".time_pass_work").css("display", "none");
                        $(".reason").removeClass("text-error");
                        $(".confim-apply").show();
                        if (response.apply.status == "interview") {
                            $(".confim-box").show();
                            $("#btn-change").hide();
                            $("#status-apply").removeClass("label label-info");
                            $("#status-apply").addClass("label label-warning");
                            $(".confim-apply").find("#confim-profile").text("");
                            $("input[name=interview_date]").addClass(
                                "text-error"
                            );
                            $("input[name=interview_date]").val("");
                            $(".interview_date").css("display", "none");
                            if (id_role != "" && id_role != 1) {
                                $(".confim-apply")
                                    .find("#confim-profile")
                                    .html(
                                        '<p style="color: red;">Vui lòng chờ xác nhận từ phía quản trị!</p>'
                                    );
                            }
                            if (id_role == "" || id_role == 1) {
                                $(".confim-apply")
                                    .find("#confim-profile")
                                    .text("Xác nhận phỏng vấn:");
                                if (response.status) {
                                    $(".confim-apply")
                                        .find("#confim-profile")
                                        .show();
                                    $(".footer-apply").show();
                                    $.each(
                                        response.status,
                                        function (key, value) {
                                            let confimData = `<div><input type="radio" data-status="${key}" name="confimApply" id="${key}"><label for="${key}">${value}</label></div>`;
                                            $(".confim-box").append(confimData);
                                        }
                                    );
                                }
                            }
                        }
                        if (
                            response.apply.status == "rejected" ||
                            response.apply.status == "failed"
                        ) {
                            $(".confim-box").show();
                            $("#btn-change").hide();
                            $(".confim-apply").find("#confim-profile").show();
                            $(".confim-apply")
                                .find("#confim-profile")
                                .text("Lý do:");
                            $(".confim-box").text(response.apply.reason);
                            $("#status-apply").removeClass("label label-info");
                            $("#status-apply").addClass("label label-danger");
                        }
                        if (response.apply.status == "passed") {
                            $(".time_onboard").removeClass("text-error");
                            $(".contract-info")
                                .find(".text-error")
                                .removeClass("text-error");
                            $("#btn-edit").show();
                            $("#btn-previous").hide();
                            $("#btn-next").hide();
                            $("#btn-change").hide();
                            $(".time_pass_work").hide();
                            $(".contract-info").hide();
                            $("#status-apply").removeClass("label label-info");
                            $("#status-apply").addClass("label label-success");
                            $(".confim-apply").find("#confim-profile").hide();
                            $(".confim-box").html(`
                                    <div class="row" style="margin-top: 8px;">
                                        <div class="col-md-6 pull-left">
                                            <span class="font-12">Thời gian dự kiến: <b class="time_onboard">${response.customer.time_onboard}</b></span>
                                        </div>
                                        <div class="col-md-6 pull-left">
                                            <span class="font-12">Thời gian thực tế: <b class="time_actual">${response.customer.time_actual}</b></span>
                                        </div>
                                        <div class="col-md-6 pull-left">
                                            <span class="font-12">Ngày bắt đầu làm việc: <b class="start_date">${response.customer.start_date}</b></span>
                                        </div>
                                        <div class="col-md-6 pull-left">
                                            <span class="font-12">Ngày kết thúc làm việc: <b class="end_date">${response.customer.end_date}</b></span>
                                        </div>
                                    </div>`);
                            // $('.confim-apply').find('#confim-profile').show();
                            // $('#confim-profile').html(`<button id="changeDate" class="btn btn-warning">Sửa</button>`)
                            $("#btn-change").data(
                                "route",
                                response.apply.route
                            );
                            $("#btn-change").addClass("edit_info");
                        }
                        $("#status-apply").text(
                            changeTextStatus(response.apply.status)
                        );
                        get_notify_bubble();
                    },
                    error: function (response) {
                        $(".lds-ellipsis").hide();
                    },
                });
            }
        }
    }
});

function ChangeTextCV(checkText) {
    $("#status-apply").removeClass("label label-danger");
    $("#status-apply").removeClass("label label-success");
    $("#status-apply").removeClass("label label-warning");
    $("#status-apply").addClass("label label-info");
    // $('.confim-apply').find('#confim-profile').hide();
    $(".confim-box").html("");
    if (checkText) {
        $("#title-job").text("");
        $("#btn-change").data("route", null);
        $("#skill-apply").html("");
        $("#date-apply").text("");
        $("#name-cadidate").text("");
        $("#exp-job").text("");
        $("#education-job").text("");
        $("#salary-job").text("");
        $("#level-job").text("");
        $("#job_type").text("");
        $("#deadline-job").text("");
        $(".confim-apply").find("#confim-profile").text("Xác nhận hồ sơ:");
    }
}

function changeTextStatus(status) {
    switch (status) {
        case "new":
            status = "Mới";
            break;
        case "interview":
            status = "Xác nhận";
            break;
        case "rejected":
            status = "Từ chối";
            break;
        case "passed":
            status = "Thành công";
            break;
        case "failed":
            status = "Trượt";
            break;
        default:
            break;
    }
    return status;
}

$(document).on("click", "#btn-edit", function () {
    // $(".time_onboard").replaceWith("<input type='date' name='time_onboard' class='form-control mb-2' value='" + $('.time_onboard').text() + "'>");
    // $(".time_actual").replaceWith("<input type='date' name='time_actual' class='form-control mb-2' value='" + $('.time_actual').text() + "'>");
    // $(".start_date").replaceWith("<input type='date' name='start_date' class='form-control mb-2' value='" + $('.start_date').text() + "'>");
    // $(".end_date").replaceWith("<input type='date' name='end_date' class='form-control mb-2' value='" + $('.end_date').text() + "'>");
    $("#changeDate").hide();
    $(".confim-box").hide();
    $(".time_pass_work").show();
    $(".footer-apply").show();
    $("#btn-next").show();
    $("#btn-change").show();
    $(this).hide();

    $("#btn-change").addClass("edit_info");

    // $('.confim-apply').append('<button type="button" id="editDate" class="btn-apply">Lưu thay đổi</button>')
});

// update status bonus user
$("#btn-change").on("click", function (e) {
    if ($("#btn-change").hasClass("edit_info")) {
        var validate = true;
        var time_onboard = $("input[name=time_onboard]").val();
        var salary = $("input[id=salary]").val();
        var recruitment_money = $("input[id=recruitment_money]").val();
        var sale_money = $("input[id=sale_money]").val();
        var salary_13 = $("input[id=salary_13]").val();
        var bonus_onsite = $("input[id=bonus_onsite]").val();
        var insurance = $("input[id=insurance]").val();
        var team_building = $("input[id=team_building]").val();
        var income = $("input[id=income]").val();
        if (time_onboard == "") {
            $(".time_onboard").addClass("text-error");
            $.toast({
                heading: "Thông báo lỗi!",
                text: "Vui lòng nhập thời gian dự kiến",
                showHideTransition: "slide",
                position: "top-right",
                icon: "error",
            });
            validate = false;
        }
        var regex = /^\d{1,3}(,\d{3})*?$/;
        if (salary == "" || !regex.test(salary)) {
            $("#salary").addClass("text-error");
            $.toast({
                heading: "Thông báo lỗi!",
                text: "Vui lòng nhập chi phí lương",
                showHideTransition: "slide",
                position: "top-right",
                icon: "error",
            });
            validate = false;
        }
        if (recruitment_money == "" || !regex.test(recruitment_money)) {
            $("#recruitment_money").addClass("text-error");
            $.toast({
                heading: "Thông báo lỗi!",
                text: "Vui lòng nhập Chi phí tuyển dụng",
                showHideTransition: "slide",
                position: "top-right",
                icon: "error",
            });
            validate = false;
        }
        if (sale_money == "" || !regex.test(sale_money)) {
            $("#sale_money").addClass("text-error");
            $.toast({
                heading: "Thông báo lỗi!",
                text: "Vui lòng nhập Chi phí Sale",
                showHideTransition: "slide",
                position: "top-right",
                icon: "error",
            });
            validate = false;
        }
        if (salary_13 == "" || !regex.test(salary_13)) {
            $("#salary_13").addClass("text-error");
            $.toast({
                heading: "Thông báo lỗi!",
                text: "Vui lòng nhập Lương T13",
                showHideTransition: "slide",
                position: "top-right",
                icon: "error",
            });
            validate = false;
        }
        if (bonus_onsite == "" || !regex.test(bonus_onsite)) {
            $("#bonus_onsite").addClass("text-error");
            $.toast({
                heading: "Thông báo lỗi!",
                text: "Vui lòng nhập Bonus onsite",
                showHideTransition: "slide",
                position: "top-right",
                icon: "error",
            });
            validate = false;
        }
        if (insurance == "" || !regex.test(insurance)) {
            $("#insurance").addClass("text-error");
            $.toast({
                heading: "Thông báo lỗi!",
                text: "Vui lòng nhập Bảo hiểm",
                showHideTransition: "slide",
                position: "top-right",
                icon: "error",
            });
            validate = false;
        }
        if (team_building == "" || !regex.test(team_building)) {
            $("#team_building").addClass("text-error");
            $.toast({
                heading: "Thông báo lỗi!",
                text: "Vui lòng nhập Team Building",
                showHideTransition: "slide",
                position: "top-right",
                icon: "error",
            });
            validate = false;
        }
        if (income == "" || !regex.test(income)) {
            $("#income").addClass("text-error");
            $.toast({
                heading: "Thông báo lỗi!",
                text: "Vui lòng nhập Đơn giá",
                showHideTransition: "slide",
                position: "top-right",
                icon: "error",
            });
            validate = false;
        }
        if (validate) {
            var route = $("#getRouteChangeDate").data("route");
            var id = $("#idApply").data("id");
            e.preventDefault();
            $.ajax({
                type: "post",
                url: route,
                data: {
                    id: id,
                    time_onboard: $("input[name=time_onboard]").val(),
                    time_actual: $("input[name=time_actual]").val(),
                    start_date: $("input[name=start_date]").val(),
                    end_date: $("input[name=end_date]").val(),
                    salary: $("input[id=salary]").val(),
                    recruitment_money: $("input[id=recruitment_money]").val(),
                    sale_money: $("input[id=sale_money]").val(),
                    salary_13: $("input[id=salary_13]").val(),
                    bonus_onsite: $("input[id=bonus_onsite]").val(),
                    insurance: $("input[id=insurance]").val(),
                    team_building: $("input[id=team_building]").val(),
                    income: $("input[id=income]").val(),
                },
                success: function (response) {
                    $(".time_onboard").removeClass("text-error");
                    $(".contract-info")
                        .find(".text-error")
                        .removeClass("text-error");
                    $("#btn-edit").show();
                    $("#btn-previous").hide();
                    $("#btn-next").hide();
                    $("#btn-change").hide();

                    $.toast({
                        heading: "Thông báo thành công!",
                        text: "Cập nhật thành công!",
                        showHideTransition: "slide",
                        position: "top-right",
                        icon: "success",
                    });
                    if (dataTable != undefined) {
                        dataTable.ajax.reload();
                    }
                    $("#editDate").remove();
                    $("#changeDate").show();
                    $(".confim-box").show();
                    $(".confim-apply").show();
                    $(".time_pass_work").hide();
                    $(".contract-info").hide();
                    // $('.footer-apply').hide();
                    $(".confim-box").html(`
                    <div class="row" style="margin-top: 8px;">
                        <div class="col-md-6 pull-left">
                            <span class="font-12">Thời gian dự kiến: <b class="time_onboard">${response.apply.time_onboard}</b></span>
                        </div>
                        <div class="col-md-6 pull-left">
                            <span class="font-12">Thời gian thực tế: <b class="time_actual">${response.apply.time_actual}</b></span>
                        </div>
                        <div class="col-md-6 pull-left">
                            <span class="font-12">Ngày bắt đầu làm việc: <b class="start_date">${response.apply.start_date}</b></span>
                        </div>
                        <div class="col-md-6 pull-left">
                            <span class="font-12">Ngày kết thúc làm việc: <b class="end_date">${response.apply.end_date}</b></span>
                        </div>

                    </div>`);
                },
            });
            $("#btn-change").removeClass("edit_info");
        }
    }
});

$("#btn-next").click(function (e) {
    $(".contract-info").show();
    $(".confim-apply").hide();
    $("#btn-change").show();
    $("#btn-previous").show();
    $(this).hide();
});
$("#btn-previous").click(function (e) {
    $(".contract-info").hide();
    $(".confim-apply").show();
    $("#btn-next").show();
    $(this).hide();
});

//Validate input currency
$("input[data-type='currency']").on({
    keyup: function () {
        formatCurrency($(this));
    },
    blur: function () {
        formatCurrency($(this), "blur");
    },
});

function formatNumber(n) {
    // format number 1234567 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function formatCurrency(input, blur) {
    // and puts cursor back in right position.
    // get input value
    var input_val = input.val();

    // don't validate empty input
    if (input_val === "") {
        return;
    }

    // original length
    var original_len = input_val.length;

    // initial caret position
    var caret_pos = input.prop("selectionStart");
    input_val = formatNumber(input_val);

    // final formatting
    if (blur === "blur") {
        input_val += "";
    }
    // send updated string to input
    input.val(input_val);

    // put caret back in the right position
    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
}
