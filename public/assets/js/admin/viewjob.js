//show modal delete
$(".button-delete").click(function () {
    $("#modal-form").toggle();
    $("#modal-form-overlay").toggle();
    $("#content-modal").text('Bạn có chắc chắn muốn xóa job này ?')
    $(".btn.btn-primary.button").text("Xóa");
    $("#button").attr("id", "delete");
    $("#formValidate").append('<input value="' + $(this).data("route") + '" id="route_url" type="hidden">')
});
// show modal hidden
$(".button-hidden").click(function () {
    $("#modal-form").toggle();
    $("#modal-form-overlay").toggle();
    $("#content-modal").text('Bạn có chắc chắn muốn ẩn job này ?')
    $(".btn.btn-primary.button").text("Đổi trạng thái");
    $("#button").attr("id", "storeStatus");
    $("#formValidate").append('<input value="' + $(this).data("route") + '" id="route_url" type="hidden">')
    $("#formValidate").append('<input value="' + $(this).data("id") + '" id="id_change" type="hidden">')

});
//show modal add and edit
$(".button-close").click(function () {
    $("#modal-form").toggle();
    $("#modal-form-overlay").toggle();
    $(".button-close").attr("id", "button");
    $(".btn.btn-primary.button").text("");
    $("#route_url").remove()
});
//update status
$(document).on('click', '#storeStatus', function () {
    $.ajax({
        type: "POST",
        url: $("#route_url").val(),
        dataType: "JSON",
        success: function (response) {
            $.toast({
                heading: 'Thông báo thành công!',
                text: 'Đổi trạng thái thành công!',
                showHideTransition: 'slide',
                position: 'top-right',
                icon: 'success'
            });
            $(".status_changer"+ $("#id_change").val()).html(response)
            $("#modal-form").toggle();
            $("#modal-form-overlay").toggle();
            $("#route_url").remove()
        }
    });
})
//delete job
$(document).on('click', '#delete', function () {
    $.ajax({
        type: "DELETE",
        url: $("#route_url").val(),
        // data: "data",
        dataType: "JSON",
        success: function (response) {
            $.toast({
                heading: 'Thông báo thành công!',
                text: 'Xóa job thành công!',
                showHideTransition: 'slide',
                position: 'top-right',
                icon: 'success'
            });
            $(".odd" + response.id).remove()
            $("#modal-form").toggle();
            $("#modal-form-overlay").toggle();
            $("#route_url").remove()
        }
    });
})