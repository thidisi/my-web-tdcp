$(document).ready(function(){
    $('body').on('click','.icheckbox_square-aero',function(){
        let check = $(this).hasClass('checked');
        if(check){
            $(this).removeClass('checked');
            $(this).find('input').prop('checked',false);
        }else{
            $(this).addClass('checked');
            $(this).find('input').attr('checked','checked');
        }
        $(this).attr('aria-checked',!check);
    });
})