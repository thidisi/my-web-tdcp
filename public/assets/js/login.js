window.addEventListener('DOMContentLoaded', (event) => {
    particlesJS("particles-js", {
        "particles": {
            "number": {
                "value": 100
            },
            "color": {
                "value": "#ffff4d"
            },
            "shape": {
                "type": "star",
                "stroke": {
                    "width": 3,
                    "color": "ffff4d"
                }
            },
            "opacity": {
                "value": 0.7,
                "random": true,
                "anim": {
                    "enable": false,
                    "speed": 1
                }
            },
            "size": {
                "value": 3,
                "random": false,
                "anim": {
                    "enable": false,
                    "speed": 30
                }
            },
            "line_linked": {
                "enable": true,
                "distance": 130,
                "color": "#ffff99",
                "width": 1
            },
            "move": {
                "enable": true,
                "speed": 2,
                "direction": "none",
                "straight": false
            }
        },
        "interactivity": {
            "events": {
                "onhover": {
                    "enable": false,
                    "mode": "repulse"
                },
                "onclick": {
                    "enable": false,
                    "mode": "push"
                }
            },
            "modes": {
                "repulse": {
                    "distance": 150,
                    "duration": 0.4
                },
                "bubble": {
                    "distance": 100,
                    "size": 10
                }
            }
        }
    });
});