@extends('admin.layouts.layoutAdmin')
@section('title', 'Quản lý người dùng')
@push('css')
    <link href="{{ asset('/assets/css/datatable-1.10.21.min.css') }}" rel="stylesheet">
    <style>
        div#datatable_paginate {
            margin: 16px 0px 30px;
        }

        button.close {
            padding: 5px !important;
            margin-top: -7px !important;
        }

        .btn_modal {
            display: flex;
            justify-content: flex-end;
            gap: 5px;
        }

        .select2-container {
            width: 100% !important;
        }
    </style>
@endpush
@section('content')
    <!-- ============================================================== -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <div class="content datatable_custom_content">
        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if ($errors->any())
            <h4></h4>
            <div class="alert alert-danger">
                {{ $errors->first() }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="widget bg-none" style="padding-bottom: 20px">
                    <div class="widget-content">
                        <table class="table table-hover yajra-datatable" id="datatable">
                            <thead>
                                <div class="_title-datatable row">
                                    <div class="text-left col-lg-6 lh-40">
                                        DANH SÁCH NGƯỜI DÙNG
                                    </div>
                                </div>
                                <tr>
                                    <th>
                                        STT
                                    </th>
                                    <th>
                                        <i class="fa fa-user" style="margin-right: 3px;"></i></i> Tên
                                        người dùng
                                    </th>
                                    <th>
                                        <i class="fa fa-envelope" style="margin-right: 3px;"></></i> Email
                                    </th>
                                    <th>
                                        <i class="fa fa-briefcase" style="margin-right: 3px;"></i></i> Cấp bậc
                                    </th>
                                    <th>
                                        <i class="fa fa-circle" style="margin-right: 3px;"></i></i> Trạng thái
                                    </th>
                                    <th>
                                        <i class="fa fa-list" style="margin-right: 3px;"></i>Hoạt động
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div id="modal-form-overlay" class="position-fixed"
                        style="z-index: 999; background-color: rgba(83, 83, 83, 0.281); display: none; top: 0; right: 0; left: 0; bottom: 0;">
                    </div>
                    <div class="modal-dialog modal-sm d-none-modal" id="modal-form">
                        <div class="modal-content">
                            <div class="modal-header" id="formChangeStatus">
                                <h4 class="modal-title" id="mySmallModalLabel">Xác nhận</h4>
                                <p id="content-modal"></p>
                            </div>
                            <div class="modal-body">
                                <div class="btn_modal">
                                    <button class="btn btn-danger md-close button-close">Hủy</button>
                                    <button class="btn btn-success md-close button" style="float: right;" id="button">Đổi
                                        trạng thái</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
                        aria-hidden="true" style="display: none;" id="update_information">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                                        onclick="document.getElementById('update_information').style.display='none'">×</button>
                                    <h4 class="modal-title">Thông tin người dùng</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name">Tên người dùng</label>
                                        <input type="text" class="form-control p-2" name="name"
                                            placeholder="Tên người dùng*" value="" id="name_user">
                                        <input type="hidden" name="id_user" id="id_user" value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="roles">Vai trò</label>
                                        <select name="roles" id="roles" class="form-control">
                                            @foreach ($roles as $role)
                                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="leader_user">Quản lý</label>
                                        <select name="leader" id="leader_id" class="leader_user">
                                        </select>
                                    </div>
                                    <div class="btn_modal">
                                        <button class="btn btn-danger"
                                            onclick="document.getElementById('update_information').style.display='none'">Hủy</button>
                                        <button class="btn btn-success" id="update_user">Cập
                                            nhật</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <!-- ============================================================== -->
    <!-- End div .md-content -->
    </div>
    <!-- End content here -->
    <!-- ============================================================== -->
@endsection
@push('js')
    <script src="{{ asset('/assets/libs/datatables/datatables-1.10.21.min.js') }}"></script>
    <script src="/assets/libs/select2/select2.min.js"></script>
    <script>
        var table = $('.yajra-datatable').DataTable({
            lengthChange: false,
            paging: true,
            info: false,
            autoWidth: false,
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: true,
            searching: false,
            order: [
                [4, 'desc'],
                [1, 'desc']
            ],
            ajax: {
                url: "{{ route('api.account.list') }}",
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'rank',
                    orderable: false,
                    className: 'dt-left'
                },
                {
                    data: 'name',
                    name: 'name',
                    orderable: true,
                    className: 'dt-left'
                },
                {
                    data: 'email',
                    name: 'email',
                    orderable: false,
                    className: 'dt-left'
                },
                {
                    data: 'role',
                    name: 'role',
                    orderable: false,
                    className: 'dt-left'
                },
                {
                    data: 'status',
                    name: 'status',
                    orderable: true,
                    className: 'dt-left'
                },
                {
                    data: 'action',
                    orderable: false,
                    name: 'action',
                },
            ],
            columnDefs: [{
                "defaultContent": "-",
                "targets": "_all"
            }],
            language: {
                "sProcessing": "<span class='fa-stack fa-lg text-yellow'>\n\
                                <i class='fa fa-spinner fa-spin fa-stack-2x fa-fw'></i>\n\
                           </span>&emsp;",
                "sLengthMenu": 'Hiển thị _MENU_ bản ghi',
                "sZeroRecords": 'Không có dữ liệu trong bảng',
                "sInfo": "",
                "sInfoEmpty": 'Hiển thị 0 đến 0 trong số 0 bản ghi',
                "sInfoFiltered": '(Đã lọc từ _MAX_ bản ghi)',
                "sSearch": "",
                "oPaginate": {
                    "sFirst": 'Đầu trang',
                    "sLast": 'Cuối trang',
                    "sNext": '»',
                    "sPrevious": '«'
                },
                searchPlaceholder: "Tên người dùng"
            }
        });
        //Modal Update User
        $(document).on("click", ".update_information", function() {
            var id = $(this).data('id');
            $.ajax({
                type: "GET",
                url: "{{ route('accounts.show', '') }}" + '/' + id,
                dataType: "JSON",
                success: function(response) {
                    $("#update_information").toggle();
                    $("#id_user").val(response.id);
                    $("#name_user").val(response.name);
                    document.getElementById("roles").value = response.role_id;

                    if (response.parent != null) {
                        var option = new Option(response.parent.name, response.parent.id, true, true);
                        $('.leader_user').append(option).trigger('change');
                    } else {
                        $('.leader_user').val(null).trigger('change');
                    }
                }
            });
        });
        //Update Name or Role of user
        $(document).on("click", "#update_user", function() {
            var id = $("#id_user").val();
            $.ajax({
                type: "PUT",
                url: "{{ route('accounts.update', '') }}" + '/' + id,
                data: {
                    name: $("#name_user").val(),
                    role_id: $("#roles").val(),
                    parent_id: $("#leader_id").val()
                },
                success: function(response) {
                    $.toast({
                        heading: 'Thông báo thành công!',
                        text: 'Thay đổi thông tin người dùng thành công!',
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'success'
                    });
                    table.draw();
                    $("#update_information").toggle();
                }
            });
        });

        //Modal Confirm Change Status
        $(document).on("click", ".change_status", function() {
            $("#modal-form").toggle();
            $("#modal-form-overlay").toggle();
            $("#content-modal").text('Bạn có chắc chắn muốn đổi trạng thái của người dùng?');
            $("#button").attr("id", "changeStatus");
            $("#formChangeStatus").append('<input value="' + $(this).data("route") +
                '" id="route_url" type="hidden">');
        });
        //Change Status
        $(document).on('click', '#changeStatus', function() {
            $.ajax({
                type: "POST",
                url: $("#route_url").val(),
                dataType: "JSON",
                success: function(response) {
                    $.toast({
                        heading: 'Thông báo thành công!',
                        text: 'Đổi trạng thái thành công!',
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'success'
                    });
                    $("#modal-form").toggle();
                    $("#modal-form-overlay").toggle();
                    $("#route_url").remove()
                    table.draw();
                }
            });
        });
        //Close modal Change Status
        $(".button-close").click(function() {
            $("#modal-form").toggle();
            $("#modal-form-overlay").toggle();
            $(".button-close").attr("id", "button");
            $(".btn.btn-primary.button").text("");
            $("#route_url").remove()
        });

        function formatResult(result) {
            return result.text;
        };

        $('.leader_user').select2({
            placeholder: "Chọn người quản lý",
            allowClear: true,
            minimumInputLength: 1,
            ajax: {
                url: '{{ route('api.user.list') }}',
                dataType: 'json',
                data: function(params) {
                    return {
                        search: params.term,
                        member_name: $("#name_user").val(),
                    };
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: false
            },
            templateResult: formatResult,
            language: {
                inputTooShort: function() {
                    return "Nhập tối thiểu 1 ký tự để tìm kiếm...";
                },
                searching: function() {
                    return "";
                },
                noResults: function() {
                    return "Không tìm thấy.";
                }
            }
        });
    </script>
@endpush
