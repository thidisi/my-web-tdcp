@extends('admin.layouts.layoutLoginAdmin')
@if (!empty($checkStatus))
    @section('title', 'Xác nhận tài khoản')
@else
    @auth
        @section('title', 'Tạo Mật khẩu')
    @endauth
    @guest
        @section('title', 'Đăng nhập')
    @endguest
@endif
@section('role', 'ADMIN')

@section('image')
    <div class="box_image">
        <img src="{{ asset('assets/img/admin-background.png') }}" class="image">
    </div>
@endsection

@section('content')
    @if (!empty($checkStatus))
        <div style="width: 100%;height: 100%;position: relative;">
            <div style="margin: 0;position: absolute;top: 40%;left: 50%;margin-right: -50%;transform: translate(-50%, -50%)">
                <p style="margin: 0;font-size: 1.2rem;font-weight: 600;">Vui lòng đợi xác nhận tài khoản từ ADMIN</p>
                <span style="font-size: 0.8rem;">Hoặc đăng nhập bằng tài khoản khác
                    <form class="d-inline-block" action="{{ route('admin.logout') }}" method="post">
                        @csrf
                        <button class="btn-back" type="submit">tại đây</button>
                    </form>
                </span>
            </div>
        </div>
    @else
        @auth
            <form action="{{ route('admin.register') }}" method="POST" class="mt-md-4">
                @csrf
                <div class="mb-2">
                    <label class="mb-2">
                        <h6 class="mb-0 text-sm">Tạo mật khẩu</h6>
                    </label>
                    <input class='form-control @if ($errors->has('password')) input-error @endif' type="password"
                        name="password" value="{{ old('password') }}" placeholder="Nhập mật khẩu">
                    @if ($errors->has('password'))
                        <p class="text-error">{{ $errors->first('password') }}</p>
                    @endif
                </div>
                <div class="mb-2">
                    <label class="mb-2">
                        <h6 class="mb-0 text-sm">Xác nhận mật khẩu</h6>
                    </label>
                    <input class='form-control @if ($errors->has('password_confirmation')) input-error @endif' type="password"
                        name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Nhập lại mật khẩu">
                    @if ($errors->has('password_confirmation'))
                        <p class="text-error">{{ $errors->first('password_confirmation') }}</p>
                    @endif
                </div>
                <div class="mt-3">
                    <button type="submit" class="btn btn-submit text-center">Đăng nhập</button>
                </div>
            </form>
        @endauth
        @guest
            <form action="{{ route('admin.handleLogin') }}" method="POST" class="mt-md-4">
                @csrf
                <div class="form-group">
                    <label class="mb-1">
                        <h6 class="mb-0 text-sm">Email</h6>
                    </label>
                    <input class="form-control" type="text" name="email" placeholder="Nhập email của bạn">
                </div>
                <div class="form-group mb-2">
                    <label class="mb-1">
                        <h6 class="mb-0 text-sm">Mật khẩu</h6>
                    </label>
                    <input class="form-control" type="password" name="password" placeholder="Nhập mật khẩu">
                </div>
                <div class="d-flex mb-3">
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input id="chk1" type="checkbox" name="remember" class="custom-control-input form-control">
                        <label for="chk1" class="custom-control-label text-sm">Lưu đăng nhập</label>
                    </div>
                    <a href="#" class="ml-auto mb-0 text-sm">Quên mật khẩu</a>
                </div>
                <input type="hidden" name="masterAdmin" value="admin">
                <div class="d-flex mb-3 " style="justify-content:space-around;">
                    {{-- <a href="{{ route('auth.redirect', 'git-hachinet') }}" class=""><img class="gitlab-login-icon"
                            alt="" src="/plugin_assets/redmine_auth_gitlab/images/gitlab_icon.svg?1654614110">
                        Login with GitLab
                    </a> --}}
                    <a style=" height: 30px;" href="{{ route('auth.redirect', 'gitlab') }}" class="">Login with <img
                            style="width: 30px; height: 100%; margin-left: 6px;" class="gitlab-login-icon" alt=""
                            src="{{ asset('assets/img/gitlab.png') }}">

                    </a>
                    <a style=" height: 30px;" href="{{ route('auth.redirect', 'google') }}" class="">Login with<img
                            style="width: 30px; height: 100%; margin-left: 6px;" class="gitlab-login-icon" alt=""
                            src="{{ asset('assets/img/google.png') }}">

                    </a>
                    <a style=" height: 30px;" href="{{ route('auth.redirect', 'facebook') }}" class="">Login with<img
                            style="width: 30px; height: 100%; margin-left: 6px;" class="gitlab-login-icon" alt=""
                            src="{{ asset('assets/img/fb.png') }}">
                    </a>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-submit text-center">Đăng nhập</button>
                </div>
            </form>
        @endguest
        @error('error')
            <span class="error">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    @endif
@endsection
