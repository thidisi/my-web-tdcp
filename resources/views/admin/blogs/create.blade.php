@extends('admin.layouts.layoutAdmin')
@section('title', 'Tạo tin tức')
@push('css')
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.12.1/b-2.2.3/b-colvis-2.2.3/b-html5-2.2.3/b-print-2.2.3/date-1.1.2/fc-4.1.0/fh-3.2.3/r-2.3.0/rg-1.2.0/sc-2.0.6/sb-1.3.3/sl-1.4.0/datatables.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('/assets/css/datatable.css') }}">



    <style>
        tbody tr td:nth-child(2),
        th {
            text-align: center !important;
        }

        div#cke_1_contents {
            height: 406px !important;
        }
    </style>
@endpush

@section('content')

    <div class="content datatable_custom_content">
        {{-- <div class="col-lg-12 bg-white-1" style="padding: 15px"> --}}
        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <div class="row">
            <form action="{{ route('blogs.store') }}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="col-lg-4">
                    <label class="font-12">Tiêu đề:</label>
                    <div class="form-group">
                        <input style=" height: 35px;" type="text" class="form-control" placeholder="Nhập tiêu đề"
                            value="{{ old('title') }}" name="title">
                    </div>
                    @error('title')
                        <p class="text-danger text-center alertSpan errDes">
                            {{ $message }}
                        </p>
                    @enderror
                </div>
                <div class="col-lg-4">
                    <label class="font-12">Ảnh:</label>
                    <input type="file" accept="image/*" value="{{ old('banner') }}" name="banner" id="banner"
                        style="color: transparent;">
                    <img id="preview-image" width="60">
                    @error('banner')
                        <p class="text-danger text-center alertSpan errDes">
                            {{ $message }}
                        </p>
                    @enderror
                </div>


                <div class="col-lg-4">
                    <label class="font-12">Danh mục:</label>
                    <select class="form-control" name="category_id">
                        <option>--- Chọn danh mục ---</option>
                        @foreach ($category as $it)
                            <option value="{{ $it->id }}" {{ old('category_id') == $it->id ? 'selected' : '' }}>
                                {{ $it->name }}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                        <p class="text-danger text-center alertSpan errDes">
                            {{ $message }}
                        </p>
                    @enderror
                </div>

                <div class="col-lg-12 ">
                    <label>Tóm tắt</label>
                    <textarea name="highlight" id="" maxlength="1000" options="cleaveOptions" rows="3"
                        class="form-control description-profile input-profile">{{ old('highlight') }}</textarea>
                    @error('highlight')
                        <p class="text-danger text-center alertSpan errDes">
                            {{ $message }}
                        </p>
                    @enderror
                </div>

                <div class="col-md-12 text-center" style="margin-top: 20px;">
                    <div class="widget bg-none" style="padding-bottom: 20px">
                        <textarea class="form-control" id="summary-ckeditor" name="content">{{ old('content') }}</textarea>
                    </div>
                    @error('content')
                        <span class="text-danger alertSpan errDes">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="col-md-12">
                    <div class="d-f-end">
                        <button type="submit" class="btn btn-primary">Thêm mới</button>
                    </div>
                </div>
            </form>
        </div>
        {{-- </div> --}}
        <!-- ============================================================== -->
        <!-- End content here -->
        <!-- ============================================================== -->
    </div>
    <!-- End right content -->



@endsection

@push('js')
    <script type="text/javascript"
        src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.12.1/b-2.2.3/b-colvis-2.2.3/b-html5-2.2.3/b-print-2.2.3/date-1.1.2/fc-4.1.0/fh-3.2.3/r-2.3.0/rg-1.2.0/sc-2.0.6/sb-1.3.3/sl-1.4.0/datatables.min.js">
    </script>
    <script src="{{ asset('/assets/js/admin/categories.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace('summary-ckeditor', {
            filebrowserUploadUrl: "{{ '/admin/blog' }}",
            filebrowserUploadMethod: 'form'
        });
    </script>

    <script>
        $(document).on("change", "#banner", function() {
            let reader = new FileReader();
            reader.onload = (e) => {
                console.log(e.target.result);
                $("#preview-image").attr("src", e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        });
    </script>
@endpush
