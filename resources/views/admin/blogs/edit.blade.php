@extends('admin.layouts.layoutAdmin')
@section('title', 'Sửa tin tức')
@push('css')
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.12.1/b-2.2.3/b-colvis-2.2.3/b-html5-2.2.3/b-print-2.2.3/date-1.1.2/fc-4.1.0/fh-3.2.3/r-2.3.0/rg-1.2.0/sc-2.0.6/sb-1.3.3/sl-1.4.0/datatables.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('/assets/css/datatable.css') }}">



    <style>
        tbody tr td:nth-child(2),
        th {
            text-align: center !important;
        }

        div#cke_1_contents {
            height: 406px !important;
        }
    </style>
@endpush
@section('title')
    <a href="/admin/blogs/create"><button class="btn btn-default btn-sm hover-clear" type="button"><i
                class="fa fa-plus-circle"></i> Tạo việc làm mới</button></a>
@endsection
@section('content')

    <div class="content datatable_custom_content">

        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <div class="row">
            <form action="{{ route('blogs.update', $blog->id) }}" enctype="multipart/form-data" method="post">
                @csrf
                @method('put')
                <div class="col-lg-4">
                    <label class="font-12">Tiêu đề:</label>
                    <div class="form-group">
                        <input value="{{ $blog->title }}" style=" height: 35px;" required type="text"
                            class="form-control" placeholder="Nhập tiêu đề" name="title">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-label">Ảnh</div>
                    <input accept="image/*" type="file" name="banner" id="banner" style="color: transparent;">
                    <img id="preview-image" img src="{{ asset($blog->banner) }}" width="60px">
                    <span class="text-danger alertSpan errDes">
                    </span>
                </div>

                <div class="col-lg-4">
                    <label class="font-12">Danh mục:</label>
                    <select class="form-control" name="category_id">
                        <option>--- Chọn danh mục ---</option>
                        @foreach ($category as $it)
                            <option value="{{ $it->id }}" {{ $it->id == $blog->category_id ? 'selected' : '' }}>
                                {{ $it->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-12">
                    <label>Tóm tắt</label>
                    <textarea name="highlight" id="" maxlength="1000" options="cleaveOptions" rows="3" required
                        class="form-control description-profile input-profile"> {{ $blog->highlight }}</textarea>
                </div>
                <div class="col-md-12" style="margin-top: 20px;">
                    <div class="widget bg-none" style="padding-bottom: 20px">
                        <textarea style="height: 300px;" class="form-control" id="summary-ckeditor" name="content">{{ $blog->content }}</textarea>
                    </div>
                </div>
                <div class="col-md-12 my-3 d-flex" style="margin: 8px 0px; justify-content: flex-end;">
                    <button class="btn btn-primary button">Sửa</button>
                    <a href="{{ route('blogs.index') }}"><button style="margin-left: 4px;" type="button" id="button-add"
                            class="float-right btn btn-danger">Hủy</button>
                </div>
            </form>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->

    <!-- End right content -->



@endsection

@push('js')
    <script type="text/javascript"
        src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.12.1/b-2.2.3/b-colvis-2.2.3/b-html5-2.2.3/b-print-2.2.3/date-1.1.2/fc-4.1.0/fh-3.2.3/r-2.3.0/rg-1.2.0/sc-2.0.6/sb-1.3.3/sl-1.4.0/datatables.min.js">
    </script>
    <script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace('summary-ckeditor', {
            filebrowserUploadUrl: "{{ '/admin/blog' }}",
            filebrowserUploadMethod: 'form'
        });
    </script>

    <script>
        $(document).on("change", "#banner", function() {
            let reader = new FileReader();
            reader.onload = (e) => {
                console.log(e.target.result);
                $("#preview-image").attr("src", e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        });
    </script>
@endpush
