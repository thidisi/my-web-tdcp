@extends('admin.layouts.layoutAdmin')
@section('title', 'Danh sách tin tức')
@push('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('/assets/css/datatable.css') }}">

    <style>
        tbody tr td:nth-child(2),
        th {
            text-align: center !important;
        }

        #DataTables_Table_0_filter input {
            padding: 6px 8px !important;
        }

        .clear_input {
            position: relative;
        }

        .icon_close {
            cursor: pointer;
            display: block;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            right: 6%;
        }

        .apply-cv-btn {
            background-color: #fff;
            color: #444;
            border-color: #e5e5e5;
            border-radius: 5px;
            font-size: 11px;
            font-weight: bold;
            font-family: 'Montserrat', sans-serif;
        }
    </style>
@endpush
@section('content')
    <!-- ============================================================= -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <div class="content datatable_custom_content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget bg-none" style="padding-bottom: 20px">
                    <div class="widget-content">

                        <div class="">
                            <table class="table table-hover yajra-datatable">
                                <thead>
                                    <div class="_title-datatable row">
                                        <div class="text-left col-lg-6 lh-40" style="font-weight: 700;padding: 0;">
                                            DANH SÁCH BÀI VIẾT
                                        </div>
                                        <div class="text-right col-lg-6 fz-13" style="text-transform: none;padding: 0;">
                                            <a href="{{ route('blogs.create') }}"
                                                class="btn btn-default btn-lg background-hachinet form-control p-2 border-radius-select"
                                                style="width: 200px;">
                                                <i class="fa fa-plus-circle"></i>
                                                <span class="white-space: break-spaces;">Tạo bài viết mới</span>
                                            </a>
                                        </div>
                                    </div>
                                    <tr>
                                        <th>
                                            STT</th>
                                        <th><i data-v-38af99a7="" class="fa fa-book" style="margin-right: 3px;"></i></i>
                                            Tiêu đề</th>

                                        <th><i data-v-38af99a7="" class="fa fa-thin fa-comment"
                                                style="margin-right: 3px;"></i>Danh mục
                                        </th>

                                        <th><i data-v-38af99a7="" class="fa fa-align-justify"
                                                style="margin-right: 3px;"></i>Hoạt động</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($blog as $index => $it)
                                        <tr>
                                            <td>{{ $index + 1 }} </td>
                                            <td>
                                                    <a href="{{ route('blog.detail', $it->slug ) }} " style="color: black" >{{ $it->title }}</a>

                                            </td>

                                            <td>{{ $it->categories->name }}</td>

                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    <a href="{{ route('blogs.edit', $it->id) }}" data-toggle="tooltip"
                                                        title="Edit" class=""><button type="button"
                                                            class="btn-custom-action"><i
                                                                class="fa fa-edit"></i>&nbsp;&nbsp;Sửa</button></a><br>

                                                    <form action="{{ route('blogs.destroy', $it->id) }}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <a onclick="return confirm('Bạn có muốn xóa blog này?')">
                                                            <button type="submit" class="btn-custom-action"><i
                                                                    class="fa fa-trash-o"></i>&nbsp;&nbsp;Xóa</button>
                                                        </a>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End content here -->
        <!-- ============================================================== -->
    </div>
@endsection
@push('js')

    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
@endpush
