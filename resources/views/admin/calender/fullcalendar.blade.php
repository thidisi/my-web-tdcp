@extends('admin.layouts.layoutAdmin')
@section('title', 'Lịch phỏng vấn')
@push('css')
    <link rel="stylesheet" href="{{ asset('/assets/libs/fullcalendar/fullcalendar.css') }}">
    <style>
        #calendar {
            max-width: 95%;
            margin: 40px auto;
            padding: 0 10px;
            background: white;
        }

        #calendar td {
            padding: 0 14px 5px;

        }

        .fc-ltr .fc-basic-view .fc-day-top .fc-day-number {
            color: black;
            font-size: 15px;
        }

        tr:first-child>td>.fc-day-grid-event {
            margin-top: 8px;
        }

        .fc-unthemed .fc-content {
            padding: 4px;
            text-shadow: 0px 0px 3px black;
        }

        .fc .fc-toolbar>*>:first-child {
            margin-left: 0;
            font-weight: 600;
        }
    </style>
    <link href="{{ asset('/assets/css/modal-custom.css') }}" rel="stylesheet">
@endpush
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12" style="background: white">
                <div id="calendar"></div>
            </div>
        </div>
    </div>
    @include('admin.modal-show-cv.modal')
@endsection
@push('js')
    <script src="{{ asset('/assets/libs/moment/moment.js') }}"></script>
    <script src="{{ asset('/assets/libs/fullcalendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('/assets/js/admin/modal-cv.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale-all.js"></script>



    <script>
        $(document).ready(function() {
            function generateRandomColor() {
                let maxVal = 0xFFFFFF; // 16777215
                let randomNumber = Math.random() * maxVal;
                randomNumber = Math.floor(randomNumber);
                randomNumber = randomNumber.toString(16);
                let randColor = randomNumber.padStart(6, 0);
                return `#${randColor.toUpperCase()}`
            }

            var arr = new Array();
            $.ajax({
                url: "{!! route('fullcalendar.showCalendar') !!}",
                type: "GET",
                data: {
                    role: '{!! Auth::user()->role_id !!}'
                },
                success: function(data) {
                    data.forEach(element => {
                        var obj = {
                            title: element.candidates.name,
                            start: element.interview_date,
                            color: generateRandomColor(),
                            pdf: element.pdf,
                            role_id: '{!! Auth::user()->role_id !!}',
                            route: element.route,
                        }
                        arr.push(obj)
                    });
                    $('#calendar').fullCalendar({
                        locale: 'vi',
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,basicWeek,basicDay'
                        },
                        defaultDate: new Date(),
                        navLinks: true,
                        editable: true,
                        eventLimit: true,
                        events: arr,
                        timeFormat: 'H:mm',
                        eventClick: function(info) {
                            showModalCV(info.role_id, info.pdf, info.route);
                        }
                    });
                }
            });

        });
    </script>


@endpush
