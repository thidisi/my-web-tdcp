@extends('admin.layouts.layoutAdmin')
@section('title', 'Danh sách danh mục')
@push('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('/assets/css/datatable.css') }}">
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.12.1/b-2.2.3/b-colvis-2.2.3/b-html5-2.2.3/b-print-2.2.3/date-1.1.2/fc-4.1.0/fh-3.2.3/r-2.3.0/rg-1.2.0/sc-2.0.6/sb-1.3.3/sl-1.4.0/datatables.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        tbody tr td:nth-child(2),
        th {
            text-align: center !important;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button {
            padding: 0.2em 0.8em !important;
        }
        div#datatable_paginate {
            margin: 16px 0px 30px;
        }
    </style>
@endpush
@section('content')
    <!-- ============================================================== -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <div class="content datatable_custom_content">
        <div class="row">
            <div class="col-md-12" style="padding-right: 0px;">
                <h3 style="padding: 10px 0px;"><b>Danh sách danh mục</b></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 m-l-0 m-r-0">
                <div class="widget bg-none m-l-0 m-r-0" style="border-radius: 4px!important; padding-right: 0 !important; padding-left: 0 !important;">
                    <div class="widget-content">
                        <div class="">
                            <table class="table table-hover yajra-datatable" id="datatable">
                                <thead>
                                    <tr>
                                        {{-- <th><i style="margin-right: 3px;"></i>*</th> --}}
                                        <th>
                                            STT</th>

                                        <th><i class="fa fa-book" style="margin-right: 3px;"></i></i>Tên danh mục</th>
                                        <th><i class="fa fa-align-justify" style="margin-right: 3px;"></i>Hoạt động
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 position-back">
                <div class="card position-relative">
                    <div class="card-body">
                        <form data-route="{{ route('categories.store') }}" id="formValidate" style="padding-top: 12px;"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="mb-2 input-name">
                                <label class="form-label required">Tên danh mục</label>

                                <input type="text" name="name" id="name" value="{{ old('name') }}"
                                    class="form-control" placeholder="Nhập Tên..." />
                                <span class="text-danger alertSpan nameErr">
                                </span>
                            </div>
                            <div class="mb-2">
                                <label class="form-label required">Mô tả</label>

                                <textarea name="description" cols="30" rows="11" value="" id="description" class="form-control"
                                    placeholder="Mô tả ..."></textarea>
                                <span class="text-danger alertSpan errDes">
                                </span>
                            </div>

                            <div class="mb-3 d-flex" style="margin: 4px 0px;" style="margin: 4px 0px;">
                                <button class="btn btn-primary button" id="storeAdd">Thêm</button>
                                <button style="display: none; margin-left: 4px;;" type="button" id="button-add"
                                    class="float-right btn btn-danger">Hủy</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->
@endsection
@push('js')
    <script type="text/javascript"
        src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.12.1/b-2.2.3/b-colvis-2.2.3/b-html5-2.2.3/b-print-2.2.3/date-1.1.2/fc-4.1.0/fh-3.2.3/r-2.3.0/rg-1.2.0/sc-2.0.6/sb-1.3.3/sl-1.4.0/datatables.min.js">
    </script>
    <script src="{{ asset('/assets/js/admin/categories.js') }}"></script>
    <script>
        // Data Display Code
        var table = $('#datatable').DataTable({
            ajax: "{{ route('api.categories.list') }}",
            lengthMenu: [6],
            columns: [{
                    "data": "id"
                },

                {
                    "data": "name"
                },
                {
                    "data": "id",
                    render: function(data, type, row) {
                        return `<div class="btn-group btn-group-xs">
                                    <a data-id="${row.id}" id="" data-toggle="tooltip" title="Edit" class=" button-edit"><button type="button" class="btn-custom-action"><i class="fa fa-edit"></i>&nbsp;&nbsp;Sửa</button></a><br>
                                    <a data-id="${row.id}"  id="delete" class="btn-delete" data-toggle="tooltip" title="Delete" class="action-custom button-delete"><button type="button" class="btn-custom-action"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Xóa</button></a>
                                </div>`;
                    }
                },
            ],
            "bLengthChange": false,
            sDom: 'lrtip',
            language: {
                "sProcessing": "<span class='fa-stack fa-lg text-yellow'>\n\
                            <i class='fa fa-spinner fa-spin fa-stack-2x fa-fw'></i>\n\
                       </span>&emsp;",
                "sLengthMenu": "Hiển thị _MENU_ bản ghi",
                "sZeroRecords": "Không có dữ liệu trong bảng",
                "sInfo": "",
                "sInfoEmpty": "Hiển thị 0 đến 0 trong số 0 bản ghi",
                "sInfoFiltered": "(Đã lọc từ _MAX_ bản ghi)",
                "sSearch": "",
                "oPaginate": {
                    "sFirst": "Đầu trang",
                    "sLast": "Cuối trang",
                    "sNext": "»",
                    "sPrevious": "«"
                },
                searchPlaceholder: "Tên kỹ năng",
            }
        });
        table.on('order.dt search.dt', function() {
            let i = 1;

            table.cells(null, 0).every(function(cell) {
                this.data(i++);
            });
        }).draw();
    </script>
@endpush
