@extends('admin.layouts.layoutAdmin')
@section('title', 'Danh sách CV')
@push('css')
    <link href="{{ asset('/assets/css/datatable-1.10.21.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/libs/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/css/modal-custom.css') }}" rel="stylesheet">
    <style>
        a[data-type="currency"] {
            color: black;
        }

        .nav-tabs {
            border-bottom: none !important;
        }

        .nav-tabs>li.active>a {
            color: white !important;
            background-color: #ffb701 !important;
            /* border: 1px solid #ddd !important; */
        }

        .nav-tabs>li>a {
            color: white !important;
            background: darkgray !important;
            border: 0 1px solid #ddd !important;
            border-radius: 4px 4px 0 0 !important;
            margin-right: 2px !important;
            font-weight: bold !important;
            cursor: pointer !important;
        }

        .widget-header {
            border: none !important;
        }

        .widget .tab-content {
            padding-top: 0 !important;
        }

        .nav-tabs>li>a:hover {
            color: #fff !important;
            background: #ffb701 !important;
        }

        th {
            font-weight: bold !important;
        }

        .btn-custom-actions {
            background: none;
            /* padding: 0px 40px; */
            border: 1px solid rgba(0, 0, 0, .15);
            width: 70px;
            height: 36px;
            border-radius: 5px;
            font-weight: 600;
            font-size: 11px;
            color: #1f2d3d;
            margin-bottom: 3px;
            margin-left: 1.5px;
        }

        .btn-custom-action {
            background: none;
            padding: 0px 40px;
            border: 1px solid rgba(0, 0, 0, .15);
            width: 100px;
            height: 25px;
            border-radius: 5px;
            font-weight: 600;
            font-size: 11px;
            color: #1f2d3d;
            margin-bottom: 3px;
            margin-left: 1.5px;
        }

        .datepicker td span {
            width: 39px !important;
            height: 35px !important;
            line-height: 35px !important;
        }

        #DataTables_Table_0_filter input {
            padding: 6px 8px !important;
        }

        .dataTables_filter {
            float: right !important;
        }

        .btn_modal {
            display: flex;
            justify-content: flex-end;
            gap: 5px;
        }

        .tab_table {
            display: flex;
            justify-content: space-between;
        }

        .show-filepdf-btn:hover {
            cursor: pointer;
        }

        .show-filepdf-btn {
            color: #0275d8;
        }

        #datepicker:hover {
            cursor: pointer;
        }
    </style>
@endpush
@section('content')
    <div class="content datatable_custom_content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="widget">
                    <div class="widget-header">
                        <p>
                        <h2>Danh sách hồ sơ</h2>
                        </p>
                    </div>
                    <div class="tab_table">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="tab active" id="table_all">
                                <a data-toggle="tab" data-id="table_all">Tất cả</a>
                            </li>
                            <li class="tab" id="table_new">
                                <a data-toggle="tab" data-id="table_new">Mới</a>
                            </li>
                            <li class="tab" id="table_interview">
                                <a data-toggle="tab" data-id="table_interview">Xác nhận</a>
                            </li>
                            <li class="tab" id="table_rejected">
                                <a data-toggle="tab" data-id="table_rejected">Từ chối</a>
                            </li>
                            <li class="tab" id="table_passed">
                                <a data-toggle="tab" data-id="table_passed">Thành công</a>
                            </li>
                            <li class="tab" id="table_failed">
                                <a data-toggle="tab" data-id="table_failed">Trượt</a>
                            </li>
                        </ul>
                        <div class="tab_table">
                            <form class="tab" action="{{ route('api.export.excel.apply') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id" value="{{ \Auth::user()->id }}">
                                <input type="hidden" id="date-excel" name="date" value="{{ date('m/Y') }}">
                                <button type="submit"
                                    style="margin-right: 0.5rem;
                                height: 100%;
                                color: #fff;
                                background: #00965c;
                                border: none;
                                border-radius: 2px;">EXCEL</button>
                            </form>
                            <input class="" placeholder="" aria-controls="DataTables_Table_0" id="datepicker"
                                readonly="readonly" value="{{ date('m/Y') }}">
                        </div>
                    </div>

                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="">
                        <table class="table table-hover yajra-datatable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên ứng viên</th>
                                    <th>HR PIC</th>
                                    <th>Khách hàng</th>
                                    <th>Level</th>
                                    <th>Thời gian onboard</th>
                                    <th>Ngày bắt đầu/ kết thúc</th>
                                    <th>Trạng thái</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Assign to me --}}
    <div id="modal-form-overlay" class="position-fixed"
        style="z-index: 999; background-color: rgba(83, 83, 83, 0.281); display: none; top: 0; right: 0; left: 0; bottom: 0;">
    </div>
    <div class="modal-dialog modal-sm d-none-modal" id="modal-form">
        <div class="modal-content">
            <div class="modal-header" id="assignToMe">
                <h4 class="modal-title" id="mySmallModalLabel">Xác nhận</h4>
                <p id="content-modal"></p>
            </div>
            <div class="modal-body">
                <div class="btn_modal">
                    <button class="btn btn-danger md-close button-close">Hủy</button>
                    <button class="btn btn-success md-close button" id="button">Nhận</button>
                </div>
            </div>
        </div>
    </div>
    <div hidden id="id_role" data-id="{{ auth()->user()->role_id }}"></div>
    <div hidden id="user_name" data-name="{{ auth()->user()->name }}"></div>
    <div hidden id="user_id" data-id="{{ auth()->user()->id }}"></div>

    {{-- modal show cv --}}
    @include('admin.modal-show-cv.modal')
@endsection
@push('js')
    <script src="{{ asset('/assets/libs/datatables/datatables-1.10.21.min.js') }}"></script>
    <script src="{{ asset('/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/assets/js/admin/modal-cv.js') }}"></script>
    <script>
        $(document).ready(function() {
            var datepicker = $("#datepicker").datepicker({
                autoclose: true,
                format: "mm/yyyy",
                startView: "months",
                minViewMode: "months",
                changeMonth: true,
                changeYear: true,
                defaultDate: null,
            });
        });
        localStorage.setItem('getInfoModal', JSON.stringify({
            "data_pdf": "{{ $info['data_pdf'] }}",
            "data_route": "{{ $info['data_route'] }}",
            "role_id": "{{ $info['role_id'] }}",
        }));
        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            ajax: {
                type: "GET",
                url: "{{ route('api.cv.list.status') }}",
                data: function(d) {
                    d.status = $('.active.tab')[0].id,
                        d.user_id = '{{ auth()->user()->id }}',
                        d.date = $('#datepicker').val()
                },
                complete: function(d) {
                    var getInfoModal = localStorage.getItem('getInfoModal') ? localStorage.getItem(
                            'getInfoModal') :
                        '';
                    if (getInfoModal != undefined && getInfoModal != 0) {
                        if (table.data().toArray().length > 0) {
                            var getInfo = JSON.parse(getInfoModal);
                            if (getInfo.data_pdf != '' && getInfo.data_route != '') {
                                showModalCV(getInfo.role_id, getInfo.data_pdf, getInfo.data_route);
                            }
                            localStorage.removeItem('getInfoModal');
                        }
                    }

                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'rank',
                    className: 'dt-left'
                },
                {
                    data: 'candidate_name',
                    name: 'candidate_name',
                    className: 'dt-left',
                    width: '15%',
                },
                {
                    data: 'hr_pic',
                    name: 'hr_pic',
                    className: 'dt-left',
                    width: '15%',
                },
                {
                    data: 'customer',
                    name: 'customer',
                    className: 'dt-left',
                },
                {
                    data: 'level',
                    name: 'level',
                    className: 'dt-left'
                },
                {
                    data: 'onboard',
                    name: 'onboard',
                    width: '15%',
                    className: 'dt-left'
                },
                {
                    data: 'time_start_end',
                    name: 'time_start_end',
                    // className: 'dt-left'
                    width: '15%',
                },
                {
                    data: 'status',
                    name: 'status',
                    width: '10%',
                    className: 'dt-left'
                },
                {
                    data: 'action',
                    name: 'action',
                    width: '12%'
                },
            ],
            columnDefs: [{
                "defaultContent": "-",
                "targets": "_all",
            }],
            language: {
                "sProcessing": "<span class='fa-stack fa-lg text-yellow'>\n\
                                        <i class='fa fa-spinner fa-spin fa-stack-2x fa-fw'></i>\n\
                                   </span>&emsp;",
                "sLengthMenu": "Hiển thị _MENU_ bản ghi",
                "sZeroRecords": "Không có dữ liệu trong bảng",
                "sSearch": "",
                "sInfo": "",
                "sInfoEmpty": "",
                "sInfoFiltered": "(Đã lọc từ _MAX_ bản ghi)",
                "oPaginate": {
                    "sFirst": "Đầu trang",
                    "sLast": "Cuối trang",
                    "sNext": "»",
                    "sPrevious": "«"
                },
            },
        });
        $("#DataTables_Table_0_filter").remove();
        $('#DataTables_Table_0_length').remove();

        setDataTable(table);
        setRole({{ auth()->user()->role_id }});

        var dateSelect = $("#datepicker").val();
        $("#datepicker").on("changeDate", function(e) {
            dateSelect = $("#datepicker").val();
            $('#date-excel').val(dateSelect);
            table.ajax.reload();
        });

        $(document).on('click', 'a[data-toggle="tab"]', function(e) {
            $('.active.tab').removeClass('active');
            var id = $(this).data('id');
            $('li[id="' + id + '"]').addClass('active');
            $(this).addClass('active');
            table.ajax.reload();
            // $("#datepicker").datepicker("setDate", dateSelect);
        });
        $(document).on("click", ".assign_to_me", function() {
            $("#modal-form").toggle();
            $("#modal-form-overlay").toggle();
            $("#content-modal").text('Bạn có chắc chắn muốn nhận hỗ trợ ứng viên này?');
            $("#button").attr("id", "updateAssign");
            $("#assignToMe").append('<input value="' + $(this).data("id") +
                '" id="candidate_id" type="hidden">');
            $("#assignToMe").append('<input value="' + $(this).data("apply") +
                '" id="apply_id" type="hidden">');
            $(".button").text('Nhận')

        });
        $(document).on("click", ".cancel_assign", function() {
            $("#modal-form").toggle();
            $("#modal-form-overlay").toggle();
            $(".button").text('Chắc chắn')
            $("#content-modal").text('Bạn có chắc chắn muốn hủy hỗ trợ ứng viên này?');
            $("#button").attr("id", "cancelAssign");
            $("#assignToMe").append('<input value="' + $(this).data("apply") +
                '" id="apply_id" type="hidden">');

        });

        $(".button-close").click(function() {
            $("#modal-form").toggle();
            $("#modal-form-overlay").toggle();
            $(".button").attr("id", "button");
            $(".btn.btn-primary.button").text("");
            $("#candidate_id").remove();
            $("#apply_id").remove();
        });

        $(document).on('click', '#updateAssign', function() {
            $.ajax({
                type: "POST",
                url: "{{ route('api.assign.candidate') }}",
                data: {
                    candidate_id: $("#candidate_id").val(),
                    user_id: "{{ auth()->user()->id }}",
                    apply_id: $("#apply_id").val(),
                },
                success: function(response) {
                    $.toast({
                        heading: 'Thông báo thành công!',
                        text: 'Nhận hỗ trợ ứng viên thành công!',
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'success'
                    });
                    $("#modal-form").toggle();
                    $("#modal-form-overlay").toggle();
                    $(".button").attr("id", "button");
                    $(".btn.btn-primary.button").text("");
                    $("#candidate_id").remove();
                    $("#apply_id").remove();
                    // $("#cancel_assign").remove();
                    table.ajax.reload();
                },
                error: function(response) {
                    if (response.responseJSON.mess) {
                        $.toast({
                            heading: 'Thông báo lỗi!',
                            text: response.responseJSON.mess,
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: 'error'
                        });
                    };
                }
            });
        });
        $(document).on('click', '#cancelAssign', function() {
            $.ajax({
                type: "POST",
                url: "{{ route('api.cancel.candidate') }}",
                data: {
                    candidate_id: $("#candidate_id").val(),
                    user_id: "{{ auth()->user()->id }}",
                    apply_id: $("#apply_id").val(),
                },
                success: function(response) {
                    $.toast({
                        heading: 'Thông báo thành công!',
                        text: 'Hủy nhận hỗ trợ ứng viên thành công!',
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'success'
                    });
                    $("#modal-form").toggle();
                    $("#modal-form-overlay").toggle();
                    $(".button").attr("id", "button");
                    $(".btn.btn-primary.button").text("");
                    $("#candidate_id").remove();
                    $("#apply_id").remove();
                    // $("#cancel_assign").remove();
                    table.ajax.reload();
                },
                error: function(response) {
                    if (response.responseJSON.mess) {
                        $.toast({
                            heading: 'Thông báo lỗi!',
                            text: response.responseJSON.mess,
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: 'error'
                        });
                    };
                }
            });
        });
    </script>
@endpush
