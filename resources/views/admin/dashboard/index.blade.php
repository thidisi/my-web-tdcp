@extends('admin.layouts.layoutAdmin')
@section('title', 'Dashboard')
@push('css')
    <style>
        .card {
            background: #fff !important;
            box-shadow: 0px 3px 8px -4px rgb(0 0 0 / 15%);
        }

        .card_title {
            background: none !important;
            text-transform: uppercase;
            border: none !important;
            font-size: 13px;
            font-weight: 500;
            border-radius: 3px 3px 0 0;
            margin: 0;
            padding: 16px 24px 16px 16px !important;
        }

        .card_body {
            padding-bottom: 16px !important;
            padding: 0 15px;
        }



        .custom_content {
            margin: 25px;
        }

        .description {
            margin: 0px;
            letter-spacing: 1px;
            text-transform: uppercase;
            font-weight: bold;
            text-align: center;
            font-size: 13px;
            color: #666666;
            font-family: "SF Pro Text", "SF Pro Icons", "Helvetica Neue", "Helvetica", "Arial", sans-serif !important;
            margin-top: 0 !important;
        }

        #sum_box h4 {
            font-family: "SF Pro Text", "SF Pro Icons", "Helvetica Neue", "Helvetica", "Arial", sans-serif !important;
            color: #000000;
            margin-bottom: 0 !important;
        }

        .currency {
            font-size: 13px;
        }

        .show_more {

            display: flex;
            justify-content: end;
            margin-top: -24px;
        }

        a.show_more_link {
            text-transform: none !important;
            color: darkorange;
        }
    </style>
@endpush
@section('content')
    <!-- ============================================================== -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <!-- Start Dashboard for Admin -->
    <div class="content datatable_custom_content">
        <div class="row">
            <div id="sum_box" class="mbl">
                <div class="col-sm-6 col-md-3 block-space">
                    <div class="panel profit db mbm gray-back">
                        <div class="panel-body">
                            <p class="description">Bonus đầu vào</p>
                            <h4 class="value">
                                <span>{{ $bonus_input }}
                                </span> <span class="currency">M</span>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 block-space">
                    <div class="panel income db mbm">
                        <div class="panel-body">
                            <p class="description">Bonus hàng tháng</p>
                            <h4 class="value"><span>{{ $bonus_monthly }}</span> <span class="currency">M</span>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 block-space gray-back">
                    <div class="panel task db mbm">
                        <div class="panel-body">
                            <p class="description">Tổng lợi nhuận</p>
                            <h4 class="value"><span>{{ $profit }}</span> <span class="currency">M</span></h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 block-space">
                    <div class="panel visit db mbm">
                        <div class="panel-body">
                            <p class="description">KPI</p>
                            <h4 class="value"><span>{{ $kpi }}</span></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card_title">
                    <div class="text-center">
                        <p class="description">Lợi nhuận năm {{ $currentYear }}</p>
                        <div>(Đơn vị: M)</div>
                    </div>

                </div>
                <div class="card_body">
                    <div class="unit">M</div>
                    <div id="profit_chart"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="custom_content">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card_title row">
                        <div class="text-center">
                            <p class="description">Bonus đầu vào năm {{ $currentYear }}</p>
                            <div>(Đơn vị: M)</div>
                        </div>
                    </div>
                    <div class="card_body">
                        <div class="unit">M</div>
                        <div id="bonus_input_chart"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="card">
                    <div class="card_title">
                        <div class="text-center">
                            <p class="description">
                                Bonus hàng tháng năm {{ $currentYear }}</p>
                            <div>(Đơn vị: M)</div>
                        </div>
                    </div>
                    <div class="card_body">
                        <div class="unit">M</div>
                        <div id="bonus_monthly_chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (auth()->user()->role_id == null)
        <div class="custom_content">
            <div class="row" style="display: flex;">
                <div class="col-md-8">
                    <div class="card" style="height: 100%">
                        <div class="card_title">
                            <div class="text-center">
                                <p class="description">
                                    KPI Nhân sự tháng {{ $currentMonth }}</p>
                            </div>
                            <div class="show_more">
                                <span>
                                    <a href="{{ route('list-kpi.index') }}" class="show_more_link">Xem thêm</a>
                            </div>
                        </div>
                        <div class="card_body">
                            <table class="table table-hover yajra-datatable" id="datatable_kpi_employee">
                                <thead>
                                    <tr>
                                        <th class="dt-left">STT</th>
                                        <th class="dt-left">Tên nhân sự</th>
                                        <th class="dt-right">Ứng viên</th>
                                        <th class="dt-right">KPI</th>
                                        <th class="dt-right">Tổng bonus TT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($kpi_employee as $employee)
                                        <tr>
                                            <td class="dt-left">{{ $employee->index }}</td>
                                            <td class="dt-left">{{ $employee->name }}</td>
                                            <td class="dt-right">{{ $employee->bonus['candidate'] }}</td>
                                            @if ($employee->bonus['kpi'] > 100)
                                                <td class="dt-right" style="color: green">{{ $employee->bonus['kpi'] }}%
                                                </td>
                                            @elseif ($employee->bonus['kpi'] == 100)
                                                <td class="dt-right" style="color: #ffb701">{{ $employee->bonus['kpi'] }}%
                                                </td>
                                            @else
                                                <td class="dt-right" style="color: red"> {{ $employee->bonus['kpi'] }}%
                                                </td>
                                            @endif

                                            <td class="dt-right">{{ $employee->bonus_tt }} đ</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="height: 100%">
                        <div class="card_title">
                            <div class="text-center">
                                <p class="description">
                                    KPI Team</p>
                            </div>
                        </div>
                        <div class="card_body">
                            <table class="table table-hover yajra-datatable" id="datatable_kpi_team">
                                <thead>
                                    <tr>
                                        <th class="dt-left">Team</th>
                                        <th class="dt-right">KPI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($kpi_team as $team)
                                        <tr>
                                            <td class="dt-left">{{ $team->name }}</td>
                                            <td class="dt-right">{{ $team->kpi }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="custom_content">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card_title">
                        <div class="text-center">
                            <p class="description">
                                Chỉ số cv từ source trong năm {{ $currentYear }}</p>
                        </div>
                    </div>
                    <div class="card_body">
                        <div id="grouped-bar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Dashboard for Admin -->

@endsection
@push('js')
    <script src="/assets/libs/morrischart/morris.min.js"></script>
    <script src="/assets/libs/raphael/raphael-min.js"></script>
    <script src="/assets/libs/apexcharts/apexcharts.min.js"></script>
    <script>
        $(document).ready(function() {
            $.ajax({
                type: "GET",
                url: "{{ route('api.get.data.chart') }}",
                @if (auth()->user()->role_id != null)
                    data: {
                        user_id: "{{ auth()->user()->id }}",
                    },
                @endif
                success: function(response) {
                    Morris.Bar({
                        element: 'bonus_monthly_chart',
                        data: response.bonus_monthly,
                        xkey: 'month',
                        ykeys: ['value'],
                        labels: ['Bonus'],
                    });
                    Morris.Bar({
                        element: 'bonus_input_chart',
                        data: response.bonus_input,
                        xkey: 'month',
                        ykeys: ['value'],
                        labels: ['Bonus']
                    });
                    Morris.Bar({
                        element: 'profit_chart',
                        data: response.profit,
                        xkey: 'month',
                        ykeys: ['value'],
                        labels: ['Lợi nhuận']
                    });
                }
            });
            $.ajax({
                type: "GET",
                url: "{{ route('api.source.chart') }}",
                @if (auth()->user()->role_id != null)
                    data: {
                        role_id: "{{ auth()->user()->role_id }}",
                    },
                @endif
                success: function(response) {
                    var colors = ["#ffb701", "#40c989"];
                    (dataColors = $("#grouped-bar").data("colors")) &&
                    (colors = dataColors.split(","));
                    var options = {
                        chart: {
                            height: 380,
                            type: "bar",
                            toolbar: {
                                show: !1,
                            },
                        },
                        plotOptions: {
                            bar: {
                                horizontal: !0,
                                dataLabels: {
                                    position: "top",
                                },
                            },
                        },
                        dataLabels: {
                            enabled: !0,
                            offsetX: -6,
                            style: {
                                fontSize: "12px",
                                colors: ["#fff"],
                            },
                        },
                        colors: colors,
                        stroke: {
                            show: !0,
                            width: 1,
                            colors: ["#fff"],
                        },
                        series: [{
                                name: "CV",
                                data: response.totalCV,
                            },
                            {
                                name: "CV Pass",
                                data: response.totalCVPass,
                            },
                        ],
                        xaxis: {
                            categories: response.sources,
                        },
                        legend: {
                            offsetY: 5,
                        },
                        states: {
                            hover: {
                                filter: "none",
                            },
                        },
                        grid: {
                            borderColor: "#f1f3fa",
                            padding: {
                                bottom: 5,
                            },
                        },
                    };
                    var chart = new ApexCharts(document.querySelector("#grouped-bar"), options);
                    chart.render();
                }
            });
        });
    </script>
@endpush
