<table>
    <thead>
        <tr>
            <th>STT</th>
            <th>Ngày gửi cv</th>
            <th>Họ và tên</th>
            <th>Vị trí</th>
            <th>Địa chỉ làm việc</th>
            <th>PIC</th>
            <th>SĐT</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $data)
            <tr>
                <th>{{ $loop->index + 1 }}</th>
                <td>{{ $data->format_created_at }}</td>
                <td>{{ $data->format_name }}</td>
                <td>{{ $data->format_skill }}</td>
                <td>{{ $data->format_address }}</td>
                <td>{{ $data->format_user }}</td>
                <td>{{ $data->format_phone }}</td>
                <td>{{ $data->format_email }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
