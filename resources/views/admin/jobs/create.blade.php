@extends('admin.layouts.layoutAdmin')
@section('title', 'Tạo công việc')
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-header transparent">
                        <h2>Tạo Jobs</h2>
                        <div class="additional-btn">
                            <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                            <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                            <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                        </div>
                    </div>
                    <div class="widget-content padding">
                        <form action="{{ route('jobs.store') }}" role="form" id="contactForm" method="POST">
                            @csrf
                            <div class="form-group">
                                <div class="font-12">
                                    <div class="row">
                                        <div class="col-md-12 col-12 from-create-job form-group">
                                            <div class="col-md-6"><span>
                                                    <label for="" class="font-12">Tên công việc:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <input required name="title" value="{{ old('title') }}"
                                                        type="text" title="" class="form-control b-r-5">
                                                    @if ($errors->has('title'))
                                                        <div class="errors">{{ $errors->first('title') }}</div>
                                                    @endif
                                            </div>
                                            <div class="col-md-6">
                                                <label class="font-12">Số lượng:(<span style="color: red;">*</span>)</label>
                                                <input required name="slot" value="{{ old('slot') }}" type="number"
                                                    min="1" class="form-control b-r-5">
                                                @if ($errors->has('slot'))
                                                    <div class="errors">{{ $errors->first('slot') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-12 form-group">
                                            <div class="row">
                                                <div class="col-md-6 pull-left m-w-50 pl-27">
                                                    <label class="font-12">Thời gian làm việc bắt đầu:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <input required name="start_date" value="{{ old('start_date') }}"
                                                        type="time" title="" class="form-control b-r-5">
                                                    @if ($errors->has('start_date'))
                                                        <div class="errors">{{ $errors->first('start_date') }}</div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 pull-left m-w-50 pr-27">
                                                    <label class="font-12">Thời gian làm việc kết thúc:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <input required name="end_date" value="{{ old('end_date') }}"
                                                        type="time" title="" class="form-control b-r-5">
                                                    @if ($errors->has('end_date'))
                                                        <div class="errors">{{ $errors->first('end_date') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <div class="row">
                                                <div class="col-md-6 pl-27">
                                                    <label class="font-12">Cấp bậc:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="level"
                                                        class="form-control pd-12 custom-select b-r-5">
                                                        <option value="" hidden disabled selected> -- Chọn chức vụ--
                                                        </option>
                                                        @foreach ($list_jobs['level'] as $each)
                                                            <option @if (old('level') == $each->id) selected @endif
                                                                value="{{ $each->id }}">{{ $each->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('level'))
                                                        <div class="errors">{{ $errors->first('level') }}</div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 pr-27">
                                                    <label class="font-12">Kinh nghiệm:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="exp"
                                                        class="form-control pd-12 custom-select b-r-5" id="experience">
                                                        <option value="" hidden disabled selected> -- Chọn kinh
                                                            nghiệm-- </option>
                                                        @foreach ($list_jobs['exp'] as $key => $each)
                                                            <option @if (old('exp') == $key) selected @endif
                                                                value="{{ $key }}">{{ $each }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('exp'))
                                                        <div class="errors">{{ $errors->first('exp') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 form-group ">
                                            <div class="row">
                                                <div class="col-md-6 pl-27">
                                                    <label class="font-12">Hình thức làm việc:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="work_time"
                                                        class="form-control pd-12 custom-select b-r-5">
                                                        <option value="" hidden disabled selected> -- Chọn hình thức
                                                            làm việc-- </option>
                                                        @foreach ($list_jobs['work_time'] as $key => $each)
                                                            <option @if (old('work_time') == $key) selected @endif
                                                                value="{{ $key }}">{{ $each }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('work_time'))
                                                        <div class="errors">{{ $errors->first('work_time') }}</div>
                                                    @endif
                                                </div>

                                                <div class="col-md-6 pr-27">
                                                    <label class="font-12">Trình độ học vấn:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="education"
                                                        class="form-control browser-default browser-default custom-select b-r-5">
                                                        <option value="" hidden disabled selected> -- Chọn trình độ
                                                            học vấn-- </option>
                                                        @foreach ($list_jobs['educate'] as $key => $each)
                                                            <option @if (old('education') == $key) selected @endif
                                                                value="{{ $key }}">{{ $each }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('education'))
                                                        <div class="errors">{{ $errors->first('skills') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <div class="row">
                                                <div class="col-md-6 pl-27">
                                                    <label class="font-12">Hạn nộp hồ sơ:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <input required name="deadline" value="{{ old('deadline') }}"
                                                        type="date" title="" class="form-control b-r-5">
                                                    @if ($errors->has('deadline'))
                                                        <div class="errors">{{ $errors->first('deadline') }}</div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 pr-27">
                                                    <label class="font-12">Danh mục kỹ năng:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="skills[]"
                                                        class="b-r-5 form-control select2 select2-skills select2-multiple"
                                                        data-toggle="select2" multiple="multiple"
                                                        data-placeholder="   -- Chọn kỹ năng-- ">
                                                        @foreach ($list_jobs['skill'] as $item)
                                                            <option value="{{ $item->id }}">{{ $item->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('skills'))
                                                        <div class="errors">{{ $errors->first('skills') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12  form-group">
                                            <div class="row">
                                                <div class="col-md-6 pl-27">
                                                    <label class="font-12">Mức lương:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="salary"
                                                        class="form-control pd-12 custom-select b-r-5" id="wage">
                                                        <option value="" hidden disabled selected> -- Chọn mức
                                                            lương--
                                                        </option>
                                                        @foreach ($list_jobs['salary'] as $each)
                                                            <option @if (old('salary') == $each->id) selected @endif
                                                                value="{{ $each->id }}">{{ $each->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('salary'))
                                                        <div class="errors">{{ $errors->first('salary') }}</div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 pr-27">
                                                    <label class="font-12">Tỉnh/thành phố làm việc:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="province_id"
                                                        class="form-control pd-12 custom-select b-r-5" id="wage">
                                                        <option value="" hidden disabled selected> -- Chọn địa chỉ --
                                                        </option>
                                                        @foreach ($list_jobs['province'] as $each)
                                                            <option @if (old('province_id') == $each->id) selected @endif
                                                                value="{{ $each->id }}">{{ $each->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('province_id'))
                                                        <div class="errors">{{ $errors->first('province_id') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 pl-27 form-group">
                                            <label class="font-12">Assigne:(<span style="color: red;">*</span>)</label>
                                            <select id="assignee" name="assignee" required
                                                class="form-control pd-12 custom-select b-r-5">
                                                <option value="0" selected>Tất cả</option>
                                                @foreach ($list_jobs['assignee'] as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-6 pr-27 form-group">
                                            <label class="font-12">Địa chỉ làm việc:(<span
                                                    style="color: red;">*</span>)</label>
                                            <input required name="address" value="{{ old('address') }}" type="text"
                                                title="" class="form-control bd-c b-r-5"
                                                placeholder="Số nhà/ văn phòng toà nhà - đường, phường/xã, quận/huyện hoặc Remote">
                                            @if ($errors->has('address'))
                                                <div class="errors">{{ $errors->first('address') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-12 form-group">
                                                <label class="font-12">Chọn danh mục:(<span
                                                        style="color: red;">*</span>)</label>
                                                <select id="category_job" name="category_job" required
                                                    class="form-control pd-12 custom-select b-r-5">
                                                    <option value="0" hidden disabled selected>-- Chọn danh mục --
                                                    </option>
                                                    @foreach ($list_jobs['category_job'] as $key => $item)
                                                        <option value="{{ $key }}">{{ $item }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('category_job'))
                                                    <div class="errors">{{ $errors->first('category_job') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-12 form-group">
                                                <label class="font-12">Mô tả công việc:(<span
                                                        style="color: red;">*</span>)</label>
                                                <textarea required class="widthmax form-control b-r-5" name="description" id="" rows="9"
                                                    placeholder="Hãy mô tả chi tiết những đầu mục công việc để ứng viên có thể hiểu rõ hơn về yêu cầu của công ty bạn với vị trí này"></textarea>
                                                @if ($errors->has('description'))
                                                    <div class="errors">{{ $errors->first('description') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-12 form-group">
                                                <label class="font-12">Quyền lợi được hưởng:(<span
                                                        style="color: red;">*</span>)</label>
                                                <textarea required class="widthmax form-control b-r-5" name="benefit" id="" rows="9" placeholder=""></textarea>
                                                @if ($errors->has('benefit'))
                                                    <div class="errors">{{ $errors->first('benefit') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-12 form-group">
                                                <label class="font-12">Yêu cầu công việc:(<span
                                                        style="color: red;">*</span>)</label>
                                                <textarea required class="widthmax form-control b-r-5" name="require" id="" rows="9"
                                                    placeholder="Là những yêu cầu cần thiết với vị trí tuyển dụng (bằng cấp, trình độ, kỹ năng, kinh nghiệm v.v...)"></textarea>
                                                @if ($errors->has('require'))
                                                    <div class="errors">{{ $errors->first('require') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="d-f-end">
                                    <button type="submit" class="btn btn-primary">Thêm mới</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('js')
    <script src="/assets/libs/select2/select2.min.js"></script>
    <script>
        $(".select2-skills").select2();
        $(".select2-skills").on("select2:select", function(evt) {
            var element = evt.params.data.element;
            var $element = $(element);
            $element.detach();
            $(this).append($element);
            $(this).trigger("change");
        });
    </script>
@endpush
