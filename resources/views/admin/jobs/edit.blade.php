@extends('admin.layouts.layoutAdmin')
@section('title', 'Sửa công việc')
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-header transparent">
                        <h2>Sửa Jobs</h2>
                        <div class="additional-btn">
                            <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                            <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                            <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                        </div>
                    </div>
                    <div class="widget-content padding">
                        <form action="{{ route('jobs.update', $showJob->id) }}" role="form" id="contactForm"
                            method="POST">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <div class="font-12">
                                    <div class="row">
                                        <div class="col-md-12 col-12 from-create-job form-group">
                                            <div class="col-md-6"><span>
                                                    <label for="" class="font-12">Tên công việc:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <input required name="title"
                                                        value="{{ $showJob->title ? $showJob->title : old('title') }}"
                                                        type="text" title="" class="form-control b-r-5">
                                                    @if ($errors->has('title'))
                                                        <div class="errors">{{ $errors->first('title') }}</div>
                                                    @endif

                                            </div>
                                            <div class="col-md-6">
                                                <label class="font-12">Số lượng:(<span style="color: red;">*</span>)</label>
                                                <input required name="slot"
                                                    value="{{ $showJob->job_details->slot ? $showJob->job_details->slot : old('slot') }}"
                                                    type="number" min="1" class="form-control b-r-5">
                                                @if ($errors->has('slot'))
                                                    <div class="errors">{{ $errors->first('slot') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-12 form-group">
                                            <div class="row">
                                                <div class="col-md-6 pull-left m-w-50 pl-27">
                                                    <label class="font-12">Thời gian làm việc bắt đầu:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <input required name="start_date"
                                                        value="{{ $showJob->start_date ? $showJob->start_date : old('start_date') }}"
                                                        type="time" title="" class="form-control b-r-5">
                                                    @if ($errors->has('start_date'))
                                                        <div class="errors">{{ $errors->first('start_date') }}</div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 pull-left m-w-50 pr-27">
                                                    <label class="font-12">Thời gian làm việc kết thúc:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <input required name="end_date"
                                                        value="{{ $showJob->end_date ? $showJob->end_date : old('end_date') }}"
                                                        type="time" title="" class="form-control b-r-5">
                                                    @if ($errors->has('end_date'))
                                                        <div class="errors">{{ $errors->first('end_date') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <div class="row">
                                                <div class="col-md-6 pl-27">
                                                    <label class="font-12">Cấp bậc:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="level"
                                                        class="form-control pd-12 custom-select b-r-5">
                                                        <option value="" hidden disabled selected> -- Chọn chức vụ--
                                                        </option>
                                                        @foreach ($list_jobs['level'] as $each)
                                                            <option @if (($showJob->job_details->level ? $showJob->job_details->level : old('level')) == $each->id) selected @endif
                                                                value="{{ $each->id }}">{{ $each->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('level'))
                                                        <div class="errors">{{ $errors->first('level') }}</div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 pr-27">
                                                    <label class="font-12">Kinh nghiệm:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="exp"
                                                        class="form-control pd-12 custom-select b-r-5" id="experience">
                                                        <option value="" hidden disabled selected> -- Chọn kinh
                                                            nghiệm-- </option>
                                                        @foreach ($list_jobs['exp'] as $key => $each)
                                                            <option @if (($showJob->job_details->exp ? $showJob->job_details->exp : old('exp')) == $key) selected @endif
                                                                value="{{ $key }}">{{ $each }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('exp'))
                                                        <div class="errors">{{ $errors->first('exp') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 form-group ">
                                            <div class="row">
                                                <div class="col-md-6 pl-27">
                                                    <label class="font-12">Hình thức làm việc:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="work_time"
                                                        class="form-control pd-12 custom-select b-r-5">
                                                        <option value="" hidden disabled selected> -- Chọn hình thức
                                                            làm việc-- </option>
                                                        @foreach ($list_jobs['work_time'] as $key => $each)
                                                            <option @if (($showJob->job_details->job_type ? $showJob->job_details->job_type : old('work_time')) == $key) selected @endif
                                                                value="{{ $key }}">{{ $each }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('work_time'))
                                                        <div class="errors">{{ $errors->first('work_time') }}</div>
                                                    @endif
                                                </div>

                                                <div class="col-md-6 pr-27">
                                                    <label class="font-12">Trình độ học vấn:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="education"
                                                        class="form-control browser-default browser-default custom-select b-r-5">
                                                        <option value="" hidden disabled selected> -- Chọn trình độ
                                                            học vấn-- </option>
                                                        @foreach ($list_jobs['educate'] as $key => $each)
                                                            <option @if (($showJob->job_details->education ? $showJob->job_details->education : old('educate')) == $key) selected @endif
                                                                value="{{ $key }}">{{ $each }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('education'))
                                                        <div class="errors">{{ $errors->first('education') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <div class="row">
                                                <div class="col-md-6 pl-27">
                                                    <label class="font-12">Hạn nộp hồ sơ:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <input required name="deadline"
                                                        value="{{ $showJob->job_details->deadline ? $showJob->job_details->deadline : old('deadline') }}"
                                                        type="date" title="" class="form-control b-r-5">
                                                    @if ($errors->has('deadline'))
                                                        <div class="errors">{{ $errors->first('deadline') }}</div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 pr-27">
                                                    <label class="font-12">Danh mục kỹ năng:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="skills[]"
                                                        class="b-r-5 form-control select2 select2-skills select2-multiple"
                                                        data-toggle="select2" multiple="multiple">
                                                        @foreach ($list_jobs['skill'] as $item)
                                                            <option
                                                                @foreach ($showJob->skills as $each)
                                                                    @if ($each->id == $item->id) selected @endif @endforeach
                                                                value="{{ $item->id }}">{{ $item->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('skills'))
                                                        <div class="errors">{{ $errors->first('skills') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12  form-group ">
                                            <div class="row">
                                                <div class="col-md-6 pl-27">
                                                    <label class="font-12">Mức lương:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="salary"
                                                        class="form-control pd-12 custom-select b-r-5" id="wage">
                                                        <option value="" hidden disabled selected> -- Chọn mức
                                                            lương--
                                                        </option>
                                                        @foreach ($list_jobs['salary'] as $each)
                                                            <option @if (($showJob->job_details->salary ? $showJob->job_details->salary : old('salary')) == $each->id) selected @endif
                                                                value="{{ $each->id }}">{{ $each->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('salary'))
                                                        <div class="errors">{{ $errors->first('salary') }}</div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 pr-27">
                                                    <label class="font-12">Địa chỉ:(<span
                                                            style="color: red;">*</span>)</label>
                                                    <select required name="province_id"
                                                        class="form-control pd-12 custom-select b-r-5" id="wage">
                                                        <option value="" hidden disabled selected> -- Chọn địa chỉ --
                                                        </option>
                                                        @foreach ($list_jobs['province'] as $each)
                                                            <option @if (($showJob->job_details->province_id ? $showJob->job_details->province_id : old('province_id')) == $each->id) selected @endif
                                                                value="{{ $each->id }}">{{ $each->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('province_id'))
                                                        <div class="errors">{{ $errors->first('province_id') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 pl-27 form-group">
                                            <label class="font-12">Assigne:(<span style="color: red;">*</span>)</label>
                                            <select id="assignee" name="assignee"
                                                class="form-control pd-12 custom-select b-r-5">
                                                @foreach ($list_jobs['assignees'] as $each)
                                                    <option @if (($showJob->assignee ? $showJob->assignee : old('assignee')) == $each->id) selected @endif
                                                        value="{{ $each->id }}">{{ $each->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 pr-27 form-group">
                                            <label class="font-12">Địa điểm làm
                                                việc:(<span style="color: red;">*</span>)</label>
                                            <input required name="address"
                                                value="{{ $showJob->job_details->address ? $showJob->job_details->address : old('address') }}"
                                                type="text" title="" class="form-control bd-c b-r-5">
                                            @if ($errors->has('address'))
                                                <div class="errors">{{ $errors->first('address') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-12 form-group">
                                                <label class="font-12">Chọn danh mục:(<span
                                                        style="color: red;">*</span>)</label>
                                                <select id="category_job" name="category_job" required
                                                    class="form-control pd-12 custom-select b-r-5">
                                                    <option value="0" hidden disabled selected>-- Chọn danh mục --
                                                    </option>
                                                    @foreach ($list_jobs['category_job'] as $key => $item)
                                                        <option value="{{ $key }}"
                                                            @if (($showJob->category_job ? $showJob->category_job : old('category_job')) == $key) selected @endif>
                                                            {{ $item }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('category_job'))
                                                    <div class="errors">{{ $errors->first('category_job') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-12 form-group">
                                                <label class="font-12">Mô tả công việc:(<span
                                                        style="color: red;">*</span>)</label>
                                                <textarea required class="widthmax form-control b-r-5" name="description" id="" rows="9"
                                                    placeholder="Hãy mô tả chi tiết những đầu mục công việc để ứng viên có thể hiểu rõ hơn về yêu cầu của công ty bạn với vị trí này">{{ $showJob->job_details->description ? $showJob->job_details->description : old('description') }}
                                                </textarea>
                                                @if ($errors->has('description'))
                                                    <div class="errors">{{ $errors->first('description') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-12 form-group">
                                                <label class="font-12">Quyền lợi được hưởng:(<span
                                                        style="color: red;">*</span>)</label>
                                                <textarea required class="widthmax form-control b-r-5" name="benefit" id="" rows="9" placeholder="">{{ $showJob->job_details->benefit ? $showJob->job_details->benefit : old('benefit') }}
                                                </textarea>
                                                @if ($errors->has('benefit'))
                                                    <div class="errors">{{ $errors->first('benefit') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-12 form-group">
                                                <label class="font-12">Yêu cầu công việc:(<span
                                                        style="color: red;">*</span>)</label>
                                                <textarea required class="widthmax form-control b-r-5" name="require" id="" rows="9"
                                                    placeholder="Là những yêu cầu cần thiết với vị trí tuyển dụng (bằng cấp, trình độ, kỹ năng, kinh nghiệm v.v...)">{{ $showJob->job_details->require ? $showJob->job_details->require : old('require') }}</textarea>
                                                @if ($errors->has('require'))
                                                    <div class="errors">{{ $errors->first('require') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="d-f-end">
                                    <button type="submit" class="btn btn-primary">Sửa</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('js')
    <script src="/assets/libs/select2/select2.min.js"></script>
    <script>
        $(".select2-skills").select2({
            placeholder: " -- Chọn kỹ năng-- ",
        });
        $(".select2-skills").on("select2:select", function(evt) {
            var element = evt.params.data.element;
            var $element = $(element);
            $element.detach();
            $(this).append($element);
            $(this).trigger("change");
        });
    </script>
@endpush
