@extends('admin.layouts.layoutAdmin')
@section('title', 'Danh sách công việc')
@push('css')
    <link href="{{ asset('/assets/css/datatable-1.10.21.min.css') }}" rel="stylesheet">
    <style>
        #DataTables_Table_0_filter input {
            padding: 6px 8px !important;
        }

        .clear_input {
            position: relative;
        }

        .icon_close {
            cursor: pointer;
            display: block;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            right: 7%;
        }

        @media (max-width: 1549px) {
            .icon_close {
                right: 9%;
            }
        }

        @media (max-width: 1249px) {
            .icon_close {
                right: 10%;
            }
        }

        @media (max-width: 1099px) {
            .icon_close {
                right: 11%;
            }
        }

        .apply-cv-btn {
            background-color: #fff;
            color: #444;
            border-color: #e5e5e5;
            border-radius: 5px;
            font-size: 11px;
            font-weight: bold;
            font-family: 'Montserrat', sans-serif;
        }
    </style>
@endpush
@section('content')
    <!-- ============================================================= -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <div class="content datatable_custom_content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget bg-none" style="padding-bottom: 20px">
                    <div class="widget-content">
                        <div class="data-table-toolbar">
                            <div class="row">
                                <div class="col-md-3 append-search clear_input">
                                </div>
                                @if (\Auth::user()->role_id == null || \Auth::user()->role_id == 1)
                                    <div class="col-md-3 form-group clear_input">
                                        <select name="" class="form-control border-radius-select" id="user">
                                            <option data-display="Chọn assignee" selected hidden disabled value="">
                                                Chọn
                                                assignee
                                            </option>
                                            @foreach ($users as $each)
                                                <option value="{{ $each->id }}">{{ $each->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                <div class="col-md-3 form-group clear_input">
                                    <select name="" class="form-control border-radius-select" id="skill">
                                        <option data-display="Chọn kĩ năng" selected hidden disabled value="">Chọn kĩ
                                            năng</option>
                                        @foreach ($skills as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 form-group clear_input">
                                    <select name="" class="form-control border-radius-select" id="status">
                                        <option data-display="Trạng thái Job" value="" selected hidden disabled>Trạng
                                            thái Job
                                        </option>
                                        <option value="active">Đã duyệt</option>
                                        <option value="inactive">Chưa duyệt</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <table class="table table-hover yajra-datatable">
                                <thead>
                                    <div class="_title-datatable row">
                                        <div class="text-left col-lg-6 lh-40">
                                            DANH SÁCH TIN TUYỂN DỤNG
                                        </div>
                                        @if (checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['CRUD-JOB']))
                                            <div class="text-right col-lg-6 fz-13" style="text-transform: none">
                                                <a href="{{ route('jobs.create') }}"
                                                    class="btn btn-default btn-lg background-hachinet form-control p-2 border-radius-select"
                                                    style="width: 200px;">
                                                    <i class="fa fa-plus-circle"></i>
                                                    <span class="white-space: break-spaces;">Tạo việc làm mới</span>
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                    <tr>
                                        <th><i data-v-38af99a7="" class="fa fa-id-badge" style="margin-right: 3px;"></i>
                                            STT</th>
                                        <th><i data-v-38af99a7="" class="fa fa-user-circle-o"
                                                style="margin-right: 3px;"></i></i> Vị trí tuyển dụng</th>
                                        <th><i data-v-38af99a7="" class="fa fa-line-chart" style="margin-right: 3px;"></i>
                                            Thống kê</th>
                                        <th><i data-v-38af99a7="" class="fa fa-clock-o" style="margin-right: 3px;"></i>Hạn
                                            nộp hồ sơ</th>
                                        <th><i data-v-38af99a7="" class="fa fa-paper-plane-o"
                                                style="margin-right: 3px;"></i>Link JD</th>
                                        @if (checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['CRUD-JOB']))
                                            <th><i data-v-38af99a7="" class="fa fa-align-justify"
                                                    style="margin-right: 3px;"></i>Hoạt động</th>
                                        @endif

                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                        {{-- MODAL deeplink --}}
                        <div class="modal-dialog modal-lg modal-cv-p">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close apply-cv-btn-close" data-dismiss="modal"
                                        aria-hidden="true">×</button>
                                    <h4 class="modal-title text-center" id="myLargeModalLabel">Danh sách link JD</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="widget bg-none overflow-auto">
                                        <div class="widget-content">
                                            <table class="table table-modal table-hover" id="table-listJd">
                                                <thead>
                                                    <tr>
                                                        <th><i data-v-38af99a7="" class="fa fa-id-badge"
                                                                style="margin-right: 3px;"></i>
                                                            Source</th>
                                                        <th><i data-v-38af99a7="" class="fa fa-book"
                                                                style="margin-right: 3px;"></i>Link JD</i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        {{-- xoa and status --}}
                        <div id="modal-form-overlay" class="position-fixed"
                            style="z-index: 999; background-color: rgba(83, 83, 83, 0.281); display: none; top: 0; right: 0; left: 0; bottom: 0;">
                        </div>
                        <div class="modal-dialog modal-sm d-none-modal" id="modal-form">
                            <div class="modal-content">
                                <div class="modal-header" id="formValidate">
                                    <h4 class="modal-title" id="mySmallModalLabel">Xác nhận</h4>
                                    <p id="content-modal"></p>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex" style="justify-content: flex-end;">
                                        <button class="btn btn-danger md-close button-close">Hủy</button>
                                        <button class="btn btn-success md-close button" style="margin-left: 4px;"
                                            id="button">Xóa</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End content here -->
        <!-- ============================================================== -->
    </div>
@endsection
@push('js')
    <script src="{{ asset('/assets/libs/datatables/datatables-1.10.21.min.js') }}"></script>
    <script type="text/javascript">
        function clear_input(name) {
            var data = $('#' + name);
            data.val('');
            data.removeClass('h-chevron-down');
            data.parents('.clear_input').find('.icon_close').remove();
            table.draw();
        }
        var table = $('.yajra-datatable').DataTable({
            paging: true,
            // info: false,
            autoWidth: false,
            responsive: true,
            processing: true,
            serverSide: true,
            "ordering": false,
            pageLength: 10,
            ajax: {
                url: "{!! route('api.jobs.list') !!}",
                data: function(d) {
                    d.status = $('#status').val(),
                        d.user_role = {{ \Auth::user()->role_id ? \Auth::user()->role_id : 0 }},
                        d.skill = $('#skill').val(),
                        d.name = $('#DataTables_Table_0_filter input').val(),
                        d.user = $('#user').val()
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'rank',
                    className: 'dt-left'
                },
                {
                    data: 'name',
                    name: 'name',
                    className: 'dt-left'
                },
                {
                    data: 'statistical',
                    name: 'statistical',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'deadline',
                    name: 'deadline',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'linkJd',
                    name: 'linkJd',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                    visible: {{ checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['CRUD-JOB']) ? 'true' : 'false' }}
                },
            ],
            columnDefs: [{
                "defaultContent": "-",
                "targets": "_all"
            }],
            language: {
                "sProcessing": "<span class='fa-stack fa-lg text-yellow'>\n\
                                <i class='fa fa-spinner fa-spin fa-stack-2x fa-fw'></i>\n\
                           </span>&emsp;",
                "sLengthMenu": "<?php echo __('Hiển thị _MENU_ bản ghi'); ?>",
                "sZeroRecords": "<?php echo __('Không có dữ liệu trong bảng'); ?>",
                "sInfo": "",
                "sInfoEmpty": '{{ \Auth::user()->role_id ? '' : 'Hiển thị 0 đến 0 trong số 0 bản ghi' }}',
                "sInfoFiltered": '{{ \Auth::user()->role_id ? '' : '(Đã lọc từ _MAX_ bản ghi)' }}',
                "sSearch": "",
                "oPaginate": {
                    "sFirst": "<?php echo __('Đầu trang'); ?>",
                    "sLast": "<?php echo __('Cuối trang'); ?>",
                    "sNext": "<?php echo __('»'); ?>",
                    "sPrevious": "<?php echo __('«'); ?>"
                },
                searchPlaceholder: "Tên việc làm"
            }
        });

        $('#status').change(function() {
            table.draw();
            if ($(this).val()) {
                $(this).addClass('h-chevron-down');
                $(this).parents('.clear_input').append(
                    `<i onclick="clear_input('status')" class="fa fa-times icon_close"></i>`
                );
            }
        });
        $('#skill').change(function() {
            table.draw();
            if ($(this).val()) {
                $(this).addClass('h-chevron-down');
                $(this).parents('.clear_input').append(
                    `<i onclick="clear_input('skill')" class="fa fa-times icon_close"></i>`
                );
            }
        });
        $('#user').change(function() {
            table.draw();
            if ($(this).val()) {
                $(this).addClass('h-chevron-down');
                $(this).parents('.clear_input').append(
                    `<i onclick="clear_input('user')" class="fa fa-times icon_close"></i>`
                );
            }
        });
        $("#DataTables_Table_0_filter").prependTo(".append-search");
        $('#DataTables_Table_0_length').remove();

        $(document).on("click", ".button-delete", function() {
            $("#modal-form").toggle();
            $("#modal-form-overlay").toggle();
            $("#content-modal").text('Bạn có chắc chắn muốn xóa job này ?')
            $(".btn.btn-success.md-close.button").text("Xóa");
            $("#button").attr("id", "delete");
            $("#formValidate").append('<input value="' + $(this).data("route") + '" id="route_url" type="hidden">')
        })

        // show modal hidden
        $(document).on("click", ".change_status", function() {
            $("#modal-form").toggle();
            $("#modal-form-overlay").toggle();
            $("#content-modal").text('Bạn có chắc chắn muốn ẩn job này ?')
            $(".btn.btn-success.md-close.button").text("Đổi trạng thái");
            $("#button").attr("id", "storeStatus");
            $("#formValidate").append('<input value="' + $(this).data("route") + '" id="route_url" type="hidden">')
            $("#formValidate").append('<input value="' + $(this).data("id") + '" id="id_change" type="hidden">')
        });
        //show modal add and edit
        $(".button-close").click(function() {
            $("#modal-form").toggle();
            $("#modal-form-overlay").toggle();
            $(".button-close").attr("id", "button");
            $(".btn.btn-primary.button").text("");
            $("#route_url").remove()
        });
        //update status
        $(document).on('click', '#storeStatus', function() {
            $.ajax({
                type: "POST",
                url: $("#route_url").val(),
                dataType: "JSON",
                success: function(response) {
                    $.toast({
                        heading: 'Thông báo thành công!',
                        text: 'Đổi trạng thái thành công!',
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'success'
                    });
                    $("#modal-form").toggle();
                    $("#modal-form-overlay").toggle();
                    $("#route_url").remove()
                    table.draw();
                }
            });
        })

        //delete job
        $(document).on('click', '#delete', function() {
            $.ajax({
                type: "DELETE",
                url: $("#route_url").val(),
                data: "data",
                dataType: "JSON",
                success: function(response) {
                    $.toast({
                        heading: 'Thông báo thành công!',
                        text: 'Xóa job thành công!',
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'success'
                    });
                    $("#modal-form").toggle();
                    $("#modal-form-overlay").toggle();
                    $("#route_url").remove();
                    table.draw();
                },
                error: function(response) {
                    $.toast({
                        heading: 'Thông báo lỗi!',
                        text: "Không thể xóa Job này!",
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'error'
                    });
                    $("#modal-form").toggle();
                    $("#modal-form-overlay").toggle();
                    $("#route_url").remove();
                    table.draw();
                }
            });
        })

        $(document).on("click", ".apply-cv-btn", function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: $(this).data('route'),
                data: {
                    'job': $(this).data('slug')
                },
                success: function(response) {
                    $('#table-listJd').find('tbody').html('');
                    $.each(response.data, function(key, value) {
                        let link_jd =
                            `<div class="copy-text"><p>${value.short_url}</p><button class="copy"><i class="fa fa-clone"></i></button></div>`
                        $('#table-listJd').append($('<tr>')
                            .append($('<td>').append(key))
                            .append($('<td>').append(link_jd))
                        );
                    });
                }
            });
            $(".modal-cv-p").toggle();
            $("#modal-form-overlay").toggle();
        });
        $(document).on("click", ".apply-cv-btn-close", function(e) {
            e.preventDefault();
            $(".modal-cv-p").toggle();
            $("#modal-form-overlay").toggle();
            $("#id_apply_cv").remove()
        });

        $(document).on("click", "#modal-form-overlay", function(e) {
            e.preventDefault();
            $(".modal-cv-p").toggle();
            $("#modal-form-overlay").toggle();
            $("#id_apply_cv").remove()
        });

        $(document).on("click", ".copy", function(e) {
            let boxText = $(this).parents('.copy-text');
            let textCopy = boxText.find('p');
            let $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(textCopy).html()).select();
            document.execCommand("copy");
            $temp.remove();
            boxText.addClass("active");
            setTimeout(function() {
                boxText.removeClass("active");
            }, 1000);
        });
    </script>
@endpush
