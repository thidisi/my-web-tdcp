@extends('admin.layouts.layoutAdmin')
@section('title', 'Chi tiết công việc')
@push('css')
    <link rel="stylesheet" href="{{ asset('/assets/css/modal-custom.css') }}">
    <link href="/client/css/flaticon.css" rel="stylesheet">
    <style>
        .pre-line {
            white-space: pre-line
        }
    </style>
@endpush
@section('content')
    <!-- ============================================================== -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <div class="content datatable_custom_content">
        <div class="row">
            <div class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-8">
                    <h3><b>Chi tiết công việc</b></h3>
                </div>
                <div class="col-xs-6 col-md-4">
                    @if (checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['CRUD-JOB']))
                        <a class="btn btn-warning float-right mb-1" href="{{ route('jobs.edit', $job->id) }}">Sửa</a>
                    @endif
                    <a class="btn btn-warning float-right mb-1 mr-8" href="#cv">Danh sách CV</a>
                </div>
            </div>
            <div class="col-md-12">
                <div class="widget bg-none" style="padding-bottom: 20px">
                    <div class="widget-content">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-6 col-md-8">
                                <div class="job-details-content" style="padding: 4px;">
                                    <div class="upper-box shadow-custom p-5 mb-5 bg-white rounded">
                                        <div class="inner-box d-flex">
                                            <figure class="company-logo"><img width="120px" src="{{ asset($image) }}"
                                                    alt="logo">
                                            </figure>
                                            <div class="inner">
                                                <span class="b-t text-title">{{ $jobDetail->level }}</span>
                                                <h3 class="t-black text-title">{{ $job->title }}</h3>
                                                <span>
                                                    @foreach ($jobDetail->skills as $sk)
                                                        <span class="skill">{{ $sk }}</span>
                                                    @endforeach
                                                </span>
                                                <p><i class="flaticon-place"></i>{{ $jobDetail->address }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <h4 class="border_bottom">MÔ TẢ CÔNG VIỆC</h4>
                                        <p class="lh-26 pre-line">{{ $jobDetail->description }}</p>
                                        <p class="lh-26">-Thời gian làm việc từ {{ $jobDetail->start_date }} đến
                                            {{ $jobDetail->end_date }} </p>
                                        <h4 class="border_bottom">QUYỀN LỢI</h4>
                                        <p class="lh-26 pre-line">{{ $jobDetail->benefit }} </p>
                                        <h4 class="border_bottom">YÊU CẦU</h4>
                                        <p class="lh-26 pre-line">{{ $jobDetail->require }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <div class="job-sidebar">
                                    <div class="sidebar-widget job-discription">
                                        <ul class="list">
                                            <li>
                                                <span class="t-black">Hình thức làm việc</span>
                                                <p class="t-size">{{ $getType[$jobDetail->job_type] }}</p>
                                            </li>
                                            <li>
                                                <span class="t-black">Mức lương</span>
                                                <p class="t-size" style="color: red;">{{ $jobDetail->salary }}</p>
                                            </li>
                                            <li>
                                                <span class="t-black">Số lượng</span>
                                                <p class="t-size">{{ $jobDetail->slot }}</p>
                                            </li>
                                            <li>
                                                <span class="t-black">Trình độ</span>
                                                <p class="t-size">{{ $getEducate[$jobDetail->education] }}</p>
                                            </li>
                                            <li>
                                                <span class="t-black">Hạn nộp hồ sơ</span>
                                                <p class="t-size">
                                                    @if ($diffInDays < 0)
                                                        <del>{{ date('d/m/Y', strtotime($jobDetail->deadline)) }}</del>
                                                    @else
                                                        {{ date('d/m/Y', strtotime($jobDetail->deadline)) }}
                                                    @endif
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="job-sidebar">
                                    <div class="sidebar-widget job-discription">
                                        <ul class="list">
                                            <li>
                                                <span class="t-black text-center">Thống kê</span>
                                                <p class="t-size">Lượt ứng tuyển: <b
                                                        class="float-right countApplyCv">{{ $countApply }}</b></p>
                                            </li>
                                            <li>
                                                <p class="t-size">Lượt xem: <b class="float-right">{{ $countView }}</b>
                                                </p>
                                            </li>
                                        </ul>
                                        @if ($diffInDays >= 0)
                                            @if ($job->status == 'active')
                                                <div class="row">
                                                    <button class=" col-md-12 apply-cv-btn btn btn-warning mb-1">Gửi
                                                        cv</button>
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-8">
                    <h3><b>Danh sách CV</b></h3>
                </div>
            </div>
            <div class="col-md-12" id="cv">
                <div class="widget bg-none" style="padding-bottom: 20px">
                    <div class="widget-content">
                        <table class="table table-hover yajra-datatable" tyle="width: 1160px;">
                            <thead>
                                <tr>
                                    <th style="width: 43px;">
                                        <i class="fa fa-id" style="margin-right: 3px;"></i>
                                        STT
                                    </th>
                                    <th style="width: 131px;"><i class="fa fa-user"></i>
                                        Tên ứng viên</th>
                                    <th style="width: 125px;">
                                        <i class="fa fa-users" style="margin-right: 3px;"></i>
                                        HR PIC
                                    </th>
                                    <th style="width: 150px;">
                                        <i class="fa fa-circle" style="margin-right: 3px;"></i>
                                        Trạng thái
                                    </th>
                                    <th style="width: 136px;"><i class="fa fa-clock-o" style="margin-right: 3px;"></i> Thời
                                        gian nộp</th>
                                    <th style="width: 136px;"><i class="fa fa-list" style="margin-right: 3px;"></i> Hành
                                        động</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- modal show cv --}}
    @include('admin.modal-show-cv.modal')
    {{-- end show file pdf --}}
    <div id="modal-form-overlay" class="position-fixed"
        style="z-index: 999; background-color: rgba(83, 83, 83, 0.281); display: none; top: 0; right: 0; left: 0; bottom: 0;">
    </div>
    {{-- MODAL APLLY CV --}}
    <div class="modal-dialog modal-lg modal-cv-p">
        <div class="modal-content col-md-9">
            <div class="modal-header">
                <button type="button" class="close apply-cv-btn-close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="widget">
                    <div class="widget-content padding">
                        <form role="form" id="NotEmptyValidator" data-route="{{ route('apply.store') }}"
                            enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="text" class="form-control p-2" name="name" placeholder="Tên đầy đủ*">
                                <input type="hidden" value="{{ $jobDetail->job_id }}" class="form-control p-2"
                                    name="job_id" placeholder="Tên đầy đủ*">
                                <input type="hidden" value="{{ Auth::user()->id }}" class="form-control p-2"
                                    name="user_id" placeholder="user_id">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control p-2" name="email" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control p-2" name="phone" placeholder="Điện thoại*">
                            </div>
                            <div class="form-group">
                                <input type="file" id="file" accept="application/pdf" class="hidden p-2"
                                    name="file" multiple aria-label="File browser example">
                                <label class="custom-file-label col-md-12" id="apply_candidate" for="file">CV của
                                    bạn<span style="color: red;">*</span></label>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control p-2" name="customer_onsite"
                                    placeholder="Khách hàng*">
                            </div>
                            <div class="form-group">
                                <select name="skills" class="form-control">
                                    <option hidden disabled selected>---Kỹ năng chính---</option>
                                    @foreach ($skillAll as $skill)
                                        <option value="{{ $skill->id }}">{{ $skill->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="source_id" class="form-control">
                                    <option hidden disabled selected>---Source---</option>
                                    @foreach ($sources as $source)
                                        <option value="{{ $source->id }}">{{ $source->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="button"
                                class="btn btn-default btn-lg background-hachinet form-control p-2 border-radius-select apply_cv_admin"><i
                                    class="fa fa-paper-plane"></i> Gửi</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="id_role" data-id="{{ auth()->user()->role_id }}"></div>
    <!-- /.modal-content -->
    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->
@endsection
@push('js')
    <script src="{{ asset('/assets/libs/datatables/datatables-1.10.21.min.js') }}"></script>
    <script src="{{ asset('/assets/js/admin/modal-cv.js') }}"></script>
    <script type="text/javascript">
        var dataCv = $('.yajra-datatable').DataTable({
            paging: true,
            searching: false,
            lenghtMenu: [5],
            ordering: false,
            lengthChange: false,
            processing: true,
            serverSide: true,
            info: false,
            ajax: {
                url: '{{ route('api.getCv.job', $jobDetail->job_id) }}',
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'rank'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'hrpic',
                    name: 'hrpic'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'date',
                    name: 'date'
                },
                {
                    data: 'file',
                    name: 'file',
                }
            ],
            columnDefs: [{
                "defaultContent": "-",
                "targets": "_all"
            }],
        });

        setDataTable(dataCv);
        setRole({{ auth()->user()->role_id }});

        $(document).on("click", ".apply_cv_admin", function() {
            var form_data = new FormData(document.getElementById("NotEmptyValidator"));
            $.ajax({
                type: "POST",
                url: $("#NotEmptyValidator").data("route"),
                data: form_data,
                beforeSend: function() {
                    $('.lds-ellipsis').show();
                },
                complete: function() {
                    $('.lds-ellipsis').hide();
                },
                async: true,
                processData: false,
                contentType: false,
                dataType: "JSON",
                success: function(response) {
                    $.toast({
                        heading: 'Thông báo thành công!',
                        text: "Gửi cv thành công!",
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'success'
                    });
                    $("#NotEmptyValidator")[0].reset();
                    $(".modal-cv-p").toggle();
                    $("#modal-form-overlay").toggle();
                    $("#apply_candidate").html('CV của bạn<span style="color: red;">*</span>');
                    $(".countApplyCv").text(response)
                    dataCv.ajax.reload();
                    get_notify_bubble();
                },
                error: function(response) {
                    $('.lds-ellipsis').hide();
                    $("input[name=name]").removeClass("text-error")
                    $("input[name=email]").removeClass("text-error")
                    $("input[name=phone]").removeClass("text-error")
                    $("#apply_candidate").removeClass("text-error")
                    $("select[name=skills]").removeClass("text-error")
                    if (response.responseJSON.mess) {
                        $.toast({
                            heading: 'Thông báo lỗi!',
                            text: response.responseJSON.mess,
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: 'error'
                        });
                    };
                    if (response.responseJSON.phone) {
                        $("input[name=phone]").addClass("text-error")
                    }
                    if (response.responseJSON.email) {
                        $("input[name=email]").addClass("text-error")
                    }
                    if (response.responseJSON.errors) {
                        if (response.responseJSON.errors.name) {
                            $("input[name=name]").addClass("text-error")
                        }
                        if (response.responseJSON.errors.skills) {
                            $("select[name=skills]").addClass("text-error")
                        }
                        if (response.responseJSON.errors.email) {
                            $.toast({
                                heading: 'Thông báo lỗi!',
                                text: response.responseJSON.errors.email,
                                showHideTransition: 'slide',
                                position: 'top-right',
                                icon: 'error'
                            });
                            $("input[name=email]").addClass("text-error")
                        }
                        if (response.responseJSON.errors.phone) {
                            $.toast({
                                heading: 'Thông báo lỗi!',
                                text: response.responseJSON.errors.phone,
                                showHideTransition: 'slide',
                                position: 'top-right',
                                icon: 'error'
                            });
                            $("input[name=phone]").addClass("text-error")
                        }
                        if (response.responseJSON.errors.file) {
                            $.toast({
                                heading: 'Thông báo lỗi!',
                                text: response.responseJSON.errors.file,
                                showHideTransition: 'slide',
                                position: 'top-right',
                                icon: 'error'
                            });
                            $("#apply_candidate").addClass("text-error")
                        }
                    }
                }
            });
        })
        // modal cv
        $(document).on('change', '#file', function() {
            var file = $('#file')[0].files[0]
            if (file) {
                $('.custom-file-label').html(file.name);
            }
        });
        $(document).on("click", ".apply-cv-btn", function(e) {
            e.preventDefault();
            $(".modal-cv-p").toggle();
            $("#modal-form-overlay").toggle();
        });
        $(document).on("click", ".apply-cv-btn-close", function(e) {
            e.preventDefault();
            $("#NotEmptyValidator")[0].reset();
            $("input[name=name]").removeClass("text-error")
            $("input[name=email]").removeClass("text-error")
            $("input[name=phone]").removeClass("text-error")
            $("#apply_candidate").removeClass("text-error")
            $(".modal-cv-p").toggle();
            $("#modal-form-overlay").toggle();
            $("#apply_candidate").html('CV của bạn<span style="color: red;">*</span>');
            $("#id_apply_cv").remove()
        });
        // end modal cv
    </script>
@endpush
