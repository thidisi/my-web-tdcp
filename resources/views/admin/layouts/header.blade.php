<div class="topbar">
    <div class="topbar-left">
        <div class="logo">
            <h1><a href="{{ url('') }}">HACHINET</a></h1>
        </div>

    </div>
    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar" role="navigation">

        <div class="container">
            <div class="navbar-collapse2">
                <button class="button-menu-mobile open-left">
                    <i class="fa fa-bars"></i>
                </button>
                <h1 class="title"></h1>
                <ul class="nav navbar-nav navbar-right top-navbar">
                    <li class="dropdown notifications-menu hide-phone">
                        <a id="btn-notify" class="dropdown-toggle" data-toggle="dropdown"><i
                                class="fa fa-bell"></i><span
                                class="label label-blue absolute count-notify"></span></a>
                        <ul id="list-notify" class="dropdown-menu">
                            <li class="header">Thông báo</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <div class="slimScrollDiv">
                                    <ul class="menu" id="menu-list-notify">
                                    </ul>
                                    <div class="slimScrollBar"
                                        style="width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 200px; background: rgb(0, 0, 0);">
                                    </div>
                                    <div class="slimScrollRail"
                                        style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);">
                                    </div>
                                </div>
                            </li>
                            {{-- <li class="footer"><a href="#">View all</a></li> --}}
                        </ul>
                    </li>
                    <li class="dropdown topbar-profile">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><span
                                class="rounded-image topbar-profile-image"><img
                                    src="{{ Auth::user()->avatar != null ? Auth::user()->avatar : 'https://thumbs.dreamstime.com/b/default-avatar-profile-image-vector-social-media-user-icon-potrait-182347582.jpg' }}"></span>
                            {{ Auth::user()->name }} </a>
                        <ul class="dropdown-menu">
                            <li><a href="#">My Profile</a></li>
                            <li><a href="#">Change Password</a></li>
                            <li><a href="#">Account Setting</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="icon-help-2"></i> Help</a></li>
                            <li><a href="lockscreen.html"><i class="icon-lock-1"></i> Lock me</a></li>
                            <li><a class="md-trigger" data-modal="logout-modal"><i class="icon-logout-1"></i>
                                    Logout</a></li>
                        </ul>
                    </li>
                    <li class="right-opener">
                        <form action="{{ route('admin.logout') }}" method="post">
                            @csrf
                            <button type="submit"
                                style="padding: 10px 25px;padding-top: 25px;padding-bottom: 24px;color: #fff;background: transparent; border: none;"><i
                                    class="fa fa-power-off" aria-hidden="true"></i></button>
                        </form>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
