<!DOCTYPE html>
<html>

<!-- Mirrored from themesground.com/nexus-admin/template3/HTML/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 11 Mar 2021 14:40:12 GMT -->

<head>
    <meta charset="UTF-8">
    <title>Hachinet | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="">
    <meta name="keywords" content="Nexus bootstrap template, Nexus admin, bootstrap,admin template, bootstrap admin,">
    <meta name="author" content="ThemesGround">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="https://hachinet.jp/static/img/favicon.png">
    <!-- Google Fonts -->
    <link href="{{ asset('/fonts/css.css') }}" rel="stylesheet">
    <link href="{{ asset('/fonts/main1.css') }}" rel="stylesheet">
    <link href="{{ asset('/fonts/main2.css') }}" rel="stylesheet">

    <!-- Base Css Files -->
    <link rel="stylesheet" href="{{ asset('/assets/css/datatable.css') }}">
    {{-- <link href="{{ asset('/assets/libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css') }}" rel="stylesheet" /> --}}
    <link href="{{ asset('/assets/libs/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/assets/libs/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('/assets/libs/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link href="{{ asset('/assets/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/main.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/style-responsive.css') }}" rel="stylesheet" />
    {{-- <link href="{{ asset('/assets/libs/jquery-notifyjs/styles/metro/notify-metro.css') }}" rel="stylesheet" /> --}}

    <link href="{{ asset('/assets/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/assets/css/toast.min.css') }}" rel="stylesheet" />

    @stack('css')
</head>

<body class="fixed-left">
    <div class="lds-ellipsis">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
    <a class="btn btn-danger autohidebut auto-click-error hidden" href="javascript:;"
        onclick="autohidenotify('error','top right','{{ $errors->first('notify_error_system') }}','Error')">Error</a>
    <a class="btn btn-danger autohidebut auto-click-success hidden" href="javascript:;"
        onclick="autohidenotify('success','top right','{{ session()->get('notify_success_system') }}','Success')">Success</a>
    <a class="btn btn-success hidden" href="javascript:;" onclick="notify('success')">Success</a>
    <!-- Modal Start -->
    <!-- Begin page -->
    <div id="wrapper" style="position: relative;">

        <!-- Top Bar Start -->
        @include('admin.layouts.header')
        <!-- Top Bar End -->
        <!-- Left Sidebar Start -->
        @include('admin.layouts.sidebar')
        <!-- Left Sidebar End -->

        <!-- Start right content -->
        <div class="content-page">
            {{-- <div class="content-wrapper"> --}}
            @yield('content')
            {{-- </div> --}}
        </div>
        <!-- End right content -->
    </div>
    <div id="contextMenu" class="dropdown clearfix">
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu"
            style="display:block;position:static;margin-bottom:5px;">
            <li><a tabindex="-1" href="javascript:;" data-priority="high"><i class="fa fa-circle-o text-red-1"></i>
                    High
                    Priority</a></li>
            <li><a tabindex="-1" href="javascript:;" data-priority="medium"><i
                        class="fa fa-circle-o text-orange-3"></i>
                    Medium Priority</a></li>
            <li><a tabindex="-1" href="javascript:;" data-priority="low"><i class="fa fa-circle-o text-yellow-1"></i>
                    Low Priority</a></li>
            <li><a tabindex="-1" href="javascript:;" data-priority="none"><i
                        class="fa fa-circle-o text-lightblue-1"></i> None</a></li>
        </ul>
    </div>
    <!-- End of page -->
    <!-- the overlay modal element -->
    <div class="md-overlay"></div>
    <!-- End of eoverlay modal -->
    <!-- jQuery 2.1.4 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    {{-- datatables --}}

    <script src="{{ asset('/assets/libs/toast/toast.min.js') }}"></script>
    @stack('js')
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });



        $(document).ready(function() {
            get_notify_bubble();
        })

        function get_notify_bubble() {
            $.ajax({
                url: "{{ route('api.notify.bubble') }}",
                type: 'GET',
                dataType: 'json',
                data: {
                    'role_id': "{{ \Auth::user()->role_id }}",
                    'user_id': "{{ \Auth::user()->id }}",
                },
                success: function(response) {
                    $('.count-notify').text(response.count_notify);
                    if (response.notifications.length > 0) {
                        $('#menu-list-notify').html('');
                        response.notifications.forEach(function(each) {
                            var actisionActive = '';
                            var statusDot = '';
                            if (each.actision == 'active') {
                                actisionActive = 'actision-active';
                            }
                            if (each.actision != 'active') {
                                statusDot = 'actision-active';
                            }
                            $('#menu-list-notify').append(`<li>
                                <a data-route="${each.route}" href="${each.redirect}" class="${actisionActive} actision position-relative">
                                    <div>
                                        <div class="avatar-noti">
                                            <img src="${each.image_path}" alt="image">
                                        </div>
                                    </div>
                                    <div class="content-noti">
                                        <div>
                                            <p>${each.content}</p>
                                        </div>
                                        <div class="date-noti"><i
                                                class="fa fa-clock-o"></i>${each.date}
                                        </div>
                                    </div>
                                    <div class="status-dot">
                                        <span class="${statusDot}"></span>
                                    </div>
                                    <span class="position-absolute">${each.title}</span>
                                </a>
                                </li>`)
                        })
                    } else {
                        $('#menu-list-notify').append(`<li><a><span>Không có thông báo mới!</span></a></li>`);
                    }
                },
            });
        }

        var NotifiSuccess = "{{ session('success') ? session('success') : 'false' }}";
        if (NotifiSuccess !== 'false') {
            $.toast({
                heading: 'Thông báo thành công!',
                text: NotifiSuccess,
                showHideTransition: 'slide',
                position: 'top-right',
                icon: 'success'
            });
        }

        $(".dropdown .topbar-profile").click(function() {
            $(".dropdown-menu").toggle();
        });
        $(".has_sub_settings").click(function() {
            $(".sub_menu_settings").toggle();
        });
        $(".has_sub_settings_job").click(function() {
            $(".sub_menu_settings_job").toggle();
        });
        $(".has_subBlog").click(function() {
            $(".sub_menuBlog").toggle();
        });

        $("#sidebar-menu a").on('click', function(e) {
            if (!$("#wrapper").hasClass("enlarged")) {

                if ($(this).parent().hasClass("has_sub")) {
                    e.preventDefault();
                }

                if (!$(this).hasClass("subdrop")) {
                    // hide any open menus and remove all other classes
                    $("ul", $(this).parents("ul:first")).slideUp(350);
                    $("a", $(this).parents("ul:first")).removeClass("subdrop");
                    $("#sidebar-menu .pull-right i").removeClass("fa-angle-up").addClass("fa-angle-down");

                    // open our new menu and add the open class
                    $(this).next("ul").slideDown(350);
                    $(this).addClass("subdrop");
                    $(".pull-right i", $(this).parents(".has_sub:last")).removeClass("fa-angle-down").addClass(
                        "fa-angle-up");
                    $(".pull-right i", $(this).siblings("ul")).removeClass("fa-angle-up").addClass("fa-angle-down");
                } else if ($(this).hasClass("subdrop")) {
                    $(this).removeClass("subdrop");
                    $(this).next("ul").slideUp(350);
                    $(".pull-right i", $(this).parent()).removeClass("fa-angle-up").addClass("fa-angle-down");
                    //$(".pull-right i",$(this).parents("ul:eq(1)")).removeClass("fa-chevron-down").addClass("fa-chevron-left");
                }
            }
        });

        // NAVIGATION HIGHLIGHT & OPEN PARENT
        // $("#sidebar-menu ul li.has_sub a.active").parents("li:last").children("a:first").addClass("active").trigger(
        //     "click");

        $('.side-nav a').each(function() {
            var pageUrl = window.location.href.split(/[?#]/)[0];
            if (this.href == pageUrl) {
                $(this).parent().children('a').addClass('subdrop');
                if ($(this).parents('.side-nav-third-level')[0]) {
                    $(this).addClass('active');
                    $(this).parent().parent().parent().parent().parent().children('a').addClass('subdrop');
                    $(this).parents('.side-nav-third-level').parent().children('a').addClass('active');
                    $(this).parent().parent().parent().parent().slideDown(350);
                    $(this).parent().parent().slideDown(350);
                }
                if ($(this).parents('.side-nav-second-level')[0]) {
                    $(this).addClass('active');
                    $(this).parents('.side-nav-second-level').parent().children('a').addClass('subdrop');
                    $(this).parent().parent().slideDown(350);
                }
            }
        });

        $('#btn-notify').click(function() {
            $('#list-notify').toggleClass('d-block');
            $('.md-overlay').toggleClass('d-block');
        })

        $('.md-overlay').click(function() {
            $('#list-notify').toggleClass('d-block');
            $('.md-overlay').toggleClass('d-block');
        })

        $("body").on("click", ".actision", function() {
            $.ajax({
                type: "POST",
                url: $(this).data("route"),
                dataType: 'json',
                data: {
                    'role_id': "{{ \Auth::user()->role_id }}",
                    'user_id': "{{ \Auth::user()->id }}",
                },
            });
        });
    </script>
</body>
<!-- Mirrored from themesground.com/nexus-admin/template3/HTML/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 11 Mar 2021 14:41:32 GMT -->

</html>
