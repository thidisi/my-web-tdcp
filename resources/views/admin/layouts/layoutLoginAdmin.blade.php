<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Hachinet | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="">
    <meta name="keywords" content="Nexus bootstrap template, Nexus admin, bootstrap,admin template, bootstrap admin,">
    <meta name="author" content="ThemesGround">

    <link href="{{ asset('assets/css/login.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <!-- MAIN CONTENT -->
    <div id="particles-js"></div>
    <div class="container container_content">
        <div class="row">
            <div class="col-lg-6">
                <div class="">
                    <div style="text-align: center">
                        <h4>ĐĂNG NHẬP HỆ THỐNG @yield('role')</h4>
                    </div>
                    <div>
                        @yield('image')
                    </div>
                </div>
            </div>
            <!-- LOGIN -->
            <div class="col-lg-6">
                @yield('content')
            </div>
        </div>
    </div>
</body>

</html>
<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js" defer data-deferred="1"></script>
<script src="{{ asset('/assets/js/login.js') }}"></script>
