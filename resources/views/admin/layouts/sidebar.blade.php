<?php $path = '/' . request()->path(); ?>
<div class="left side-menu" id="style-scroll">
    <div class="sidebar-inner slimscrollleft">
        <!--- Profile -->
        <div id="sidebar-menu">
            <ul class="side-nav">
                <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
                @if (auth()->user()->role_id == null)
                    <li><a href="{{ route('accounts.index') }}"><i class="fa fa-user"></i><span>Quản lý người
                                dùng</span></a>
                @endif

                </li>
                @if (checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['SHOW-JOB']))
                    <li><a href="{{ route('jobs.index') }}"><i class="fa fa-th"></i><span>Danh sách công việc</span></a>
                    </li>
                @endif

                <li><a href="{{ route('cv.index') }}"><i class="fa fa-file"></i><span>Danh sách hồ sơ</span></a>
                </li>
                <li><a href="{{ route('fullcalendar.index') }}"><i class="fa fa-calendar"></i><span>Lịch phỏng vấn</span></a>
                </li>
                {{-- <li class='has_sub'>
                    <a href='javascript:void(0);'><i class="fa fa-file"></i><span>Hồ sơ và lịch phỏng vấn</span>
                        <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                    <ul class="side-nav-second-level" style="display: none;">
                        <li><a href="{{ route('cv.index') }}"><span><strong>Danh sách hồ sơ</strong></span></a></li>
                        <li><a href="{{ route('fullcalendar.index') }}"><span><strong>Lịch phỏng vấn</strong></span></a></li>
                    </ul>
                </li> --}}

                <li class='has_sub'>
                    <a href='javascript:void(0);'><i class="fa fa-align-justify"></i><span>Tin tức</span>
                        <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                    <ul class="side-nav-second-level" style="display: none;">
                        <li><a href="{{ route('categories.index') }}"><span><strong>Danh sách danh mục</strong></span></a></li>
                        <li><a href="{{ route('blogs.index') }}"><span><strong>Bài viết</strong></span></a></li>
                    </ul>
                </li>

                <li class="has_sub"><a href="javascript:void(0);"><i class="fa fa-cog"></i><span>Cấu hình</span> <span
                            class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                    <ul class="side-nav-second-level" style="display: none;">
                        @if (checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['SETTING-JOB']))
                        <li class="has_sub"><a href="javascript:void(0);"><span><strong>Công việc</strong></span>
                                <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                            <ul class="side-nav-third-level" style="display: none;">
                                <li><a href='{{ route('admin.salary.show') }}'><span>Danh sách mức lương</span></a>
                                </li>
                                <li><a href='{{ route('admin.skill.show') }}'><span>Danh sách kỹ năng</span></a></li>
                                <li><a href='{{ route('admin.level.show') }}'><span>Danh sách cấp bậc</span></a></li>
                            </ul>
                        </li>
                        @endif

                        @if (checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['SETTING-KPI']) ||
                            checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['PERMISSION']) ||
                            checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['BONUS']))
                            <li><a href="{{ route('settings.index') }}"><span><strong>Role / KPI /
                                        Bonus</strong></span></a>
                            </li>
                        @endif

                        <li><a href="{{route('sources.index')}}" ><span><strong>Nguồn JD</strong></span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
