@extends('admin.layouts.layoutAdmin')
@section('title', 'Cấp bậc')
@push('css')
    <link href="{{ asset('/assets/css/datatable-1.10.21.min.css') }}" rel="stylesheet">
@endpush
@section('content')
    <!-- ============================================================== -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <div class="content datatable_custom_content">
        <div class="row">
            <div class="col-md-12">
                <h3 style="padding: 10px 0px;"><b>Danh sách cấp bậc</b></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="widget bg-none p-l-0 p-r-0" style="border-radius: 4px!important;">
                    <div class="widget-content">
                        <table class="table table-hover yajra-datatable" id="datatable">
                            <thead>
                                <tr>
                                    {{-- <th><i data-v-38af99a7="" style="margin-right: 3px;"></i>*</th> --}}
                                    <th><i data-v-38af99a7="" class="fa fa-id-badge" style="margin-right: 3px;"></i>
                                        STT</th>
                                    <th><i data-v-38af99a7="" class="fa fa-book" style="margin-right: 3px;"></i></i> Tên
                                        level</th>
                                    <th><i data-v-38af99a7="" class="fa fa-align-justify"
                                            style="margin-right: 3px;"></i>Hoạt động
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 position-back">
                <div class="card position-relative">
                    <div class="card-body">
                        <form id="formValidate">
                            <div class="mb-2 input-name">
                                <label class="form-label required">Cấp bậc (<span class="require">*</span>)</label>
                                <input type="hidden" name="type" value="{{ $type }}">
                                <input type="text" name="name" id="name" value="{{ old('name') }}"
                                    class="form-control text-danger" placeholder="Nhập cấp bậc..." />
                            </div>
                            <div class="mb-2">
                                <label class="form-label">Mô tả</label>
                                <textarea name="description" cols="30" rows="17" value="" id="description" class="form-control"
                                    placeholder="Mô tả ..."></textarea>
                            </div>
                            <div class="my-3 d-flex" style="margin: 8px 0px;">
                                <button class="btn btn-primary button" id="storeAdd">Thêm mới</button>
                                <button style="display: none; margin-left: 4px;" type="button" id="button-add"
                                    class="btn btn-danger">Hủy</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->
@endsection
@push('js')
    <script src="{{ asset('/assets/libs/datatables/datatables-1.10.21.min.js') }}"></script>
    <script src="{{ asset('/assets/js/admin/job.js') }}"></script>
    <script>
        // Data Display Code
        var table = $('#datatable').DataTable({
            ajax: {
                "url": "{{ route('api.type.show', 'levels') }}",
            },
            lengthMenu: [10],
            columns: [{
                    "data": "id"
                },
                {
                    "data": "name"
                },
                {
                    "data": "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, row) {
                        return `<div class="btn-group btn-group-xs" style="display: flex; justify-content: center;">
                                    <a data-id="${row.id}" id="button-edit" data-toggle="tooltip" title="Edit" class="mr-8"><button type="button" class="btn-custom-action"><i class="fa fa-edit"></i>&nbsp;&nbsp;Sửa</button></a><br>
                                    <a data-id="${row.id}"  id="delete" class="btn-delete" data-toggle="tooltip" title="Delete" class="action-custom button-delete"><button type="button" class="btn-custom-action"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Xóa</button></a>
                                </div>`;
                    }
                },
            ],
            "bLengthChange": false,
            sDom: 'lrtip',
            language: {
                "sProcessing": "<span class='fa-stack fa-lg text-yellow'>\n\
                                <i class='fa fa-spinner fa-spin fa-stack-2x fa-fw'></i>\n\
                           </span>&emsp;",
                "sLengthMenu": "",
                "sZeroRecords": "Không có dữ liệu trong bảng",
                "sInfo": "Hiển thị _START_ đến _END_ trong _TOTAL_ bản ghi",
                "sInfoEmpty": "Hiển thị 0 đến 0 trong số 0 bản ghi",
                "sInfoFiltered": "(Đã lọc từ _MAX_ bản ghi)",
                "sSearch": "",
                "oPaginate": {
                    "sFirst": "Đầu trang",
                    "sLast": "Cuối trang",
                    "sNext": "»",
                    "sPrevious": "«"
                },
                searchPlaceholder: "",
            }
        });

        table.on('order.dt search.dt', function() {
            let i = 1;

            table.cells(null, 0).every(function(cell) {
                this.data(i++);
            });
        }).draw();
    </script>
@endpush
