@extends('admin.layouts.layoutAdmin')
@section('title', 'Danh sách KPI')
@push('css')
    <link href="{{ asset('/assets/css/datatable-1.10.21.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/css/daterangepicker.css') }}" rel="stylesheet">
@endpush
@section('content')
    <div class="content datatable_custom_content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="widget">
                    <div class="widget-header my-3">
                        <div class="tab-content">
                            <div class="tab-pane active" id="">
                                <table class="table table-hover yajra-datatable" cellspacing="0" width="100%">
                                    <thead>
                                        <div class="_title-datatable row">
                                            <div class="text-left col-lg-6 lh-40">
                                                DANH SÁCH KPI
                                            </div>
                                            <div class="col-lg-6 tab_table">
                                                <div class="float-right">
                                                    <input type="text" class="form-control" id="daterangepicker">
                                                </div>
                                            </div>
                                        </div>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên nhân sự</th>
                                            <th>CV gửi KH</th>
                                            <th>CV phỏng vấn</th>
                                            <th>Ứng viên</th>
                                            <th>KPI</th>
                                            <th>Tổng bonus TT</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript" src="{{ asset('/assets/libs/moment/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/libs/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('/assets/libs/datatables/datatables-1.10.21.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#daterangepicker').daterangepicker({
                startDate: moment().subtract(29, 'days'),
                endDate: moment().add(1, 'days')
            });
            $('#daterangepicker').change(function() {
                table.ajax.reload();
            });
        });
        var table = $('.yajra-datatable').DataTable({
            pageLength: 10,
            ordering: true,
            ajax: {
                type: "GET",
                url: "{{ route('api.kpi.list') }}",

                data: function(d) {
                    d.user_id =
                        @if (auth()->user()->role_id != null)
                            "{{ auth()->user()->id }}"
                        @else
                            null
                        @endif ;
                    d.date = $("#daterangepicker").val();
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'rank',
                    className: 'dt-left'
                },
                {
                    data: 'name_user',
                    name: 'name_user',
                    className: 'dt-left'
                },
                {
                    data: 'cv_customer',
                    name: 'cv_customer',
                    className: 'dt-right'
                },
                {
                    data: 'cv_interview',
                    name: 'cv_interview',
                    className: 'dt-right'
                },
                {
                    data: 'cv_candidate',
                    name: 'cv_candidate',
                    className: 'dt-right'
                },
                {
                    data: 'kpi',
                    name: 'kpi',
                    className: 'dt-right'
                },
                {
                    data: 'total_bonus',
                    name: 'total_bonus',
                    className: 'dt-right'
                }
            ],
            columnDefs: [{
                "defaultContent": "-",
                "targets": "_all"
            }],
            language: {
                "sProcessing": "<span class='fa-stack fa-lg text-yellow'>\n\
                                <i class='fa fa-spinner fa-spin fa-stack-2x fa-fw'></i>\n\
                           </span>&emsp;",
                "sLengthMenu": 'Hiển thị _MENU_ bản ghi',
                "sZeroRecords": 'Không có dữ liệu trong bảng',
                "sInfo": "",
                "sInfoEmpty": 'Hiển thị 0 đến 0 trong số 0 bản ghi',
                "sInfoFiltered": '(Đã lọc từ _MAX_ bản ghi)',
                "sSearch": "",
                "oPaginate": {
                    "sFirst": 'Đầu trang',
                    "sLast": 'Cuối trang',
                    "sNext": '»',
                    "sPrevious": '«'
                },
            }
        });
        $("#DataTables_Table_0_filter").remove();
        $('#DataTables_Table_0_length').remove();
    </script>
@endpush
