<style>
    .confim-apply {
        padding-top: 10px !important;
    }

    .time_pass_work {
        margin: 0;
    }

    .confim-apply {
        /* display: none; */
    }

    .contract-info {
        padding-top: 10px !important;
    }

    .title-contract-info {
        padding: 2px 4px;
        font-weight: 700;
        font-size: 1.4rem;
        font-family: "Nunito Sans", sans-serif;
        margin-bottom: 0;
        color: #333333;
    }

    input[data-type="currency"] {
        text-align: right;
    }

    #btn-next {
        display: none;
    }

    .btn_edit {
        display: flex;
        justify-content: flex-end;
    }
</style>
<div class="modal-candidate">
    <div class="close show-candidate-btn-close"><i class="fi fi-rr-cross"></i></div>
    <div class="candidate-content">
        <div class="col-content" style="padding-left: 0;">
            <embed id="showFilePdf" src="" style="width:100%;" height="800px">
            <div class="image-pdf">
                <div class="card-image">
                    <div class="content-image">
                        <img class="" src="{{ asset('assets/img/PDF_file_icon.png') }}" alt=""
                            height="120">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-content" style="padding-right: 0;">
            <div class="header-apply mb-4">
                <div class="title-apply">
                    <h4>Vị trí ứng tuyển: <span id="title-job"></span></h4>
                </div>
                <div class="nav-apply">
                    <div class="time-apply">
                        <i class="fi fi-rr-clock-five"></i>
                        <span id="date-apply"></span>
                    </div>
                    <div class="status-apply">
                        <span id="status-apply"></span>
                    </div>
                </div>
            </div>
            <div class="info-apply">
                <div>
                    <span class="p-4px">Họ tên:</span> <span class="name" id="name-cadidate"></span>
                </div>
                <div class="skills d-flex justify-content-between">
                    <p class="p-4px">HR PIC: <span class="name" id="hr-apply"></span></p>
                    <p class="p-4px"><span class="name" id="source"></span></p>
                </div>
                <div class="list-infoJob">
                    <div class="list-infoJob-left">
                        <ul>
                            <li>
                                <div class="mb-2">
                                    <i class="fi fi-rr-envelope"></i>
                                    <span>Email: <span class="name" id="email-candidate"></span></span>
                                </div>
                            </li>
                            <li>
                                <div class="mb-2">
                                    <i class="fi fi-rr-phone-call"></i>
                                    <span>Điện thoại: <span class="name" id="phone-candidate"></span></span>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <i class="fi fi-rr-book-open-cover"></i>
                                    <span>Kỹ năng: <span class="name" id="skill-apply"></span></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="confim-apply">
                <div data-route="{{ route('api.apply.date') }}" hidden id="getRouteChangeDate"></div>
                <h5 id="confim-profile"></h5>
                <div class="confim-box"></div>
                <div class="interview_date" style="width: 100%; display: none; padding: 0px 8px;">
                    <h6>Thời gian phỏng vấn</h6>
                    <input type="datetime-local" name="interview_date" class="form-control">
                </div>
                <textarea class="widthmax form-control b-r-5 reason" style="width: 100%; display: none;" name="reason" id=""
                    rows="5"
                    placeholder="Lí do từ chối phỏng vấn hay phỏng vấn thất bại (bằng cấp, trình độ, kỹ năng, kinh nghiệm v.v...) (Bắt buộc *)"></textarea>
                @if (auth()->user()->role_id == null || auth()->user()->role_id == 1)
                    <div class="row time_pass_work" style="display: none;">
                        <div class="col-md-6 pull-left">
                            <span class="font-12">Thời gian dự kiến:(<span style="color: red;">*</span>)</span>
                            <input required="" name="time_onboard" value="" type="date" title=""
                                class="form-control b-r-5 time_onboard">
                        </div>
                        <div class="col-md-6 pull-left">
                            <span class="font-12">Thời gian thực tế:(<span style="color: red;">*</span>)</span>
                            <input required="" name="time_actual" value="" type="date" title=""
                                class="form-control b-r-5 time_actual">
                        </div>
                        <div class="col-md-6 pull-left">
                            <span class="font-12">Ngày bắt đầu làm việc:(<span style="color: red;">*</span>)</span>
                            <input required="" name="start_date" value="" type="date" title=""
                                class="form-control b-r-5 start_date">
                        </div>
                        <div class="col-md-6 pull-left">
                            <span class="font-12">Ngày kết thúc làm việc:(<span style="color: red;">*</span>)</span>
                            <input required="" name="end_date" value="" type="date" title=""
                                class="form-control b-r-5 end_date">
                        </div>
                    </div>
                @endif
            </div>
            @if (auth()->user()->role_id == null || auth()->user()->role_id == 1)
                <div class="contract-info" style="display: none;">
                    <h5 class="title-contract-info">Thông tin hợp đồng</h5>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="font-12">Chi phí lương</span><br>
                                    <input id="salary" type="text" name="currency-field" value="0"
                                        pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency"> đ
                                </div>
                                <div class="form-group">
                                    <span class="font-12">Chi phí tuyển dụng</span><br>
                                    <input id="recruitment_money" type="text" name="currency-field"
                                        value="0" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency"> đ
                                </div>
                                <div class="form-group">
                                    <span class="font-12">Chi phí Sale</span><br>
                                    <input id="sale_money" type="text" name="currency-field" value="0"
                                        pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency"> đ
                                </div>
                                <div class="form-group">
                                    <span class="font-12">Lương T13</span><br>
                                    <input id="salary_13" type="text" name="currency-field" value="0"
                                        pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency"> đ
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="font-12">Bonus Onsite</span><br>
                                    <input id="bonus_onsite" type="text" name="currency-field" value="0"
                                        pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency"> đ
                                </div>
                                <div class="form-group">
                                    <span class="font-12">Bảo hiểm</span><br>
                                    <input id="insurance" type="text" name="currency-field" value="0"
                                        pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency"> đ
                                </div>
                                <div class="form-group">
                                    <span class="font-12">Team Building</span><br>
                                    <input id="team_building" type="text" name="currency-field" value="0"
                                        pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency"> đ
                                </div>
                                <div class="form-group">
                                    <span class="font-12">Đơn giá</span><br>
                                    <input id="income" type="text" name="currency-field" value="0"
                                        pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency"> đ
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="footer-apply">
                <div class="box-btn">
                    <button type="button" id="btn-change" class="btn-apply">Lưu thay đổi</button>
                    @if (auth()->user()->role_id == null || auth()->user()->role_id == 1)
                        <button type="button" id="btn-next" class="btn-outline-apply">Thông tin hợp đồng</button>
                        <button type="button" id="btn-previous" class="btn-apply">Quay lại</button>
                        <button type="button" id="btn-edit" class="btn-apply">Sửa</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
