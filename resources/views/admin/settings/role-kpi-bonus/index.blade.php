@extends('admin.layouts.layoutAdmin')
@section('title', 'Role/KPI/Bonus')
@push('css')
    <style>
        table,
        th,
        td {
            border: 1px solid;
        }

        td {
            padding: 15px 80px !important;
            font-size: 15px;
        }

        input[type="number"] {
            text-align: center;
            width: 50px !important;
        }

        input[name="level"] {
            text-align: right;
            width: 150px !important;
        }

        input[data-type="currency"] {
            text-align: right;
        }

        .widget-header {
            cursor: default !important;
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        .content_element {
            margin-bottom: 18px !important;
            margin-right: 0 !important;
            margin-left: 0 !important;
            padding: 0 12px !important;
        }

        .permisison_element {
            font-weight: normal !important;
        }

        .error {
            position: relative;
            animation: shake .1s linear;
            animation-iteration-count: 3;
            border: 1px solid red;
        }

        .btn {
            border: none !important;
        }

        .btn-role {
            color: white !important;
            background: darkgray !important;
            border-radius: 4px 4px !important;
            margin-right: 2px !important;
            font-weight: bold !important;
        }

        .btn_selected {
            background-color: #ffb701 !important;
        }
    </style>
@endpush
@section('content')
    {{-- Cấu hình thông số KPI, Phân quyền, Bonus theo Role --}}
    @if (checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['PERMISSION']) ||
        checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['SETTING-KPI']) ||
        checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['BONUS']))
        <div class="content datatable_custom_content">
            <div class="row">
                <div class="col-md-12 portlets">
                    <div class="widget">
                        <div class="widget-header">
                            <p>
                            <h2>Cấu hình theo Role</h2>
                            </p>
                        </div>
                        <div class="widget-content padding">
                            <div class="m-0">
                                @foreach ($roles as $role)
                                    <button class="btn btn-role @if ($role->name == $roleDefault->name) btn_selected @endif"
                                        id="role-{{ $role->id }}">
                                        {{ $role->name }}
                                    </button>
                                @endforeach
                            </div>
                        </div>

                        @if (checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['SETTING-KPI']))
                            <div class="row content_element">
                                <strong>KPI cho <span class="role_selected">{{ $roleDefault->name }}</span></strong>
                                <input type="hidden" name="role" id="role_id_selected" value="{{ $roleDefault->id }}">
                                <div class="col-md-12">
                                    <div class="col-md-2">
                                        <p>Ứng viên :</p>
                                    </div>
                                    <div class="col-md-10">

                                        <input id="candidates_of_week" type="number" min="0"
                                            oninput="validity.valid||(value='0');" class="input_border_none p-0"
                                            value="{{ empty($roleDefault->kpis) ? 0 : $roleDefault->kpis->week->candidates }}" />/Tuần

                                        <input id="candidates_of_month" type="number" min="0"
                                            oninput="validity.valid||(value='0');" class="input_border_none p-0"
                                            value="{{ empty($roleDefault->kpis) ? 0 : $roleDefault->kpis->month->candidates }}" />/Tháng
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-2">
                                        <p>CV phỏng vấn</p>
                                    </div>
                                    <div class="col-md-10">
                                        <input id="cv_interview_of_week" type="number" min="0"
                                            oninput="validity.valid||(value='0');" class="input_border_none p-0"
                                            value="{{ empty($roleDefault->kpis) ? 0 : $roleDefault->kpis->week->cv_interview }}" />/Tuần

                                        <input id="cv_interview_of_month" type="number" min="0"
                                            oninput="validity.valid||(value='0');" class="input_border_none p-0"
                                            value="{{ empty($roleDefault->kpis) ? 0 : $roleDefault->kpis->month->cv_interview }}" />/Tháng
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-2">
                                        <p>CV gửi khách hàng:</p>
                                    </div>
                                    <div class="col-md-10">
                                        <input id="cv_customer_of_week" type="number" min="0"
                                            oninput="validity.valid||(value='0');" class="input_border_none p-0"
                                            value="{{ empty($roleDefault->kpis) ? 0 : $roleDefault->kpis->week->cv_customer }}" />/Tuần

                                        <input id="cv_customer_of_month" type="number" min="0"
                                            oninput="validity.valid||(value='0');" class="input_border_none p-0"
                                            value="{{ empty($roleDefault->kpis) ? 0 : $roleDefault->kpis->month->cv_customer }}" />/Tháng
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if (checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['BONUS']))
                            <div class="row content_element">
                                <strong>Bonus tuyển dụng hàng tháng cho <span
                                        class="role_selected">{{ $roleDefault->name }}</span></strong>
                                <input type="hidden" name="role" id="role_id_selected" value="{{ $roleDefault->id }}">
                                <div class="col-md-12">
                                    <div class="col-md-2">
                                        <p>Tiền thưởng :</p>
                                    </div>
                                    <div class="col-md-10">
                                        <input id="bonus_of_month" type="text" name="currency-field"
                                            pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency"
                                            value="{{ empty($bonusDefault) ? 0 : $bonusDefault->value }}"
                                            class="input_border_none p-0" /> VNĐ
                                    </div>
                                </div>

                            </div>
                        @endif

                        @if (checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['PERMISSION']))
                            <div class="row content_element">
                                <strong>Phân quyền cho <span class="role_selected">{{ $roleDefault->name }}</span></strong>
                                <div class="row">
                                    @php
                                        $permissionList = [];
                                        foreach ($roleDefault->permissions as $index) {
                                            $permissionList[] = $index->id;
                                        }
                                    @endphp

                                    <input type="hidden" name="permission" id="permissions" value="">
                                    <div class="col-md-3">
                                        <input type="checkbox" id="permission-0" name="permission" value="0">
                                        <label for="permission-0" class="permisison_element">Tất cả quyền</label><br>
                                    </div>
                                    @foreach ($permissionDefault as $permission)
                                        <div class="col-md-3">
                                            <input type="checkbox" id="permission-{{ $permission->slug }}"
                                                name="permission" value="{{ $permission->id }}"
                                                @foreach ($permissionList as $element)
                                            @if ($element == $permission->id) checked @endif @endforeach>
                                            <label for="permission-{{ $permission->slug }}" class="permisison_element">
                                                {{ $permission->name }}</label><br>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        <button type="submit" class="btn btn-primary" id="btn_submit_by_role">Lưu thay đổi</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    {{-- Cấu hình thông số Bonus đầu vào theo Level --}}
    @if (checkPermissionToRedirect(App\Models\Permission::PERMISSION_ID['BONUS']))
        <div class="content datatable_custom_content">
            <div class="row">
                <div class="col-md-12 portlets">
                    <div class="widget">
                        <div class="widget-header">
                            <p>
                            <h2>Cấu hình Bonus đầu vào</h2>
                            </p>
                        </div>
                        <div class="widget-content padding">
                            <div class="row content_element">
                                <table>
                                    <thead>
                                        <tr>
                                            <td><strong>Cấp bậc</strong></td>
                                            <td style="text-align: center"><strong>Bonus (VNĐ)</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($levels as $level)
                                            <tr>
                                                <td>{{ $level['key'] }}</td>
                                                <td><input id="level-{{ $level['id'] }}" type="text" name="level"
                                                        pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency"
                                                        value=@if ($level['value'] == 0) "{{ 0 }}" @else "{{ $level['value'] }}" @endif"
                                                        class="input_border_none p-0" />
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <button type="submit" class="btn btn-primary" id="btn_submit_by_level">Lưu thay
                                đổi</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
@push('js')
    <script>
        $(document).ready(function() {
            $("button").click(function(e) {
                if (this.id != '') {
                    if ((this.id != 'btn_submit_by_role') && (this.id != 'btn_submit_by_level')) {
                        let id = this.id.replace(/\D/g, '');
                        e.preventDefault();
                        $.ajax({
                            url: "{{ route('api.setting.role.show', '') }}/" + id,
                            success: function(data) {
                                changeRoleSelected(data);
                            }
                        });
                    } else {
                        if (this.id == 'btn_submit_by_role') {
                            if (validateSubmitByRole()) {
                                let id = $('#role_id_selected').val();
                                e.preventDefault();
                                $.ajax({
                                    url: "{{ route('api.setting.role.update', '') }}/" + id,
                                    type: 'PUT',
                                    data: {
                                        id: id,
                                        candidates_of_week: $('#candidates_of_week').val(),
                                        candidates_of_month: $('#candidates_of_month').val(),
                                        cv_interview_of_week: $('#cv_interview_of_week').val(),
                                        cv_interview_of_month: $('#cv_interview_of_month').val(),
                                        cv_customer_of_week: $('#cv_customer_of_week').val(),
                                        cv_customer_of_month: $('#cv_customer_of_month').val(),
                                        bonus_of_month: $('#bonus_of_month').val(),
                                        permissions: getPermission(),
                                        submit_type: 'by_role'
                                    },
                                    success: function(data) {
                                        $.toast({
                                            heading: 'Thông báo thành công!',
                                            text: 'Lưu cấu hình theo Role thành công!',
                                            showHideTransition: 'slide',
                                            position: 'top-right',
                                            icon: 'success'
                                        });
                                    }
                                });
                            }
                        }
                        if (this.id == 'btn_submit_by_level') {
                            if (validateSubmitByLevel()) {
                                var id = 0;
                                e.preventDefault();
                                $.ajax({
                                    url: "{{ route('api.setting.role.update', '') }}/" + id,
                                    type: 'PUT',
                                    data: {
                                        bonus: getBonusByLevel(),
                                        submit_type: 'by_level'
                                    },
                                    success: function(data) {
                                        $.toast({
                                            heading: 'Thông báo thành công!',
                                            text: 'Lưu cấu hình Bonus đầu vào thành công!',
                                            showHideTransition: 'slide',
                                            position: 'top-right',
                                            icon: 'success'
                                        });
                                    }
                                });
                            }
                        }
                    }
                }
            });

            $("input").each(function() {
                $(this).click(function(e) {
                    if ($(this)[0].classList.contains('error')) {
                        $(this)[0].classList.remove('error');
                    }
                })
            });

            $('#permission-0').click(function(e) {
                if (!$(this).is(":checked")) {
                    $("input:checkbox[name=permission]").prop('checked', false);
                } else {
                    $("input:checkbox[name=permission]").prop('checked', true);
                }
            });

            $("input:checkbox[name=permission]").each(function() {
                $(this).click(function(e) {
                    if (!$(this).is(":checked")) {
                        $('#permission-0').prop('checked', false);
                    } else {
                        //Check if all checkbox is Checked
                        checkAll();
                    }
                });
            });

            //Check if all checkbox is Checked
            checkAll();
        });

        function changeRoleSelected(data) {
            role = data.role;
            bonus = data.bonus;
            id = role.id;
            for (let element of document.querySelectorAll("[id^=role-]")) {
                $button = document.getElementById(element.id);
                element = element.id.replace(/\D/g, "");

                if (element != id) {
                    $button.classList.remove('btn_selected');
                } else {
                    $button.classList.add('btn_selected');
                    document.getElementById("role_id_selected").value = element;
                    $('.role_selected').text(role.name);
                    if (role.kpis != null) {
                        kpis_month = JSON.parse(role.kpis.month);
                        kpis_week = JSON.parse(role.kpis.week);

                        $('#candidates_of_week').val(kpis_week.candidates);
                        $('#candidates_of_month').val(kpis_month.candidates);
                        $('#cv_interview_of_week').val(kpis_week.cv_interview);
                        $('#cv_interview_of_month').val(kpis_month.cv_interview);
                        $('#cv_customer_of_week').val(kpis_week.cv_customer);
                        $('#cv_customer_of_month').val(kpis_month.cv_customer);
                    } else {
                        //Set value default = 0
                        $('#candidates_of_week').val(0);
                        $('#candidates_of_month').val(0);
                        $('#cv_interview_of_week').val(0);
                        $('#cv_interview_of_month').val(0);
                        $('#cv_customer_of_week').val(0);
                        $('#cv_customer_of_month').val(0);
                    }
                    if (bonus != null) {
                        $('#bonus_of_month').val(bonus.value);
                    } else {
                        //Set value default = 0
                        $('#bonus_of_month').val(0);
                    }
                }
            }

            $("input:checkbox[name=permission]").each(function() {
                $(this).prop('checked', false);
            });

            permission = data.role.permissions;
            $("input:checkbox[name=permission]").each(function() {
                id = $(this).val();
                permission.forEach(element => {
                    if (element.id == id) {
                        $(this).prop('checked', true);
                    }
                });
            });
            //Check if all checkbox is Checked
            checkAll();
        }

        function getPermission() {
            var permissions = [];
            $("input:checkbox[name=permission]:checked").each(function() {
                if ($(this).val() != 0) {
                    permissions.push($(this).val());
                }
            });
            return permissions;
        }

        function getBonusByLevel() {
            var bonusByLevel = [];
            $("input[name=level]").each(function() {
                result = [];
                id = $(this).attr('id').replace(/\D/g, '');
                value = $(this).val();
                result.push(id);
                result.push(value);
                bonusByLevel.push(result);
            });
            return bonusByLevel;
        }

        //Check if all checkbox is Checked
        function checkAll() {
            var allChecked = true;
            $("input:checkbox[name=permission]").each(function() {
                if (($(this).val() != 0) && (!$(this).is(":checked"))) {
                    allChecked = false;
                }
            });

            if (allChecked) {
                $('#permission-0').prop('checked', true);
            }
        }

        function validateSubmitByRole() {
            if ($('#candidates_of_week').val() == '') {
                $.toast({
                    heading: 'Cảnh báo!',
                    text: 'Chưa nhập KPI Ứng viên theo Tuần!',
                    showHideTransition: 'slide',
                    position: 'top-right',
                    icon: 'error'
                });
                $('#candidates_of_week')[0].classList.add('error');
                return false;
            }
            if ($('#candidates_of_month').val() == '') {
                $.toast({
                    heading: 'Cảnh báo!',
                    text: 'Chưa nhập KPI Ứng viên theo Tháng!',
                    showHideTransition: 'slide',
                    position: 'top-right',
                    icon: 'error'
                });
                $('#candidates_of_month')[0].classList.add('error');
                return false;
            }
            if ($('#cv_customer_of_week').val() == '') {
                $.toast({
                    heading: 'Cảnh báo!',
                    text: 'Chưa nhập KPI CV gửi khách hàng theo Tuần!',
                    showHideTransition: 'slide',
                    position: 'top-right',
                    icon: 'error'
                });
                $('#cv_customer_of_week')[0].classList.add('error');
                return false;
            }
            if ($('#cv_customer_of_month').val() == '') {
                $.toast({
                    heading: 'Cảnh báo!',
                    text: 'Chưa nhập KPI CV gửi khách hàng theo Tháng!',
                    showHideTransition: 'slide',
                    position: 'top-right',
                    icon: 'error'
                });
                $('#cv_customer_of_month')[0].classList.add('error');
                return false;
            }
            if ($('#cv_interview_of_week').val() == '') {
                $.toast({
                    heading: 'Cảnh báo!',
                    text: 'Chưa nhập KPI CV phỏng vấn theo Tuần!',
                    showHideTransition: 'slide',
                    position: 'top-right',
                    icon: 'error'
                });
                $('#cv_interview_of_week')[0].classList.add('error');
                return false;
            }
            if ($('#cv_interview_of_month').val() == '') {
                $.toast({
                    heading: 'Cảnh báo!',
                    text: 'Chưa nhập KPI CV phỏng vấn theo Tháng!',
                    showHideTransition: 'slide',
                    position: 'top-right',
                    icon: 'error'
                });
                $('#cv_interview_of_month')[0].classList.add('error');
                return false;
            }
            if ($('#bonus_of_month').val() == '') {
                $.toast({
                    heading: 'Cảnh báo!',
                    text: 'Chưa nhập Bonus tuyển dụng háng tháng!',
                    showHideTransition: 'slide',
                    position: 'top-right',
                    icon: 'error'
                });
                $('#bonus_of_month')[0].classList.add('error');
                return false;
            }
            return true;
        }

        function validateSubmitByLevel() {
            $("input[name=level]").each(function() {
                if ($(this).val() == '') {
                    $.toast({
                        heading: 'Cảnh báo!',
                        text: 'Không được để trống thông số Cấu hình Bonus đầu vào',
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'error'
                    });
                    $(this)[0].classList.add('error');
                    return false;
                }
            });
            return true;
        }

        //Validate input currency
        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() {
                formatCurrency($(this), "blur");
            }
        });

        function formatNumber(n) {
            // format number 1234567 to 1,234,567
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }

        function formatCurrency(input, blur) {
            // and puts cursor back in right position.
            // get input value
            var input_val = input.val();

            // don't validate empty input
            if (input_val === "") {
                return;
            }

            // original length
            var original_len = input_val.length;

            // initial caret position
            var caret_pos = input.prop("selectionStart");

            input_val = formatNumber(input_val);

            // final formatting
            if (blur === "blur") {
                input_val += "";
            }

            // send updated string to input
            input.val(input_val);

            // put caret back in the right position
            var updated_len = input_val.length;
            caret_pos = updated_len - original_len + caret_pos;
            input[0].setSelectionRange(caret_pos, caret_pos);
        }
    </script>
@endpush
