@extends('admin.layouts.layoutAdmin')
@section('title', 'Tạo sources')
@push('css')
    <link href="{{ asset('/assets/css/datatable-1.10.21.min.css') }}" rel="stylesheet">
    <style>
        .center {
            position: absolute;
            left: 50%;
            top: 25%;
            transform: translate(-50%, -50%);
        }

        button.close {
            padding: 5px !important;
            margin-top: 0 !important;
        }

        .btn_modal {
            display: flex;
            justify-content: flex-end;
            gap: 5px;
        }
    </style>
@endpush
@section('content')
    <div class="content datatable_custom_content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget bg-none" style="border-radius: 4px!important;">
                    <div class="widget-content">
                        <table class="table table-hover yajra-datatable" id="datatable">
                            <thead>
                                <div class="_title-datatable row">
                                    <div class="text-left col-lg-6 lh-40">
                                        Danh sách nguồn JD
                                    </div>
                                    <div class="text-right col-lg-6 fz-13" style="text-transform: none;padding: 0;">
                                        <a class="create_source btn btn-default btn-lg background-hachinet form-control p-2 border-radius-select"
                                            style="width: 200px;">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="white-space: break-spaces;">Tạo nguồn JD mới</span>
                                        </a>
                                    </div>
                                </div>
                                <tr>
                                    <th><i class="fa fa-id-badge" style="margin-right: 3px;"></i>
                                        STT</th>
                                    <th><i class="fa fa-image" style="margin-right: 3px;"></i>Ảnh JD</th>
                                    <th><i class="fa fa-link" style="margin-right: 3px;"></i>Nguồn JD</th>
                                    <th><i class="fa fa-tag" style="margin-right: 3px;"></i>Url</th>
                                    <th><i class="fa fa-align-justify" style="margin-right: 3px;"></i>Hoạt động</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    {{-- Modal Confirm Delete Source --}}
                    <div class="modal bs-example-modal-sm " tabindex="-1" role="dialog"
                        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;" id="delete_source">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="mySmallModalLabel">Xác nhận</h4>
                                    <p id="content-modal">Bạn có chắc chắn muốn xóa Nguồn JD phía dưới?</p>
                                    <input type="url" name="delete_name_source" id="delete_name_source" value=""
                                        disabled>
                                    <input type="hidden" name="delete_id_source" id="delete_id_source" value="">
                                </div>
                                <div class="modal-body">
                                    <div class="btn_modal">
                                        <button class="btn btn-danger md-close button-close"
                                            onclick="document.getElementById('delete_source').style.display='none'">Hủy</button>
                                        <button class="btn btn-success md-close button" id="delete_button">Xóa</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Modal CRU Source --}}
                    <form class="modal bs-example-modal-sm " tabindex="-1" role="dialog"
                        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;"
                        enctype="multipart/form-data" id="information_source">
                        <div class="modal-dialog modal-sm center">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                                        onclick="closeModal()">×</button>
                                    <h4 class="modal-title">Thông tin Nguồn JD</h4>
                                </div>
                                @csrf
                                <div class="modal-body" id="information_source">
                                    <div class="form-group">
                                        <label for="name_source">Tên nguồn JD<strong>*</strong></label>
                                        <input type="text" class="form-control p-2" name="name"
                                            placeholder="Nhập tên nguồn JD" value="" id="name_source">
                                        <input type="hidden" id="id_source" value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="url_source">Đường dẫn (URL): </label>
                                        <input type="text" class="form-control p-2" name="url" value=""
                                            id="url_source">
                                    </div>
                                    <div class="form-group">
                                        <label for="url_source">Ảnh: </label>
                                        <input type="file" class="form-control p-2" name="image" id="image_source">
                                        <img src="" id="preview-image" width="60px" alt="">
                                    </div>
                                    <div class="btn_modal">
                                        <button type="button" class="btn btn-danger" onclick="closeModal()">Hủy</button>
                                        <button type="button" class="btn btn-success" id="update_source">Cập
                                            nhật</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('/assets/libs/datatables/datatables-1.10.21.min.js') }}"></script>
    <script>
        // Data Display Code
        var roleUser = {{ \Auth::user()->role_id ? 'true' : 'false' }};
        var table = $('#datatable').DataTable({
            ajax: "{{ route('api.source.list') }}",
            lengthMenu: [10],
            autoWidth: false,
            responsive: true,
            columns: [{
                    "data": "id"
                },
                {
                    "data": "image",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, row) {
                        var img = `{{ asset('storage/${row.image}') }}`;
                        return `<img src="${img}" width="50px">`;
                    }
                },
                {
                    "data": "name",
                    orderable: false,
                    searchable: false,
                },
                {
                    "data": "url",
                    orderable: false,
                    searchable: false,
                },
                {
                    "data": "id",
                    orderable: false,
                    searchable: false,

                    render: function(data, type, row) {
                        var btnEdit =
                            `<div class="btn-group btn-group-xs">
                            <button type="button" data-id="${row.id}" data-image="${row.image}" data-name="${row.name}" data-url="${row.url}" title="Thay đổi thông tin nguồn"  class="btn-custom-action update_source"><i class="fa fa-edit"></i>&nbsp;&nbsp;Sửa</button>`;
                        var btnDel = '';
                        if (roleUser == false) {
                            btnDel = `<br />
                            <button type="button" data-id="${row.id}" data-name="${row.name}" title="Xóa" class="btn-custom-action delete_source"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Xóa</button>
                            </div>`
                        };
                        return btnEdit + btnDel;
                    }
                },
            ],
            "bLengthChange": false,
            sDom: 'lrtip',
            language: {
                "sProcessing": "<span class='fa-stack fa-lg text-yellow'>\n\
                                        <i class='fa fa-spinner fa-spin fa-stack-2x fa-fw'></i>\n\
                                   </span>&emsp;",
                "sLengthMenu": "",
                "sZeroRecords": "Không có dữ liệu trong bảng",
                "sInfo": "Hiển thị _START_ đến _END_ trong _TOTAL_ bản ghi",
                "sInfoEmpty": "Hiển thị 0 đến 0 trong số 0 bản ghi",
                "sInfoFiltered": "(Đã lọc từ _MAX_ bản ghi)",
                "sSearch": "",
                "oPaginate": {
                    "sFirst": "Đầu trang",
                    "sLast": "Cuối trang",
                    "sNext": "»",
                    "sPrevious": "«"
                },
            }
        });

        table.on('order.dt search.dt', function() {
            let i = 1;

            table.cells(null, 0).every(function(cell) {
                this.data(i++);
            });
        }).draw();

        function closeModal() {
            document.getElementById('information_source').style.display = 'none';
            $("#information_source")[0].reset();
            $("#preview-image").attr("src", '')
        }
        $("#image_source").change(function() {
            let reader = new FileReader();
            reader.onload = (e) => {
                $("#preview-image").attr("src", e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        });
        //Modal Create Source
        $(document).on("click", ".create_source", function() {
            $('#update_source').text('Tạo mới');
            $("#information_source").toggle();
            $("#id_source").val('');
            $("#name_source").val('');
            $("#url_source").val('');
            $("#image_source").val('');
        });

        //Modal Update Source
        $(document).on("click", ".update_source", function() {
            $('#update_source').text('Cập nhật');
            $("#information_source").toggle();
            $("#id_source").val($(this).data('id'));
            $("#name_source").val($(this).data('name'));
            $("#url_source").val($(this).data('url'));
            // $("#image_source").val($(this).data('image'));
        });

        //Modal Delete Source
        $(document).on("click", ".delete_source", function() {
            $("#delete_source").toggle();
            $('#delete_name_source').val($(this).data('name'));
            $('#delete_id_source').val($(this).data('id'));

        });

        //Click Update button
        $(document).on("click", "#update_source", function(e) {
            e.preventDefault();
            var form_data = new FormData(document.getElementById("information_source"));
            var id = $("#id_source").val();
            var route = "{{ route('sources.store') }}"
            if (id != '') {
                route = "{{ route('sources.update', '') }}/" + id;
                form_data.append("_method", "put");
            }
            $.ajax({
                url: route,
                type: 'POST',
                dataType: "json",
                async: false,
                processData: false,
                contentType: false,
                data: form_data,
                success: function(response) {
                    table.ajax.reload();
                    $.toast({
                        heading: 'Thông báo thành công!',
                        text: response.message,
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'success'
                    });
                    $("#information_source")[0].reset();
                    $("#information_source").toggle();
                },
                error: function(response) {
                    $.toast({
                        heading: 'Thông báo thất bại!',
                        text: response.responseJSON.message,
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'error'
                    });
                }
            });

        });

        $(document).on("click", "#delete_button", function() {
            var id = $('#delete_id_source').val();
            $.ajax({
                url: '{{ route('sources.destroy', '') }}/' + id,
                type: 'DELETE',
                success: function(response) {
                    table.ajax.reload();
                    $.toast({
                        heading: 'Thông báo thành công!',
                        text: response.message,
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'success'
                    });
                    $("#delete_source").toggle();
                },
                error: function(response) {
                    $.toast({
                        heading: 'Thông báo thất bại!',
                        text: response.responseJSON.message,
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'error'
                    });
                    $("#delete_source").toggle();
                }
            });

        });
    </script>
@endpush
