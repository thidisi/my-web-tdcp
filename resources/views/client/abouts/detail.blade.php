@extends('client.layout.layout')
@section('title', $blog->title)
@section('meta')
    <meta property="og:locale" content="vi">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ route('blog.detail', $blog->slug) }}">
    <meta property="og:title" content="Hachinet Blog">
    <meta property="og:description"
        content="Tìm việc làm IT- Đăng tin miễn phí-Trang tuyển dụng IT hiệu quả và uy tín,cập nhật hàng nghìn việc làm mới mỗi ngày.Hệ thống kiếm tiền của Hr và Headhunter">
    <meta property="og:site_name" content="Hachinet Blog">
    <meta property="og:image" content="{{ $image }}">
@endsection
@section('content')
    <section>
        <div class="pxp-container">
            <div class="pxp-blog-hero">
                <div class="row justify-content-between align-items-end">
                    <div class="col-lg-8 col-xxl-6">
                        <h1>{{ $blog->title }}</h1>
                        <div class="pxp-hero-subtitle pxp-text-light">{{ $blog->highlight }}</div>
                    </div>
                    <div class="col-lg-4 col-xxl-6">
                        <div class="text-start text-lg-end mt-4 mt-lg-0">
                            <div class="pxp-single-blog-top-category">
                                <a style="font-size: 20px;">{{ $blog->categories->name }}</a>
                            </div>
                            {{-- <a class="pxp-posts-card-2-category" style="font-size: 13.5px; color:black;"> <i
                                    class="fa fa-eye"></i> {{ $blog->viewer }} Lượt
                                xem</a> --}}
                        </div>
                    </div>
                </div>
            </div>

            <img class="pxp-single-blog-featured-img" src=" {{ asset('storage/' . $blog->banner) }}"
                alt="How to Start Looking for a New Job">
        </div>
    </section>

    <section class="mt-100">
        <div class="pxp-container">
            <div class="row justify-content-center">
                <div class="col-xl-7">
                    <div class="pxp-single-blog-content">
                        {!! $blog->content !!}
                    </div>

                    <div class="mt-100">
                        <div class="pxp-single-blog-share">
                            <span class="me-4">Chia sẻ bài viết này:</span>
                            <ul class="list-unstyled">
                                <li>
                                    <div class="fb-share-button" data-href="{{ url()->current() }}"
                                        data-layout="button_count" data-size="large"><a class="" target="_blank"
                                            href=""
                                            class="fb-xfbml-parse-ignore"><span class="fa fa-facebook"></span></a></div>
                                </li>
                                <li>
                                    <a class="" href="https://twitter.com/share?url={{ url()->current() }}&amp;text=&amp;hashtags=bloghachinet"
                                        target="_blank">
                                        <span class="fa fa-twitter"></span>
                                    </a>
                                </li>
                                <li>
                                    <a class=""
                                        href="https://www.pinterest.com/pin/create/button/?url={{ url()->current() }}&media={{ $image }}&description="
                                        target="_blank">
                                        <span class="fa fa-pinterest"></span>
                                    </a>
                                </li>
                                <li>
                                    <a class=""
                                        href="http://www.linkedin.com/shareArticle?mini=true&amp;{{ url()->current() }}"
                                        target="_blank">
                                        <span class="fa fa-linkedin"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>

    <section class="mt-100">
        <div class="pxp-container">
            <h2 class="pxp-subsection-h2">{{ $blog->categories->name }}</h2>
            <p class="pxp-text-light">Những bài viết liên quan</p>
            <div class="row mt-3 mt-md-4">
                @foreach ($categories as $categorie)
                    <div class="col-md-6 col-xl-4 col-xxl-3 pxp-posts-card-1-container">
                        <div class="pxp-posts-card-1 pxp-has-border">
                            <div class="pxp-posts-card-1-top">
                                <div class="pxp-posts-card-1-top-bg">
                                    <div>
                                        <img class="pxp-posts-card-1-image pxp-cover object-fit-cover"
                                            src=" {{ asset('storage/' . $categorie->banner) }}"
                                            style="width: 100%; border: 1px solid var(--pxpLightGrayColor);">
                                    </div>
                                    <div class="pxp-posts-card-1-info">
                                        <div class="d-flex justify-content-between">
                                            <div class="pxp-posts-card-1-date">
                                                {{ date('d/m/Y', strtotime($categorie->created_at)) }}
                                            </div>

                                        </div>
                                        <a class="pxp-jobs-card-1-company">
                                            {{ $categorie->categories->name }}</a>
                                    </div>
                                </div>
                                <div class="pxp-posts-card-1-content">
                                    <a href="{{ route('blog.detail', $categorie->slug) }}"
                                        class="pxp-posts-card-1-title">{{ $categorie->title }}</a>
                                    <div class="pxp-posts-card-1-summary pxp-text-light">{{ $categorie->highlight }}
                                    </div>
                                </div>
                            </div>
                            <div class="pxp-posts-card-1-bottom">
                                <div class="pxp-posts-card-1-cta">
                                    <a href="{{ route('blog.detail', $categorie->slug) }}">Xem thêm<span
                                            class="fa fa-angle-right"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </section>
@endsection
@push('js')
    <script>
        $(document).ready(function() {
            var href = 'https://www.facebook.com/sharer/sharer.php?u=' + $('.fb-share-button').data('href') +
                '&src=sdkpreparse';
            if ($(window).width() < 769) {
                $('.fb-share-button').find('a').attr('href', 'fb://facewebmodal/f?href=' + href);
            } else {
                $('.fb-share-button').find('a').attr('href', href);
            }
        });
    </script>
@endpush
