@extends('client.layout.layout')
@section('title', 'Trang tuyển dụng lập trình viên')
@section('content')
    <section>
        <div class="pxp-container">
            <div class="pxp-blog-hero">
                <h1>Tin tức</h1>
                <div class="pxp-hero-subtitle pxp-text-light">Nơi chia sẻ những thông tin hữu ích và thú vị về Hachinet.
                </div>
            </div>

            <div id="pxp-blog-featured-posts-carousel" class="carousel slide carousel-fade pxp-blog-featured-posts-carousel"
                data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="pxp-featured-posts-item pxp-cover"
                            style="background-image: url({{ asset('client/images/hachinet-banner3.png') }});">
                            <div class="pxp-hero-opacity"></div>
                            <div class="pxp-featured-posts-item-caption">
                                <div class="pxp-featured-posts-item-caption-content">
                                    <div class="row align-content-center justify-content-center">
                                        <div class="col-9 col-md-8 col-xl-7 col-xxl-6">
                                            <div class="pxp-featured-posts-item-date">31/08/2022</div>
                                            <div class="pxp-featured-posts-item-title">Mục tiêu của Hachinet</div>
                                            <div class="pxp-featured-posts-item-summary pxp-text-light mt-2">Xây dựng một tổ
                                                chức học tập,luôn cùng nhau học hỏi và nâng cao trình độ để vươn ra tầm quốc
                                                tế.</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="pxp-featured-posts-item pxp-cover"
                            style="background-image: url({{ asset('client/images/hachinet-banner3.png') }});">
                            <div class="pxp-hero-opacity"></div>
                            <div class="pxp-featured-posts-item-caption">
                                <div class="pxp-featured-posts-item-caption-content">
                                    <div class="row align-content-center justify-content-center">
                                        <div class="col-9 col-md-8 col-xl-7 col-xxl-6">
                                            <div class="pxp-featured-posts-item-date">10/09/2022</div>
                                            <div class="pxp-featured-posts-item-title">Hachinet định nghĩa hạnh phúc là gì?
                                            </div>
                                            <div class="pxp-featured-posts-item-summary pxp-text-light mt-2">Mang được ngoại
                                                tệ từ chất xám của người Việt về cho đất nước và xây dựng những hệ thống
                                                giúp ích cho xã hội.</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="pxp-featured-posts-item pxp-cover"
                            style="background-image: url({{ asset('client/images/hachinet-banner3.png') }});">
                            <div class="pxp-hero-opacity"></div>
                            <div class="pxp-featured-posts-item-caption">
                                <div class="pxp-featured-posts-item-caption-content">
                                    <div class="row align-content-center justify-content-center">
                                        <div class="col-9 col-md-8 col-xl-7 col-xxl-6">
                                            <div class="pxp-featured-posts-item-date">15/09/2022</div>
                                            <div class="pxp-featured-posts-item-title">Bạn sẽ được gì khi gia nhập vào
                                                Hachinet?</div>
                                            <div class="pxp-featured-posts-item-summary pxp-text-light mt-2">Luôn được tôn
                                                trọng, được giao trọng trách và được tôn vinh,quan trọng hơn là bạn được học
                                                hỏi để có khả năng kiếm tiền từ kiến thức của bạn.</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#pxp-blog-featured-posts-carousel"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#pxp-blog-featured-posts-carousel"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
            <div class="mt-4 mt-lg-5">
                <div class="row">
                    <div class="col-lg-7 col-xl-8 col-xxl-9">

                        <div class="row" id="showJobFilter">
                            @foreach ($newpaper as $each)
                                <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                    <div class="pxp-posts-card-1 pxp-has-border">
                                        <div class="pxp-posts-card-1-top">
                                            <div class="pxp-posts-card-1-top-bg">
                                                <div class="pxp-posts-card-1-image pxp-cover"><img
                                                        class="pxp-posts-card-1-image apxp-cover object-fit-cover"
                                                        src="  {{ asset('storage/' . $each->banner) }}"
                                                        style="width: 100%;border: 1px solid var(--pxpLightGrayColor);"
                                                        alt="How to Start Looking for a New Job"></div>
                                                <div class="pxp-posts-caSrd-1-info">
                                                    <div class="pxp-posts-card-1-info">
                                                        <div class="d-flex justify-content-between">
                                                            <div class="pxp-posts-card-1-date">
                                                                {{ date('d/m/Y', strtotime($each->created_at)) }}
                                                            </div>
                                                            {{-- <div class="pxp-posts-card-2-category"
                                                                style="font-size: 13px; color:black;"> <i
                                                                    class="fa fa-eye"></i> {{ $each->viewer }} Lượt xem
                                                            </div> --}}
                                                        </div>
                                                        <a class="pxp-jobs-card-1-company">
                                                            {{ $each->categories->name }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pxp-posts-card-1-content">
                                                <a href="{{ route('blog.detail', $each->slug) }}"
                                                    class="pxp-posts-card-1-title"
                                                    style=" text-overflow: ellipsis; -webkit-line-clamp:1 ; height: 25px; overflow: hidden; white-space: nowrap; ">{{ $each->title }}</a>
                                                <div class="pxp-posts-card-1-summary pxp-text-light"
                                                    style="  display: block;
                                                    display: -webkit-box;
                                                    height: 65px;
                                                    font-size: 14px;
                                                    -webkit-line-clamp: 3;
                                                    -webkit-box-orient: vertical;
                                                    overflow: hidden;
                                                    text-overflow: ellipsis;
                                                    overflow: hidden;">
                                                    {{ $each->highlight }}</div>
                                            </div>
                                        </div>
                                        <div class="pxp-posts-card-1-bottom">
                                            <div class="pxp-posts-card-1-cta">
                                                <a href="{{ route('blog.detail', $each->slug) }}">Đọc thêm<span
                                                        class="fa fa-angle-right"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="row mt-4 mt-lg-5 justify-content-between align-items-center">
                            <div class="col-auto">
                                <nav class="mt-3 mt-sm-0" aria-label="Blog articles pagination">
                                    {{ $newpaper->appends(request()->input())->links('client.pagination.index') }}
                                </nav>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-5 col-xl-4 col-xxl-3">
                        <div class="pxp-blogs-list-side-panel">

                            <h3 class="mt-3 mt-lg-4">Danh mục</h3>
                            <ul class="list-unstyled pxp-blogs-side-list mt-2 mt-lg-3">
                                @foreach ($categories as $item)
                                    <li><a style="cursor: pointer;" onclick="filterBlog({{ $item->id }})">{{ $item->name }}</a></li>
                                @endforeach
                            </ul>

                            <h3 class="mt-3 mt-lg-4">Tin tức thú vị</h3>
                            <ul class="list-unstyled pxp-blogs-side-articles-list mt-2 mt-lg-3">
                                @foreach ($newpaper_view as $each)
                                    <li class="mb-3">
                                        <a href="{{ route('blog.detail', $each->slug) }}"
                                            class="pxp-blogs-side-articles-list-item">
                                            <img class="pxp-blogs-side-articles-list-item-fig pxp-cover object-fit-cover"
                                                src="{{ asset('storage/' . $each->banner) }}">
                                            <div class="pxp-blogs-side-articles-list-item-content ms-3">
                                                <div class="pxp-blogs-side-articles-list-item-title">{{ $each->title }}
                                                </div>
                                                <div class="pxp-blogs-side-articles-list-item-date pxp-text-light">
                                                    {{ date('d/m/Y', strtotime($each->created_at)) }}</div>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>

                            <h3 class="mt-3 mt-lg-4">Tags</h3>
                            <div class="mt-2 mt-lg-3 pxp-blogs-side-tags">
                                @foreach ($skills as $each)
                                    <a style="color: black"
                                        href="{{ route('client.findjob.skill', $each->slug) }}">{{ $each->name }}</a>
                                @endforeach

                                @foreach ($categories as $item)
                                    <a style="color: black"
                                        href="{{ route('abouts', $item->slug) }}">{{ $item->name }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection
@push('js')
    <script>
        function filterBlog(id) {
            // alert("{!! route('blog.filterBlog', '') !!}/" + id)
            $.ajax({
                type: "GET",
                url: "{!! route('blog.filterBlog', '') !!}/" + id,
                data: {
                    id: id,
                },
                dataType: "JSON",
                success: function(response) {
                    console.log(response.data);
                    $('#showJobFilter').html('')
                    $('.pagination').html('')
                    response.data.forEach((element, index) => {
                        var url = "{{ route('blog.detail', '') }}/" + element.slug
                        $('#showJobFilter').append(`<div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                    <div class="pxp-posts-card-1 pxp-has-border">
                                        <div class="pxp-posts-card-1-top">
                                            <div class="pxp-posts-card-1-top-bg">
                                                <div class="pxp-posts-card-1-image pxp-cover"><img
                                                        class="pxp-posts-card-1-image apxp-cover object-fit-cover"
                                                        src="${element.banner}"
                                                        style="width: 100%;border: 1px solid var(--pxpLightGrayColor);"
                                                        alt="How to Start Looking for a New Job"></div>
                                                <div class="pxp-posts-caSrd-1-info">
                                                    <div class="pxp-posts-card-1-info">
                                                        <div class="d-flex justify-content-between">
                                                            <div class="pxp-posts-card-1-date">
                                                                ${ element.created_at }
                                                            </div>
                                                        </div>
                                                        <a class="pxp-jobs-card-1-company">
                                                            ${ element.categories.name }</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pxp-posts-card-1-content">
                                                <a href="{{ route('blog.detail', $each->slug) }}"
                                                    class="pxp-posts-card-1-title"
                                                    style=" text-overflow: ellipsis; -webkit-line-clamp:1 ; height: 25px; overflow: hidden; white-space: nowrap; ">${ element.title }</a>
                                                <div class="pxp-posts-card-1-summary pxp-text-light"
                                                    style="  display: block;
                                                    display: -webkit-box;
                                                    height: 65px;
                                                    font-size: 14px;
                                                    -webkit-line-clamp: 3;
                                                    -webkit-box-orient: vertical;
                                                    overflow: hidden;
                                                    text-overflow: ellipsis;
                                                    overflow: hidden;">
                                                    ${ element.highlight }</div>
                                            </div>
                                        </div>
                                        <div class="pxp-posts-card-1-bottom">
                                            <div class="pxp-posts-card-1-cta">
                                                <a href="${url}">Đọc thêm<span
                                                        class="fa fa-angle-right"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>`)
                    });
                    // let pagination = response.jobs.links;
                    // pagination[0]['label'] = '&laquo;';
                    // pagination.at(-1)['label'] = '&raquo;';
                    // if (response.jobs.data.length > 0) {
                    //     pagination.forEach((each, index) => {
                    //         $('.pagination').append(
                    //             `<li class="page-item ${each.active ? 'active' : ''}"><button class="${each.active ? 'active' : ''} page-link" data-page="${ each.url }">${ each.label }</button></li>`
                    //         )
                    //     });
                    // }
                }
            });
        }
    </script>
@endpush
