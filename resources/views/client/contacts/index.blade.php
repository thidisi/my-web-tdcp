@extends('client.layout.layout')
@section('title', 'Trang tuyển dụng lập trình viên')
@section('content')
    <link rel="stylesheet" href="{{ asset('assets/css/toast.min.css') }}">
    <style>
        .text-error {
            border: 1px solid #dc3545 !important;
        }
    </style>
    <section class="mt-100 pxp-no-hero">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">Chúng tôi rất muốn nghe từ bạn</h2>
            <p class="pxp-text-light text-center">Liên lạc với chúng tôi</p>

            <div class="row mt-4 mt-md-5 justify-content-center pxp-animate-in pxp-animate-in-top">
                <div class="col-lg-4 col-xxl-3 pxp-contact-card-1-container">
                    <a target="_blank" href="https://goo.gl/maps/dYyGxLjfmoGJfCu89" class="pxp-contact-card-1">
                        <div class="pxp-contact-card-1-icon-container">
                            <div class="pxp-contact-card-1-icon">
                                <span class="fa fa-globe"></span>
                            </div>
                        </div>
                        <div class="pxp-contact-card-1-title">Ha Noi, Viet Nam</div>
                    </a>
                </div>
                <div class="col-lg-4 col-xxl-3 pxp-contact-card-1-container">
                    <a href="tel:2462900388" class="pxp-contact-card-1">
                        <div class="pxp-contact-card-1-icon-container">
                            <div class="pxp-contact-card-1-icon">
                                <span class="fa fa-phone"></span>
                            </div>
                        </div>
                        <div class="pxp-contact-card-1-title">(+84) 2462-900-388</div>
                    </a>
                </div>
                <div class="col-lg-4 col-xxl-3 pxp-contact-card-1-container">
                    <a href="mailto:contact@hachinet.com" class="pxp-contact-card-1">
                        <div class="pxp-contact-card-1-icon-container">
                            <div class="pxp-contact-card-1-icon">
                                <span class="fa fa-envelope-o"></span>
                            </div>
                        </div>
                        <div class="pxp-contact-card-1-title">hello@hachinet.com</div>
                    </a>
                </div>
            </div>


            <div class="row mt-100 justify-content-center pxp-animate-in pxp-animate-in-top">
                <div class="col-lg-6 col-xxl-4">
                    <div class="pxp-contact-us-form pxp-has-animation pxp-animate">
                        <h2 class="pxp-section-h2 text-center">Liên hệ chúng tôi</h2>
                        <form id="formContact" class="mt-4">
                            <div class="mb-3">
                                <label for="contact-us-name" class="form-label">Họ và tên</label>
                                <input type="text" name="name" class="form-control" id="contact-us-name"
                                    placeholder="Nhập tên của bạn">
                                <div class="text-center text-danger">

                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="contact-us-email" class="form-label">Email</label>
                                <input type="email" name="email" class="form-control" id="contact-us-email"
                                    placeholder="Nhập địa chỉ email của bạn">
                                <div class="text-center text-danger">

                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="contact-us-message" class="form-label">Tin nhắn</label>
                                <textarea class="form-control" name="content" id="contact-us-message" placeholder="Nhập nội dung tin nhắn của bạn..."></textarea>
                                <div class="text-center text-danger">

                                </div>
                            </div>
                            <button class="btn rounded-pill pxp-section-cta col-12 d-block seen-button" type="button">Gửi
                                tin nhắn</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('js')
    <script src="{{ asset('assets/libs/toast/toast.min.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <style>
        .form-control.is-invalid,
        .was-validated .form-control:invalid {
            border-color: #dc3545 !important;
        }
    </style>
    <script type="text/javascript">
        $(document).on("click", ".seen-button", function() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "{{ route('client.contact.store') }}",
                data: {
                    name: $("input[name=name]").val(),
                    email: $("input[name=email]").val(),
                    content: $("textarea[name=content]").val(),
                },
                dataType: "JSON",
                success: function(response) {
                    $("input[name=name]").val(''),
                        $("input[name=email]").val(''),
                        $("textarea[name=content]").val('')
                    Swal.fire(
                        'Thông báo',
                        'Gửi liên hệ thành công!',
                        'success'
                    )


                },
                error: function(response) {
                    $("input[name=name]").removeClass("text-error")
                    $("input[name=email]").removeClass("text-error")
                    $("textarea[name=content]").removeClass("text-error")
                    if (response.responseJSON.errors.name) {
                        $("input[name=name]").addClass("text-error")
                    }
                    if (response.responseJSON.errors.content) {
                        $.toast({
                            heading: 'Thông báo lỗi!',
                            text: response.responseJSON.errors.content,
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: 'error'
                        });
                        $("textarea[name=content]").addClass("text-error")
                    }
                    if (response.responseJSON.errors.email) {
                        $.toast({
                            heading: 'Thông báo lỗi!',
                            text: response.responseJSON.errors.email,
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: 'error'
                        });
                        $("input[name=email]").addClass("text-error")
                    }
                }
            });
        })
    </script>
@endpush
