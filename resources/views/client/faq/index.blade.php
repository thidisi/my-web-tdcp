@extends('client.layout.layout')
@section('title', 'Trang tuyển dụng lập trình viên')
@section('content')
<section class="mt-100 pxp-no-hero">
    <div class="pxp-container">
        <h2 class="pxp-section-h2 text-center">NGƯỜI HACHINET</h2>
        <p class="pxp-text-light text-center">Giải đáp các thắc mắc</p>

        <div class="row mt-4 mt-lg-5 justify-content-center">
            <div class="col-xxl-7">
                <div class="accordion pxp-faqs-accordion" id="pxpFAQsAccordion">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="pxpFAQsHeader1">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#pxpCollapseFAQs1" aria-expanded="false"
                                aria-controls="pxpCollapseFAQs1">
                                01. Mục tiêu của Hachinet là gì??
                            </button>
                        </h2>
                        <div id="pxpCollapseFAQs1" class="accordion-collapse collapse"
                            aria-labelledby="pxpFAQsHeader1" data-bs-parent="#pxpFAQsAccordion">
                            <div class="accordion-body">
                                Xây dựng một tổ chức học tập,luôn cùng nhau học hỏi và nâng cao trình độ để vươn ra tầm quốc tế.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="pxpFAQsHeader2">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#pxpCollapseFAQs2" aria-expanded="false"
                                aria-controls="pxpCollapseFAQs2">
                                02. Làm thế nào để trở thành người Hachinet ?
                            </button>
                        </h2>
                        <div id="pxpCollapseFAQs2" class="accordion-collapse collapse"
                            aria-labelledby="pxpFAQsHeader2" data-bs-parent="#pxpFAQsAccordion">
                            <div class="accordion-body">
                                Hãy học hỏi và luôn nâng cao trình độ,cơ hội sẽ đến với những người thật sự chăm chỉ và được lựa chọn là chiến binh.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="pxpFAQsHeader3">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#pxpCollapseFAQs3" aria-expanded="false"
                                aria-controls="pxpCollapseFAQs3">
                                03. Hachinet định nghĩa hạnh phúc là gì?
                            </button>
                        </h2>
                        <div id="pxpCollapseFAQs3" class="accordion-collapse collapse"
                            aria-labelledby="pxpFAQsHeader3" data-bs-parent="#pxpFAQsAccordion">
                            <div class="accordion-body">
                                Mang được ngoại tệ từ chất xám của người Việt về cho đất nước và xây dựng những hệ thống giúp ích cho xã hội.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="pxpFAQsHeader4">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#pxpCollapseFAQs4" aria-expanded="false"
                                aria-controls="pxpCollapseFAQs4">
                                04. Bạn sẽ được gì khi gia nhập vào Hachinet?
                            </button>
                        </h2>
                        <div id="pxpCollapseFAQs4" class="accordion-collapse collapse"
                            aria-labelledby="pxpFAQsHeader4" data-bs-parent="#pxpFAQsAccordion">
                            <div class="accordion-body">
                                Luôn được tôn trọng, được giao trọng trách và được tôn vinh,quan trọng hơn là bạn được học hỏi để có khả năng kiếm tiền từ kiến thức của bạn.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="pxpFAQsHeader5">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#pxpCollapseFAQs5" aria-expanded="false"
                                aria-controls="pxpCollapseFAQs5">
                                05. Hachinet có làm việc cuối tuần không?
                            </button>
                        </h2>
                        <div id="pxpCollapseFAQs5" class="accordion-collapse collapse"
                            aria-labelledby="pxpFAQsHeader5" data-bs-parent="#pxpFAQsAccordion">
                            <div class="accordion-body">
                                Đương nhiên là không, thông thường cuối tuần là để nghỉ ngơi và dành thời gian cho gia đình, tuy nhiên thi thoảng do hoàn cảnh bạn còn phải làm việc thông đêm.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="pxpFAQsHeader6">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#pxpCollapseFAQs6" aria-expanded="false"
                                aria-controls="pxpCollapseFAQs6">
                                06. Có được chơi game tại Hachinet?
                            </button>
                        </h2>
                        <div id="pxpCollapseFAQs6" class="accordion-collapse collapse"
                            aria-labelledby="pxpFAQsHeader6" data-bs-parent="#pxpFAQsAccordion">
                            <div class="accordion-body">
                                Trong giờ làm việc thì không nhé, làm ra làm chơi ra chơi.Ngoài giờ thì thường hay bị đồng nghiệp rủ rê game và bóng đá, clb Japanese. Vậy nên đừng quá sốc khi bị "gạ".
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
