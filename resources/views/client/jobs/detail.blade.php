@extends('client.layout.layout')
@section('title', $job->title)
@section('meta')
    @php $site_name = $job->job_details->skills[0]; @endphp
    <meta property="og:locale" content="vi">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ route('client.job.detail', $job->slug) }}">
    <meta property="og:title" content="Hachinet tuyển dụng {{ $job->title }}">
    <meta property="og:description"
        content="Mức lương: {{ $jobDetail->salary }}. Địa chỉ làm việc: {{ $jobDetail->address }}.">
    <meta property="og:site_name" content="{{ $site_name['slug'] }}">
    <meta property="og:image" content="{{ $image }}">
@endsection
@section('content')
    <section>
        <div class="pxp-container">
            <div class="pxp-single-job-cover pxp-cover"
                style="background-image: url({{ asset('/client/images/category' . '/' . $job->category_job . '.jpg') }});">
            </div>
            <figure class="company-logo"><img class="pxp-single-job-cover-logo" src="{{ $image }}" alt="image">
            </figure>

            <div class="pxp-single-job-content mt-4 mt-lg-5">
                <div class="row">
                    <div class="col-lg-7 col-xl-8 col-xxl-9">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-xl-8 col-xxl-6">
                                <h1>{{ $job->title }}</h1>
                                <div class="pxp-single-job-company-location">
                                    <a class="pxp-jobs-card-1-location">
                                        <span class="fa fa-globe"></span>{{ $job->job_details->address }},
                                        {{ $job->job_details->province_name }}
                                    </a>
                                </div>
                            </div>
                            <div class="col-auto">
                                <div class="pxp-single-job-options mt-4 col-xl-0">
                                    <button class="btn pxp-single-job-save-btn"><span class="fa fa-heart-o"></span></button>
                                    <div class="dropdown ms-2">
                                        <button class="btn pxp-single-job-share-btn dropdown-toggle" type="button"
                                            id="socialShareBtn" data-bs-toggle="dropdown" aria-expanded="false">
                                            <span class="fa fa-share-alt">
                                            </span></button>
                                        <ul class="dropdown-menu pxp-single-job-share-dropdown"
                                            aria-labelledby="socialShareBtn">
                                            <li>
                                                <div class="fb-share-button" data-href="{{ url()->current() }}"
                                                    data-layout="button_count" data-size="large"><a class="dropdown-item"
                                                        target="_blank" href="" class="fb-xfbml-parse-ignore"><span
                                                            class="fa fa-facebook"></span>
                                                        Facebook</a></div>
                                            </li>
                                            <li>
                                                <a class="dropdown-item"
                                                    href="https://twitter.com/share?url={{ url()->current() }}&amp;text=&amp;hashtags=tuyendunglaptrinh"
                                                    target="_blank">
                                                    <span class="fa fa-twitter"></span>
                                                    Twitter
                                                </a>
                                            </li>
                                            <li>
                                                <a class="dropdown-item"
                                                    href="https://www.pinterest.com/pin/create/button/?url={{ url()->current() }}&media={{ $image }}&description="
                                                    target="_blank">
                                                    <span class="fa fa-pinterest"></span>
                                                    Pinterest
                                                </a>
                                            </li>
                                            <li>
                                                <a class="dropdown-item"
                                                    href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ url()->current() }}"
                                                    target="_blank">
                                                    <span class="fa fa-linkedin"></span>
                                                    LinkedIn</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <button class="btn ms-2 pxp-single-job-apply-btn rounded-pill" data-bs-toggle="modal"
                                        href="#pxp-signin-modal" role="button">Ứng tuyển</button>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4 justify-content-between align-items-center">
                            <div class="col-6">
                                <a href="/tuyen-dung-lap-trinh?category_job={{ $job->category_job }}"
                                    class="pxp-single-job-category category_job">
                                    <div class="pxp-jobs-card-1-category-icon category_job-active">
                                        <span
                                            class="fa @foreach ($category_jobs as $index => $each)
                                            @if ('business-development' == $index)
                                                fa-pie-chart
                                            @elseif('marketing-communication' == $index)
                                                fa-bullhorn
                                            @elseif('customer-service' == $index)
                                                fa-comments-o
                                            @elseif('human-resources' == $index)
                                                fa-address-card-o
                                            @elseif('project-management' == $index)
                                                fa-calendar-o
                                                @elseif('software-engineering' == $index)
                                                fa-terminal
                                            @endif @endforeach">
                                        </span>
                                    </div>
                                    <div class="pxp-jobs-card-1-category-label category_job-active">
                                        {{ $category_jobs[$job->category_job] }}
                                    </div>
                                </a>
                            </div>
                            <div class="col-auto">
                                <div class="pxp-single-job-date pxp-text-light"> Hạn
                                    {{ $jobDetail->deadline }}</div>
                            </div>
                        </div>

                        <div class="pxp-single-job-content-details mt-4 mt-lg-5">
                            <h4>MÔ TẢ CÔNG VIỆC</h4>
                            <p>
                            <ul>
                                {!! nl2br(e($jobDetail->description)) !!}
                            </ul>
                            </p>
                            <div class="mt-4">
                                <h4>QUYỀN LỢI</h4>
                                <ul>
                                    {!! nl2br(e($jobDetail->benefit)) !!}
                                </ul>
                            </div>
                            <div class="mt-4">
                                <h4>YÊU CẦU</h4>
                                <ul>
                                    {!! nl2br(e($jobDetail->require)) !!}
                                </ul>
                            </div>


                            <div class="mt-4 mt-lg-5">
                                <a class="btn rounded-pill pxp-section-cta" data-bs-toggle="modal" href="#pxp-signin-modal"
                                    role="button">Ứng tuyển</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-xl-4 col-xxl-3">
                        <div class="pxp-single-job-side-panel mt-5 mt-lg-0">
                            <div>
                                <div class="pxp-single-job-side-info-label pxp-text-light">Số lượng</div>
                                <div class="pxp-single-job-side-info-data">{{ $jobDetail->slot }}</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-label pxp-text-light">Cấp bậc</div>
                                <div class="pxp-single-job-side-info-data">{{ $jobDetail->level }}</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-label pxp-text-light">Kinh nghiệm</div>
                                <div class="pxp-single-job-side-info-data">{{ $jobDetail->exp }}</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-label pxp-text-light">Hình thức làm việc</div>
                                <div class="pxp-single-job-side-info-data">{{ $getType[$jobDetail->job_type] }}</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-label pxp-text-light">Mức lương</div>
                                <div class="pxp-single-job-side-info-data">{{ $jobDetail->salary }}</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-label pxp-text-light">Kỹ năng</div>
                                <div class="mt-2 mt-lg-3 pxp-blogs-side-tags">
                                    @foreach ($job->job_details->skills as $each)
                                        <a style="color: black"
                                            href="{{ route('client.findjob.skill', $each['slug']) }}">{{ $each['name'] }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div id="AboutHachinet" class="mt-3 mt-lg-4 pxp-single-job-side-panel">
                            <div class="pxp-single-job-side-company">
                                <div class="pxp-single-job-side-company-logo pxp-cover"><a><img
                                            class="pxp-single-job-side-company-logo pxp-cover"
                                            src="{{ asset('/client/images/Hachinet Software Long (Yellow).png') }}"
                                            alt="hachinet tuyen dung"
                                            style="height: 60px;
                                    width: auto;"></a>
                                </div>

                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-label pxp-text-light">Lĩnh vực phát triển</div>
                                <div class="pxp-single-job-side-info-data">Phần mềm</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-label pxp-text-light">Quy mô</div>
                                <div class="pxp-single-job-side-info-data">50-500</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-label pxp-text-light">Ngày thành lập</div>
                                <div class="pxp-single-job-side-info-data">11/01/2018</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-label pxp-text-light">Điện thoại</div>
                                <div class="pxp-single-job-side-info-data">(+84) 24-6290-0388</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-label pxp-text-light">Email</div>
                                <div class="pxp-single-job-side-info-data">hello@hachinet.com</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-label pxp-text-light">Địa điểm</div>
                                <div class="pxp-single-job-side-info-data">2A - 27A3, 234 Phạm Văn Đồng</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-label pxp-text-light">Website</div>
                                <div class="pxp-single-job-side-info-data"><a target="_blank"
                                        href="https://hachinet.com">https://hachinet.com</a></div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-job-side-info-data">
                                    <ul class="list-unstyled pxp-single-job-side-info-social">
                                        <li><a target="_blank" href="https://www.facebook.com/hr.hachinet"><span
                                                    class="fa fa-facebook"></span></a></li>
                                        <li><a target="_blank" href="https://twitter.com/HachinetJ"><span
                                                    class="fa fa-twitter"></span></a></li>
                                        <li><a target="_blank" href="https://www.instagram.com/hachinet.japan/"><span
                                                    class="fa fa-instagram"></span></a></li>
                                        <li><a target="_blank"
                                                href="https://www.linkedin.com/company/hachinet-software1/"><span
                                                    class="fa fa-linkedin"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-100">
        <div class="pxp-container">
            <h2 class="pxp-subsection-h2">Công việc cùng kỹ năng</h2>
            <p class="pxp-text-light">Những công việc cùng kỹ năng mà bạn có thể quan tâm</p>
            <div class="row mt-3 mt-md-4 pxp-animate-in pxp-animate-in-top pxp-in">
                @foreach ($jobRelated as $item)
                    <div class="col-xl-6 pxp-jobs-card-2-container">

                        <div class="pxp-jobs-card-2 pxp-has-border">

                            <div class="pxp-jobs-card-2-top">
                                <figure class="company-logo"><img class="pxp-jobs-card-2-company-logo"
                                        src="{{ $image }}" alt="image">
                                </figure>
                                <div class="pxp-jobs-card-2-info">
                                    <a href="{{ route('client.job.detail', $item->slug) }}"
                                        class="pxp-jobs-card-2-title">
                                        {{ $item->title }}</a>
                                    <div class="pxp-jobs-card-2-details">
                                        <a class="pxp-jobs-card-2-location">
                                            <span class="fa fa-globe"></span>{{ $item->job_details->address }}
                                        </a>
                                        <div class="pxp-jobs-card-2-type">{{ $getType[$item->job_details->job_type] }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pxp-jobs-card-2-bottom">
                                <a class="pxp-jobs-card-2-category">
                                    <div class="pxp-jobs-card-1-company">Lương {{ $item->salary->name }}</div>
                                </a>
                                <div class="pxp-jobs-card-2-bottom-right">
                                    <span class="pxp-jobs-card-2-date pxp-text-light">Hạn
                                        {{ date('d/m/Y', strtotime($item->job_details->deadline)) }}</span>

                                </div>

                            </div>
                        </div>

                    </div>
                @endforeach
            </div>

        </div>
    </section>

    <div class="modal fade pxp-user-modal modal-cv-p" id="pxp-signin-modal" aria-hidden="true"
        aria-labelledby="signinModal" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="pxp-user-modal-fig text-center">
                        <img src="{{ asset('/client/images/Hachinet Software Long (Yellow).png') }}" alt="Sign in">
                    </div>
                    <h5 class="modal-title text-center mt-4" id="signinModal">{{ $job->title }}</h5>
                    <form class="mt-4" id="formValidate" data-route="{{ route('apply.store') }}"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="form-floating mb-3">
                            <input type="text" name="name" class="form-control" id="pxp-signin-name"
                                placeholder="Tên đầy đủ *" style="font-weight:400">
                            <label for="pxp-signin-name">Tên đầy đủ *</label>
                            <span class="fa fa-user-o"></span>
                        </div>
                        <input type="hidden" value="{{ $jobDetail->job_id }}" name="job_id">
                        <input type="hidden" name="source">
                        <input type="hidden" name="user_id">

                        <div class="form-floating mb-3">
                            <input type="email" name="email" class="form-control" id="pxp-signin-email"
                                placeholder="Email *" style="font-weight:400">
                            <label for="pxp-signin-email">Email *</label>
                            <span class="fa fa-envelope-o"></span>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="text" name="phone" class="form-control" id="pxp-signin-email"
                                placeholder="Điện thoại * " style="font-weight:400">
                            <label for="pxp-signin-email">Điện thoại *</label>
                            <span class="fa fa-phone"></span>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                            <select name="skills" class="form-control p-3" style="font-weight:400">
                                <option hidden disabled selected>--- Kỹ năng chính (*) ---</option>
                                @foreach ($skillAll as $skill)
                                    <option style="font-weight:350" value="{{ $skill->id }}">{{ $skill->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-floating mb-3" id="file_cv"
                            style="margin-top: 17px;border-radius:25px; border:1px solid #dedede; background:white; height:54px;">
                            <div class="col-lg-12">
                                <label for="files" class="custom-file-label" id="apply_candidate"
                                    style="padding: 0 20px;
                                    width: 100%;
                                    line-height: 52px;">CV
                                    của
                                    bạn*</label>

                                <input hidden id="files" type="file" class="form-control position-relative"
                                    multiple accept="application/pdf" name="file" aria-label="File browser example">
                                <label id="btn-cv" for="files" class="position-absolute end-0 top-0 bottom-0"
                                    style="cursor: pointer; padding: 6px 20px 0px; background: #FFB701;border-radius:25px; margin-top:7px;margin-bottom:7px;margin-right:7px; color:aliceblue">Chọn</label>
                            </div>

                        </div>
                        <button class="btn rounded-pill apply-button pxp-modal-cta" style="width: 100%">Ứng tuyển</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <link rel="stylesheet" href="{{ asset('assets/css/toast.min.css') }}">
    <script src="{{ asset('assets/libs/toast/toast.min.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        var dataInfo = {!! $dataInfo !!};
        if (dataInfo != null) {
            $("input[name=source]").val(dataInfo['source'])
            $("input[name=user_id]").val(dataInfo['id'])
        }
        $(document).ready(function() {
            var href = 'https://www.facebook.com/sharer/sharer.php?u=' + $('.fb-share-button').data('href') +
                '&src=sdkpreparse';
            if ($(window).width() < 769) {
                $('.fb-share-button').find('a').attr('href', 'fb://facewebmodal/f?href=' + href);
            } else {
                $('.fb-share-button').find('a').attr('href', href);
            }
        });
        // gọi vào api nếu null thì không bỏ vào data tạo 1 cột kiểu text null trong apply
        $(document).on("click", ".apply-button", function(e) {
            e.preventDefault();
            var form_data = new FormData(document.getElementById("formValidate"));
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: $("#formValidate").data("route"),
                type: "post",
                dataType: "json",
                beforeSend: function() {
                    $('.lds-ellipsis').show()
                },
                complete: function() {
                    $('.lds-ellipsis').hide()
                },
                async: true,
                processData: false,
                contentType: false,
                data: form_data,
                success: function(response) {
                    $.toast({
                        heading: 'Thông báo',
                        text: 'Gửi CV thành công!',
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'success'
                    });
                    $("body").css('overflow', 'scroll');
                    $("#formValidate")[0].reset();
                    $("input[name=name]").removeClass("text-error")
                    $("input[name=email]").removeClass("text-error")
                    $("input[name=phone]").removeClass("text-error")
                    $("#file_cv").removeClass("text-error")
                    $(".nice-select").removeClass("text-error")
                    $(".modal-cv-p").toggle();
                    $(".modal-backdrop").hide();
                    $("#apply_candidate").html('CV của bạn<span style="color: red;">*</span>');
                    $("#pxp-signin-modal").hide();
                },
                error: function(response) {
                    $('.lds-ellipsis').hide()
                    if (response.responseJSON.mess) {
                        $.toast({
                            heading: 'Thông báo lỗi!',
                            text: response.responseJSON.mess,
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: 'error'
                        });
                    };
                    $("input[name=name]").removeClass("text-error")
                    $("input[name=email]").removeClass("text-error")
                    $("input[name=phone]").removeClass("text-error")
                    $("#file_cv").removeClass("text-error")
                    $(".nice-select").removeClass("text-error")
                    if (response.responseJSON.errors.name) {
                        $("input[name=name]").addClass("text-error")
                    }
                    if (response.responseJSON.errors.skills) {
                        $.toast({
                            heading: 'Thông báo lỗi!',
                            text: response.responseJSON.errors.skills,
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: 'error'
                        });
                        $("select[name=skills]").addClass("text-error")
                    }
                    if (response.responseJSON.errors.email) {
                        $.toast({
                            heading: 'Thông báo lỗi!',
                            text: response.responseJSON.errors.email,
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: 'error'
                        });
                        $("input[name=email]").addClass("text-error")
                    }
                    if (response.responseJSON.errors.phone) {
                        $.toast({
                            heading: 'Thông báo lỗi!',
                            text: response.responseJSON.errors.phone,
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: 'error'
                        });
                        $("input[name=phone]").addClass("text-error")
                    }
                    if (response.responseJSON.errors.file) {
                        $.toast({
                            heading: 'Thông báo lỗi!',
                            text: response.responseJSON.errors.file,
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: 'error'
                        });
                        $("#file_cv").addClass("text-error")
                    }
                }
            });
        })
        // hiển thị tên file
        $(document).on('change', '#files', function() {
            var file = $('#files')[0].files[0]
            if (file) {
                $('.custom-file-label').html(file.name.slice(0, 48) + '...');
                $('#btn-cv').hide();
            }
        });
    </script>
@endpush
