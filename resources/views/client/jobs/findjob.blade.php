@extends('client.layout.layout')
@section('title', 'Trang tuyển dụng lập trình viên')
@section('content')
    <section class="pxp-page-header-simple" style="background-color: var(--pxpMainColorLight);">
        <div class="pxp-container">
            <h1>Tìm kiếm việc làm</h1>
            <div class="pxp-hero-subtitle pxp-text-light">Tìm kiếm cơ hội nghề nghiệp mơ ước của bạn
            </div>
            <div class="pxp-hero-form pxp-hero-form-round pxp-large mt-3 mt-lg-4">
                <form action="{{ route('client.findjob.url') }}" method="post" class="row gx-3 align-items-center">
                    @csrf
                    <div class="col-12 col-lg">
                        <div class="input-group mb-3 mb-lg-0">
                            <span class="input-group-text"><span class="fa fa-search"></span></span>
                            <input type="search" name="skill" class="form-control" placeholder="Từ khóa...."
                                value="{{ @$findJob['skill'] }}">
                        </div>
                    </div>
                    <div class="col-12 col-lg pxp-has-left-border">
                        <div class="input-group mb-3 mb-lg-0 flex-nowrap">
                            <span class="input-group-text"><span class="fa fa-globe"></span></span>
                            <select class="select2 form-control select-address" name="address"
                                data-placeholder=" -- Địa chỉ -- ">
                                <option></option>
                                <option value=" ">Tất cả</option>
                                @foreach ($address as $item)
                                    <option value="{{ $item->slug }}" @if (@$findJob['address'] == $item->slug) selected @endif>
                                        {{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-lg pxp-has-left-border">
                        <div class="input-group mb-3 mb-lg-0 flex-nowrap">
                            <span class="input-group-text"><span class="fa fa-folder-o"></span></span>
                            <select class="select2 form-control select-address" id="category_job" name="category_job"
                                data-placeholder=" -- Danh mục -- ">
                                <option></option>
                                <option value=" ">Tất cả</option>
                                @foreach ($category_job as $key => $item)
                                    <option value="{{ $key }}" @if (@$findJob['category_job'] == $key) selected @endif>
                                        {{ $item }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-lg-auto">
                        <button>Tìm kiếm</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <section class="mt-100">
        <div class="pxp-container">
            <div class="row">
                <div class="col-lg-5 col-xl-4 col-xxl-3">
                    <div class="pxp-jobs-list-side-filter">
                        <div class="pxp-list-side-filter-header d-flex d-lg-none">
                            <div class="pxp-list-side-filter-header-label">Filter Jobs</div>
                            <a role="button"><span class="fa fa-sliders"></span></a>
                        </div>
                        <div class="mt-4 mt-lg-0 d-lg-block pxp-list-side-filter-panel">
                            <h3>Loại công việc</h3>
                            <div class="list-group mt-2 mt-lg-3">
                                @foreach ($getType as $key => $each)
                                    <label
                                        class="list-group-item d-flex justify-content-between align-items-center  @foreach ($jobs as $item) @if ($key == $item->job_details->job_type && !empty($job))  pxp-checked @endif @endforeach">
                                        <span class="d-flex">
                                            <input class="filterJob form-check-input me-2" name="type" type="checkbox"
                                                autocomplete='off'
                                                @foreach ($jobs as $item) @if ($key == $item->job_details->job_type && !empty($job)) checked @endif @endforeach
                                                value="{{ $key }}">
                                            {{ $each }}
                                        </span>
                                        <span class="badge rounded-pill">{{ $getCount['type'][$key] }}</span>
                                    </label>
                                @endforeach
                            </div>

                            <h3 class="mt-3 mt-lg-4">Kinh nghiệm</h3>
                            <div class="list-group mt-2 mt-lg-3">
                                @foreach ($getExp as $key => $each)
                                    <label
                                        class="list-group-item d-flex justify-content-between align-items-center @foreach ($jobs as $item) @if ($key == $item->job_details->exp && !empty($job))  pxp-checked @endif @endforeach">
                                        <span class="d-flex">
                                            <input class="filterJob form-check-input me-2" name="exp" type="checkbox"
                                                autocomplete='off'
                                                @foreach ($jobs as $item) @if ($key == $item->job_details->exp && !empty($job)) checked @endif @endforeach
                                                value="{{ $key }}">
                                            {{ $each }}
                                        </span>
                                        <span class="badge rounded-pill">{{ $getCount['exp'][$key] }}</span>
                                    </label>
                                @endforeach
                            </div>

                            <h3 class="mt-3 mt-lg-4">Mức lương</h3>
                            <div class="list-group mt-2 mt-lg-3">
                                @foreach ($getSalary as $key => $each)
                                    <label
                                        class="list-group-item d-flex justify-content-between align-items-center @foreach ($jobs as $item) @if ($each->id == $item->job_details->salary && !empty($job))  pxp-checked @endif @endforeach">
                                        <span class="d-flex">
                                            <input class="filterJob form-check-input me-2" name="salary" type="checkbox"
                                                autocomplete='off'
                                                @foreach ($jobs as $item) @if ($each->id == $item->job_details->salary && !empty($job)) checked @endif @endforeach
                                                value="{{ $each->id }}">
                                            {{ $each->name }}
                                        </span>
                                        <span class="badge rounded-pill">{{ $getCount['salary'][$each->slug] }}</span>
                                    </label>
                                @endforeach
                            </div>
                            <h3 class="mt-3 mt-lg-4">Kỹ năng</h3>
                            <div class="list-group mt-2 mt-lg-3">
                                <div class="mt-2 mt-lg-3 pxp-blogs-side-tags">
                                    @foreach ($getSkill as $each)
                                        <a style="color: black"
                                            href="{{ route('client.findjob.skill', $each->slug) }}">{{ $each->name }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-xl-8 col-xxl-9">
                    <div class="pxp-jobs-list-top mt-4 mt-lg-0">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-auto">
                                <h2><span class="pxp-text-light">Hiển thị</span> <span
                                        id="countJobFilter">{{ count($jobs) }}</span> <span class="pxp-text-light">việc
                                        làm</span>
                                </h2>
                            </div>
                            <div class="col-auto">
                                <select class="form-select filterJob" name="timeSearch">
                                    <option value="1">Mới nhất</option>
                                    <option value="2">Cũ nhất</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    @php
                        $category_jobs = config('const.category_job');
                        foreach ($category_jobs as $it) {
                            $category_job['name'][] = $it;
                        }
                    @endphp
                    <div class="row" id="showJobFilter">
                        @foreach ($jobs as $item)
                            <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-jobs-card-1-container">
                                <div class="pxp-jobs-card-1 pxp-has-border">
                                    <div class="pxp-jobs-card-1-top">
                                        <a class="pxp-jobs-card-1-category">
                                            <div class="pxp-jobs-card-1-category-icon">
                                                <span
                                                    class="fa @foreach ($category_job as $index => $each)
                                                        @if ($item->category_job == $index)
                                                        @if ('business-development' == $item->category_job)
                                                            fa-pie-chart
                                                        @elseif('marketing-communication' == $item->category_job)
                                                            fa-bullhorn
                                                        @elseif('customer-service' == $item->category_job)
                                                            fa-comments-o
                                                        @elseif('human-resources' == $item->category_job)
                                                            fa-address-card-o
                                                        @elseif('project-management' == $item->category_job)
                                                            fa-calendar-o
                                                            @elseif('software-engineering' == $item->category_job)
                                                            fa-terminal

                                                        @endif
                                                        @endif @endforeach">
                                                </span>
                                            </div>
                                            <div class="pxp-jobs-card-1-category-label">
                                                {{ $category_job[$item->category_job] }}</div>
                                        </a>

                                        <a href="{{ route('client.job.detail', $item->slug) }}"
                                            class="pxp-jobs-card-1-title">{{ $item->title }}</a>
                                        <div class="pxp-jobs-card-1-details">
                                            <a class="pxp-jobs-card-1-location">
                                                <span class="fa fa-globe"></span>{{ $item->job_details->address }}
                                            </a>
                                            <div class="pxp-jobs-card-1-type">{{ $getType[$item->job_details->job_type] }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pxp-jobs-card-1-bottom">
                                        <div class="pxp-jobs-card-1-bottom-left">
                                            <div class="pxp-jobs-card-1-date pxp-text-light">Hạn
                                                {{ $item->job_details->deadline }}</div>
                                            <a class="pxp-jobs-card-1-company">Lương
                                                {{ $item->salary->name }}</a>
                                        </div>
                                        @php
                                            if (count($item->skills) > 0 && $item->skills['0']->image_path != null) {
                                                $avatar = url('/' . 'storage/' . $item->skills['0']->image_path);
                                            } else {
                                                $avatar = '';
                                            }
                                        @endphp
                                        <figure class="company-logo"><img class="pxp-jobs-card-1-company-logo"
                                                src="{{ $avatar }}" alt="">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row mt-4 mt-lg-5 justify-content-between align-items-center">
                        <div class="col-auto">
                            <nav class="mt-3 mt-sm-0" aria-label="Jobs list pagination">
                                {{ $jobs->appends(request()->input())->links('client.pagination.index') }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('js')
    <script>
        $(document).on('click', ".filterJob", function() {
            let resuft = get_data_filter_jobs();
            api_data("{{ route('api.jobs.filter') }}", resuft['type'], resuft['exp'], resuft['salary'])
        })
        $(document).on('click', '.pagination > li > button', function(event) {
            event.preventDefault();
            let resuft = get_data_filter_jobs();
            api_data($(this).data('page'), resuft['type'], resuft['exp'], resuft['salary'])
        });

        function api_data(url, type, exp, salary) {
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    type: type,
                    exp: exp,
                    salary: salary,
                    timeSearch: $("select[name=timeSearch]").val(),
                    str_jobId: "{{ $jobID }}",
                },
                dataType: "JSON",
                success: function(response) {
                    $('#showJobFilter').html('')
                    $('.pagination').html('')
                    $("#countJobFilter").text(response.jobs.data.length)
                    response.jobs.data.forEach((element, index) => {
                        var icon = '';
                        // console.log(element.jobs.category_job)
                        Object.entries(response.category_job.icon).forEach((el) => {
                            if (element.jobs.category_job == el[0]) {
                                icon = el[1];
                            }
                        })

                        var url = '{!! route('client.job.detail', '') !!}/' + element.jobs.slug;
                        $('#showJobFilter').append(
                            `<div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-jobs-card-1-container">
                                <div class="pxp-jobs-card-1 pxp-has-border">
                                    <div class="pxp-jobs-card-1-top">
                                        <a class="pxp-jobs-card-1-category">
                                            <div class="pxp-jobs-card-1-category-icon"><span class="fa ${icon}"></span>
                                            </div>
                                            <div class="pxp-jobs-card-1-category-label">${response.category_jobs[element.jobs.category_job]}</div>
                                        </a>
                                        <a href="${url}"
                                            class="pxp-jobs-card-1-title">${ element.jobs.title }</a>
                                        <div class="pxp-jobs-card-1-details">
                                            <a class="pxp-jobs-card-1-location">
                                                <span class="fa fa-globe"></span>${element.address}
                                            </a>
                                            <div class="pxp-jobs-card-1-type">${element.job_type}</div>
                                        </div>
                                    </div>
                                    <div class="pxp-jobs-card-1-bottom">
                                        <div class="pxp-jobs-card-1-bottom-left">
                                            <div class="pxp-jobs-card-1-date pxp-text-light">Hạn
                                                ${element.deadline}</div>
                                            <a class="pxp-jobs-card-1-company">Lương
                                                ${element.salary.name}</a>
                                        </div>
                                        <figure class="company-logo"><img class="pxp-jobs-card-1-company-logo"
                                                src="${response.url}/storage/${element.skills[0].image_path}" alt="">
                                        </figure>
                                    </div>
                                </div>
                            </div>`
                        );
                    });
                    let pagination = response.jobs.links;
                    pagination[0]['label'] = '&laquo;';
                    pagination.at(-1)['label'] = '&raquo;';
                    if (response.jobs.data.length > 0) {
                        pagination.forEach((each, index) => {
                            $('.pagination').append(
                                `<li class="page-item ${each.active ? 'active' : ''}"><button class="${each.active ? 'active' : ''} page-link" data-page="${ each.url }">${ each.label }</button></li>`
                            )
                        });
                    }
                }
            });
        }

        function get_data_filter_jobs() {
            var checkType = $("input[name='type']:checked"); // returns object of checkeds.
            var type = []
            for (var i = 0; i < checkType.length; i++) {
                type.push($(checkType[i]).val())
            };
            var checkExp = $("input[name='exp']:checked"); // returns object of checkeds.
            var exp = []
            for (var i = 0; i < checkExp.length; i++) {
                exp.push($(checkExp[i]).val())
            };
            var checkSalary = $("input[name='salary']:checked"); // returns object of checkeds.
            var salary = []
            for (var i = 0; i < checkSalary.length; i++) {
                salary.push($(checkSalary[i]).val())
            };
            let data = [];
            data['type'] = type;
            data['exp'] = exp;
            data['salary'] = salary;
            return data;
        }
    </script>
@endpush
