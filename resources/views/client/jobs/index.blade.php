@extends('client.layout.layout')
@section('title', 'Trang tuyển dụng lập trình viên')
@section('content')

    <section class="pxp-hero vh-100" style="background-color: var(--pxpMainColorLight);">
        <div class="pxp-hero-caption">
            <div class="pxp-container">
                <div class="row pxp-pl-80 align-items-center justify-content-between">
                    <div class="col-12 col-xl-6 col-xxl-5">
                        <h1>Công việc <br><span style="color: var(--pxpMainColor);"> mơ ước </span>tại Hachinet</h1>
                        <div class="pxp-hero-subtitle mt-3 mt-lg-4">Tìm kiếm cơ hội nghề nghiệp và việc làm
                        </div>

                        <div class="pxp-hero-form pxp-hero-form-round mt-3 mt-lg-4">
                            <form action="{{ route('client.findjob.url') }}" method="post"
                                class="row gx-3 align-items-center">
                                @csrf
                                <div class="col-12 col-sm">
                                    <div class="mb-3 mb-sm-0">
                                        <input type="search" name="skill" class="form-control"
                                            placeholder="Vị trí cần tìm . . ." value="{{ old('skill') }}" />
                                    </div>
                                </div>
                                <div class="col-12 col-sm pxp-has-left-border">
                                    <div class="mb-3 mb-sm-0">
                                        <select class="select2 form-control select-address" name="address"
                                            data-placeholder=" -- Chọn địa chỉ -- ">
                                            <option></option>
                                            <option value=" ">Tất cả</option>
                                            @foreach ($address as $item)
                                                <option value="{{ $item->slug }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-auto">
                                    <button><span class="fa fa-search"> </span></button>
                                </div>

                            </form>
                        </div>

                        <div class="pxp-hero-searches-container">
                            <div class="pxp-hero-searches-label">Tìm kiếm Phổ biến</div>
                            <div class="pxp-hero-searches">
                                <div class="pxp-hero-searches-items">
                                    @foreach ($skills as $index => $each)
                                        @if ($index <= 11)
                                            <a
                                                href="{{ route('client.findjob.skill', $each->slug) }}">{{ $each->name }}</a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-none d-xl-block col-xl-5 position-relative">
                        <div class="pxp-hero-cards-container pxp-animate-cards pxp-mouse-move" data-speed="160">
                            <div class="pxp-hero-card pxp-cover pxp-cover-top"
                                style="background-image: url({{ asset('client/images/banner_index.png') }});"></div>
                            <div class="pxp-hero-card-dark"></div>
                            <div class="pxp-hero-card-light"></div>
                        </div>

                        <div class="pxp-hero-card-info-container pxp-mouse-move" data-speed="60">
                            <div class="pxp-hero-card-info pxp-animate-bounce">
                                @foreach ($count_skill_jobs as $index => $item)
                                    <div class="pxp-hero-card-info-item">
                                        <div class="pxp-hero-card-info-item-number">
                                            {{ $item }}<span>Công việc</span></div>
                                        <div class="pxp-hero-card-info-item-description">Kỹ năng {{ $index }}</div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="pxp-hero-logos-carousel-container">
            <div class="pxp-container">
                <div class="row pxp-pl-80">
                    <div class="col-12 col-xl-6">
                        <div class="pxp-hero-logos-carousel owl-carousel" style="font-weight: 600; font-size:16px">Về đối
                            tác
                            <img src="{{ asset('client/images/Hachinet Software Long (Yellow).png') }}" alt="Logo 1">
                            <img src="{{ asset('client/images/devwork_2.png') }}" alt="Logo 2">
                            <img src="{{ asset('client/images/logo_Takumi.png') }}" alt="Logo 3">
                            <img src="{{ asset('client/images/Funix_1.png') }}" alt="Logo 4">
                            <img src="{{ asset('client/images/Fpt_1.png') }}" alt="Logo 5">
                            <img src="{{ asset('client/images/tpbank_1.png') }}" alt="Logo 6">
                            {{-- <img src="{{ asset('client/images/Techcombank.png" alt="Logo 7">
                            <img src="{{ asset('client/images/MB bank.jpg" alt="Logo 8"> --}}
                            {{-- <img src="{{ asset('client/images/hero-logo-2.svg" alt="Logo 9">
                            <img src="{{ asset('client/images/hero-logo-3.svg" alt="Logo 10"> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="pxp-hero-right-bg-card pxp-has-animation"></div>
    </section>
    <section class="mt-100 pt-100 pb-100" style="background-color: var(--pxpSecondaryColorLight);">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">Danh sách việc làm</h2>
            <p class="pxp-text-light text-center">Tìm kiếm cơ hội việc làm {{ config('app.name') }} được cập nhật thường
                xuyên
            </p>

            <div class="row mt-4 mt-md-5 pxp-animate-in pxp-animate-in-top">
                @foreach ($jobs as $item)
                    <div class="col-md-6 col-xl-4 col-xxl-3 pxp-jobs-card-1-container">
                        <div class="pxp-jobs-card-1 pxp-has-shadow">
                            <div class="pxp-jobs-card-1-top">
                                <a class="pxp-jobs-card-1-category justify-content-between">
                                    <div class="d-flex align-items-center"><span
                                            class="pxp-jobs-card-1-category-icon fa
                                            @foreach ($category_job as $index => $each)
                                            @if ($item->category_job == $index)
                                            @if ('business-development' == $item->category_job)
                                                fa-pie-chart
                                            @elseif('marketing-communication' == $item->category_job)
                                                fa-bullhorn
                                            @elseif('customer-service' == $item->category_job)
                                                fa-comments-o
                                            @elseif('human-resources' == $item->category_job)
                                                fa-address-card-o
                                            @elseif('project-management' == $item->category_job)
                                                fa-calendar-o
                                                @elseif('software-engineering' == $item->category_job)
                                                fa-terminal
                                            @endif
                                            @endif @endforeach"></span>
                                        <div class="pxp-jobs-card-1-category-label">
                                            {{ $category_job[$item->category_job] }}</div>
                                    </div>
                                    <div class="d-inline-block" style="color: gray; font-size:12px">#{{ $item->code }}
                                    </div>
                                </a>
                                <a href="{{ route('client.job.detail', $item->slug) }}"
                                    class="pxp-jobs-card-1-title">{{ $item->title }}</a>
                                <div class="pxp-jobs-card-1-details">
                                    <a class="pxp-jobs-card-1-location">
                                        <span class="fa fa-globe"></span>{{ $item->job_details->address }}
                                    </a>
                                    <div class="pxp-jobs-card-1-type">Full-time</div>
                                </div>
                            </div>
                            <div class="pxp-jobs-card-1-bottom">
                                <div class="pxp-jobs-card-1-bottom-left">
                                    <div class="pxp-jobs-card-1-date pxp-text-light">
                                        Hạn {{ date('d/m/Y', strtotime($item->job_details->deadline)) }}</div>
                                    <a class="pxp-jobs-card-1-company">Lương
                                        {{ $item->salary->name }}</a>
                                </div>
                                @php
                                    if (count($item->skills) > 0 && $item->skills['0']->image_path != null) {
                                        $avatar = url('/' . 'storage/' . $item->skills['0']->image_path);
                                    } else {
                                        $avatar = asset('/assets/img/hachinet-viet-nam-logo.png');
                                    }
                                @endphp
                                <figure class="company-logo"><img class="pxp-jobs-card-1-company-logo"
                                        src="{{ $avatar }}" alt="">
                                </figure>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>

            <div class="mt-4 mt-md-5 text-center pxp-animate-in pxp-animate-in-top">
                <a href="/tuyen-dung-lap-trinh" class="btn rounded-pill pxp-section-cta">Xem thêm<span
                        class="fa fa-angle-right"></span></a>
            </div>
        </div>
    </section>
    <section class="mt-100">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">Danh mục việc làm</h2>
            <p class="pxp-text-light text-center">Tìm kiếm cơ hội nghề nghiệp của bạn với danh mục của chúng tôi</p>

            <div class="row mt-4 mt-md-5 pxp-animate-in pxp-animate-in-top">
                @foreach ($category_job as $index => $item)
                    <div class="col-12 col-md-4 col-lg-3 col-xxl-2 pxp-categories-card-1-container">
                        <a href="/tuyen-dung-lap-trinh?category_job={{ $index }}" class="pxp-categories-card-1">
                            <div class="pxp-categories-card-1-icon-container">
                                <div class="pxp-categories-card-1-icon">
                                    <span
                                        class="fa @if ($index == 'business-development') fa-pie-chart
                                    @elseif ($index == 'marketing-communication')fa-bullhorn @elseif ($index == 'customer-service')fa-comments-o
                                    @elseif ($index == 'human-resources')fa-address-card-o @elseif ($index == 'project-management')fa-calendar-o
                                    @elseif ($index == 'software-engineering')fa-terminal
                                     {{-- @elseif ($index == 'business-analysis') fa-pie-chart --}} @endif"></span>
                                </div>
                            </div>
                            <div class="pxp-categories-card-1-title">{{ $item }}</div>
                            <div class="pxp-categories-card-1-subtitle">{{ $category_count[$index] }} Công việc
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>

            <div class="mt-4 mt-md-5 text-center pxp-animate-in pxp-animate-in-top">
                <a href="/tuyen-dung-lap-trinh" class="btn rounded-pill pxp-section-cta">Xem thêm<span
                        class="fa fa-angle-right"></span></a>
            </div>
        </div>
    </section>
    <section class="mt-100">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">Đối tác</h2>
            <p class="pxp-text-light text-center">Hợp tác với những công ty tốt nhất trong nước.</p>

            <div class="row mt-4 mt-md-5 pxp-animate-in pxp-animate-in-top">
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-companies-card-1-container">
                    <div class="pxp-companies-card-1 pxp-has-border">
                        <div class="pxp-companies-card-1-top">
                            <a target="_blank" href="https://tpb.vn/" class="pxp-companies-card-1-company-logo"
                                style="background-image: url({{ asset('client/images/TPbank.png);') }}"></a>
                            <a target="_blank" href="https://tpb.vn/" class="pxp-companies-card-1-company-name">TP
                                BANK</a>
                            <div class="pxp-companies-card-1-company-description pxp-text-light">Với tuyên ngôn thương
                                hiệu “Vì chúng tôi hiểu bạn” , TPBank luôn nỗ lực mang đến các giải pháp, sản phẩm tài
                                chính ngân hàng hiệu quả nhất, xây dựng trên nền tảng thấu hiểu sâu sắc nhu cầu của
                                khách hàng.</div>
                        </div>

                    </div>
                </div>
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-companies-card-1-container">
                    <div class="pxp-companies-card-1 pxp-has-border">
                        <div class="pxp-companies-card-1-top">
                            <a target="_blank" href="https://funix.edu.vn/" class="pxp-companies-card-1-company-logo"
                                style="background-image: url({{ asset('client/images/funix_2.png);') }}"></a>
                            <a target="_blank" href="https://funix.edu.vn/"
                                class="pxp-companies-card-1-company-name">FUNIX</a>
                            <div class="pxp-companies-card-1-company-description pxp-text-light">FUNIX là chương trình học
                                trực tuyến CNTT trực tuyến thuộc tập đoàn FPT, 100% dựa trên Internet, cung cấp khóa học từ
                                cơ bản đến chuyên sâu về công nghệ thông tin.</div>
                        </div>

                    </div>
                </div>
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-companies-card-1-container">
                    <div class="pxp-companies-card-1 pxp-has-border">
                        <div class="pxp-companies-card-1-top">
                            <a target="_blank" href="https://career.fpt-software.com/"
                                class="pxp-companies-card-1-company-logo"
                                style="background-image: url({{ asset('client/images/FPT.png);') }}"></a>
                            <a target="_blank" href="https://career.fpt-software.com/"
                                class="pxp-companies-card-1-company-name">FPT
                                SOFTWARE</a>
                            <div class="pxp-companies-card-1-company-description pxp-text-light">“Nắm lấy sự thay đổi”
                                -
                                FPT Châu Á Thái Bình Dương đề cao khả năng thích ứng với những thay đổi của thị trường,
                                ngành, khách hàng, cơ cấu tổ chức và thị trường. Thay đổi là cần thiết để phát triển và
                                vượt trội.</div>
                        </div>

                    </div>
                </div>
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-companies-card-1-container">
                    <div class="pxp-companies-card-1 pxp-has-border">
                        <div class="pxp-companies-card-1-top">
                            <a target="_blank" href="https://staffing.devwork.vn/"
                                class="pxp-companies-card-1-company-logo"
                                style="background-image: url({{ asset('client/images/devwork_1.png);') }}"></a>
                            <a target="_blank" href="https://staffing.devwork.vn/"
                                class="pxp-companies-card-1-company-name">DEVWORK</a>
                            <div class="pxp-companies-card-1-company-description pxp-text-light">Với thế mạnh về khả
                                năng tuyển dụng nhân sự ngành CNTT,Devwork là lựa chọn tin cậy để giải quyết bài
                                toán về nhu cầu nhân sự trong thời gian ngắn hạn của các doanh nghiệp CNTT Việt
                                Nam.</div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </section>

    <section class="mt-100">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">Tin Tức</h2>
            <p class="pxp-text-light text-center">Thông tin mới nhất về công ty chúng tôi luôn được cập nhật.</p>

            <div class="row mt-3 mt-md-4">
                @foreach ($blogs as $each)
                    <div class="col-md-6 col-xl-4 col-xxl-3 pxp-posts-card-1-container" style="margin-top: 3rem">
                        <div class="pxp-posts-card-1 pxp-has-border">
                            <div class="pxp-posts-card-1-top">
                                <div class="pxp-posts-card-1-top-bg">
                                    <div>
                                        <img class="pxp-posts-card-1-image pxp-cover object-fit-cover"
                                            src="  {{ asset('storage/' . $each->banner) }}"
                                            style="width: 100%;border: 1px solid var(--pxpLightGrayColor);"
                                            alt="How to Start Looking for a New Job">
                                    </div>
                                    <div class="pxp-posts-card-1-info">
                                        <div class="d-flex justify-content-between">
                                            <div class="pxp-posts-card-1-date">
                                                {{ date('d/m/Y', strtotime($each->created_at)) }}
                                            </div>
                                            {{-- <div class="pxp-posts-card-2-category" style="font-size: 13px; color:black;">
                                                <i class="fa fa-eye"></i> {{ $each->viewer }} Lượt xem
                                            </div> --}}
                                        </div>
                                        <a class="pxp-jobs-card-1-company">
                                            {{ $each->categories->name }}</a>
                                    </div>
                                </div>
                                <div class="pxp-posts-card-1-content">
                                    <a href="{{ route('blog.detail', $each->slug) }}"
                                        class="pxp-posts-card-1-title">{{ $each->title }}</a>
                                    <div class="pxp-posts-card-1-summary pxp-text-light">{{ $each->highlight }}
                                    </div>
                                </div>
                            </div>
                            <div class="pxp-posts-card-1-bottom">
                                <div class="pxp-posts-card-1-cta">
                                    <a href="{{ route('blog.detail', $each->slug) }}">Đọc thêm<span
                                            class="fa fa-angle-right"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="mt-4 mt-md-5 text-center pxp-animate-in pxp-animate-in-top">
                <a href="/tin-tuc" class="btn rounded-pill pxp-section-cta">Xem thêm<span
                        class="fa fa-angle-right"></span></a>
            </div>
        </div>
    </section>

    <section class="mt-100">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">Hachinet Software</h2>
            <p class="pxp-text-light text-center">Đăng ký ngay để nhận nguồn cấp dữ liệu hàng tuần của chúng tôi.</p>

            <div class="row mt-4 mt-md-5 justify-content-center">
                <div class="col-md-9 col-lg-7 col-xl-6 col-xxl-5">
                    <div class="pxp-subscribe-1-container pxp-animate-in pxp-animate-in-top">
                        <div class="pxp-subscribe-1-image">
                            <img src="{{ asset('client/images/subscribe.png') }}" alt="Stay Up to Date">
                        </div>
                        <div class="pxp-subscribe-1-form">
                            <form id="formValidate">
                                @csrf
                                <div class="input-group">
                                    <input type="text" name="email" class="form-control"
                                        placeholder="Nhập email đăng ký...">
                                    <button class="btn btn-primary apply-email" type="button">Đăng ký<span
                                            class="fa fa-angle-right"></span></button>
                                </div>
                                {{-- <div class="text-center text-danger">
                                    @error('email')
                                        {{ $message }}
                                    @enderror
                                </div> --}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="/assets/libs/toast/toast.min.js"></script>
    <link href="{{ asset('/assets/css/toast.min.css') }}" rel="stylesheet" />
    <style>
        .form-control.is-invalid,
        .was-validated .form-control:invalid {
            border-color: #dc3545 !important;
        }
    </style>
    {{-- <script type="text/javascript">
        var NotifiSuccess = "{{ session('success') ? session('success') : 'false' }}";
        if (NotifiSuccess !== 'false') {
            $.toast({
                heading: 'Thông báo thành công!',
                text: NotifiSuccess,
                showHideTransition: 'slide',
                position: 'top-right',
                icon: 'success'
            });
        }
        var NotifiError = "{{ session('error') ? session('error') : 'false' }}";
        if (NotifiError !== 'false') {
            $.toast({
                heading: 'Thông báo lỗi!',
                text: NotifiError,
                showHideTransition: 'slide',
                position: 'top-right',
                icon: 'error'
            });
        }
    </script> --}}
@endsection
@push('js')
    <script src="{{ asset('assets/libs/toast/toast.min.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <style>
        .form-control.is-invalid,
        .was-validated .form-control:invalid {
            border-color: #dc3545 !important;
        }
    </style>
    <script type="text/javascript">
        $(document).on("click", ".apply-email", function() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "{{ route('client.contact.email') }}",
                data: {
                    email: $("input[name=email]").val()
                },
                dataType: "JSON",
                success: function(response) {
                        $("input[name=email]").val(''),
                    // Swal.fire(
                        // 'Thông báo',
                        // 'Gửi liên hệ thành công!',
                        // 'success'
                    // )
                    $.toast({
                            heading: 'Thông báo!',
                            text: 'Đăng ký email thành công',
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: 'success'
                        });
                        $("input[name=email]").removeClass("text-error")

                },
                error: function(response) {
                    $("input[name=email]").removeClass("text-error")
                    if (response.responseJSON.errors.email) {
                        $.toast({
                            heading: 'Thông báo lỗi!',
                            text: response.responseJSON.errors.email,
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: 'error'
                        });
                        $("input[name=email]").addClass("text-error")
                    }
                }
            });
        })
    </script>
@endpush
