<footer class="pxp-main-footer mt-100">
    <div class="pxp-main-footer-top pt-100 pb-100" style="background-color: var(--pxpMainColorLight);">
        <div class="pxp-container">
            <div class="row">
                <div class="col-lg-6 col-xl-5 col-xxl-4 mb-4">
                    <div class="pxp-footer-logo">
                        <a href="/" class="pxp-animate"><img
                            src="{{ asset('client/images/Hachinet Software Long (Yellow).png') }}" alt="hachinet tuyen dung"
                            style="height: 60px;
                            width: auto; margin-left: -8px;"></a>
                    </div>
                    <div class="text" style="width: 350px; margin-top: 5px;">
                        <p>Hachinet không chỉ là công ty mà còn là miền đất nuôi dưỡng
                            tinh thần của những chiến binh dũng cảm, luôn sẵn sàng đương
                            đầu với các thử thách mới ở phía trước.</p>
                    </div>
                    <div class="pxp-footer-section mt-3 mt-md-4">
                        <h3 class="pxp-footer-text"><i class="fa fa-envelope-open"></i> Liên hệ với chúng tôi:</h3>
                        <div class="pxp-footer-logo "> <a class="pxp-animate " ><span
                                    style="color: var(--pxpMainColor)">hello</span>@hachinet.com</a></div>
                    </div>
                    <div class="mt-3 mt-md-4 pxp-footer-section">
                        <div class="pxp-footer-text">
                            <a class="pxp-jobs-card-1-location">
                                <span class="fa fa-globe"></span>2A - 27A3, 234 Phạm Văn Đồng, Cổ Nhuế, Từ Liêm, Hà Nội
                            </a>
                        </div>
                    </div>
                    <div class="mt-3 mt-md-4 pxp-footer-section">
                        <div class="pxp-footer-text">
                            <a href="tel:2462900388" class="pxp-jobs-card-1-location ">
                                <i class="fa fa-phone"></i> Điện thoại:
                                (+84) 2462-900-388
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-7 col-xxl-8">
                    <div class="row">
                        <div class="col-md-6 col-xl-4 col-xxl-3 mb-4">
                            <div class="pxp-footer-section">
                                <h3>Dành cho ứng viên</h3>
                                <ul class="pxp-footer-list">
                                    <li><a href="/tuyen-dung-lap-trinh">Tìm việc làm</a></li>
                                    {{-- <li><a href="mailto:contact@hachinet.com">Contact@hachinet.com</a></li> --}}


                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4 col-xxl-3 mb-4">
                            <div class="pxp-footer-section">
                                <h3>Về chúng tôi</h3>
                                <ul class="pxp-footer-list">
                                    <li><a href="/tin-tuc">Tin tức</a></li>
                                    <li><a href="/lien-he">Liên hệ</a></li>
                                    <li><a href="/nguoi-hachinet">Hỏi đáp</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-6 col-xl-4 col-xxl-3 mb-4">
                            <div class="pxp-footer-section">
                                <h3>Kỹ năng</h3>
                                <ul class="pxp-footer-list">
                                    @php
                                        $skills = DB::table('setting_jobs')
                                            ->where('type', 'skills')
                                            ->limit(4)
                                            ->get(['id', 'name', 'slug']);
                                    @endphp

                                    @foreach ($skills as $each)
                                        <li><a
                                                href="{{ route('client.findjob.skill', $each->slug) }}">{{ $each->name }}</a>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4 col-xxl-3 mb-4">
                            <div class="pxp-footer-section">
                                <h3>Việc làm theo khu vực</h3>
                                <ul class="pxp-footer-list">
                                    <li><a href="{{ route('client.findjob.all', 'ha-noi') }}">Hà Nội</a></li>
                                    <li><a href="{{ route('client.findjob.all', 'nghe-an') }}">Vinh</a></li>
                                    {{-- <li><a href="/">Nhật Bản</a></li> --}}
                                    {{-- @php
                                    $address = DB::table('job_details')
                                        ->where('type', 'address')
                                        ->limit(4)
                                        ->get(['id', 'name', 'slug']);
                                @endphp
                                    @foreach ($address as $each)
                                    <li><a
                                            href="/search?{{ urlencode('job[address ]') }}={{ urlencode($each->slug) }}">{{ $each->name }}</a>
                                    </li>
                                @endforeach --}}

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pxp-main-footer-bottom" style="background-color: var(--pxpSecondaryColor);">
        <div class="pxp-container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-auto">
                    <div class="pxp-footer-copyright pxp-text-light">© {{ date('Y') }} Hachinet. All Right Reserved.</div>
                </div>
                <div class="col-lg-auto">
                    <div class="pxp-footer-social mt-3 mt-lg-0">
                        <ul class="list-unstyled">
                            <li><a href="https://www.facebook.com/hr.hachinet"><span class="fa fa-facebook"></span></a>
                            </li>
                            <li><a href="https://twitter.com/HachinetJ"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="https://www.instagram.com/hachinet.software/"><span
                                        class="fa fa-instagram"></span></a></li>
                            <li><a href="https://www.linkedin.com/company/hachinet-software1/"><span
                                        class="fa fa-linkedin"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
