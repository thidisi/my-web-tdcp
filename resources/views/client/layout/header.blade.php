<header class="pxp-header fixed-top">
    <div class="pxp-container">
        <div class="pxp-header-container">
            <div class="pxp-logo">
                <a href="/" class="pxp-animate"><img
                        src="{{ asset('client/images/Hachinet Software Long (Yellow).png') }}" alt="hachinet tuyen dung"
                        style="height: 60px;
                        width: auto;"></a>
            </div>
            <div class="pxp-nav-trigger navbar d-xl-none flex-fill">
                <a role="button" data-bs-toggle="offcanvas" data-bs-target="#pxpMobileNav"
                    aria-controls="pxpMobileNav">
                    <div class="pxp-line-1"></div>
                    <div class="pxp-line-2"></div>
                    <div class="pxp-line-3"></div>
                </a>
                <div class="offcanvas offcanvas-start pxp-nav-mobile-container" tabindex="-1" id="pxpMobileNav">
                    <div class="offcanvas-header">
                        <div class="pxp-logo">
                            <a href="/" class="pxp-animate"><img
                                src="{{ asset('client/images/Hachinet Software Long (Yellow).png') }}" alt="hachinet tuyen dung"
                                style="height: 60px;
                                width: auto;"></a>
                        </div>
                        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                            aria-label="Close"></button>
                    </div>
                    <div class="offcanvas-body">
                        <nav class="pxp-nav-mobile">
                            <ul class="navbar-nav justify-content-end flex-grow-1">
                                <li class="nav-item dropdown">
                                    <a href="/" class="nav-link" style="font-weight: 700;">Trang chủ</a>

                                </li>
                                <li class="nav-item dropdown">
                                    <a href="/tuyen-dung-lap-trinh" class="nav-link" style="font-weight: 700;">Việc làm</a>

                                </li>
                                <li class="nav-item dropdown">

                                    <a href="https://hachinet.com/" class="nav-link" style="font-weight: 700;">Giới thiệu</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="/nguoi-hachinet" class="nav-link" style="font-weight: 700;">Hỏi đáp</a>

                                </li>
                                <li class="nav-item dropdown">
                                    <a href="/tin-tuc" class="nav-link" style="font-weight: 700;">Tin tức</a>

                                </li>
                                <li class="nav-item dropdown">
                                    <a href="/lien-he" class="nav-link" style="font-weight: 700;">Liên hệ</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

            <nav class="pxp-nav dropdown-hover-all d-none d-xl-block">
                <ul>
                    <li class="dropdown">
                        <a href="/">Trang chủ</a>
                    </li>
                    <li class="dropdown">
                        <a href="/tuyen-dung-lap-trinh">Việc làm</a>
                    </li>
                    <li class="dropdown">
                        <a href="https://hachinet.com/">Giới thiệu</a>
                    </li>
                    <li class="dropdown">
                        <a href="/nguoi-hachinet">Hỏi đáp</a>

                    </li>
                    <li class="dropdown">
                        <a href="/tin-tuc">Tin tức</a>

                    </li>


                </ul>
            </nav>
            <nav class="pxp-user-nav d-none d-sm-flex">
                <a href="/lien-he" class="btn rounded-pill pxp-nav-btn">Liên hệ</a>
            </nav>
        </div>
    </div>
</header>
