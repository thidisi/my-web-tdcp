<!doctype html>
<html lang="en" class="pxp-root">

<head>
    <meta name="referrer" content="no-referrer-when-downgrade">
    <meta name="robots" content="all">
    <meta name="robots" content="index,follow" />
    <meta name="keywords"
        content="việc,làm, tuyển,dụng, tìm,việc,làm, tìm việc nhanh, ITViec,TopDEV, viec lam IT, tuyen dung IT, tim viec lam IT, tim viec nhanh IT,tuyển dụng Developer , Tuyển dụng lập trình viên , Tuyển dụng IT,Dịch vụ Headhunter,Lập trình viên Back-end, Lập trình viên Front-End,Vietnam work,ITnavi,lập,trình,php,ios,java,.net,android,reactnative,reactjs,vuejs,laravel,tuyen,dung,moi">
    <meta name="geo.placename" content="Laravel">
    <link rel="shortcut icon" href="https://hachinet.jp/static/img/favicon.png">
    <meta name="description"
        content="Tìm việc làm IT- Đăng tin miễn phí-Trang tuyển dụng IT hiệu quả và uy tín,cập nhật hàng nghìn việc làm mới mỗi ngày.Hệ thống kiếm tiền của Hr và Headhunter">
    <!-- Dublin Core basic info -->



    <!-- Facebook OpenGraph -->

    {{-- Share Facebook --}}

    @yield('meta')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link
        href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;0,900;1,300;1,400;1,600;1,700;1,800;1,900&amp;display=swap"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css2?family=Muli:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap"
        rel="stylesheet">
    <link href="{{ asset('/assets/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('client/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('client/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/style.css') }}">
    <title>@yield('title')</title>
</head>

<body>
    <div class="pxp-preloader"><span>Loading...</span></div>

    <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
    @include('client.layout.header')
    @yield('content')
    @include('client.layout.footer')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v15.0"
        nonce="uTqeK9s8"></script>
    <script src="{{ asset('client/js/jquery-3.4.1.min.js') }}"></script>
    <script src="/assets/libs/select2/select2.min.js"></script>
    <script src="{{ asset('client/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('client/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('client/js/nav.js') }}"></script>
    <script src="{{ asset('client/js/main.js?v=111213114') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2({
                allowClear: true,
            });
            $('.select2-container').addClass('select-address');
        });
    </script>
    @stack('js')
</body>

</html>
