@extends('client.layout.layout')
@section('title', 'Trang tuyển dụng lập trình viên')


@section('content')
    <!-- error-section -->
    <div class="error-section centred">
        <div class="auto-container">
            <div class="row clearfix d-flex justify-content-center">
                <div class="col-lg-8 col-md-8 col-sm-8 offset-lg-1 content-column">
                    <div class="error-content">
                        <figure class="image-box d-flex justify-content-center"><img src="{{ asset('client/images/error-img.png') }}" alt="icons"  style="width: 75%;"></figure>
                        <h2>Xin lỗi trang này không có sẵn</h2>
                        <p>Chúng tôi không thể tìm thấy trang bạn đang tìm kiếm</p>
                        <a href="{{ route('jobs') }}" class="theme-btn-two">Quay lại trang chủ</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- error-section end -->
@endsection
