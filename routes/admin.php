<?php

use App\Http\Controllers\Admin\AccountController;
use App\Http\Controllers\Admin\ApplyController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CvController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\JobController;
use App\Http\Controllers\Admin\KpiController;
use App\Http\Controllers\Admin\NotifyController;
use App\Http\Controllers\Admin\SettingJobController;
use App\Http\Controllers\Admin\SettingRoleController;
use App\Http\Controllers\Admin\SettingSourceController;
use App\Http\Controllers\Admin\ShortUrlController;
use App\Http\Controllers\FullCalendarController;
use App\Models\Notify;
use App\Models\Permission;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

Route::controller(AuthController::class)
    ->middleware('prevent-back-history')
    ->prefix("auth")
    ->group(function () {
        Route::post('login', 'handleLogin')->name('admin.handleLogin');
        Route::post('register', 'registering')->name('admin.register');
        Route::get('redirect/{provider}', function ($provider) {
            return Socialite::driver($provider)->redirect();
        })->name('auth.redirect');
        Route::get('callback/{provider}', [AuthController::class, 'callback'])->name('auth.callback');
        Route::post('logout', 'logout')->name('admin.logout');
    });
Route::prefix('admin')
    ->middleware(['auth:sanctum', 'check.auth.user', 'prevent-back-history'])
    ->group(function () {
        Route::get('404', function () {
            return view('admin.errors.404');
        })->name('admin.errors');
        Route::resource('dashboard', DashboardController::class)->only([
            'index'
        ]);
        Route::resource('accounts', AccountController::class)->only([
            'index', 'show', 'update'
        ]);
        Route::post('accounts/status/{id}', [AccountController::class, 'changeStatus'])->name('account.status');
        Route::controller(SettingJobController::class)
            ->prefix('setting-job')
            ->middleware('check.permission.user:' . Permission::PERMISSION_ID['SETTING-JOB'])
            ->group(function () {
                // level
                Route::get('levels', 'showLevel')->name('admin.level.show');
                // salary
                Route::get('salaries', 'showSalary')->name('admin.salary.show');
                // skill
                Route::get('skills', 'showSkill')->name('admin.skill.show');
                // jobs
            });
        Route::resource('jobs', JobController::class)->except([
            'index', 'show'
        ])->middleware('check.permission.user:' . Permission::PERMISSION_ID['CRUD-JOB']);
        Route::get('jobs', [JobController::class, 'index'])
            ->middleware('check.permission.user:' . Permission::PERMISSION_ID['SHOW-JOB'])
            ->name('jobs.index');
        Route::get('jobs/{job}', [JobController::class, 'show'])
            ->middleware('check.permission.user:' . Permission::PERMISSION_ID['SHOW-JOB'])
            ->name('jobs.show');
        Route::post('jobs/status/{id}', [JobController::class, 'changeStatus'])
            ->middleware('check.permission.user:' . Permission::PERMISSION_ID['CRUD-JOB'])
            ->name('jobs.status');
        Route::resource('settings', SettingRoleController::class)->only([
            'index'
        ])->middleware('check.permission.user:' . Permission::PERMISSION_ID['PERMISSION'] . '|' . Permission::PERMISSION_ID['SETTING-KPI'] . '|' . Permission::PERMISSION_ID['BONUS']);
        Route::resource('categories', CategoryController::class);
        Route::resource('blogs', BlogController::class);
        Route::post('shortUrl', [ShortUrlController::class, 'store'])->name('store.shortUrl');

        //cv
        Route::resource('cv', CvController::class)->only('index');

        Route::resource('sources', SettingSourceController::class)->only(['index', 'store', 'update', 'destroy']);
        Route::get('list-kpi', [KpiController::class, 'index'])->name('list-kpi.index');
        Route::get('fullcalendar',[FullCalendarController::class, 'index'])->name('fullcalendar.index');
        Route::get('getfullcalendar',[FullCalendarController::class, 'showCalendar'])->name('fullcalendar.showCalendar');
    });
//apply
Route::resource('apply', ApplyController::class)->only('store');


Route::get('admin/tesing', function () {
    return view('welcome');
})->name('notify');

//fullcalender


