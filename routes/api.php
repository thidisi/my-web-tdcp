<?php

use App\Http\Controllers\Admin\AccountController;
use App\Http\Controllers\Admin\ApplyController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CvController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\JobController;
use App\Http\Controllers\Admin\KpiController;
use App\Http\Controllers\Admin\NotifyController;
use App\Http\Controllers\Admin\SearchController;
use App\Http\Controllers\Admin\SettingJobController;
use App\Http\Controllers\Admin\SettingRoleController;
use App\Http\Controllers\Admin\SettingSourceController;
use App\Models\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(SettingJobController::class)
    ->prefix('setting-job')
    ->group(function () {
        Route::get('/type/{type}', 'showJobType')
            ->name('api.type.show');
        Route::post('store', 'store')->name('api.type.store');
        Route::delete('type/{id}', 'destroy')->name('api.type.destroy');
        Route::get('type/edit/{id}', 'edit')->name('api.type.edit');
        Route::put('type/update/{id}', 'update')->name('api.type.update');
    });


Route::controller(SettingRoleController::class)
    ->prefix('setting-role')
    ->group(function () {
        Route::get('/{id}', 'show')->name('api.setting.role.show');
        Route::put('update/{id}', 'update')->name('api.setting.role.update');
    });
// lọc job
Route::get('filterJobsList', [SearchController::class, 'filterJobsList'])->name('api.jobs.filter');
//end lọc job
Route::get('jobsList', [JobController::class, 'list_api'])->name('api.jobs.list');
Route::get('getShowJob/{id}', [ApplyController::class, 'get_show_job'])->name('api.jobs.show');
Route::post('changeStatusApply/{id}', [ApplyController::class, 'change_status'])->name('api.apply.status');
Route::post('changeDateApplyPassed', [ApplyController::class, 'change_date'])->name('api.apply.date');

Route::get('categoryList', [CategoryController::class, 'list'])->name('api.categories.list');
Route::get('categoryEdit/{id}', [CategoryController::class, 'edit'])->name('api.categories.edit');
Route::put('categoryUpdate/{id}', [CategoryController::class, 'update'])->name('api.categories.update');
Route::delete('categoryDelete/{id}', [CategoryController::class, 'destroy'])->name('api.categories.delete');
//get cv theo job
Route::get('getCv/{id}', [ApplyController::class, 'getCvJob'])->name('api.getCv.job');
//get list accounts
Route::get('accountsList', [AccountController::class, 'list_api'])->name('api.account.list');
//get list cv by status
Route::get('cvList', [CvController::class, 'list_by_status'])->name('api.cv.list.status');
//get profile list cv
Route::get('sourceList', [SettingSourceController::class, 'list_api'])->name('api.source.list');
//get list user
Route::get('userList', [AccountController::class, 'list_user_api'])->name('api.user.list');
//get data to draw chart
Route::get('dataToDrawChart', [DashboardController::class, 'getDataToDrawChart'])->name('api.get.data.chart');
//assign to me
Route::post('assignToMe', [CvController::class, 'assignToMe'])->name('api.assign.candidate');
// cancel assign
Route::post('cancelAssign', [CvController::class, 'cancelAssign'])->name('api.cancel.candidate');
// get list kpi
Route::get('kpiList', [KpiController::class, 'api_list_kpi'])->name('api.kpi.list');
//get list sources to dashboard
Route::get('sourceChart', [DashboardController::class, 'getDataSource'])->name('api.source.chart');
// change notify
Route::post('notification/{id}', [NotifyController::class, 'actision'])->name('api.notify.actision');

Route::get('bubbleNotify', [NotifyController::class, 'bubble_notify'])->name('api.notify.bubble');

Route::post('export-excel', [ApplyController::class, 'export'])->name('api.export.excel.apply');
