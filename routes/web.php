<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\JobController;
use App\Http\Controllers\Admin\SearchController;
use App\Http\Controllers\Admin\ShortUrlController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('prevent-back-history')
    ->group(function () {
        Route::get('/', [JobController::class, 'getAll'])->name('jobs');
        Route::get('/login', [AuthController::class, 'login'])->name('admin.login');
        Route::get('/tuyen-dung/{slug}', [JobController::class, 'showJobDetailClient'])->name('client.job.detail');
        Route::get('/lien-he', function () {
            return view('client.contacts.index', []);
        })->name('contact');
        Route::get('/nguoi-hachinet', function () {
            return view('client.faq.index', []);
        })->name('faq');
        Route::get('/tin-tuc', [BlogController::class, 'getAll'])->name('about');
        Route::get('/tin-tuc/{slug}', [BlogController::class, 'getCategoryBlog'])->name('abouts');
        Route::get('/blog/{slug}', [BlogController::class, 'showBlogDetailClient'])->name('blog.detail');
        Route::post('/contactStore', [ContactController::class, 'store'])->name('client.contact.store');
        Route::post('/contactEmail', [ContactController::class, 'store_email'])->name('client.contact.email');
        Route::post('/contactSubscribe', [ContactController::class, 'store'])->name('client.contact.subscribe');
        Route::get('/404', function () {
            return view('errors.404');
        })->name('404');
        Route::get('/tuyen-dung-lap-trinh-tai/{address?}/{skill?}', [SearchController::class, 'searchAll'])->name('client.findjob.all');
        Route::get('/tuyen-dung-lap-trinh/{skill?}', [SearchController::class, 'searchSkill'])->name('client.findjob.skill');
        Route::post('/tuyen-dung-lap-trinh', [SearchController::class, 'search'])->name('client.findjob.url');
        Route::get('/filterBlog/{id}', [BlogController::class, 'filterBlogsList'])->name('blog.filterBlog');
        // luôn luôn phải để url này cuối cùng để không bị ghi đè lên cách url khác
        Route::get('/{code}', [ShortUrlController::class, 'shortenLink']);
    });
