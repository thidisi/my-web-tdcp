-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 18, 2023 at 05:51 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tuyendung`
--

-- --------------------------------------------------------

--
-- Table structure for table `applies`
--

CREATE TABLE `applies` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'uuid gen auto in code',
  `candidate_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` int NOT NULL,
  `file` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_id` bigint UNSIGNED DEFAULT NULL,
  `user_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('new','interview','rejected','passed','failed') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `skills` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `interview_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applies`
--

INSERT INTO `applies` (`id`, `candidate_id`, `job_id`, `created_at`, `updated_at`, `code`, `file`, `source_id`, `user_id`, `status`, `skills`, `reason`, `interview_date`) VALUES
('01f331c4-820b-4aca-891f-e059e1e39e25', '18affe8d-1cda-46cc-b2bd-119a048a21cc', '6b67cee5-b3c3-4c44-8467-c48f465bfa04', '2023-02-07 04:31:18', '2023-02-08 09:56:36', 129, 'applyCv/hNOAeRgtpfXkNksXZb9Z41YKQyIPEipwONs4hN0d.pdf', 2, NULL, 'rejected', '\"16\"', 'jhgf', NULL),
('05572a21-9211-49ae-b65a-49dd93569301', '374cb0a0-bf6e-4067-872b-349a5a304636', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-02-07 02:54:56', '2023-02-07 03:08:09', 123, 'applyCv/e7DE090Xc9VEoiee3PEqZHCxNtLFXywPkykwoyKp.pdf', NULL, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'interview', '\"27\"', NULL, '2023-02-07 10:08:00'),
('0af6b45d-748a-4024-9b09-11f91458d0eb', '46c32388-ca82-4d64-9c19-d67eaf200663', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-02-02 04:33:17', '2023-02-08 04:17:23', 120, 'applyCv/uPhVlPADXpseqKpYYvMRWKGsnVMjde9nKMLXE3DK.pdf', NULL, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 'failed', '\"25\"', '4w6', '2023-02-07 10:09:00'),
('1c087bf7-2138-4342-9d25-b9e1f590cf59', '767fd128-72b1-43e6-a4a4-1978741d05df', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-02-08 02:16:48', '2023-02-08 03:26:50', 137, 'applyCv/ho51dArlljAEn0UZtmrBhUkFQMexv5bgv8glQPTy.pdf', 2, '4b9fadba-2b78-420d-bb5a-14a47901b72d', 'rejected', '\"17\"', 'ertwr', NULL),
('1c79b7ae-4b96-4299-af75-dc0f0b8f667c', 'beeb8d36-373a-454e-b4d0-d34734d2f8b9', '6b67cee5-b3c3-4c44-8467-c48f465bfa04', '2023-02-07 03:54:47', '2023-02-07 03:54:47', 126, 'applyCv/IQUQvifZFJnYfV6cSODLdYPwxS3nBNb0WacwBQzs.pdf', NULL, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'new', '\"17\"', NULL, NULL),
('1db60f0b-797c-4792-b6b5-7e664ef07ec2', 'b66643fd-92f8-41da-b217-60a708ecec59', 'ecfd659e-1736-4146-958a-090eef75048a', '2023-02-08 09:49:07', '2023-02-08 09:50:32', 144, 'applyCv/Ss3ltr4YbWcVMCDVVVCor3hcPjqTFPakJqp9l8Tq.pdf', NULL, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 'rejected', '\"16\"', 'eqweqwe', NULL),
('2213fba0-3dfd-4a2a-b7fe-525abfbddbfa', '826e4190-8fbe-4cbc-8c92-8190b399bb26', '6b67cee5-b3c3-4c44-8467-c48f465bfa04', '2023-02-08 03:50:08', '2023-02-08 05:31:39', 139, 'applyCv/elcXkes7QMv8QNZEHaHcF68bh3eoJ8I6khPbqMBr.pdf', NULL, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 'interview', '\"27\"', NULL, '2023-02-08 12:31:00'),
('2614a011-f179-4537-81a4-5d4e719741a9', '9ce63f25-ee62-4763-9679-fdf585763e74', '6b67cee5-b3c3-4c44-8467-c48f465bfa04', '2023-02-07 03:10:53', '2023-02-07 03:10:53', 124, 'applyCv/xFigxrkVQ3qEFRfDnxvIoUMfb1HoPBwHeRkx3760.pdf', 2, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'new', '\"17\"', NULL, NULL),
('32603b01-39bb-452e-b660-d55c81e9d78c', '63290b13-f3fb-4f53-be7d-1abc3e129c3b', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-02-07 07:00:12', '2023-02-07 07:00:12', 134, 'applyCv/qc3qYVwUhMPxR1dr4E40aYGtDChO2v0PTxxwxEYj.pdf', 2, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'new', '\"21\"', NULL, NULL),
('34f82794-9829-4273-8768-979fe2d06b60', 'ca835647-2bc2-45ff-aa87-e643fa47ec54', '6b67cee5-b3c3-4c44-8467-c48f465bfa04', '2023-02-07 04:34:28', '2023-02-07 04:34:28', 130, 'applyCv/6sAH5kmJDyb2Ho2neSCnlc6qEqVR2tJqCox2AUUk.pdf', NULL, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'new', '\"22\"', NULL, NULL),
('3b32a9ab-23a6-4241-9640-11e92183a19d', 'a5e389c8-55a9-41fe-8dda-ef0825131e19', 'ecfd659e-1736-4146-958a-090eef75048a', '2023-02-08 04:09:15', '2023-02-08 05:38:20', 141, 'applyCv/EsVdVBM5tbfjnvfCM0L9DT7iQf7uEIjvsswLMfG3.pdf', NULL, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'passed', '\"21\"', NULL, '2023-02-08 11:16:00'),
('48287b97-c010-47bf-bf90-1a5182d80b07', '7120c315-8464-4a39-a49b-8067ca18ad47', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-03-13 10:52:36', '2023-03-13 10:52:49', 148, 'applyCv/8Ii3QiWqhqwY70HRwDWrz45sTMy39nHPgpEX97xT.pdf', NULL, NULL, 'new', '\"17\"', NULL, NULL),
('4834d8be-9edf-4744-afb9-d298b94ac7b1', '7255e97c-f87d-4820-83b1-a84a168e5536', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-02-07 06:58:53', '2023-02-07 06:59:41', 133, 'applyCv/pyDlJC9p28tPFV3HBabP5eYGDMruMWz4tIwatwvM.pdf', 2, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'new', '\"16\"', NULL, NULL),
('49b71eff-56b1-4293-9780-3a62c8f3bfab', '8343d57e-3501-42fe-8bb1-2aee3e90e437', 'ecfd659e-1736-4146-958a-090eef75048a', '2023-02-08 04:10:19', '2023-02-08 05:28:43', 142, 'applyCv/y8QuZibG3syEzUj7RbhGAcR8UMe0Zi6qr4pSKGBu.pdf', NULL, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 'failed', '\"28\"', 'rwqewq', '2023-02-08 12:25:00'),
('4a400a85-5d3a-41c7-94ab-b1b7968956cd', 'c53e66cd-14e9-49c8-8803-bbf991543f64', '6b67cee5-b3c3-4c44-8467-c48f465bfa04', '2023-02-07 03:12:01', '2023-02-07 03:12:01', 125, 'applyCv/uF2v9apPcRcpNhboDzuFZXJQXuXy4EbU2qPWqmk0.pdf', 2, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'new', '\"17\"', NULL, NULL),
('4f5d9dd1-75bf-4b0b-af6a-89da02613173', '60269aee-d9d7-4c49-ba25-f53b1cb7f7a8', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-02-08 03:53:35', '2023-02-08 05:34:28', 140, 'applyCv/W7piJwn9Yr38xf8hndb8DpksdPieqdmZ7QWE8VeL.pdf', 2, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 'failed', '\"16\"', 'qưe', '2023-02-08 12:29:00'),
('55f2d471-f59e-4d81-879d-e14ac89fbd93', 'fcde6f0d-6ccf-4bc8-a885-e448e1c41762', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-02-07 04:35:26', '2023-02-07 04:45:24', 131, 'applyCv/cWmzBDMwyf8Q16WDd2NHesLGP2IZ60b9tE9jCLF1.pdf', 2, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 'failed', '\"16\"', 'hehe', '2023-02-07 23:35:00'),
('7b847bd0-000c-4740-8a6c-58419b75e37d', '6f5fe161-cfa1-432f-b43a-e2e7a2ad29a8', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-02-02 04:39:22', '2023-02-06 11:36:36', 121, 'applyCv/wDsc1m6R7NwnhlOT3AWgd3gnHhd3d6obgUpYJPvc.pdf', 2, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'interview', '\"28\"', NULL, '2023-02-15 18:36:00'),
('7be9bafd-31d8-4c7d-8f6a-28bf56cd7e7e', 'dfd26b54-4bb0-4070-9c1e-233a7bc83de4', 'ecfd659e-1736-4146-958a-090eef75048a', '2023-02-09 09:21:37', '2023-02-14 08:05:10', 145, 'applyCv/1pLaXaBm74w2wCrRrXdWpp44k5roQRWfuJMBJZ3m.pdf', NULL, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 'new', '\"17\"', NULL, NULL),
('7cae553b-1845-429a-a31c-812408ea3bed', '137ebc97-e6a7-45e5-bce0-5e979db6fb90', '0da1dd50-8922-4160-97af-9ad3d5429ef3', '2023-02-14 10:12:43', '2023-02-14 10:12:43', 147, 'applyCv/09D9CaSj3svLC85f7GWjb4oRdPkSk67RHh2cYYra.pdf', 2, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'new', '\"17\"', NULL, NULL),
('7d35cf2e-3a85-4fb7-ad51-cfae075de436', '060900bd-de7c-4dd1-8852-eeed9911fae7', 'fa5b5751-bd85-433a-a606-ab847bc07f76', '2023-02-08 02:18:54', '2023-02-08 03:25:51', 138, 'applyCv/9yXxq69cmRDsjBfqeNrAzs6oTIYynQdGzNmi4nWd.pdf', 2, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 'rejected', '\"21\"', 'eqwq', NULL),
('882998c0-b055-4fb5-a5f0-c264f1f14540', '994bd018-7243-428d-98a5-e4c01dad5eb2', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-02-07 07:01:56', '2023-02-08 01:58:39', 135, 'applyCv/xMOje6aCZ6rVIvsZEHgES5ZcOe8F2HDB0RwGFPKK.pdf', 2, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'failed', '\"16\"', 'uk', '2023-02-07 15:47:00'),
('89a1d5c1-9935-4971-9ace-8a2bd0912983', 'a2b6128f-2aca-4455-a94e-b26f2ec57f2d', '3c24cc9d-ccc0-4d6d-a1c6-35dccb0dac27', '2023-02-09 09:26:17', '2023-02-09 09:27:44', 146, 'applyCv/9DXJop7gAUXnkW64j9Krnt4g9vWCeUj2iaEi7wCt.pdf', NULL, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'interview', '\"17\"', NULL, '2023-02-09 16:27:00'),
('8a99408a-3913-44c2-aa82-653c3beaf23b', '332870c0-c8a6-494a-97e6-721a8270e96e', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-03-13 10:55:09', '2023-03-13 10:55:09', 150, 'applyCv/YG12yvs0V2lUFRcANR3xK0ZjZad61Mu8JGW9Wn5T.pdf', NULL, NULL, 'new', '\"17\"', NULL, NULL),
('9d0ddff9-b72f-4103-9772-b8297700dbac', '66c6980c-bcf9-4c45-bfce-4ba3d5e210e3', '6b67cee5-b3c3-4c44-8467-c48f465bfa04', '2023-02-06 11:41:45', '2023-02-06 11:42:11', 122, 'applyCv/d7PKUInLpqL3jIgoN1xkBIwcV1JIlqCVNS4NDIhT.pdf', 2, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'interview', '\"16\"', NULL, '2023-02-08 18:42:00'),
('a3362844-6924-488a-93a1-2c77c62b48e2', '0c791517-cc95-4037-bdc5-6f9bed9d7166', 'fa5b5751-bd85-433a-a606-ab847bc07f76', '2023-02-07 08:38:56', '2023-02-08 02:09:54', 136, 'applyCv/dk8U4jysT4QDYBiB2de4ebkyO62WpG059WxqEmBb.pdf', NULL, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 'interview', '\"28\"', NULL, '2023-02-08 09:09:00'),
('a67b290e-86d7-490a-b5f0-fa6aad443c9b', '963b1e24-979a-4910-b635-40e162e0d191', '6b67cee5-b3c3-4c44-8467-c48f465bfa04', '2023-02-07 03:58:49', '2023-02-07 04:46:58', 128, 'applyCv/5aDVDLuh6FGX74oeOp2pQQLBgkkJORvmYf3GqkSw.pdf', 2, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'failed', '\"17\"', 'qưeqw', '2023-02-07 11:12:00'),
('b0eadd83-366b-4bc9-9b5f-a33bbc1e88cf', '4374c0d8-15d0-405c-b75d-eef31bfe1074', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-02-02 04:28:54', '2023-02-09 10:59:17', 118, 'applyCv/XdmEYAdalIPH4gpIi4QehjKdsQY5NkWUi03TjJrb.pdf', NULL, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 'interview', '\"25\"', NULL, '2023-02-09 17:59:00'),
('c24d12bb-c2fd-4393-ad9b-09644b36e718', 'aa2e896f-a30a-46a5-86c5-4f6460237e56', '6b67cee5-b3c3-4c44-8467-c48f465bfa04', '2023-02-07 03:55:58', '2023-02-07 03:55:58', 127, 'applyCv/NApLoeRoTi90UoSrWe2IwxCEsTnGY9GHxwbz4JRC.pdf', 2, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'new', '\"16\"', NULL, NULL),
('ce8285e3-8754-475b-b7c3-91eb044033d7', '82697acc-4879-4d3c-b533-c320d8d0f7b8', 'ecfd659e-1736-4146-958a-090eef75048a', '2023-02-08 09:44:54', '2023-02-09 08:41:45', 143, 'applyCv/xLVJsCIi43o6An4x3XYdrFJPxT2XKVjBvqCnac81.pdf', 2, '4d69b2b9-6b9a-4052-821b-b6b5c02cd46b', 'failed', '\"16\"', 'fdsa', '2023-02-09 14:40:00'),
('d08761b6-eb74-4caa-9911-41bb4cbd9b64', '081c8f39-29e1-4d65-86da-464208e5c79c', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-03-13 10:56:29', '2023-03-13 10:56:29', 151, 'applyCv/nnDSCGCxpGUfiQ91dEpvlX0dFPDvdjFYENpgq9xV.pdf', NULL, NULL, 'new', '\"16\"', NULL, NULL),
('d1ecf15e-454d-49cb-83ea-a7756cc3a508', '0b638f0d-c85b-4339-9fb7-ae11212c5ebb', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-02-07 06:58:05', '2023-02-07 06:58:05', 132, 'applyCv/2aPAiKE9GXRzTGxvfN5V5Kd0nrmnPS6KmhxmmZIE.pdf', 2, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'new', '\"17\"', NULL, NULL),
('ee8eb6ee-4673-47d4-84a5-ee30d7182c17', '561dd0c1-bdb9-4e5e-9285-0735e59d2cca', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-03-13 10:54:00', '2023-03-13 10:54:00', 149, 'applyCv/HTZNKaDC88b6DQTp11qwszOHlhIEPL6lqUPcwqzh.pdf', NULL, NULL, 'new', '\"21\"', NULL, NULL),
('fe849def-90bf-4875-840e-eb5528396207', 'db7e380e-e78e-4219-90b8-a37474cb7167', 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', '2023-02-02 04:29:42', '2023-02-03 03:35:12', 119, 'applyCv/JP6bnr88OuqyulvQS3lG6Q2wJZhQuVfcfZCp3BBt.pdf', 2, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'failed', '\"23\"', 'wqew', '2023-02-04 10:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'uuid gen auto in code',
  `category_id` bigint UNSIGNED NOT NULL,
  `highlight` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `viewer` int NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `category_id`, `highlight`, `title`, `slug`, `content`, `banner`, `viewer`, `created_at`, `updated_at`, `code`) VALUES
('066fe7cb-d83a-422d-8845-849c715bf2b1', 15, 'tffyawdas', 'khjhjmad', 'khjhjmad', '<p>oiuytrew</p>', 'imageblogs/Rbw2pgaXQF6yIgpw3SGr0Kvr1NsVHrCWlNqRvauB.png', 0, '2023-02-09 09:23:26', '2023-02-09 09:23:26', 31),
('0c4b5604-5a9d-4fa0-a0d9-403d9337fedb', 15, 'Ngoại ngữ là một trong những chiếc “chìa khóa quan trọng” để giúp mỗi người có thể tiến xa hơn trong tương lai.', 'Tổng kết khóa học giao tiếp tiếng Anh', 'tong-ket-khoa-hoc-giao-tiep-tieng-anh', '<p>Với sự chuy&ecirc;n nghiệp từ giảng vi&ecirc;n cũng như khả năng bắt nhịp tốt nội dung b&agrave;i học của c&aacute;c HaDevers, kh&oacute;a học lu&ocirc;n được giữ được trạng th&aacute;i tương t&aacute;c rất tốt. Giảng vi&ecirc;n thường d&agrave;nh khoảng 5-10 ph&uacute;t để học vi&ecirc;n chia nh&oacute;m, mỗi nh&oacute;m gồm 2 th&agrave;nh vi&ecirc;n. Mọi người sẽ c&ugrave;ng nhau thảo luận v&agrave; đưa ra đ&aacute;p &aacute;n từ c&acirc;u hỏi được đặt ra tại buổi học bằng c&aacute;ch &aacute;p dụng những kiến thức đ&atilde; được trang bị sau mỗi buổi học trước đ&oacute;. Cuối c&ugrave;ng giảng vi&ecirc;n sẽ g&oacute;p &yacute; về những &yacute; tưởng hoặc l&agrave; kết quả b&agrave;i tập của từng nh&oacute;m.</p>\r\n\r\n<p><img alt=\"\" src=\"https://tuyendung.hachinet.com/storage/uploads/HCN%20A%CC%89NH%2001_1667817223.png\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Th&ecirc;m nữa, trong c&aacute;c buổi dạy, nhiều tr&ograve; chơi nh&oacute;m kết hợp c&ugrave;ng nội dung b&agrave;i học lu&ocirc;n được tổ chức để k&iacute;ch th&iacute;ch khả năng vốn c&oacute; cũng như tư duy học tập của c&aacute;c học vi&ecirc;n. Từ đ&oacute; mọi người sẽ biết c&aacute;ch ph&aacute;t huy v&agrave; khắc phục một c&aacute;ch tốt nhất ưu v&agrave; nhược điểm hơn.</p>\r\n\r\n<p><img alt=\"\" src=\"https://tuyendung.hachinet.com/storage/uploads/HCN%20A%CC%89NH%2002_1667876784.png\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Xuy&ecirc;n suốt 4 th&aacute;ng học tập (từ ng&agrave;y 11/05/2022 đến 12/09/2022), d&ugrave; c&ograve;n một số điều chưa thực sự đạt kết quả như mong muốn. Nhưng nh&igrave;n chung, kh&oacute;a học vẫn được tổ chức kh&aacute; th&agrave;nh c&ocirc;ng bởi sự tương t&aacute;c hiệu quả giữa người dạy v&agrave; người học, đem lại nhiều gi&aacute; trị kh&ocirc;ng chỉ cho mỗi c&aacute; nh&acirc;n m&agrave; c&ograve;n cho cả c&ocirc;ng ty.</p>\r\n\r\n<p><img alt=\"\" src=\"https://tuyendung.hachinet.com/storage/uploads/HCN%20A%CC%89NH%2003_1667817507.png\" /></p>\r\n\r\n<p>Kết th&uacute;c Kh&oacute;a học Tiếng Anh, Ban tổ chức Kh&oacute;a học cũng đ&atilde; nhận được nhiều phản hồi rất t&iacute;ch cực đến từ c&aacute;c HaDevers. Hầu hết mọi người đều cảm thấy giảng vi&ecirc;n trong c&aacute;c buổi học đều v&ocirc; c&ugrave;ng nhiệt t&igrave;nh, giảng dạy dễ hiểu. Đồng thời, những kiến thức được học cũng ph&ugrave; hợp với khả năng tiếp thu hiện tại của mỗi người, đ&aacute;p ứng được nhu cầu học để biết, hiểu v&agrave; thực h&agrave;nh. Ch&uacute;ng ta h&atilde;y c&ugrave;ng nhau điểm qua chia sẻ của một số bạn học vi&ecirc;n nh&eacute;:</p>\r\n\r\n<p><em>- Bạn Mai Th&agrave;nh Chung</em>: &ldquo;M&igrave;nh cảm thấy tr&igrave;nh độ Tiếng Anh của m&igrave;nh đ&atilde; được n&acirc;ng cao hơn, đ&atilde; c&oacute; thể giao tiếp cơ bản.&rdquo;</p>\r\n\r\n<p><em>- Bạn Nguyễn Thanh Thảo:</em>&nbsp;&ldquo;Sau kh&oacute;a học, m&igrave;nh đ&atilde; tăng th&ecirc;m tr&igrave;nh độ nghe đọc v&agrave; viết, c&oacute; th&ecirc;m vốn từ sử dụng trong c&ocirc;ng việc v&agrave; đời sống.&rdquo;</p>\r\n\r\n<p><em>- Bạn Vũ Diệu Linh:</em>&nbsp;&ldquo;Sau kh&oacute;a học th&igrave; m&igrave;nh cũng &ocirc;n tập được nhiều kiến thức, v&agrave; c&ograve;n &ldquo;nạp&rdquo; được một v&agrave;i mẹo giao tiếp rất hay.&rdquo;</p>\r\n\r\n<p><em>- Bạn Vũ Th&ugrave;y Trang:</em>&nbsp;&ldquo;M&igrave;nh học được nhiều từ mới hơn, biết sắp xếp ngữ ph&aacute;p trong c&acirc;u hơn. Đồng thời cải thiện được khả năng ph&aacute;t &acirc;m.&rdquo;</p>\r\n\r\n<p><em>- Bộ phận Dự &aacute;n chia sẻ:</em>&nbsp;&ldquo;Trước kia, v&igrave; Tiếng Anh chưa tốt, n&ecirc;n khi trao đổi với c&aacute;c Kh&aacute;ch h&agrave;ng nước ngo&agrave;i c&oacute; kh&aacute; nhiều hạn chế. Tuy nhi&ecirc;n, sau Kh&oacute;a học Tiếng Anh vừa qua, mọi người đ&atilde; học hỏi được rất nhiều đ&atilde; tự tin hơn khi trao đổi bằng Tiếng Anh với Kh&aacute;ch h&agrave;ng, v&agrave; tinh thần lu&ocirc;n sẵn s&agrave;ng nhận c&aacute;c dự &aacute;n &Acirc;u Mỹ&rdquo;.</p>\r\n\r\n<p><em>- Bộ phận Nh&acirc;n sự chia sẻ:</em>&nbsp;<em>&ldquo;</em>Qua kh&oacute;a học, mọi người đ&atilde; được bổ sung rất nhiều kiến thức từ vựng chuy&ecirc;n ng&agrave;nh v&agrave; c&aacute;c tips giao tiếp. Việc chi&ecirc;u mộ nh&acirc;n sự, trao đổi với c&aacute;c ứng vi&ecirc;n người nước ngo&agrave;i, v&agrave; hỗ trợ Technical Team phỏng vấn từ giờ trở đi đ&atilde; kh&ocirc;ng c&ograve;n qu&aacute; nhiều trở ngại.&rdquo;</p>\r\n\r\n<p><em>- Bộ phận Kinh doanh chia sẻ:</em>&nbsp;&ldquo;Những trở ngại khi giao tiếp Tiếng Anh trước đ&oacute;, giờ đ&atilde; ho&agrave;n to&agrave;n được khắc phục. Ch&uacute;ng t&ocirc;i đ&atilde; tiến bộ rất nhiều trong việc hiểu, tiếp nhận v&agrave; truyền đạt ch&iacute;nh x&aacute;c nhất y&ecirc;u cầu của Kh&aacute;ch h&agrave;ng.&rdquo;</p>\r\n\r\n<p><em>- &hellip;vv&hellip;</em></p>\r\n\r\n<p><img alt=\"\" src=\"https://tuyendung.hachinet.com/storage/uploads/HCN%20A%CC%89NH%2004_1667817555.png\" /></p>\r\n\r\n<p>Một lần nữa, Ban tổ chức Kh&oacute;a học cũng như to&agrave;n thể c&aacute;c bạn nh&acirc;n sự trong kh&oacute;a xin ch&acirc;n th&agrave;nh gửi lời cảm ơn tới&nbsp;<a href=\"https://hachinet.com/\"><strong>C&ocirc;ng ty Cổ phần Thương mại Giải ph&aacute;p Hachinet Việt Nam</strong></a><strong>&nbsp;</strong>đ&atilde; tạo điều kiện cho mọi người kh&ocirc;ng chỉ c&oacute; th&ecirc;m cơ hội n&acirc;ng cao kiến thức, kỹ năng chuy&ecirc;n m&ocirc;n để ph&aacute;t huy bản th&acirc;n tốt nhất trong m&ocirc;i trường l&agrave;m việc m&agrave; c&ograve;n gi&uacute;p gắn kết mối quan hệ giữa c&aacute;c đồng nghiệp trong c&ocirc;ng ty. Hachinet đ&atilde; rất thường xuy&ecirc;n tổ chức c&aacute;c kh&oacute;a học đ&agrave;o tạo để x&acirc;y dựng một đội ngũ c&aacute;n bộ, nh&acirc;n vi&ecirc;n c&oacute; tr&igrave;nh độ chuy&ecirc;n m&ocirc;n cao nhằm tiến tới một c&ocirc;ng ty ph&aacute;t triển vững mạnh v&agrave; hiệu quả nhất, lu&ocirc;n mang lại sự h&agrave;i l&ograve;ng tốt nhất của kh&aacute;ch h&agrave;ng sau khi sử dụng dịch vụ của c&ocirc;ng ty.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ban tổ chức cũng rất cảm ơn Ms. C&uacute;c v&agrave; Ms. Băng - l&agrave; hai giảng vi&ecirc;n tuyệt vời, với nhiều đ&oacute;ng g&oacute;p trong suốt qu&aacute; tr&igrave;nh giảng dạy. Đồng thời, cũng cảm ơn &yacute; thức, sự nghi&ecirc;m t&uacute;c học tập v&agrave; chấp h&agrave;nh tốt nội quy lớp học của c&aacute;c HaDevers thời gian qua. Mong rằng, Kh&oacute;a học Tiếng Anh đ&atilde; mang lại nhiều gi&aacute; trị d&agrave;i hạn cho c&aacute;c học vi&ecirc;n v&agrave; gi&uacute;p c&aacute;c bạn ph&aacute;t triển được tốt nhất sự nghiệp trong tương lai.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ngo&agrave;i ra, Hachinet chuy&ecirc;n cung cấp dịch vụ cho thu&ecirc;/tuyển thẳng nh&acirc;n sự IT chất lượng cao. Cam kết phục vụ kh&aacute;ch h&agrave;ng với chi ph&iacute; tối ưu, quy tr&igrave;nh r&otilde; r&agrave;ng, platform hiện đại, c&ugrave;ng đội ngũ support v&agrave; mạng lưới HR Freelancers c&oacute; chuy&ecirc;n m&ocirc;n vững v&agrave;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>C&ograve;n b&acirc;y giờ, c&aacute;c bạn h&atilde;y c&ugrave;ng đo&aacute;n xem, kh&oacute;a học tiếp theo sẽ l&agrave; g&igrave; v&agrave; sẽ được tổ chức như thế n&agrave;o nha!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nguồn: tuyendung.hachinet.com</p>', 'imageblogs/BvopVyKPiYnoZrxIFUoi259mbxefUL9VbW7sy3fZ.png', 27, '2022-12-20 06:43:25', '2023-01-04 09:05:14', 9),
('33e3703b-58d1-4970-8d8c-616f7e5bef8b', 16, 'Lập trình viên là một trong những ngành nghề được săn đón nhất hiện nay. Ngành công nghệ hiện nay được coi là “mảnh đất màu mỡ” để phát triển sự nghiệp trong thời đại số.', '7 LỢI ÍCH CỦA VIỆC HỌC LẬP TRÌNH', '7-loi-ich-cua-viec-hoc-lap-trinh', '<h3><strong>A. LẬP TR&Igrave;NH L&Agrave; G&Igrave;?</strong></h3>\r\n\r\n<p>Trước khi đi s&acirc;u v&agrave;o t&igrave;m hiểu lợi &iacute;ch của việc học lập tr&igrave;nh, ch&uacute;ng ta cần hiểu được lập tr&igrave;nh l&agrave; g&igrave;?</p>\r\n\r\n<p>Lập tr&igrave;nh l&agrave; một c&ocirc;ng việc li&ecirc;n quan đến m&aacute;y t&iacute;nh, trong đ&oacute; người lập tr&igrave;nh sử dụng c&aacute;c ng&ocirc;n ngữ lập tr&igrave;nh; c&aacute;c đoạn m&atilde; lệnh (code) v&agrave; c&aacute;c tiện &iacute;ch c&oacute; sẵn. Qua đ&oacute;, họ c&oacute; thể x&acirc;y dựng n&ecirc;n c&aacute;c chương tr&igrave;nh, phần mềm, tr&ograve; chơi, ứng dụng, hệ thống xử l&yacute;, trang web&hellip; Gi&uacute;p người d&ugrave;ng c&oacute; thể thực hiện c&aacute;c mệnh lệnh với m&aacute;y t&iacute;nh, hoặc tương t&aacute;c qua lại với nhau th&ocirc;ng qua c&aacute;c thiết bị điện tử.</p>\r\n\r\n<p>Lập tr&igrave;nh vi&ecirc;n, được hiểu l&agrave; kỹ sư phần mềm, người sẽ sử dụng c&aacute;c ng&ocirc;n ngữ lập tr&igrave;nh kh&aacute;c nhau để thiết kế, x&acirc;y dựng v&agrave; bảo tr&igrave; c&aacute;c chương tr&igrave;nh m&aacute;y t&iacute;nh. C&ocirc;ng việc của lập tr&igrave;nh vi&ecirc;n c&oacute; thể được ph&acirc;n chia cụ thể th&agrave;nh: lập tr&igrave;nh web, lập tr&igrave;nh hệ thống, lập tr&igrave;nh database, lập tr&igrave;nh game, lập tr&igrave;nh mobile.</p>\r\n\r\n<h3><strong>B. 7 LỢI &Iacute;CH CỦA VIỆC HỌC LẬP TR&Igrave;NH.</strong></h3>\r\n\r\n<h3><strong><em>1. Lập tr&igrave;nh gi&uacute;p bạn hiểu hơn về c&ocirc;ng nghệ.</em></strong></h3>\r\n\r\n<p>C&ocirc;ng nghệ c&oacute; ở khắp mọi nơi trong thế giới hiện đại v&agrave; tiếp tục ph&aacute;t triển với tốc độ đ&aacute;nh kinh ngạc. Lập tr&igrave;nh đ&oacute;ng một vai tr&ograve; quan trọng trong qu&aacute; tr&igrave;nh ph&aacute;t triển hiện nay v&agrave; sẽ ng&agrave;y c&agrave;ng trở n&ecirc;n quan trọng dưới &aacute;nh s&aacute;ng của c&aacute;c c&ocirc;ng nghệ tự động v&agrave; robot đời mới. Kiến thức về c&ocirc;ng nghệ th&ocirc;ng tin sẽ cung cấp cho bạn những c&ocirc;ng cụ cần thiết để học tập v&agrave; l&agrave;m việc, cũng như đ&oacute;ng g&oacute;p cho việc định h&igrave;nh tương lai. Học lập tr&igrave;nh, bạn c&oacute; cơ hội l&agrave;m việc ở nhiều vai tr&ograve; như tự động h&oacute;a quy tr&igrave;nh bằng robot (RPA), x&acirc;y dựng hệ thống tự động cho c&aacute;c ng&acirc;n h&agrave;ng hoặc tổ chức chăm s&oacute;c sức khỏe to&agrave;n cầu, thậm ch&iacute; l&agrave;m việc gi&uacute;p cải thiện c&ocirc;ng nghệ trong ph&aacute;t minh &ocirc; t&ocirc; kh&ocirc;ng người l&aacute;i.</p>\r\n\r\n<h3><strong><em>2. Lập tr&igrave;nh gi&uacute;p bạn n&acirc;ng cao kỹ năng giải quyết vấn đề.</em></strong></h3>\r\n\r\n<p>Việc viết chương tr&igrave;nh c&oacute; thể gi&uacute;p con người n&acirc;ng cao tư duy logic bằng c&aacute;ch cho ph&eacute;p bạn nh&igrave;n nhận vấn đề từ nhiều g&oacute;c độ mới mẻ. C&aacute;c dự &aacute;n lập tr&igrave;nh phức tạp được h&igrave;nh th&agrave;nh bằng c&aacute;ch chia nhỏ dự &aacute;n th&agrave;nh những nhiệm vụ hạt nh&acirc;n v&agrave; &aacute;p dụng tư duy phương ph&aacute;p luận để giải quyết vấn đề.</p>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<p><img alt=\"Lập trình viên cần học những gì ?\" src=\"https://laptrinhcuocsong.com/images/lap-trinh-vien.png\" /></p>\r\n\r\n<h3><strong><em>3. Lập tr&igrave;nh c&oacute; thể được &aacute;p dụng để trực quan h&oacute;a dữ liệu.</em></strong></h3>\r\n\r\n<p>L&agrave;m việc với c&aacute;c ng&ocirc;n ngữ lập tr&igrave;nh, chẳng hạn như SQL v&agrave; Python l&agrave; phương ph&aacute;p thực h&agrave;nh tuyệt vời để ph&acirc;n t&iacute;ch v&agrave; trực quan h&oacute;a dữ liệu. Những kĩ năng n&agrave;y c&oacute; lợi cho việc thiết kế những bản b&aacute;o c&aacute;o v&agrave; trang tổng quan dữ liệu phức tạp, khiến ch&uacute;ng trở n&ecirc;n dễ tiếp cận v&agrave; dễ hiểu hơn. Ngo&agrave;i ra, lập tr&igrave;nh l&agrave; một kỹ năng c&oacute; thể &aacute;p dụng cho nhiều vai tr&ograve; kh&aacute;c nhau t&ugrave;y thuộc v&agrave;o mục đ&iacute;ch dữ liệu m&agrave; một doanh nghiệp muốn quản l&yacute;, ngay cả khi doanh nghiệp ấy nằm ngo&agrave;i ng&agrave;nh c&ocirc;ng nghệ. Ta c&oacute; thể h&igrave;nh dung về vai tr&ograve; của lập tr&igrave;nh đối với ng&agrave;nh Marketing: bạn c&oacute; thể sử dụng c&aacute;c kỹ năng bạn học được từ việc lập tr&igrave;nh để hiểu dữ liệu hiệu suất, xu hướng của kh&aacute;ch h&agrave;ng v&agrave; sử dụng điều n&agrave;y để th&ocirc;ng b&aacute;o chiến lược tiếp thị của bạn.</p>\r\n\r\n<h3><strong><em>4. Lập tr&igrave;nh c&oacute; thể ho&agrave;n thiện sự s&aacute;ng tạo của mỗi c&aacute; nh&acirc;n.</em></strong></h3>\r\n\r\n<p>Một lợi &iacute;ch kh&aacute; th&uacute; vị kh&aacute;c của việc lập tr&igrave;nh đ&oacute; l&agrave; n&oacute; c&oacute; thể gi&uacute;p bạn thể hiện khả năng s&aacute;ng tạo của m&igrave;nh tr&ecirc;n mạng. Điển h&igrave;nh l&agrave; việc bạn muốn x&acirc;y dựng một website, bạn sẽ phải sử dụng những kiến thức li&ecirc;n quan đến 3 ng&ocirc;n ngữ lập tr&igrave;nh cơ bản, đ&oacute; l&agrave;: HTML, CSS v&agrave; JavaScript. Bạn sẽ sử dụng HTML v&agrave; CSS để x&acirc;y dựng giao diện người d&ugrave;ng (Frontend). C&aacute;c thẻ HTML được sử dụng để thể hiện c&aacute;c th&agrave;nh phần giao diện kh&aacute;c nhau của trang web. CSS sẽ quyết định phong c&aacute;ch, bố cục v&agrave; c&aacute;ch c&aacute;c phần tử HTML cần được hiển thị l&ecirc;n m&agrave;n h&igrave;nh. C&ograve;n JavaScript chịu tr&aacute;ch nhiệm l&agrave;m cho c&aacute;c thẻ HTML trở n&ecirc;n động. Ngo&agrave;i ra, JavaScript c&ograve;n đi k&egrave;m với nhiều ng&ocirc;n ngữ kh&aacute;c như Python, PHP, ASP.Net để tăng t&iacute;nh tương t&aacute;c của trang web. Việc tự tạo ra một trang web cho m&igrave;nh l&agrave; một c&aacute;ch tuyệt vời để gi&uacute;p bạn nổi bật khi thiết kế danh mục đầu tư trực tuyến của m&igrave;nh hoặc tạo n&ecirc;n một bản sắc trực quan mạnh mẽ cho thương hiệu của bạn với tư c&aacute;ch l&agrave; một chủ doanh nghiệp đầy tham vọng.</p>\r\n\r\n<p>Lập tr&igrave;nh cũng c&oacute; thể mở ra nhiều &ldquo;c&aacute;nh cửa&rdquo; với bạn trong c&aacute;c lĩnh vực li&ecirc;n quan đến tư duy s&aacute;ng tạo, chẳng hạn như thiết kế đồ họa, ph&aacute;t triển website hoặc tạo ra c&aacute;c ứng dụng. B&ecirc;n cạnh đ&oacute;, những ng&ocirc;n ngữ lập tr&igrave;nh như HTML hay CSS c&oacute; khả năng sẽ gi&uacute;p đỡ được bạn kh&aacute; nhiều nếu vai tr&ograve; của bạn l&agrave; một nh&agrave; văn cần chỉnh sửa bố cục blog hay soạn một email hoặc bản khảo s&aacute;t.</p>\r\n\r\n<h3><strong><em>5. Ng&ocirc;n ngữ lập tr&igrave;nh được sử dụng thống nhất tr&ecirc;n to&agrave;n thế giới.</em></strong></h3>\r\n\r\n<p>Mỗi quốc gia đều sở hữu một ng&ocirc;n ngữ kh&aacute;c nhau, thế nhưng ng&ocirc;n ngữ lập tr&igrave;nh lại ch&iacute;nh l&agrave; ng&ocirc;n ngữ phổ qu&aacute;t được cả thế giới sử dụng m&agrave; kh&ocirc;ng cần dịch. Khi di chuyển đến nhiều quốc gia tr&ecirc;n thế giới, ch&uacute;ng ta đ&ocirc;i khi sẽ vướng phải những cản trở trong việc giao tiếp bởi bất đồng ng&ocirc;n ngữ. Tuy nhi&ecirc;n, v&igrave; c&aacute;c ng&ocirc;n ngữ lập tr&igrave;nh ở tr&ecirc;n thế giới đều giống nhau n&ecirc;n đ&acirc;y l&agrave; một kỹ năng bạn c&oacute; thể mang theo đến bất kỳ đất nước n&agrave;o. Kỹ năng lập tr&igrave;nh c&oacute; thể khiến bạn trở th&agrave;nh một ứng cử vi&ecirc;n s&aacute;ng gi&aacute; trong nhiều buổi phỏng vấn kh&aacute;c nhau, c&oacute; khả năng được tuyển dụng cao v&agrave; c&oacute; khả năng ph&aacute;t triển mạnh mẽ ở bất k&igrave; m&ocirc;i trường n&agrave;o.</p>\r\n\r\n<h3><strong><em>6. T&iacute;nh linh hoạt của lập tr&igrave;nh.</em></strong></h3>\r\n\r\n<p>Trong thế giới c&ocirc;ng nghệ ng&agrave;y nay, c&agrave;ng nhiều kỹ năng th&igrave; bạn c&agrave;ng c&oacute; nhiều gi&aacute; trị. Nếu bạn l&agrave; một chuy&ecirc;n gia về Photoshop v&agrave; Illustrator, b&ecirc;n cạnh đ&oacute;, bạn c&ograve;n c&oacute; thể sử dụng những ng&ocirc;n ngữ lập tr&igrave;nh cơ bản như HTML v&agrave; CSS. Khi ấy, bạn ch&iacute;nh l&agrave; một ứng vi&ecirc;n t&agrave;i năng đối với mỗi doanh nghiệp, mỗi kỹ năng m&agrave; bạn c&oacute; sẽ dần khẳng định được gi&aacute; trị của bạn trong mắt mọi người.</p>\r\n\r\n<p>Những kiến thức v&agrave; kỹ năng n&agrave;y kh&ocirc;ng chỉ khiến bạn thu h&uacute;t sự tin tưởng từ kh&aacute;ch h&agrave;ng m&agrave; c&ograve;n hấp dẫn được những nh&agrave; tuyển dụng lớn. Bạn c&agrave;ng c&oacute; nhiều kỹ năng, c&agrave;ng hiểu r&otilde; về code th&igrave; c&agrave;ng c&oacute; nhiều cơ hội thăng tiến trong sự nghiệp.</p>\r\n\r\n<p><img alt=\"Thu nhập của lập trình viên có cao không?\" src=\"https://caodang.fpt.edu.vn/wp-content/uploads/Cao-%C4%91%E1%BA%B3ng-FPT-Polytechnic.l%E1%BA%ADp-tr%C3%ACnh-vi%C3%AAn.png\" /></p>\r\n\r\n<h3><strong><em>7. Lập tr&igrave;nh l&agrave; một ng&agrave;nh c&oacute; nhiều cơ hội thăng tiến trong tương lai.</em></strong></h3>\r\n\r\n<p>Như đ&atilde; n&oacute;i ở tr&ecirc;n, lập tr&igrave;nh l&agrave; một kỹ năng được đ&aacute;nh gi&aacute; cao ở mỗi doanh nghiệp, đặc biệt l&agrave; c&aacute;c ng&agrave;nh c&ocirc;ng nghiệp li&ecirc;n quan đến m&aacute;y m&oacute;c. Ch&iacute;nh v&igrave; xu thế c&ocirc;ng nghiệp h&oacute;a &ndash; hiện đại h&oacute;a của nước ta ng&agrave;y nay, lập tr&igrave;nh c&oacute; thể n&oacute;i l&agrave; ng&agrave;nh nghề c&oacute; nhiều tiềm năng ph&aacute;t triển nhất trong thời điểm hiện tại. Đồng thời n&oacute; cũng đem lại mức thu nhập cao với nhiều lựa chọn nghề nghiệp, chẳng hạn như Kỹ sư tự động h&oacute;a (RPA), Ph&aacute;t triển phần mềm v&agrave; Quản l&yacute; kỹ thuật. Lập tr&igrave;nh m&aacute;y t&iacute;nh mang đến nhiều cơ hội thăng tiến nghề nghiệp cho mỗi ứng vi&ecirc;n, để bạn c&oacute; thể tiến tới c&aacute;c vị tr&iacute; quản l&yacute; cấp cao, từ đ&oacute; kh&ocirc;ng ngừng học hỏi trong c&ocirc;ng việc v&agrave; mở rộng kỹ năng của bản th&acirc;n.</p>\r\n\r\n<p>Học c&aacute;c ng&agrave;nh chuy&ecirc;n s&acirc;u về m&aacute;y t&iacute;nh hoặc lập tr&igrave;nh sẽ mang lại cho bạn nhiều lợi &iacute;ch v&agrave; cơ hội nghề nghiệp. Nếu bạn đang muốn bắt đầu h&agrave;nh tr&igrave;nh học lập tr&igrave;nh hoặc bắt đầu sự nghiệp trong ng&agrave;nh lập tr&igrave;nh m&aacute;y t&iacute;nh, h&atilde;y theo d&otilde;i Devwork để nắm bắt th&ecirc;m nhiều th&ocirc;ng tin th&uacute; vị về ng&agrave;nh nghề n&agrave;y!</p>', 'imageblogs/C4xFW75gQBnFRCoR0dEA93p2cgL6htlW9fDPqdsI.jpg', 29, '2022-12-20 06:49:54', '2023-01-04 06:55:23', 10),
('36f94887-df1a-4391-a6ce-457c88c4a48f', 15, 'Tham quan, trải nghiệm thực tế tại các doanh nghiệp là hoạt động thường xuyên diễn ra tại công ty Cổ phần thương mại giải pháp Hachinet ...', 'Business Tour Tháng 8', 'business-tour-thang-8', '<p><strong>Nhu cầu nh&acirc;n lực của thị trường IT Việt Nam 2021 nh&acirc;n lực ng&agrave;nh IT</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://tuyendung.hachinet.com/storage/uploads/Picture1_1662544985.png\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Theo c&aacute;c số liệu thống k&ecirc; từ năm 2018 &ndash; 2022 được đưa ra, nhu cầu nh&acirc;n lực cho ng&agrave;nh c&ocirc;ng nghệ th&ocirc;ng tin tại Việt Nam vẫn đang tăng cao li&ecirc;n tục. Dựa tr&ecirc;n B&aacute;o c&aacute;o về thị trường IT Việt Nam 2021 của TopDev, đến năm 2021 Việt Nam sẽ c&ograve;n cần đến 450.000 nh&acirc;n lực trong ng&agrave;nh c&ocirc;ng nghệ th&ocirc;ng tin. Trong khi đ&oacute;, số lượng lập tr&igrave;nh vi&ecirc;n hiện tại của Việt Nam mới chỉ đạt khoảng 430.000 người.</p>\r\n\r\n<p>Thực tế l&agrave;, số lượng ng&agrave;nh học về c&ocirc;ng nghệ th&ocirc;ng tin ở c&aacute;c trường đại học đang mở rộng ng&agrave;y c&agrave;ng nhiều cũng như số lượng cử nh&acirc;n tốt nghiệp chuy&ecirc;n ng&agrave;nh n&agrave;y vẫn tăng cao qua mỗi năm, tại sao vẫn c&oacute; sự ch&ecirc;nh lệch n&agrave;y? Sự thiếu hụt n&agrave;y chủ yếu l&agrave; do tr&igrave;nh độ của lập tr&igrave;nh vi&ecirc;n v&agrave; y&ecirc;u cầu doanh nghiệp đặt ra vẫn chưa thực sự c&acirc;n bằng với nhau. Trong số hơn 55.000 sinh vi&ecirc;n c&ocirc;ng nghệ th&ocirc;ng tin tốt nghiệp mỗi năm chỉ c&oacute; khoảng 16.500 sinh vi&ecirc;n (30%) đ&aacute;p ứng được những kỹ năng v&agrave; chuy&ecirc;n m&ocirc;n m&agrave; doanh nghiệp cần.</p>\r\n\r\n<p><strong>Thực trạng về định hướng c&ocirc;ng việc của c&aacute;c sinh vi&ecirc;n ng&agrave;nh IT</strong></p>\r\n\r\n<p>C&aacute;c bạn sinh vi&ecirc;n n&oacute;i chung, nhất l&agrave; c&aacute;c bạn sinh vi&ecirc;n năm cuối chắc hẳn sẽ c&oacute; những lo lắng về định hướng sau n&agrave;y, về cơ hội việc l&agrave;m cũng như m&ocirc;i trường l&agrave;m việc sau khi ra trường. Đặc biệt, với ng&agrave;nh IT - ng&agrave;nh nghề lu&ocirc;n ph&aacute;t triển v&agrave; biến đổi kh&ocirc;ng ngừng, việc t&igrave;m hiểu về cơ hội việc l&agrave;m cũng như m&ocirc;i trường l&agrave;m việc trước khi rời khỏi ghế nh&agrave; trường l&agrave; rất một điều cần thiết v&agrave; kh&ocirc;ng thể bỏ qua.</p>\r\n\r\n<p><strong>Chương tr&igrave;nh tham quan thực tế của sinh vi&ecirc;n tại c&ocirc;ng ty Hachinet Software</strong></p>\r\n\r\n<p>Hiểu được điều đ&oacute;, ng&agrave;y 19/8 vừa qua, Hachinet Software kết hợp với ban l&atilde;nh đạo trường Học viện C&ocirc;ng nghệ BKACAD đ&atilde; tổ chức 1 buổi Business Tour nhằm gi&uacute;p c&aacute;c bạn sinh vi&ecirc;n c&oacute; th&ecirc;m những trải nghiệm thực tế về c&ocirc;ng việc, từ đ&oacute; chuẩn bị cho m&igrave;nh những h&agrave;nh trang cần thiết trước khi bước ch&acirc;n v&agrave;o thị trường lao động.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://tuyendung.hachinet.com/storage/uploads/e997abe6c8d80d8654c9_1662545064.jpg\" /></p>\r\n\r\n<p>Thay mặt ban l&atilde;nh đạo c&ocirc;ng ty, anh Trần Vũ Ho&agrave;ng - Gi&aacute;m đốc nh&acirc;n sự của Hachinet đ&atilde; c&oacute; những chia sẻ bổ &iacute;ch về c&aacute;c cơ hội l&agrave;m việc v&agrave; ph&aacute;t triển cũng như những định hướng cho c&aacute;c bạn sinh vi&ecirc;n. Th&ocirc;ng qua buổi tham quan, chắc hẳn c&aacute;c bạn sinh vi&ecirc;n đ&atilde; hiểu r&otilde; hơn về cơ hội việc l&agrave;m v&agrave; xu hướng ph&aacute;t triển của ng&agrave;nh IT trong tương lai.</p>\r\n\r\n<p>Đại diện trường Học viện C&ocirc;ng nghệ BKACAD cũng c&oacute; m&oacute;n qu&agrave; thay cho lời cảm ơn v&igrave; sự đ&oacute;n tiếp nồng nhiệt từ ph&iacute;a c&ocirc;ng ty.</p>\r\n\r\n<p><img alt=\"\" src=\"https://tuyendung.hachinet.com/storage/uploads/9c166a8c0eb2cbec92a3_1662545092.jpg\" /></p>\r\n\r\n<p>Đ&acirc;y cũng l&agrave; dịp để c&ocirc;ng ty c&oacute; thể kết nối với c&aacute;c trường đại học, cao đẳng đ&agrave;o tạo về ng&agrave;nh lập tr&igrave;nh v&agrave; t&igrave;m kiếm những bạn trẻ t&agrave;i năng nhiệt huyết. Th&ocirc;ng qua buổi gặp mặt Hachinet hy vọng c&oacute; thể gi&uacute;p c&aacute;c bạn sinh vi&ecirc;n hiểu r&otilde; hơn về c&ocirc;ng việc đồng thời chi&ecirc;u mộ được những &ldquo;ch&uacute; ong v&agrave;ng&rdquo; t&agrave;i năng để tham gia v&agrave;o Hachinet Family.</p>\r\n\r\n<p><strong>Th&ocirc;ng tin li&ecirc;n hệ</strong></p>\r\n\r\n<p>C&ocirc;ng ty cổ phần giải ph&aacute;p thương mại Hachinet</p>\r\n\r\n<p>Địa chỉ: Tầng 2A &ndash; 27A3, Green Stars&nbsp;234 Phạm Văn Đồng &ndash; Cầu Giấy &ndash; H&agrave; Nội</p>\r\n\r\n<p>Hotline: 024 6290 0388</p>\r\n\r\n<p>Email: hello@hachinet.com</p>\r\n\r\n<p>Website: https://tuyendung.hachinet.com</p>', 'imageblogs/QHIbuxu0auWEYcmvN0c0bgg0sO4ttQxOIxMs77WF.jpg', 22, '2022-12-29 09:18:02', '2023-01-06 08:46:52', 12),
('812779e9-f60a-4ba1-8570-257de4c619ef', 15, 'ewrewre', 'ewree', 'ewree', '<p>ewrewr</p>', 'imageblogs/yrKvrbegDuPllnOblhTlM8QnfZKzdKBQgb2PDC0l.jpg', 0, '2023-02-06 11:52:29', '2023-02-06 11:52:29', 24),
('882148c5-a1d0-484f-8478-739beb44d51a', 16, '$blog', '$blog', 'blog', '<p>$blog</p>', 'imageblogs/vUSthjbah4yWTihdkpxlQjjNxTKY8tPx40hGsing.png', 0, '2023-02-02 03:52:11', '2023-02-02 03:52:11', 23),
('b6dd4bde-f4c0-499a-a626-cbca27807d09', 15, 'Chào mừng các bạn đến với Hachinet, trong bài này, chúng ta sẽ tìm hiểu một ngôn ngữ hiện đại khá là phổ biến đó là C#', 'C# là gì ? Tổng quan về C# ....', 'c-la-gi-tong-quan-ve-c', '<h2>1. C# l&agrave; g&igrave;?</h2>\r\n\r\n<p>C# (hay C sharp) l&agrave; một ng&ocirc;n ngữ lập tr&igrave;nh đơn giản, được ph&aacute;t triển bởi đội ngũ kỹ sư của Microsoft v&agrave;o năm 2000, trong đ&oacute; người dẫn đầu l&agrave; Anders Hejlsberg v&agrave; Scott Wiltamuth.</p>\r\n\r\n<p>C# l&agrave; ng&ocirc;n ngữ lập tr&igrave;nh hiện đại, hướng đối tượng v&agrave; n&oacute; được x&acirc;y dựng tr&ecirc;n nền tảng của hai ng&ocirc;n ngữ mạnh nhất l&agrave; C++ v&agrave;&nbsp;<a href=\"https://hachinet.jp/java-systems-development\" target=\"_blank\">Java</a>.</p>\r\n\r\n<p>C# được thiết kế cho Common Language Infrastructure (CLI), m&agrave; gồm Executable Code v&agrave; Runtime Environment, cho ph&eacute;p ch&uacute;ng ta sử dụng c&aacute;c ng&ocirc;n ngữ high-level đa dạng tr&ecirc;n c&aacute;c nền tảng v&agrave; cấu tr&uacute;c m&aacute;y t&iacute;nh kh&aacute;c nhau.</p>\r\n\r\n<p>C# với sự hỗ trợ mạnh mẽ của .NET Framework gi&uacute;p cho việc tạo một ứng dụng Windows Forms hay WPF (Windows Presentation Foundation),... trở n&ecirc;n rất dễ d&agrave;ng.</p>\r\n\r\n<h2>2. Đặc trưng của C#.</h2>\r\n\r\n<p>C&aacute;c đặc điểm để l&agrave;m cho C# l&agrave; ng&ocirc;n ngữ lập tr&igrave;nh chuy&ecirc;n nghiệp được sử dụng rộng r&atilde;i:</p>\r\n\r\n<h3><strong>C# l&agrave; ng&ocirc;n ngữ đơn giản</strong></h3>\r\n\r\n<p>Như ta đ&atilde; biết th&igrave; ng&ocirc;n ngữ C# dựng tr&ecirc;n nền tảng C++ v&agrave;&nbsp;<a href=\"https://hachinet.jp/java-systems-development\" target=\"_blank\">Java</a>&nbsp;n&ecirc;n ng&ocirc;n ngữ C# kh&aacute; đơn giản. Nếu ch&uacute;ng ta th&acirc;n thiện với C v&agrave; C++ hoậc thậm ch&iacute; l&agrave;&nbsp;<a href=\"https://hachinet.jp/java-systems-development\" target=\"_blank\">Java</a>, ch&uacute;ng ta sẽ thấy C# kh&aacute; giống về diện mạo, c&uacute; ph&aacute;p, biểu thức, to&aacute;n tử v&agrave; những chức năng kh&aacute;c được lấy trực tiếp từ ng&ocirc;n ngữ C v&agrave; C++, nhưng n&oacute; đ&atilde; được cải tiến để l&agrave;m cho ng&ocirc;n ngữ đơn giản hơn. Một v&agrave;i trong c&aacute;c sự cải tiến l&agrave; loại bỏ c&aacute;c dư thừa, hay l&agrave; th&ecirc;m v&agrave;o những c&uacute; ph&aacute;p thay đổi.</p>\r\n\r\n<h3><strong>C# l&agrave; ng&ocirc;n ngữ hiện đại</strong></h3>\r\n\r\n<p>Một v&agrave;i kh&aacute;i niệm kh&aacute; mới mẻ kh&aacute; mơ hồ với c&aacute;c bạn vừa mới học lập tr&igrave;nh, như xử l&yacute; ngoại lệ, những kiểu dữ liệu mở rộng, bảo mật m&atilde; nguồn... Đ&acirc;y l&agrave; những đặc t&iacute;nh được cho l&agrave; của một ng&ocirc;n ngữ hiện đại cần c&oacute;. V&agrave; C# chứa tất cả c&aacute;c đặt t&iacute;nh ta vừa n&ecirc;u tr&ecirc;n.&nbsp;</p>\r\n\r\n<h3><strong>C# l&agrave; một ng&ocirc;n ngữ lập tr&igrave;nh hướng đối tượng</strong></h3>\r\n\r\n<p>Lập tr&igrave;nh hướng đối tượng(tiếng Anh: Object-oriented programming, viết tắt: OOP) l&agrave; một phương ph&aacute;p lập tr&igrave;nh c&oacute; 4 t&iacute;nh chất. Đ&oacute; l&agrave; t&iacute;nh trừu tượng (<em>abstraction</em>), t&iacute;nh đ&oacute;ng g&oacute;i (encapsulation), t&iacute;nh đa h&igrave;nh (polymorphism) v&agrave; t&iacute;nh kế thừa (inheritance). C# hỗ trợ cho ch&uacute;ng ta tất cả những đặc t&iacute;nh tr&ecirc;n.&nbsp;</p>\r\n\r\n<h3><strong>C# l&agrave; một ng&ocirc;n ngữ &iacute;t từ kh&oacute;a</strong></h3>\r\n\r\n<p>C l&agrave; ng&ocirc;n ngữ sử dụng giới hạn những từ kh&oacute;a (gồm khoảng 80 từ kh&oacute;a v&agrave; mười mấy kiểu dữ liệu x&acirc;y dựng sẵn). Nếu bạn nghĩ rằng ng&ocirc;n ngữ c&oacute; c&agrave;ng nhiều từ kh&oacute;a th&igrave; sẽ c&agrave;ng mạnh mẽ hơn. Điều n&agrave;y kh&ocirc;ng phải sự thật, bởi nếu bạn học s&acirc;u về C# bạn sẽ thấy rằng ng&ocirc;n ngữ n&agrave;y c&oacute; thể được sử dụng để l&agrave;m bất cứ nhiệm vụ n&agrave;o.</p>\r\n\r\n<p>Ngo&agrave;i những đặc điểm tr&ecirc;n th&igrave; c&ograve;n một số ưu điểm nổi bật của C#:</p>\r\n\r\n<ul>\r\n	<li>C# c&oacute; cấu tr&uacute;c kh&aacute; gần gũi với c&aacute;c ng&ocirc;n ngữ lập tr&igrave;nh truyền thống, n&ecirc;n cũng kh&aacute; dể d&agrave;ng tiếp cận v&agrave; học nhanh với C#.</li>\r\n	<li>C# c&oacute; thể bi&ecirc;n dịch tr&ecirc;n nhiều nền tảng m&aacute;y t&iacute;nh kh&aacute;c nhau.</li>\r\n	<li>C# được x&acirc;y dựng tr&ecirc;n nền tảng của C++ v&agrave; Java n&ecirc;n n&oacute; được thừa hưởng những ưu điểm của ng&ocirc;n ngữ đ&oacute;.</li>\r\n	<li>C# l&agrave; một phần của .NET Framework n&ecirc;n được sự chống lưng kh&aacute; lớn đến từ bộ phận n&agrave;y.</li>\r\n	<li>C# c&oacute; IDE Visual Studio c&ugrave;ng nhiều plug-in v&ocirc; c&ugrave;ng mạnh mẽ.</li>\r\n</ul>\r\n\r\n<h2>3. Lời kết</h2>\r\n\r\n<p>Trong b&agrave;i n&agrave;y ch&uacute;ng ta đ&atilde; t&igrave;m hiểu được c&aacute;c kh&aacute;i niệm ban đầu, đặc điểm v&agrave; tầm quan trọng của Ng&ocirc;n ngữ Lập tr&igrave;nh C#.</p>\r\n\r\n<p>Hachinet Software l&agrave; c&ocirc;ng ty chuy&ecirc;n cung cấp dịch vụ phần mềm tại Việt Nam với đội ngũ tận t&acirc;m, năng động, nhiệt huyết, lu&ocirc;n nỗ lực kh&ocirc;ng ngừng để l&agrave;m h&agrave;i l&ograve;ng kh&aacute;ch h&agrave;ng v&agrave; đối t&aacute;c. Chuy&ecirc;n m&ocirc;n của ch&uacute;ng t&ocirc;i bao gồm:</p>\r\n\r\n<ol>\r\n	<li>Web application (<a href=\"https://hachinet.com/net-systems-development\" target=\"_blank\">.NET</a>,&nbsp;<a href=\"https://hachinet.com/java-systems-development\" target=\"_blank\">JAVA</a>, PHP, etc.)</li>\r\n	<li>Framework (ASP, MVC, AngularJS, Angular6, Node JS, Vue JS)</li>\r\n	<li>Mobile application: IOS (Swift, Object C), Android (Kotlin, Android)</li>\r\n	<li>System applications (Cobol, ERP, etc.),</li>\r\n	<li>New Technology (Blockchain, etc.).</li>\r\n</ol>\r\n\r\n<p>Nếu bạn quan t&acirc;m đến dịch vụ của ch&uacute;ng t&ocirc;i hoặc đang t&igrave;m kiếm đối t&aacute;c gia c&ocirc;ng phần mềm CNTT tại Việt Nam, đừng ngần ngại li&ecirc;n hệ với ch&uacute;ng t&ocirc;i theo địa chỉ contact@hachinet.com.</p>', 'imageblogs/wdt3csZ5ua85TPdUcgE99QudMTRA2XTQ1F83QsX3.jpg', 45, '2022-12-29 09:28:03', '2023-02-01 07:37:41', 21),
('c53bae6f-7137-4d07-a28a-12c93f5d643f', 16, 'qweqe', 'qewewq', 'qewewq', '<p>qeqweq</p>', 'imageblogs/9GbK4mwjXa6rz7iPGlEUFpKhiF2jHeOYPhYGzYMB.png', 0, '2023-02-08 03:39:25', '2023-02-08 03:39:25', 30),
('d249d4c7-f89f-4220-835a-fa2a352591c1', 15, 'Bạn có thể nghĩ rằng bạn không phù hợp với lĩnh vực IT chỉ vì bạn không biết kiến thức chuyên môn, nhưng sự thật là bạn có thể đã có sẵn những kỹ năng mềm phù hợp với những yêu cầu của IT...', '10 KỸ NĂNG QUAN TRỌNG VỚI DÂN IT.', '10-ky-nang-quan-trong-voi-dan-it', '<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li><strong>Kỹ năng giao tiếp</strong></li>\r\n</ol>\r\n\r\n<p>L&agrave; một chuy&ecirc;n gia IT, bạn sẽ cần c&oacute; khả năng giao tiếp tốt với người gi&aacute;m s&aacute;t v&agrave; đồng nghiệp của m&igrave;nh để gi&uacute;p họ giải quyết c&aacute;c vấn đề c&ocirc;ng nghệ v&agrave; khai th&aacute;c tốt hơn sức mạnh của c&aacute;c c&ocirc;ng cụ c&ocirc;ng nghệ của họ. Nếu bạn l&agrave; nh&agrave; ph&aacute;t triển hoặc l&agrave;m việc trong lĩnh vực bảo mật IT, bạn sẽ cần l&agrave;m việc nh&oacute;m tốt v&agrave; truyền đạt tốt &yacute; tưởng của m&igrave;nh với đồng nghiệp. Hầu hết mọi c&ocirc;ng việc IT đều y&ecirc;u cầu kỹ năng giao tiếp tốt, cả bằng lời n&oacute;i v&agrave; bằng văn bản - giao tiếp qua email c&oacute; thể sẽ l&agrave; một phần quan trọng trong c&ocirc;ng việc của bạn. C&ocirc;ng nghệ kh&ocirc;ng phải l&agrave; một lĩnh vực đơn lẻ, mặc d&ugrave; n&oacute; thường được mi&ecirc;u tả như thế n&agrave;o. Nếu bạn th&iacute;ch chia sẻ cho mọi người về c&ocirc;ng nghệ v&agrave; giải th&iacute;ch c&aacute;c m&ocirc;n kỹ thuật theo những thuật ngữ m&agrave; bất kỳ ai cũng c&oacute; thể hiểu được, th&igrave; IT c&oacute; thể d&agrave;nh cho bạn.</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; &nbsp;2. Kỹ năng tổ chức</strong></p>\r\n\r\n<p>C&oacute; kỹ năng tổ chức gi&uacute;p bạn l&agrave;m việc hiệu quả v&agrave; năng suất hơn. Nhiều nghề nghiệp trong lĩnh vực IT rất năng động, mang lại cho bạn cơ hội chuyển sang một số dự &aacute;n v&agrave; nhiệm vụ kh&aacute;c nhau. Đa nhiệm c&oacute; thể l&agrave; một kỹ năng quan trọng - nhưng chỉ khi bạn c&oacute; thể tự tổ chức đ&uacute;ng c&aacute;ch. Khả năng theo d&otilde;i những thứ như lịch tr&igrave;nh v&agrave; c&ocirc;ng việc h&agrave;ng ng&agrave;y của bạn l&agrave; điều cần thiết v&agrave; do đ&oacute;, bạn c&oacute; thể sắp xếp v&agrave; ưu ti&ecirc;n c&aacute;c tr&aacute;ch nhiệm h&agrave;ng ng&agrave;y của m&igrave;nh một c&aacute;ch hiệu quả. Nếu việc tổ chức v&agrave; quản l&yacute; thời gian đến với bạn dễ d&agrave;ng, th&igrave; vai tr&ograve; của bạn trong lĩnh vực c&ocirc;ng nghệ cũng ho&agrave;n to&agrave;n ph&ugrave; hợp.</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; 3. Khả năng ph&acirc;n t&iacute;ch</strong></p>\r\n\r\n<p>Khả năng ph&acirc;n t&iacute;ch mang lại cho bạn lợi thế lớn trong lĩnh vực IT - nơi bạn thường xuy&ecirc;n t&igrave;m ra giải ph&aacute;p hợp l&yacute; cho c&aacute;c vấn đề. Khả năng ph&acirc;n t&iacute;ch cho ph&eacute;p bạn đo&aacute;n c&aacute;c vấn đề c&ocirc;ng nghệ v&agrave; khắc phục ch&uacute;ng. Ch&uacute;ng cho ph&eacute;p bạn l&agrave;m những việc như t&igrave;m ra l&yacute; do tại sao một d&ograve;ng lệnh kh&ocirc;ng mang lại kết quả mong muốn hoặc l&yacute; do tại sao m&aacute;y chủ ngừng hoạt động. Nếu bạn biết ph&acirc;n t&iacute;ch, bạn thậm ch&iacute; c&oacute; thể x&aacute;c định c&aacute;c vấn đề trước khi ch&uacute;ng nảy sinh. Nếu đ&uacute;ng như vậy, c&oacute; lẽ bạn l&agrave; một chuy&ecirc;n gia về IT. V&iacute; dụ về khả năng ph&acirc;n t&iacute;ch bao gồm khả năng tạo - v&agrave; đọc - bảng tổng hợp trong bảng t&iacute;nh, x&aacute;c định xu hướng theo thời gian v&agrave; x&aacute;c định c&aacute;c chỉ số hiệu suất ch&iacute;nh. Việc học c&aacute;ch x&aacute;c định c&aacute;c sự kiện quan trọng v&agrave; loại bỏ c&aacute;c yếu tố ngoại lệ khỏi tập dữ liệu cũng rất quan trọng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; 4. S&aacute;ng tạo</strong></p>\r\n\r\n<p>Khả năng s&aacute;ng tạo c&oacute; thể kh&ocirc;ng phải l&agrave; một y&ecirc;u cầu r&otilde; r&agrave;ng đối với tất cả c&aacute;c c&ocirc;ng việc IT, nhưng đ&oacute; l&agrave; một t&agrave;i năng hữu &iacute;ch ở nhiều người trong số họ. Nếu những &ocirc;ng tr&ugrave;m c&ocirc;ng nghệ như Mark Zuckerberg v&agrave; Steve Jobs kh&ocirc;ng s&aacute;ng tạo, họ sẽ kh&ocirc;ng bao giờ nghĩ ra những sản phẩm đột ph&aacute; v&agrave; những &yacute; tưởng mang t&iacute;nh c&aacute;ch mạng của m&igrave;nh. Ngay cả khi bạn kh&ocirc;ng khao kh&aacute;t trở th&agrave;nh Bill Gates tiếp theo, c&ocirc;ng việc c&ocirc;ng nghệ của bạn c&oacute; thể sẽ y&ecirc;u cầu bạn đưa ra c&aacute;c &yacute; tưởng để cải tiến quy tr&igrave;nh hoặc sản phẩm hoặc gi&uacute;p đồng nghiệp t&igrave;m ra s&aacute;ng kiến giải ph&aacute;p cho c&aacute;c vấn đề c&ocirc;ng nghệ của họ. Tr&ecirc;n thực tế, một trong những y&ecirc;u cầu ch&iacute;nh trong c&aacute;c c&ocirc;ng việc IT ng&agrave;y nay l&agrave; khả năng sử dụng c&ocirc;ng nghệ một c&aacute;ch s&aacute;ng tạo để đ&aacute;p ứng nhu cầu kinh doanh cụ thể hoặc tạo ra một giải ph&aacute;p gi&uacute;p c&ocirc;ng ty tiến l&ecirc;n ph&iacute;a trước.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; &nbsp;5. Quản l&yacute; dự &aacute;n</strong></p>\r\n\r\n<p>L&agrave; một chuy&ecirc;n gia IT, rất c&oacute; thể, bạn sẽ phải thực hiện nhiều dự &aacute;n v&agrave; tr&aacute;ch nhiệm, v&agrave; việc c&oacute; thể đ&aacute;p ứng thời hạn v&agrave; đạt được c&aacute;c điểm chuẩn l&agrave; rất quan trọng. Quản l&yacute; dự &aacute;n tốt li&ecirc;n quan đến khả năng lập kế hoạch chuy&ecirc;n nghiệp, thiết lập v&agrave; đạt được mục ti&ecirc;u, đồng thời l&agrave;m việc với đồng nghiệp để giải quyết c&aacute;c vấn đề của dự &aacute;n. Năng khiếu tự nhi&ecirc;n về quản l&yacute; dự &aacute;n sẽ khiến bạn trở n&ecirc;n ph&ugrave; hợp với nhiều nghề nghiệp trong lĩnh vực c&ocirc;ng nghệ. C&oacute; thể li&ecirc;n tục đưa c&aacute;c dự &aacute;n th&agrave;nh hiện thực với sự gi&uacute;p đỡ của đồng nghiệp chắc chắn c&oacute; thể gi&uacute;p bạn gi&agrave;nh được sự khen ngợi từ cấp tr&ecirc;n v&agrave; đưa bạn v&agrave;o c&aacute;c vị tr&iacute; đ&ograve;i hỏi nhiều tr&aacute;ch nhiệm hơn.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; &nbsp;6. Ki&ecirc;n tr&igrave;</strong></p>\r\n\r\n<p>Nếu bạn kh&ocirc;ng dễ d&agrave;ng từ bỏ những nhiệm vụ kh&oacute; khăn, bạn c&oacute; thể tiến xa trong c&ocirc;ng việc c&ocirc;ng nghệ. C&aacute;c chuy&ecirc;n gia IT giỏi nhất sẵn s&agrave;ng m&agrave;y m&ograve; li&ecirc;n tục với một c&ocirc;ng nghệ cụ thể để t&igrave;m ra giải ph&aacute;p cho những th&aacute;ch thức ngay cả khi những người kh&aacute;c đ&atilde; bỏ cuộc. C&aacute;c c&ocirc;ng việc c&ocirc;ng nghệ cũng y&ecirc;u cầu cam kết học hỏi v&agrave; ph&aacute;t triển bản th&acirc;n - v&igrave; c&aacute;c phương ph&aacute;p thực h&agrave;nh v&agrave; c&ocirc;ng cụ c&ocirc;ng nghệ lu&ocirc;n thay đổi. Bạn phải c&oacute; khả năng ki&ecirc;n tr&igrave; khi c&oacute; th&ocirc;ng tin mới v&agrave; thử th&aacute;ch mới đến với bạn. Khi mọi việc trở n&ecirc;n kh&oacute; khăn, những chuy&ecirc;n gia về IT xuất sắc nhất sẽ gắn b&oacute; với n&oacute; v&agrave; gặt h&aacute;i th&agrave;nh quả tốt đẹp.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; &nbsp;7. Giải quyết vấn đề</strong></p>\r\n\r\n<p>Bạn c&oacute; th&iacute;ch giải quyết c&aacute;c c&acirc;u đố? Như đ&atilde; đề cập ở tr&ecirc;n, IT l&agrave; lĩnh vực thường xuy&ecirc;n phải giải quyết vấn đề. Bạn đ&atilde; bao giờ d&agrave;nh h&agrave;ng giờ để cố gắng t&igrave;m ra l&yacute; do tại sao m&aacute;y t&iacute;nh c&aacute; nh&acirc;n, m&aacute;y in hoặc điện thoại của bạn kh&ocirc;ng hoạt động như những g&igrave; bạn muốn v&agrave; cảm thấy vui vẻ khi l&agrave;m điều đ&oacute;? Một sự nghiệp c&ocirc;ng nghệ th&ocirc;ng tin c&oacute; thể ở ngay trong ng&otilde; của bạn. Rất nhiều kỹ năng kỹ thuật bạn cần cho c&aacute;c vị tr&iacute; kh&aacute;c nhau trong IT c&oacute; thể được thu thập th&ocirc;ng qua việc m&agrave;y m&ograve; v&agrave; thử v&agrave; sai, v&igrave; vậy khả năng giải quyết vấn đề l&agrave; cần thiết để bạn bước v&agrave;o lĩnh vực n&agrave;y.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; &nbsp;8. Nhanh nhạy</strong></p>\r\n\r\n<p>Một phần của việc trở n&ecirc;n nhanh nhạy trong lĩnh vực CNTT bao gồm khả năng t&igrave;m ra c&ocirc;ng nghệ, phần mềm v&agrave; sản phẩm gi&uacute;p n&acirc;ng cao năng suất của ch&iacute;nh bạn v&agrave; năng suất của người kh&aacute;c. C&aacute;c chuy&ecirc;n gia IT biết c&aacute;ch sử dụng tốt c&aacute;c nguồn lực sẵn c&oacute; sẽ c&oacute; xu hướng tỏa s&aacute;ng trong c&ocirc;ng việc của họ. V&agrave; nh&acirc;n vi&ecirc;n IT lu&ocirc;n quan t&acirc;m đến việc t&igrave;m kiếm c&aacute;c nguồn t&agrave;i nguy&ecirc;n mới thường c&oacute; lợi thế hơn. Sự th&aacute;o v&aacute;t đặc biệt hữu &iacute;ch đối với những người c&oacute; &iacute;t kinh nghiệm về c&ocirc;ng nghệ - như những người thay đổi nghề nghiệp. Bạn kh&ocirc;ng cần phải l&uacute;c n&agrave;o cũng c&oacute; c&acirc;u trả lời - bạn chỉ cần biết c&aacute;ch t&igrave;m ra c&acirc;u trả lời.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; &nbsp;9. Sự t&ograve; m&ograve;</strong></p>\r\n\r\n<p>T&igrave;m kiếm niềm vui trong việc học những điều mới v&agrave; thực sự muốn biết mọi thứ hoạt động như thế n&agrave;o l&agrave; những kh&oacute; khăn lớn đối với d&acirc;n IT. N&oacute;i chung, sự t&ograve; m&ograve; về hoạt động b&ecirc;n trong của c&aacute;c thiết bị sẽ gi&uacute;p bạn tiến xa hơn trong lĩnh vực IT. V&iacute; dụ, c&aacute;c chuy&ecirc;n gia bảo mật giỏi nhất c&oacute; &oacute;c t&ograve; m&ograve; bẩm sinh về c&aacute;ch mọi thứ hoạt động b&ecirc;n dưới lớp vỏ bọc, như c&aacute;ch x&acirc;m nhập hệ thống. Nếu kh&ocirc;ng c&oacute; cảm gi&aacute;c t&ograve; m&ograve; n&agrave;y, rất c&oacute; thể họ sẽ kh&ocirc;ng s&aacute;ng tạo trong việc &aacute;p dụng c&aacute;c biện ph&aacute;p kiểm so&aacute;t an ninh cho c&aacute;c quy tr&igrave;nh của c&ocirc;ng ty.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; &nbsp;10. Quan t&acirc;m v&agrave; gi&uacute;p đỡ người kh&aacute;c</strong></p>\r\n\r\n<p>Mặc d&ugrave; IT theo truyền thống kh&ocirc;ng được coi l&agrave; một lĩnh vực &ldquo;trợ gi&uacute;p&rdquo; như y học hoặc c&ocirc;ng t&aacute;c x&atilde; hội, nhưng một phần lớn của hầu hết mọi c&ocirc;ng việc c&ocirc;ng nghệ đều li&ecirc;n quan đến việc gi&uacute;p đỡ mọi người, cho d&ugrave; bạn đang tạo ra c&ocirc;ng nghệ mới gi&uacute;p cuộc sống của mọi người dễ d&agrave;ng hơn hay gi&uacute;p họ t&igrave;m ra c&aacute;ch xung quanh c&aacute;c r&agrave;o cản c&ocirc;ng nghệ. C&ocirc;ng việc c&ocirc;ng nghệ th&ocirc;ng tin l&agrave; một trong những nghề nghiệp tốt nhất cho những người th&iacute;ch gi&uacute;p đỡ người kh&aacute;c. Khả năng l&agrave;m việc nh&oacute;m l&agrave; rất quan trọng, như đ&atilde; n&oacute;i trong b&agrave;i viết n&agrave;y, khi CompTIA được hỏi về tầm quan trọng của l&agrave;m việc nh&oacute;m c&oacute; lẽ l&agrave; kỹ năng mềm quan trọng nhất.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>B&agrave;i viết về &ldquo;10 kỹ năng quan trọng với d&acirc;n IT&rdquo; đ&atilde; được Hachinet tổng hợp v&agrave; bi&ecirc;n tập, mong rằng c&oacute; thể cung cấp cho bạn những th&ocirc;ng tin hữu &iacute;ch.<br />\r\n<img alt=\"\" src=\"https://tuyendung.hachinet.com/storage/uploads/z3285908157522_2bd51610002c0b434d1d8c9e73921203_1648098008.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>', 'imageBlog/6HNcbz2V8CQzEj42mjYcF8aAOvO7JahosySgCC4m.jpg', 30, '2022-12-19 06:43:32', '2023-01-04 09:38:09', 6),
('d6d760a0-1ecb-46a2-bc43-88e9233fb864', 15, '🍻 🍻Bao nhiêu lâu ta chưa hội tụ cùng nhau, bao nhiêu lâu ta chưa \"Cheers\" cùng nhau? Bao nhiêu lâu ta chưa quẩy tới bến cùng nhau? Cũng khá lâu rồi đúng không ạ?🍻 🍻', 'Hachinet Company Trip 2022.', 'hachinet-company-trip-2022', '<p>👊 Với mong muốn tiếp th&ecirc;m năng lượng, tạo m&ocirc;i trường giao lưu,th&uacute;c đẩy tinh thần đồng l&ograve;ng v&igrave; mục ti&ecirc;u ph&aacute;t triển chung trong thời gian tới, gắn kết t&igrave;nh cảm ấm &aacute;p giữa c&aacute;c &quot;Tổ ấm nhỏ&quot; trong &quot;Ng&ocirc;i nh&agrave; chung Hachinet - Devwork&quot;.🏘</p>\r\n\r\n<p>✈️ Ch&iacute;nh v&igrave; vậy HCN &amp; Devwork&nbsp; h&acirc;n hoan tổ chức Hachinet Company Trip .🛩</p>\r\n\r\n<p>Tr&acirc;n trọng k&iacute;nh mời c&aacute;c Hachinet-ers tham dự chương tr&igrave;nh team building thường ni&ecirc;n với chủ đề: Travelling - Family 2022.</p>\r\n\r\n<p>⏰&nbsp;<strong>Thời gian:</strong>&nbsp;10 - 11/9/2022</p>\r\n\r\n<p>📍<strong>&nbsp;Địa điểm:&nbsp;</strong>Vịnh Hạ Long</p>\r\n\r\n<p><strong>Summer trip together with:</strong></p>\r\n\r\n<p>🚤&nbsp;<em>Traveling b&aacute; đạo v&ugrave;ng &ldquo; biển&rdquo;</em></p>\r\n\r\n<p>🌟&nbsp;<em>Gala Dinner nức nở người Hạ Long c&ugrave;ng Dress code: Xanh da trời v&agrave; Đen b&oacute;ng đ&ecirc;m</em></p>\r\n\r\n<p>🎩<em>&nbsp;Sẵn s&agrave;ng l&ecirc;n đồ v&agrave; ENJOY TO PARTY TIME th&ocirc;i n&agrave;o!</em></p>\r\n\r\n<p>V&agrave; cuối c&ugrave;ng l&agrave;: Hỡi anh chị em! H&iacute;t một hơi thật s&acirc;u n&agrave;o! Mọi người đ&atilde; cảm nhận thấy m&ugrave;i gi&oacute; biển Hạ Long đ&atilde; căng tr&agrave;n trong lồng ngực từ ngay l&uacute;c n&agrave;y chưa?&nbsp;</p>', 'imageblogs/rjFGs0wTBi1I4axzwJfWbOondeBnd4wxDJ4zLpC2.png', 47, '2022-12-29 09:30:16', '2023-01-12 08:40:16', 22),
('e553586e-3c31-4d95-b663-f7bd6ab4c728', 15, 'Tối 29/12/2022, TAKUMIHO FC có phục thù thành công trước HACHINET FC ?', 'NỘI CHIẾN HACHINET', 'noi-chien-hachinet', '<p><strong>️🏆🏆🏆NỘI CHIẾN HACHINET</strong><br />\r\n<strong>️⚽ Tối 29/12/2022, TAKUMIHO FC c&oacute; phục th&ugrave; th&agrave;nh c&ocirc;ng trước HACHINET FC ⁉️&nbsp;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>🌨️🌨️Lượt đi trận giao hữu giữa TakumiHO v&agrave; Hachinet FC đ&atilde; diễn ra dưới thời tiết gi&aacute; lạnh của những cơn gi&oacute; đầu đ&ocirc;ng, k&egrave;m theo đ&oacute; l&agrave; những hạt mưa cắt da cắt thịt nhưng kh&ocirc;ng thể n&agrave;o giảm nhiệt được sức n&oacute;ng v&agrave; m&agrave;n tr&igrave;nh diễn hấp dẫn tr&ecirc;n s&acirc;n.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Với sức trẻ, tinh thần nhiệt huyết v&agrave; đam m&ecirc;, hai đội đ&atilde; cống hiến cho kh&aacute;n giả những c&aacute;c pha b&oacute;ng ấn tượng c&ugrave;ng v&ocirc; số b&agrave;n thắng đẹp mắt.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>🏃 Tuy bị dẫn trước với c&aacute;ch biệt 4 b&agrave;n khi kết th&uacute;c hiệp 1, nhưng với bản lĩnh trận mạc cũng như sự gi&agrave; dơ, kinh nghiệm th&igrave; Hachinet FC đ&atilde; c&oacute; m&agrave;n lội ngược d&ograve;ng ngoạn mục trong hiệp thi đấu thứ 2 v&agrave; kết th&uacute;c trận đấu với tỉ số 6-5.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Đ&acirc;y l&agrave; 1 điều tiếc nuối trong l&ograve;ng cầu thủ cũng như những Fan h&acirc;m mộ kh&ocirc;ng quản mưa gi&oacute; ra s&acirc;n cổ vũ của TakumiHO.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>💪💪V&igrave; vậy trong trận lượt về trận thư h&ugrave;ng n&agrave;y, TakumiHO sẽ thi đấu với hơn 100% năng lực để đội b&oacute;ng kh&ocirc;ng chỉ v&ocirc; địch trong l&ograve;ng người h&acirc;m mộ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Chỉ như vậy đủ để thấy trận đấu đ&atilde; n&oacute;ng trước cả giờ khai cuộc.&nbsp;</p>\r\n\r\n<p>Sau tất cả Nh&agrave; Vua chỉ c&oacute; một v&agrave; ai l&agrave; người l&ecirc;n đỉnh sẽ được định đoạt sau 90p tối nay. ️🥇️🥇️🥇</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>👉Hẹn gặp lại tất cả mọi người tại chảo lửa HỌC VIỆN CẢNH S&Aacute;T 👈</p>\r\n\r\n<p>👉 19H30 ng&agrave;y 29/12 👈</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://tuyendung.hachinet.com/storage/uploads/z3926785647814_888c563055e4fea4b97c0c7293a10178_1672289095.jpg\" /><br />\r\n<img alt=\"\" src=\"https://drive.google.com/file/d/1J9MOEUk11DFsWbvz7sh6mGAoG6LRLcmU/view\" /></p>', 'imageblogs/9WfOILUJRtYWobjzgx8OMxPWJexDFm0x1DF3F2m7.png', 6, '2022-12-29 09:16:43', '2023-01-05 09:52:02', 11);

-- --------------------------------------------------------

--
-- Table structure for table `bonuses`
--

CREATE TABLE `bonuses` (
  `id` bigint UNSIGNED NOT NULL,
  `type` enum('role','level') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` int NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bonuses`
--

INSERT INTO `bonuses` (`id`, `type`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 'role', 1, '0', '2022-12-13 04:51:29', '2022-12-13 04:51:29'),
(2, 'role', 2, '0', '2022-12-13 04:51:38', '2022-12-13 04:51:38'),
(3, 'role', 3, '0', '2022-12-13 04:51:52', '2022-12-13 04:51:52'),
(4, 'role', 4, '0', '2022-12-13 04:51:59', '2022-12-13 04:51:59');

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE `candidates` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'uuid gen auto in code',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`id`, `name`, `email`, `phone`, `status`, `created_at`, `updated_at`, `code`) VALUES
('00ff3465-688a-4d3e-ad02-c95c8e0c0792', 'truongqv11', 'truongdq9v@hachinet.com', '01239685477', 'active', '2023-01-04 09:18:33', '2023-01-04 09:18:33', 33),
('0161eb14-e0dc-434f-8813-ef769ed355df', 'Hongo', 'hfdf3@hachinet.com', '0851111111', 'active', '2023-01-03 10:24:22', '2023-01-03 10:24:22', 26),
('060900bd-de7c-4dd1-8852-eeed9911fae7', 'iuytrew', 'iouyttrre@gmail.com', '0327777777', 'active', '2023-02-08 02:18:54', '2023-02-08 02:18:54', 135),
('081c8f39-29e1-4d65-86da-464208e5c79c', 'qweqeqweqw', 'weqweq3@gmail.com', '0363521654', 'active', '2023-03-13 10:56:29', '2023-03-13 10:56:29', 148),
('0b638f0d-c85b-4339-9fb7-ae11212c5ebb', 'hehe35', 'qqwew2@gmail.com', '0352465210', 'active', '2023-02-07 06:58:05', '2023-02-07 06:58:05', 129),
('0c791517-cc95-4037-bdc5-6f9bed9d7166', 'wwwwwrrrrrrr', 'q2w2gt@gmail.com', '0355555555', 'active', '2023-02-07 08:38:56', '2023-02-07 08:38:56', 133),
('137ebc97-e6a7-45e5-bce0-5e979db6fb90', 'tesings2@gmail.com', 'tesings2@gmail.com', '0352412563', 'active', '2023-02-14 10:12:43', '2023-02-14 10:12:43', 144),
('15f37812-378d-4748-8801-d78a341dba3b', 'Trong óc bò', 'hr323@takumi.com', '0855476666', 'active', '2023-01-03 03:45:06', '2023-01-03 03:45:06', 20),
('164e1cdd-a73f-42b6-8530-48dd141b7245', '3ehwrq', 'qwrw@gmail.com', '0345756414', 'active', '2023-01-31 09:37:23', '2023-01-31 09:37:23', 106),
('184d0304-adb5-46cb-af1c-007927979869', 'longkhoahoc', 'phantv2k3+3@gmail.com', '0396569841', 'active', '2023-01-16 16:04:37', '2023-01-16 16:04:37', 78),
('18affe8d-1cda-46cc-b2bd-119a048a21cc', 'alo alo', 'phantv@gmail.com', '0364454413', 'active', '2023-02-02 04:06:39', '2023-02-02 04:06:39', 113),
('18b1ca61-6d24-44a5-a4fb-08dc7cef0010', 'thh', 'fhhh@gmail.com', '0855456489', 'active', '2022-12-15 08:20:47', '2022-12-15 08:20:47', 8),
('1b83fb35-fc4c-46c7-b5b9-0dc36647dd17', 'ewqe', 'qweq@gmail.com', '0346454515', 'active', '2023-01-12 03:31:02', '2023-01-12 03:31:02', 52),
('222dbccd-b7cf-4bc2-a473-4729812e7afc', 'tequate', 'phantv2k3+2@gmail.com', '0365695626', 'active', '2023-01-16 15:45:59', '2023-01-16 15:45:59', 77),
('262882d8-229d-4e12-a484-27a8cb04b43b', 'tớ là hoàng', 'hoangau@gmail.com', '0855975523', 'active', '2022-12-21 03:47:49', '2022-12-21 03:47:49', 14),
('273706d2-63e3-4bf7-b514-d82052f27bf6', 'Leader C', 'Viethoanssg10012002@gmail.com', '0855475861', 'active', '2023-01-05 03:29:27', '2023-01-05 03:29:27', 46),
('2c758fe6-562e-4f8e-a356-03e1985c6bf3', 'yhkuyewr', 'agdjhk@gmail.com', '0673451542', 'active', '2023-01-13 03:04:11', '2023-01-13 03:04:11', 61),
('2cff85c7-10d2-4af1-9731-f9b88507ae9c', 'Trong ngu', 'trongngunhuconcho@gmail.com', '0234567892', 'active', '2022-12-21 03:29:39', '2022-12-21 03:29:39', 13),
('2f2ba561-d945-47ac-8b51-ea793c7be2ed', 'rth', 'weZ@gmail.com', '0216745134', 'active', '2023-01-13 03:15:26', '2023-01-13 03:15:26', 67),
('2f460fe9-fca8-4ff8-9be9-e9c7ebfba745', '23 con vit', 'awee@gmail.com', '0345126541', 'active', '2023-02-02 04:25:40', '2023-02-02 04:25:40', 114),
('3199eeeb-49c9-45ce-9a14-682e997a6cfc', 'kaiw', 'kaiw@gmail.com', '0346585654', 'active', '2023-01-17 17:46:28', '2023-01-17 17:46:28', 96),
('332870c0-c8a6-494a-97e6-721a8270e96e', 'qweeeeee', 'qweq22w@gmail.com', '0362652412', 'active', '2023-03-13 10:55:09', '2023-03-13 10:55:09', 147),
('374cb0a0-bf6e-4067-872b-349a5a304636', 'ewqee', 'fwd@gmail.com', '0352412154', 'active', '2023-02-07 02:54:56', '2023-02-07 02:54:56', 121),
('3e482ede-a33b-4329-b10f-708bc1440f96', 'truongqv111', 'truongdq91v@hachinet.com', '01239685474', 'active', '2023-01-04 09:19:00', '2023-01-04 09:19:00', 34),
('40079887-8f93-4173-b9cc-5890f997d8a1', 'eqweqwe', 'weqwewq@gmnail.com', '0375854521', 'active', '2023-02-01 04:45:04', '2023-02-01 04:45:04', 111),
('4374c0d8-15d0-405c-b75d-eef31bfe1074', 'sdgwseg', 'rqqw3erwr@gmail.com', '0365654121', 'active', '2023-02-02 04:28:54', '2023-02-02 04:28:54', 116),
('46c32388-ca82-4d64-9c19-d67eaf200663', 'em den lam', 'qwyh2@gmail.com', '0346546574', 'active', '2023-02-02 04:33:17', '2023-02-02 04:33:17', 118),
('471b1516-d998-42c5-83d0-c997da2501cc', 'qwe', 'yhkuuykkuy@gmail.com', '0312467412', 'active', '2023-01-13 03:18:06', '2023-01-13 03:18:06', 68),
('479b008c-6c80-4efe-ab28-bd2168ac071f', 'Trong óc chó', 'trongtb@hachinet.com', '0978592335', 'active', '2022-12-15 08:22:28', '2022-12-15 08:22:28', 9),
('49372889-edd2-40ca-b27c-6eb148218a3a', 'hoane', 'hoangd23v@hachinet.com', '0258964785', 'active', '2023-01-04 09:13:44', '2023-01-04 09:13:44', 29),
('4f6ad03a-881a-455f-9988-ab25d996aae4', 'Trong cú cu', 'admin@takumi.com', '0856459876', 'active', '2022-12-07 09:52:45', '2022-12-07 09:52:45', 4),
('50702808-a996-4ddc-9bdb-20f263642b50', 'fdgf', 'admin5@takumi.com', '0855473568', 'active', '2022-12-15 08:18:41', '2022-12-15 08:18:41', 7),
('5083a480-28f7-4637-83ce-9473a32f037a', 'eqwe', 'qwe6tum@gmail.com', '03412152154', 'active', '2023-02-01 04:52:07', '2023-02-01 04:52:07', 112),
('50f40db8-e0c1-492b-b966-4ff17d9ce3f0', 'truongsdsdqv@hachinet.com', 'truonsdgqv@hachinet.com', '0855475589', 'active', '2023-01-04 09:20:00', '2023-01-04 09:20:00', 36),
('54d11f75-5ce6-4dbc-b4a9-1433e03e1c7d', 'eqwegq', '68lsdf@gmail.com', '0352142145', 'active', '2023-01-17 17:53:27', '2023-01-17 17:53:27', 99),
('5615983f-bd68-4b1d-8ffa-5dbe98a97f65', 'ter', 'twer@gmail.com', '0345745410', 'active', '2023-01-17 17:11:54', '2023-01-17 17:11:54', 85),
('561dd0c1-bdb9-4e5e-9285-0735e59d2cca', 'dqweqwe', 'qeq@gmail.com', '0365874541', 'active', '2023-03-13 10:54:00', '2023-03-13 10:54:00', 146),
('56790945-7da2-46d4-a1e4-101326030d75', '34he', 'eze@gmail.com', '0346154512', 'active', '2023-01-17 17:19:03', '2023-01-17 17:19:03', 89),
('5b9ead35-5efb-408c-af7d-70ce37aced88', 'ewqe', 'eqw@gmail.com', '0316424242', 'active', '2023-01-12 03:57:49', '2023-01-12 03:57:49', 53),
('5e66cda8-33f2-4fb0-b324-8ebc2720b0fd', 'wly8u', 'r65i8@gmail.com', '0364516431', 'active', '2023-01-13 03:13:16', '2023-01-13 03:13:16', 65),
('5f77f9d6-8703-4ee3-8ade-34fae389e4e2', 'twrer', 'rew@gmail.com', '0369874541', 'active', '2023-01-17 17:41:29', '2023-01-17 17:41:29', 95),
('60269aee-d9d7-4c49-ba25-f53b1cb7f7a8', 'iuywer', 'dsf@gmail.com', '0352412121', 'active', '2023-02-08 03:53:35', '2023-02-08 03:53:35', 137),
('606b4797-0be5-493c-93ca-0c5a343a5207', 'EOEQ', 'wew@gmail.com', '0379754615', 'active', '2023-01-13 03:00:17', '2023-01-13 03:00:17', 60),
('612daa7a-86ff-4b3f-b004-dff4ee5777d7', 'WHEE', 'hoangdv1@hachinet.com', '0855475524', 'active', '2022-12-07 09:56:46', '2022-12-07 09:56:46', 5),
('63290b13-f3fb-4f53-be7d-1abc3e129c3b', 'ăfawfawfa', 'fwfwwf@gmail.com', '0363222222', 'active', '2023-02-07 07:00:11', '2023-02-07 07:00:11', 131),
('66b9df14-aba0-40b9-8dc7-c52ec15861f2', 'Viethoang10012002@gmail.co', 'Viethoang100f12002@gmail.com', '0855475545', 'active', '2023-01-04 09:19:27', '2023-01-04 09:19:27', 35),
('66c6980c-bcf9-4c45-bfce-4ba3d5e210e3', 'grqwfqww', 'rge@gmail.com', '0345854121', 'active', '2023-02-06 11:41:45', '2023-02-06 11:41:45', 120),
('674b8781-b4f1-4965-a36a-aa152d944b37', 'Vieth', 'Viethoanfg10012002@gmail.com', '0121212121', 'active', '2023-01-04 09:15:46', '2023-01-04 09:15:46', 31),
('68eea9ec-1b4b-429b-a59c-fbfd388b33f8', 'eqwe', 'eqerw@gmail.com', '0345451421', 'active', '2023-01-17 17:28:52', '2023-01-17 17:28:52', 92),
('6a1e06a9-4126-4331-b56c-1f7f1ec89a90', 'hihi choi vui', 'ew3@gmail.com', '0396857412', 'active', '2023-01-17 09:22:38', '2023-01-17 09:22:38', 84),
('6d948526-61d9-4726-81f1-5d80463d85a4', 'gio ne', 'pha@gmail.com', '0346154615', 'active', '2023-01-12 03:28:44', '2023-01-12 03:28:44', 51),
('6da7c25b-ad36-489d-9cca-61fffdf82617', 'qrwq', 'rqwr@gmail.com', '0346154121', 'active', '2023-01-17 17:17:08', '2023-01-17 17:17:08', 87),
('6f5fe161-cfa1-432f-b43a-e2e7a2ad29a8', 'qwrfi', 'ahr@gmail.com', '03564675754', 'active', '2023-02-02 04:39:22', '2023-02-02 04:39:22', 119),
('6f87c90e-ee41-4017-bbc6-17cd2f12e3d5', 'eqwt', 'hr@gmail.com', '0365414254', 'active', '2023-01-17 17:52:02', '2023-01-17 17:52:02', 98),
('7120c315-8464-4a39-a49b-8067ca18ad47', 'hee', 'phantv2k3+34@gmail.com', '0236541215', 'active', '2023-03-13 10:52:36', '2023-03-13 10:52:36', 145),
('71321777-6940-45f0-9977-8f79d92909c7', 'eqeq', 'tqgre@gmail.com', '0341214124', 'active', '2023-01-17 17:51:06', '2023-01-17 17:51:06', 97),
('7255e97c-f87d-4820-83b1-a84a168e5536', 'ềqgwj', 'ywewe@gmail.com', '0356969687', 'active', '2023-02-07 06:58:53', '2023-02-07 06:58:53', 130),
('7361afce-c88d-4cc1-a724-a381894eee5e', 'eqw', 'Vietsds0012002@gmail.com', '0855445845', 'active', '2023-01-05 03:31:50', '2023-01-05 03:31:50', 47),
('7595c800-6cef-4cdc-8537-7abb573a3a54', 'hoa452', 'hoangdfv@hachinet.com', '0258964318', 'active', '2023-01-04 09:39:20', '2023-01-04 09:39:20', 39),
('767fd128-72b1-43e6-a4a4-1978741d05df', 'e2e2t323', '232afe@gmail.com', '0352412111', 'active', '2023-02-08 02:16:48', '2023-02-08 02:16:48', 134),
('7d86d82f-205f-496e-b6e7-1403b472722f', 'Leader C', 'salsse@takumi.com', '0855475753', 'active', '2023-01-05 03:33:23', '2023-01-05 03:33:23', 48),
('7ece98d6-055b-4b99-8438-82d36084ed41', 'rky', 'eweqeqweqw@gmail.com', '0349758641', 'active', '2023-01-13 02:59:31', '2023-01-13 02:59:31', 59),
('82697acc-4879-4d3c-b533-c320d8d0f7b8', 'hmhgmfds', 'kjqlmjnh@gmail.com', '0352124445', 'active', '2023-02-08 09:44:54', '2023-02-08 09:44:54', 140),
('826e4190-8fbe-4cbc-8c92-8190b399bb26', 'mn', 'fqq@gmail.com', '0354212146', 'active', '2023-02-08 03:50:08', '2023-02-08 03:50:08', 136),
('8343d57e-3501-42fe-8bb1-2aee3e90e437', 'thoi ma thui', 'khj@gmail.com', '0352164748', 'active', '2023-02-08 04:10:19', '2023-02-08 04:10:19', 139),
('862341e1-9a01-4f8e-8ee2-6eac12d129f5', 'Việt Hoàng', 'hoangdv2@hachinet.com', '0855475555', 'active', '2022-12-26 02:57:46', '2022-12-26 02:57:46', 16),
('873cca0f-4248-4040-a088-4333fa0a290f', 'dsfdfgfg', 'dfffdv@hachinet.com', '0899555554', 'active', '2023-01-03 03:57:19', '2023-01-03 03:57:19', 23),
('885af160-53e3-4820-8195-b87c2b6236dc', 'Trần Trọng', 'sdfgh@gmail.com', '0258520258', 'active', '2023-01-05 03:25:03', '2023-01-05 03:25:03', 43),
('89973088-99bc-4ce7-8997-967e73bbb43c', 'eqqwe', 'tdre@gmail.com', '0345414245', 'active', '2023-01-31 09:43:30', '2023-01-31 09:43:30', 107),
('8a8e7bd1-e342-4aff-b901-4e3e0e3ba9dc', 'qwe', 'qewg@ggmail.com', '0645734551', 'active', '2023-01-13 02:52:22', '2023-01-13 02:52:22', 56),
('8a92dad4-380a-44b8-aada-dafb91d9ecf2', 'hoa', 'phantv2k3+22@gmail.com', '0346194610', 'active', '2023-01-17 09:09:05', '2023-01-17 09:09:05', 83),
('8b2a379a-afff-4467-9ae1-72ba0d69fedc', 'hoanfg nguhoan', 'oahsosa@gmail.com', '0943473843', 'active', '2023-01-03 03:38:26', '2023-01-03 03:38:26', 18),
('8bd7de40-8f2a-487e-8ded-238a937c26c2', 'Trần Trọng', 'Viethoa02@gmail.com', '0321444444', 'active', '2023-01-05 03:19:22', '2023-01-05 03:19:22', 41),
('90757c73-a10e-4ea4-891c-64ef5139634e', 'erwhjrt', 'aw@gmail.com', '0345454545', 'active', '2023-01-17 17:20:46', '2023-01-17 17:20:46', 90),
('963b1e24-979a-4910-b635-40e162e0d191', 'eqwewqeq', 'gnew@gmail.com', '0352164121', 'active', '2023-02-07 03:58:49', '2023-02-07 03:58:49', 126),
('9640560a-2ef4-4ab4-8f30-b1858293dfb0', 'wehjte', 'qwju5@gmail.com', '0312611541', 'active', '2023-01-13 03:10:07', '2023-01-13 03:10:07', 63),
('990cca45-3df9-4e5d-8781-0c3ef9e903b8', '23 con ga', '23conga@gmail.com', '0395456232', 'active', '2023-01-13 02:43:36', '2023-01-13 02:43:36', 54),
('994bd018-7243-428d-98a5-e4c01dad5eb2', 'ddds', 'wew556@gmail.com', '0366666666', 'active', '2023-02-07 07:01:56', '2023-02-07 07:01:56', 132),
('99644bd2-65ae-423c-bf23-952c8ff7f099', 'qew', 'th@gmail.com', '0341542145', 'active', '2023-01-17 17:22:33', '2023-01-17 17:22:33', 91),
('9a83a7a5-9851-4491-afea-2c3e67ce395b', 'Viet', 'Viethoang10012002@gmail.com', '01345698743', 'active', '2023-01-04 09:14:38', '2023-01-04 09:14:38', 30),
('9a9f94d8-30ad-4976-b575-39623b31a749', 'qwewqey', 'jyt@gmail.com', '0345194513', 'active', '2023-01-13 03:09:34', '2023-01-13 03:09:34', 62),
('9ce63f25-ee62-4763-9679-fdf585763e74', 'dfgwg', 'fd@gmail.com', '0396969698', 'active', '2023-02-07 03:10:53', '2023-02-07 03:10:53', 122),
('a290bb4c-b43c-4aa9-922c-967cb9626939', 'yte', 'reh@gmail.com', '0385745625', 'active', '2023-01-17 17:57:13', '2023-01-17 17:57:13', 101),
('a2b6128f-2aca-4455-a94e-b26f2ec57f2d', 'ui,umrymrym', 'rmmrm@gmail.com', '0389874541', 'active', '2023-02-09 09:26:17', '2023-02-09 09:26:17', 143),
('a5e389c8-55a9-41fe-8dda-ef0825131e19', 'thoi tho th', 'yhtrew@gmail.com', '0341215124', 'active', '2023-02-08 04:09:15', '2023-02-08 04:09:15', 138),
('a6d7eb46-8186-4a20-bbc8-ad1b453476d8', 'Ơ kìa', 'admind@takumi.com', '0855475522', 'active', '2022-12-21 03:50:18', '2022-12-21 03:50:18', 15),
('a98cff67-a2f1-46b0-a7cd-66ceed84607e', 'veq', 'qwqwewq@gmail.com', '0354121424', 'active', '2023-02-01 04:40:48', '2023-02-01 04:40:48', 110),
('aa2e896f-a30a-46a5-86c5-4f6460237e56', 'wwwwwwww', 'tywtjk@gmail.com', '0363212124', 'active', '2023-02-07 03:55:58', '2023-02-07 03:55:58', 125),
('aaf24952-0428-401b-b0e2-b56337332094', 'hoang8765432', 'hoang852dv@hachinet.com', '0855475588', 'active', '2023-01-04 09:21:25', '2023-01-04 09:21:25', 38),
('ac476886-7dc7-4aaf-b6ae-1f01066c3e62', 'A trường', 'phantv2k3+9@gmail.com', '0396565854', 'active', '2023-01-17 02:54:40', '2023-01-17 02:54:40', 81),
('ad84c666-cb23-4951-a2ee-8bd3994f8b3f', 'eqwrh', 'ukaw@gmail.com', '0365956414', 'active', '2023-01-31 09:35:52', '2023-01-31 09:35:52', 105),
('b3d376c6-e802-4323-8980-7fe6928f8a0b', 'Trọng đần', 'trongngu@gmail.com', '01245678931', 'active', '2023-01-03 03:40:44', '2023-01-03 03:40:44', 19),
('b420b013-df0c-4b6a-a565-e391e7dbfeed', 'dffff', 'cccc@hachinet.com', '0851475523', 'active', '2023-01-03 10:34:38', '2023-01-03 10:34:38', 27),
('b66643fd-92f8-41da-b217-60a708ecec59', 'dasrrw', 'tt22@gmail.com', '0345475744', 'active', '2023-02-08 09:49:07', '2023-02-08 09:49:07', 141),
('b74cb1fe-39c3-4c41-8c19-c9a533543c60', 'wqeqwher', 'phantv2k3+666@gmail.com', '0365647124', 'active', '2023-01-17 18:00:08', '2023-01-17 18:00:08', 103),
('b7753652-c636-49b6-ab3f-a8478bdd1a1f', 'Trọng ngu', 'trongphoigiongbo@gmail.com', '0954875613', 'active', '2022-12-21 03:28:15', '2022-12-21 03:28:15', 12),
('bc55512d-9f61-43a8-bb3d-9ad51da4262b', 'eqw', 'qwe@gmail.com', '0345214124', 'active', '2023-01-17 17:37:17', '2023-01-17 17:37:17', 94),
('bd9f322a-da13-4058-b5ea-b1f4e05329f2', 'Hoàng Đinh Việt', 'hoangdv@hachinet.com', '0855475523', 'active', '2022-12-02 09:39:43', '2022-12-02 09:39:43', 3),
('bde783f1-b989-4a50-aed5-0f18bf024d72', 'truongqv1', 'truongq9v@hachinet.com', '01239685478', 'active', '2023-01-04 09:17:44', '2023-01-04 09:17:44', 32),
('bee4d88a-c1f6-4d04-bd70-14e03a4209fb', 'caylamrui', 'phantv2k3+54@gmail.com', '0346724312', 'active', '2023-01-16 16:51:00', '2023-01-16 16:51:00', 80),
('beeb8d36-373a-454e-b4d0-d34734d2f8b9', 'ffw', 'tưeetz@gmail.com', '0354121412', 'active', '2023-02-07 03:54:47', '2023-02-07 03:54:47', 124),
('c0961311-610f-443e-8579-2d2379d29ade', 'ton la aio', 'we@gmail.com', '0398653252', 'active', '2023-01-13 02:45:59', '2023-01-13 02:45:59', 55),
('c0bbd005-1cb4-4f6f-9a79-6185f4115bce', 'Việt Hoàng', 'Viethoang1002@gmail.com', '0258963149', 'active', '2023-01-05 03:07:17', '2023-01-05 03:07:17', 40),
('c41ffe0b-a0fa-4771-910e-e0e5391efed3', 'ewq', 'ehghrty@gmail.com', '0354541254', 'active', '2023-01-17 17:18:19', '2023-01-17 17:18:19', 88),
('c53e66cd-14e9-49c8-8803-bbf991543f64', 'liuy', 'wer@gmail.com', '0385654121', 'active', '2023-02-07 03:12:01', '2023-02-07 03:12:01', 123),
('c6b974c6-4886-432a-9038-25bfe56642cd', 'Trần Trọng', 'Viethvv10012002@gmail.com', '0855555523', 'active', '2023-01-05 03:26:23', '2023-01-05 03:26:23', 44),
('c8d9087b-9147-46dc-a050-75ab682f528e', 'eqweqw', 'tnrh@gmail.com', '0354654585', 'active', '2023-01-31 09:44:57', '2023-01-31 09:44:57', 108),
('c9fca887-1507-420d-a252-f2e1dc7418ea', 'luonvuituoi', 'phantv2k3@gmail.com', '0395652132', 'active', '2023-01-16 15:42:12', '2023-01-16 15:42:12', 75),
('ca835647-2bc2-45ff-aa87-e643fa47ec54', 'dwdwadhr', '1wewewe@gmail.com', '0385451421', 'active', '2023-02-07 04:34:28', '2023-02-07 04:34:28', 127),
('cad1f0f5-7b26-4cac-9237-74ab35c1b86e', 'Trọng Bê Đê', 'trongtd@hachinet.com', '0978592330', 'active', '2022-12-15 06:56:53', '2022-12-15 06:56:53', 6),
('d0e16423-eb4e-4314-a3ee-43a74df1ffe6', 'Đinh  Hoàng', 'hoangdv7@hachinet.com', '0855474512', 'active', '2022-12-26 02:58:08', '2022-12-26 02:58:08', 17),
('d15d9236-1973-4e63-b927-a6aaa8426dd3', 'hehe', 'qwjyt@gmail.com', '0356541241', 'active', '2023-01-17 17:58:20', '2023-01-17 17:58:20', 102),
('d456081e-9bcd-4514-ada0-cf98b2c05036', 'wqewq', 'qweqw@gmail.com', '0385454121', 'active', '2023-02-01 04:40:14', '2023-02-01 04:40:14', 109),
('d6510cc2-e5a1-4250-a8ff-eacde1726fc2', 'trong ngu', 'adm4in@takumi.com', '0855475569', 'active', '2022-12-19 09:59:51', '2022-12-19 09:59:51', 10),
('db5ea780-92f2-4358-acc4-605294bb1fc9', 'TTTTT', 'hjgh@gmail.com', '0124567893', 'active', '2023-01-03 04:02:37', '2023-01-03 04:02:37', 25),
('db7e380e-e78e-4219-90b8-a37474cb7167', '23so', 'wewerm@gmail.com', '0326562322', 'active', '2023-02-02 04:29:42', '2023-02-02 04:29:42', 117),
('ddb8ebce-9a47-47ac-835a-96ceec637029', '4r', 'w@gmail.com', '0398989854', 'active', '2023-01-13 02:53:12', '2023-01-13 02:53:12', 57),
('df57b63f-e9ec-4e60-8639-494bdaf95c77', 'A trường takumi', 'qvt.hus@gmail.com', '0365659541', 'active', '2023-01-17 03:27:43', '2023-01-17 03:27:43', 82),
('df807364-e0da-4ede-ac73-f1eaddd8b94e', 'Trần Trọng', 'ang10012002@gmail.com', '0855784848', 'active', '2023-01-05 06:59:37', '2023-01-05 06:59:37', 50),
('dfd26b54-4bb0-4070-9c1e-233a7bc83de4', 'jklkjjhgfdsa', 'lkjhgfe@gmail.com', '0352123232', 'active', '2023-02-09 09:21:37', '2023-02-09 09:21:37', 142),
('dfdecd27-9188-47db-8104-4f45f4c2e74c', 'Trần Trọng', 'Vietg10012002@gmail.com', '0359555555', 'active', '2023-01-05 03:23:59', '2023-01-05 03:23:59', 42),
('e081d061-1550-42ed-bfc5-b9728e7c9e5e', 'heo con', 'heoon@gmail.com', '0365454513', 'active', '2023-02-02 04:26:37', '2023-02-02 04:26:37', 115),
('e5c1c797-2676-49cf-9e77-d81fd997edd8', 'ewq', 'loeqw@gmail.com', '0389461874', 'active', '2023-01-17 17:16:14', '2023-01-17 17:16:14', 86),
('e80d92f4-9b0a-45cc-a44e-cfb4ded9d320', 'Ơ kìa', 'Viethoaxxng10012002@gmail.com', '0123212321', 'active', '2023-01-05 03:28:19', '2023-01-05 03:28:19', 45),
('e8aaed97-aeaf-46ee-975b-c4c165b0c475', 'Hoàng Đinh Việt', 'hoangdv@hachinet', '0855475525', 'active', '2022-12-20 06:48:11', '2022-12-20 06:48:11', 11),
('e9e69635-541e-4fb3-9e82-7f6ac4433665', 'eqwewq', 'eqge@gmail.com', '0375856541', 'active', '2023-01-31 07:46:42', '2023-01-31 07:46:42', 104),
('f07eaddc-a8c4-4543-b845-229c5870f380', 'dsdsd', 'Viethoan185202@gmail.com', '0231456789', 'active', '2023-01-05 03:34:12', '2023-01-05 03:34:12', 49),
('f156328c-c90e-444b-9623-20c2cbb0c1e8', 'dfg', 'dfgd2dv@hachinet.com', '0855475511', 'active', '2023-01-04 09:20:30', '2023-01-04 09:20:30', 37),
('f1a7de56-b586-44be-9454-a2f61f30ba89', 'qwr6j', 'tjys@gmail.com', '0346154215', 'active', '2023-01-13 03:14:50', '2023-01-13 03:14:50', 66),
('f2e6456d-2a24-480b-929d-90f618cf5832', 'qweqwqt', 'ew@gmail.com', '0379857645', 'active', '2023-01-13 02:54:22', '2023-01-13 02:54:22', 58),
('f37c2350-018b-44fd-8a3b-b6ebf00ad224', 'sdfdg', 'trtret@hachinet.com', '0855999999', 'active', '2023-01-03 03:54:42', '2023-01-03 03:54:42', 22),
('f5ba5e29-c0b0-4369-a0dc-7e29420ba629', 'sdf', 'xcccc@gmail.com', '0855477777', 'active', '2023-01-03 03:58:07', '2023-01-03 03:58:07', 24),
('f65a3afc-b4be-423c-ae42-e606164c3092', 'dfsgfhjk', 'truongqv@hachinet.com', '0855475527', 'active', '2023-01-03 10:35:05', '2023-01-03 10:35:05', 28),
('faf50ed4-617b-4b33-bf49-dcdb682666ee', 'sdfhg', 'dfgddv@hachinet.com', '0855666666', 'active', '2023-01-03 03:53:48', '2023-01-03 03:53:48', 21),
('fcde6f0d-6ccf-4bc8-a885-e448e1c41762', 'ejr3', 'qr3wtq@gmail.com', '0369654789', 'active', '2023-02-07 04:35:26', '2023-02-07 04:35:26', 128),
('fce02d57-6c99-4370-9544-e765522d0730', '23 con vit', 'phantv2k3+12@gmail.com', '0369565641', 'active', '2023-01-16 16:07:45', '2023-01-16 16:07:45', 79),
('fd563f84-4665-41ce-b37b-280ec324b27f', 'eqw', 'qew@gmail.com', '0341215214', 'active', '2023-01-17 17:31:38', '2023-01-17 17:31:38', 93),
('fe43958e-367e-48fa-be0a-104f97ea0af3', 'eqew@gmail.com', 'eqebgw@gmail.com', '0365412145', 'active', '2023-01-17 17:55:36', '2023-01-17 17:55:36', 100),
('ffa7422f-907b-4dd1-b71c-65d21b5a851b', 'twe', 'ewrwe@gmail.com', '0379451645', 'active', '2023-01-13 03:12:44', '2023-01-13 03:12:44', 64);

-- --------------------------------------------------------

--
-- Table structure for table `candidates_customers`
--

CREATE TABLE `candidates_customers` (
  `id` bigint UNSIGNED NOT NULL,
  `apply_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `level_id` int NOT NULL,
  `customer_onsite` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `time_onboard` date DEFAULT NULL,
  `time_actual` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `salary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `recruitment_money` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `sale_money` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `salary_13` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `bonus_onsite` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `insurance` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `team_building` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `income` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidates_customers`
--

INSERT INTO `candidates_customers` (`id`, `apply_id`, `level_id`, `customer_onsite`, `time_onboard`, `time_actual`, `start_date`, `end_date`, `created_at`, `updated_at`, `salary`, `recruitment_money`, `sale_money`, `salary_13`, `bonus_onsite`, `insurance`, `team_building`, `income`) VALUES
(1, '3f7475d8-74e6-412e-8d4b-0f095b48739d', 6, NULL, '2022-12-15', '2022-12-15', '2022-12-16', '2022-12-30', '2022-12-15 06:57:26', '2022-12-15 06:57:26', '0', '0', '0', '0', '0', '0', '0', '0'),
(2, 'ed315f0f-443f-4c52-adf5-7e85c73f63ad', 6, NULL, NULL, NULL, NULL, NULL, '2022-12-15 08:18:58', '2022-12-15 08:18:58', '0', '0', '0', '0', '0', '0', '0', '0'),
(3, '38f9820b-a9e0-47e9-a799-66973cb73010', 7, NULL, '2022-12-15', '2022-12-26', '2022-12-30', '2023-01-21', '2022-12-15 08:22:51', '2022-12-15 08:22:51', '0', '0', '0', '0', '0', '0', '0', '0'),
(4, 'd4105d69-2dca-408a-91c5-4c5a63a179a5', 18, NULL, '2023-01-19', '2023-01-12', '2023-01-25', '2023-01-17', '2023-01-17 03:20:18', '2023-01-17 03:20:18', '0', '0', '0', '0', '0', '0', '0', '0'),
(5, '891934e7-898f-4d5e-8d4b-5479a41c7c63', 19, NULL, '2023-01-18', '2023-01-05', '2023-01-27', '2023-01-31', '2023-01-17 03:29:54', '2023-01-17 03:29:54', '0', '0', '0', '0', '0', '0', '0', '0'),
(6, '8cf2d3b0-2bbb-41ab-8fef-2801416546b1', 18, NULL, '2023-02-09', '2023-02-19', '2023-02-18', '2023-02-28', '2023-02-01 10:43:47', '2023-02-01 10:43:47', '0', '0', '0', '0', '0', '0', '0', '0'),
(7, '3b32a9ab-23a6-4241-9640-11e92183a19d', 20, NULL, '2023-02-02', '2023-02-08', '2023-02-16', '2023-02-28', '2023-02-08 05:38:20', '2023-02-08 05:38:20', '0', '0', '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image_path` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `description`, `image_path`, `created_at`, `updated_at`) VALUES
(15, 'Cẩm nang tuyển dụng', 'cam-nang-tuyen-dung', 'kỹ năng kinh nghiệm cần có', 'imageCategories/p1f1llqxVRTEOveRBvATAvzLW4WemVMB4cjlYajv.webp', '2022-12-07 08:08:27', '2022-12-07 08:08:27'),
(16, 'Bản tin công nghệ', 'ban-tin-cong-nghe', 'Cập nhật những công nghệ mới nhất....', 'imageCategory/ZDz8ATIYwWJT6H0K9gNW3YDQUC9zFuF31m05R4Eq.jpg', '2022-12-07 08:09:09', '2022-12-07 08:09:19'),
(21, 'Quà Tặng cuộc sống.', 'qua-tang-cuoc-song', 'Chia sẻ kinh nghiệm, giá trị rong cuộc sống.', NULL, '2022-12-13 04:38:12', '2022-12-13 04:38:19');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint UNSIGNED NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `email` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Đinh Việt Hoàng', 'hoangdv@hachinet.com', 'Đi', '2022-12-28 11:23:44', '2022-12-28 11:23:44'),
(2, NULL, 'Viethoang10012002@gmail.com', NULL, '2023-01-04 08:54:53', '2023-01-04 08:54:53'),
(3, 'Đinh Việt Hoàng', 'hoangdv@hachinet.com', 'Cíu tuiii', '2023-01-04 08:59:17', '2023-01-04 08:59:17');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'uuid gen auto in code',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `assignee` int NOT NULL,
  `status` enum('active','inactive') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` int NOT NULL,
  `view_count` int NOT NULL DEFAULT '0',
  `category_job` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `title`, `slug`, `user_id`, `assignee`, `status`, `created_at`, `updated_at`, `code`, `view_count`, `category_job`) VALUES
('06868875-e221-4948-9461-70a396361253', 'PHP Developer', 'php-developer', '24ed8b74-b209-4458-a67e-a92a761437b3', 0, 'active', '2022-12-19 08:58:06', '2023-01-12 03:28:14', 17, 47, 'business-development'),
('0da1dd50-8922-4160-97af-9ad3d5429ef3', 'eweqeqqqweqwtrjtyktyk', 'eweqeqqqweqwtrjtyktyk', '24ed8b74-b209-4458-a67e-a92a761437b3', 1, 'active', '2023-02-08 06:34:42', '2023-02-08 06:34:42', 40, 0, 'software-engineering'),
('21c2c591-3250-45be-9a06-3f550bf1ea65', 'qewwww', 'qewwww', '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 0, 'active', '2023-02-08 09:36:49', '2023-02-08 09:36:49', 41, 0, 'software-engineering'),
('2cc9438e-f8ef-40a5-a73a-be9428849eb2', 'C++ Engineer (remote)', 'c-engineer-remote', '24ed8b74-b209-4458-a67e-a92a761437b3', 0, 'active', '2023-01-03 08:23:19', '2023-01-12 09:39:35', 26, 55, 'marketing-communication'),
('307ef0d5-4ca4-4baa-9020-e44942d05887', 'eqweqwhgtreh', 'eqweqwhgtreh', '24ed8b74-b209-4458-a67e-a92a761437b3', 2, 'active', '2023-02-02 03:49:27', '2023-02-02 03:49:27', 33, 0, 'marketing-communication'),
('314bc343-ab11-4821-baf8-2486654652cb', 'uewh', 'uewh', '24ed8b74-b209-4458-a67e-a92a761437b3', 3, 'active', '2023-02-06 11:11:43', '2023-02-07 02:53:27', 34, 1, 'project-management'),
('382dad3c-26ee-4128-a87a-958d2e2eb721', 'Golang Engineer', 'golang-engineer', '24ed8b74-b209-4458-a67e-a92a761437b3', 0, 'active', '2022-12-29 07:47:46', '2023-01-16 02:11:46', 24, 49, 'software-engineering'),
('3c24cc9d-ccc0-4d6d-a1c6-35dccb0dac27', 'ewqrerkjh', 'ewqrerkjh', '49a6da64-36a9-4580-b4fb-74c40cd95d9f', 5, 'active', '2023-02-08 10:05:07', '2023-02-08 10:05:07', 42, 0, 'project-management'),
('55126ccd-b0d1-4888-8e26-9e4023c757f9', 'htq', 'htq', '013db4b6-92a7-4cbd-a1d4-fce422684e70', 0, 'active', '2023-02-08 04:14:30', '2023-02-08 04:14:30', 39, 0, 'software-engineering'),
('6b67cee5-b3c3-4c44-8467-c48f465bfa04', 'qwqgqq3qq', 'qwqgqq3qq', '24ed8b74-b209-4458-a67e-a92a761437b3', 0, 'active', '2023-02-06 11:33:01', '2023-02-08 03:49:50', 35, 1, 'human-resources'),
('71c7cd3c-e315-4106-9981-fdadf540d76e', 'VB.Net Developer (Remote)', 'vb-net-developer-remote', '24ed8b74-b209-4458-a67e-a92a761437b3', 0, 'active', '2022-12-29 07:33:59', '2023-01-09 04:47:31', 22, 12, 'software-engineering'),
('8285edcd-644e-465b-8963-b47a1f16a6da', 'DevOps Developer (Remote)', 'devops-developer-remote', '24ed8b74-b209-4458-a67e-a92a761437b3', 0, 'active', '2023-01-03 08:21:44', '2023-01-17 02:50:48', 25, 12, 'human-resources'),
('9025f8a1-5dc9-43f7-b320-6c6e33d3e3de', 'heqwrhq', 'heqwrhq', '013db4b6-92a7-4cbd-a1d4-fce422684e70', 1, 'active', '2023-02-08 03:30:31', '2023-02-08 03:30:31', 37, 0, 'business-development'),
('91e83006-0dc1-4a1f-83f1-063fc122477c', 'Software Architect (SA)', 'software-architect-sa', '1979c566-9db7-4114-b949-7982572fe7b7', 0, 'active', '2022-12-21 05:00:10', '2023-01-12 02:30:30', 18, 81, 'software-engineering'),
('b99cd26c-f356-4749-84f8-2942d7151c51', 'ReactJS Developer (Onsite Đê La Thành)', 'reactjs-developer-onsite-de-la-thanh', '1979c566-9db7-4114-b949-7982572fe7b7', 0, 'active', '2022-12-21 06:52:21', '2023-01-10 08:13:50', 21, 72, 'customer-service'),
('be269dda-94da-4c21-944c-5d80c542578f', 'Flutter Developer (Remote)', 'flutter-developer-remote', '24ed8b74-b209-4458-a67e-a92a761437b3', 0, 'active', '2023-01-03 08:24:40', '2023-01-17 17:58:43', 27, 42, 'business-development'),
('c6a757b3-627b-496f-bf4a-14e088093bb8', 'Frontend Engineer in React (Remote)', 'frontend-engineer-in-react-remote', '1979c566-9db7-4114-b949-7982572fe7b7', 0, 'active', '2022-12-21 05:04:11', '2023-01-10 08:13:58', 20, 13, 'customer-service'),
('e3b51834-9438-42d4-b82e-bba06ec3ea44', 'Frontend Developer', 'frontend-developer', '1979c566-9db7-4114-b949-7982572fe7b7', 0, 'active', '2022-12-21 05:01:27', '2023-01-09 07:30:22', 19, 31, 'software-engineering'),
('ecfd659e-1736-4146-958a-090eef75048a', 'ReactJS Developer (Onsite Đê La Thành)', 'reactjs-developer-onsite-de-la-thanh-2', '24ed8b74-b209-4458-a67e-a92a761437b3', 0, 'active', '2023-02-08 03:34:21', '2023-02-09 09:21:00', 38, 6, 'project-management'),
('f2a77e41-1d0d-4dd0-9ba6-9bd757caa0c7', 'qweeqrqwtgtqqt3', 'qweeqrqwtgtqqt3', '013db4b6-92a7-4cbd-a1d4-fce422684e70', 1, 'active', '2023-02-09 09:22:55', '2023-02-09 09:22:55', 43, 0, 'human-resources'),
('f5ffb696-a6c3-450c-8e58-398c370e89d6', 'Fullstack Developer (Remote)', 'fullstack-developer-remote', '24ed8b74-b209-4458-a67e-a92a761437b3', 0, 'active', '2022-12-29 07:36:56', '2023-01-13 04:12:14', 23, 45, 'customer-service'),
('fa5b5751-bd85-433a-a606-ab847bc07f76', 'Customer Service Specialist (Remote)', 'customer-service-specialist-remote', '24ed8b74-b209-4458-a67e-a92a761437b3', 0, 'active', '2023-01-03 10:43:50', '2023-02-07 08:38:33', 28, 100, 'marketing-communication'),
('faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', 'Business Development Manager (Remote)', 'business-development-manager-remote', '24ed8b74-b209-4458-a67e-a92a761437b3', 0, 'active', '2023-01-03 10:45:38', '2023-03-13 10:56:00', 29, 138, 'business-development');

-- --------------------------------------------------------

--
-- Table structure for table `job_details`
--

CREATE TABLE `job_details` (
  `id` bigint UNSIGNED NOT NULL,
  `job_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` int NOT NULL DEFAULT '0',
  `skills` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deadline` date NOT NULL,
  `level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slot` int NOT NULL,
  `benefit` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `require` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `province_id` bigint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_details`
--

INSERT INTO `job_details` (`id`, `job_id`, `job_type`, `salary`, `skills`, `deadline`, `level`, `exp`, `education`, `slot`, `benefit`, `description`, `require`, `address`, `detail`, `created_at`, `updated_at`, `province_id`) VALUES
(15, '06868875-e221-4948-9461-70a396361253', 'freelancer', 4, '[\"30\"]', '2025-11-21', '19', 'duoi-1-nam', 'cao-dang', 1, '- Thực tập tại Hà Nội (2-3 tháng) sau đó sẽ về làm việc tại thành phố Vinh.\r\n- Tổng thời gian đào tạo: 4-5 tháng.\r\n- Môi trường làm việc trẻ trung, năng động, chuyên nghiệp và có nhiều cơ hội thăng tiến.\r\n- Du lịch 1 lần /năm : du lịch Hè.\r\n- Tổ chức sinh nhật và tặng quà cho nhân viên hàng tháng.\r\n- Các hoạt động khác: Dã ngoại, team building định kỳ theo quý…\r\n- Tham gia các hoạt động vui chơi khác của công ty: CLB bóng đá, Poker, Esport, Bi-a...\r\n- Các chương trình Seminar tổ chức định kỳ về các chủ đề công nghệ.\r\n- Tham gia các lớp học Tiếng Nhật, Tiếng Anh tại công ty.\r\n- Thời gian làm việc: Từ thứ 2 – thứ 6 - Sáng: 08h30-12h00; Chiều 13h30-18h00.', '- Tham gia các dự án xây dựng, phát triển website thương mại điện tử.\r\n- Phối hợp với team phát triển và các bộ phận liên quan để đảm bảo project được thực hiện đúng yêu cầu, đảm bảo chất lượng và deadline.\r\n- Tham gia vào quá trình update, maintain sản phẩm.\r\n- Các công việc khác theo yêu cầu.', '- Ứng viên sắp tốt nghiệp hoặc đã tốt nghiệp.\r\n- Có kiến thức lập trình với PHP.\r\n- Có tư duy lập trình tốt với OOP.\r\n- Hiểu biết về GIT.\r\n- Có tư duy logic, kỹ năng giải quyết vấn đề tốt.\r\n- Làm việc độc lập và làm việc nhóm tốt.', 'Hà Nội', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2022-12-19 08:58:06', '2023-01-10 02:49:55', 1),
(16, '91e83006-0dc1-4a1f-83f1-063fc122477c', 'full-time', 11, '[\"30\"]', '2023-07-31', '19', '2-nam', 'dai-hoc', 2, '- Attractive income up to 35,000,000 VND.\r\n- Connection bonus: 1~10 million bonus for anyone who introduces friends and acquaintances to the company.\r\n- Working with large and advanced systems, have the opportunity to develop comprehensive technical skills with complex problems, requiring high accuracy.\r\n- Become one of the influential KeyPerson in the project, high chance to become Leader, Project Manager.\r\n- Participating in personnel engagement activities: Weekend Online Game prizes (Half-Life, AOE, Dota2, LOL, Pubg...), Team Building by week, by month, by project.\r\n- Opportunity for advancement based on ability with corresponding increase in rank and salary increase.\r\n- Opportunity to implement ambitious projects in many countries, exposure to the latest technologies and learn from good colleagues.\r\n- Participate in skills training courses: AWS, Microservices, foreign languages ​​(English, Japanese)...', '- Implement the overall architectural framework, meeting the company\'s business strategy\r\n- Implement the development of technology standards and design methods from time to time.\r\n- Research and propose new technology approaches and put them into practice to enhance competitiveness, improve service quality and pioneer in new fields.\r\n- Instructing IT staff on technology standards and design methods.\r\n- Evaluate and/or participate in architectural design of solutions as well as solution selection.\r\n- Evaluate and/or approve solution architectures to ensure consistency with architectural and strategic standards.\r\n- Perform architectural review according to plans, assess compliance level against the standard architecture.\r\n- Participate in technology projects as assigned.\r\n- Participating in the development of business procedures and instructions for the Unit', '- Strong Java Azure\r\n- At least 3 years of experience working as software architecture\r\n- Having solid knowledge with Azure Microsoft identity platform and experience in integrating applications with Azure AD and Azure AD B2B\r\n- Having solid experience in security with OAUTH 2, OpenID connect, and knowledge related\r\n- Hands-on experiences of initializing, building, and customizing development framework from scratch\r\n- Experiences in Java/J2EE, Spring frameworks\r\n- Solid in software design pattern\r\n- Experience in Agile development environment\r\n- Hands-on experience with DevOps tools Jenkin, Git, Sonar, Docker\r\n- Experienced in Microservices architect - Strong English communication.\r\n- French is a major plus', 'Remote', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2022-12-21 05:00:10', '2023-01-10 02:48:46', 1),
(17, 'e3b51834-9438-42d4-b82e-bba06ec3ea44', 'thuc-tap', 11, '[\"16\"]', '2023-02-25', '18', '1-nam', 'cao-dang', 1, '- Phụ cấp onsite 100k/ngày làm việc.\r\n- Không cần thử việc, nhận làm chính thức hưởng 100% lương và được đóng đầy đủ bảo hiểm theo luật lao động Việt Nam (BHXH, BHYT, BHTN) ngay từ ngày onboard.\r\n- 06 tháng đánh giá hiệu suất làm việc một lần, tương ứng với kì review lương dựa trên năng lực và hiệu suất làm việc.\r\n- Thưởng thâm niên, thưởng dự án, thưởng lương tháng 13, thưởng hiệu quả công việc vào cuối năm.\r\n- Trợ cấp thai sản cho nhân viên nữ.\r\n- Tham gia các hoạt động của công ty: Liên hoan tháng, quý, teambuilding, du lịch, nghỉ dưỡng và các hoạt động khác\r\n- Cơ hội onsite và làm việc với các khách hàng lớn, công nghệ tiên tiến, phát triển bản thân...\r\n- Cơ hội làm việc nhóm với nhiều chuyên gia hàng đầu trong lĩnh vực CNTT trong nước và quốc tế.\r\n- Cơ hội triển khai các dự án tham vọng ở nhiều quốc gia, tiếp xúc với những công nghệ mới nhất và học hỏi từ những đồng nghiệp giỏi.\r\n- Làm việc trong môi trường trẻ trung, sôi động, hiện đại và đa văn hóa. Các hoạt động truyền thông, sự kiện vào các dịp lễ diễn ra thường xuyên.\r\n- Cơ hội thăng tiến dựa trên năng lực tương ứng với tăng bậc và tăng lương tương xứng.\r\n- Được quyền tham gia các khoá đào tạo kỹ năng mềm (tư duy logic, tư duy sáng tạo, kỹ năng giao tiếp, kỹ năng quản trị dự án, kỹ năng đàm phán…) và các lớp học tiếng Nhật.', '- Phát triển Frontend của ứng dụng Web hiện đại, giàu tương tác với yêu cầu cao về chất lượng và độ tin cậy. (Phát triển sản phẩm)\r\n- Thảo luận với đội ngũ Backend về thiết kế API\r\n- Liên tục cập nhật kiến thức, nghiên cứu công nghệ mới để ứng dụng trong công việc và phát triển sản phẩm của Công ty.\r\n- Nghiên cứu nắm bắt công nghệ mới thích hợp để giải quyết vấn đề.\r\n- Tham gia các khóa đào tạo liên quan tới công việc do Công ty tổ chức.', '- Tối thiểu 2,5 năm kinh nghiệm về Front End (JavaScript/HTML/Css)\r\n- Tối thiểu 2,5 năm sử dụng các framework như Angular\r\n- Có kinh nghiệm về ReactJS sẽ là một lợi thế\r\n- Quen thuộc với XML, JSON, MySQL\r\n- Kỹ năng thiết kế sẽ được đánh giá cao với Thành thạo Adobe Suite\r\n- Ý thức mạnh mẽ về nhận thức thương mại', 'Liễu Giai, Ba Đình, Hà Nội', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2022-12-21 05:01:27', '2023-01-10 02:48:33', 1),
(18, 'c6a757b3-627b-496f-bf4a-14e088093bb8', 'part-time', 12, '[\"21\"]', '2023-01-31', '18', '2-nam', 'cao-dang', 3, '- Attractive income up to 40,000,000 VND.\r\n- Connection bonus: 1~10 million bonus for anyone who introduces friends and acquaintances to the company.\r\n- Working with large and advanced systems, have the opportunity to develop comprehensive technical skills with complex problems, requiring high accuracy.\r\n- Become one of the influential KeyPerson in the project, high chance to become Leader, Project Manager.\r\n- Participating in personnel engagement activities: Weekend Online Game prizes (Half-Life, AOE, Dota2, LOL, Pubg...), Team Building by week, by month, by project.\r\n- Opportunity for advancement based on ability with corresponding increase in rank and salary increase.\r\n- Opportunity to implement ambitious projects in many countries, exposure to the latest technologies and learn from good colleagues.\r\n- Participate in skills training courses: AWS, Microservices, foreign languages ​​(English, Japanese)...', '- Develop, test and maintain responsive web/mobile apps with elegant UX that delight our users and raise their productivity.\r\n- Write clean and modular code that you are proud of, with extensibility and performance in mind.\r\n- Work in cross functional teams with product managers, UX designers and QA engineers to ship software on-quality and on-time.', '- Have 2 - 5 years developing HTML5 web appsor hybrid mobile apps, with a proven track record of successfully shipping products.\r\n- Highly skilled in JavaScript, HTML and CSS (at least 5 years of experience)\r\n- Extensive experience in some of these:\r\n+ Dev Frameworks (e.g. React, Reach Native, Flutter, Android SDK, AngularJS, Vue.js)\r\n+ Styling frameworks (e.g. Ant Design, Angular Material Design, Bootstrap)\r\n+ PWA, SPA, Bundling, React Hooks, Redux, Web workers, ES6/7\r\n+ Test frameworks (e.g. JEST, Mocha)\r\n- Cares deeply about UX and has an intuitive feel for good design.\r\n- Solid Computer Science fundamentals, excellent problem-solving skills and a strong understanding of distributed computing principles.\r\n- Excellent verbal and written communication skills.\r\n- Bachelor’s or Master’sdegree in Computer Science or related field from a top university.\r\n- Able to work within the GMT+8 time zone', 'Remote', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2022-12-21 05:04:11', '2023-01-10 08:13:58', 1),
(19, 'b99cd26c-f356-4749-84f8-2942d7151c51', 'full-time', 13, '[\"21\"]', '2023-01-28', '20', '2-nam', 'dai-hoc', 1, '- Phụ cấp onsite 100k/ngày làm việc.\r\n- Không cần thử việc, nhận làm chính thức hưởng 100% lương và được đóng đầy đủ bảo hiểm theo luật lao động Việt Nam (BHXH, BHYT, BHTN) ngay từ ngày onboard.\r\n- 06 tháng đánh giá hiệu suất làm việc một lần, tương ứng với kì review lương dựa trên năng lực và hiệu suất làm việc.\r\n- Thưởng thâm niên, thưởng dự án, thưởng lương tháng 13, thưởng hiệu quả công việc vào cuối năm.\r\n- Trợ cấp thai sản cho nhân viên nữ.\r\n- Tham gia các hoạt động của công ty: Liên hoan tháng, quý, teambuilding, du lịch, nghỉ dưỡng và các hoạt động khác\r\n- Cơ hội onsite và làm việc với các khách hàng lớn, công nghệ tiên tiến, phát triển bản thân...\r\n- Cơ hội làm việc nhóm với nhiều chuyên gia hàng đầu trong lĩnh vực CNTT trong nước và quốc tế.\r\n- Cơ hội triển khai các dự án tham vọng ở nhiều quốc gia, tiếp xúc với những công nghệ mới nhất và học hỏi từ những đồng nghiệp giỏi.\r\n- Làm việc trong môi trường trẻ trung, sôi động, hiện đại và đa văn hóa. Các hoạt động truyền thông, sự kiện vào các dịp lễ diễn ra thường xuyên.\r\n- Cơ hội thăng tiến dựa trên năng lực tương ứng với tăng bậc và tăng lương tương xứng.\r\n- Được quyền tham gia các khoá đào tạo kỹ năng mềm (tư duy logic, tư duy sáng tạo, kỹ năng giao tiếp, kỹ năng quản trị dự án, kỹ năng đàm phán…) và các lớp học tiếng Nhật.\r\n- Và rất nhiều quyền lợi hấp dẫn khác...', '- Tham gia vào các dự án lập trình phát triển phần mềm product, outsource với khách hàng trong và ngoài nước.\r\n- Tham gia thiết kế hệ thống, database và các module của sản phẩm hàng triệu người dùng.\r\n- Đóng góp ý tưởng xây dựng, cải tiến sản phẩm.\r\n- Lên kế hoạch và đề xuất ý tưởng về công việc với nhóm.\r\n- Chi tiết công việc sẽ được trao đổi thêm vào buổi phỏng vấn.', '- Có từ 1 năm kinh nghiệm lập trình ReactJS\r\n- Có kinh nghiệm sử dụng chắc về JS\r\n- Nắm vững cách tích hợp tầng Back-end thông qua APIs như RESTful API\r\n- Chủ động trong công việc, khả năng làm việc nhóm tốt.\r\n- Có khả năng làm việc với áp lực cao.\r\n- Có kinh nghiệm về Typescript.', 'Đê La Thành', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2022-12-21 06:52:21', '2023-01-10 08:13:50', 1),
(20, '71c7cd3c-e315-4106-9981-fdadf540d76e', 'full-time', 11, '[\"22\"]', '2022-01-13', '20', '2-nam', 'dai-hoc', 2, '- Mức thu nhập hấp dẫn lên đến 40,000,000 VNĐ.\r\n- Thưởng kết nối: thưởng 1~10 triệu cho bất cứ ai khi giới thiệu được bạn bè, người quen vào công ty.\r\n- Làm việc với hệ thống lớn và tiên tiến, có cơ hội phát triển technical skills toàn diện với các bài toán phức tạp, yêu cầu độ chính xác cao.\r\n- Trở thành một trong những KeyPerson có sức ảnh hưởng trong dự án, cơ hội cao trở thành Leader, Project Manager.\r\n- Tham gia các hoạt động gắn kết nhân sự: các giải Game Online cuối tuần (Half-Life, AOE, Dota2, LOL, Pubg...), Team Building theo tuần, theo tháng, theo dự án.\r\n- Cơ hội thăng tiến dựa trên năng lực tương ứng với tăng bậc và tăng lương tương xứng.\r\n- Cơ hội triển khai các dự án tham vọng ở nhiều quốc gia, tiếp xúc với những công nghệ mới nhất và học hỏi từ những đồng nghiệp giỏi.\r\n- Được tham gia các khoá đào tạo kỹ năng: AWS, Microservice, ngoại ngữ (tiếng Anh, tiếng Nhật)...', 'Dự án: Phát triển hệ thống web EC trong công ty ( Web dành riêng cho các hoạt động mua hàng nội bộ)\r\n- Phân tích yêu cầu, thiết kế cơ sở dữ liệu, phát triển back-end, vận hành và bảo trì.\r\n- Đưa ra hay phân tích các ý tưởng, thiết kế và phát triển các hệ thống tổng hợp data, phân tích, khai phá data.\r\n- Phối hợp với các thành viên trong nhóm dưới sự phân công công việc của Quản lý dự án', '- Có trên 3 năm kinh nghiệm phát triển thực tế “Web system”\r\n- Sử dụng ngôn ngữ VB.NET, JavaScript, Node.js\r\n- Ưu tiên những người có kinh nghiệm với JavaScript, Node.js\r\n- Có kinh nghiệm trong việc phát triển và triển khai (theo nghĩa là bạn có thể tự viết code)\r\n- Trình độ tiếng nhật : N3〜 (Giao tiếp cơ bản)', 'Remote', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2022-12-29 07:33:59', '2023-01-10 02:48:12', 1),
(21, 'f5ffb696-a6c3-450c-8e58-398c370e89d6', 'full-time', 13, '[\"16\",\"17\",\"21\",\"22\",\"23\",\"25\",\"26\",\"28\",\"30\"]', '2023-02-08', '19', '2-nam', 'cao-dang', 4, '- Không cần thử việc, nhận làm chính thức hưởng 100% lương ngay từ ngày onboard.\r\n- Thưởng kết nối: thưởng 3~10 triệu cho bất cứ ai khi giới thiệu được bạn bè, người quen vào công ty.\r\n- 06 tháng đánh giá hiệu suất làm việc một lần, tương ứng với kì review lương dựa trên năng lực và hiệu suất làm việc.\r\n- Trợ cấp thai sản cho nhân viên nữ.\r\n- Tham gia các hoạt động của công ty: Liên hoan tháng, quý, teambuilding, du lịch, nghỉ dưỡng và các hoạt động khác\r\n- Cơ hội làm việc nhóm với nhiều chuyên gia hàng đầu trong lĩnh vực CNTT trong nước và quốc tế.\r\n- Cơ hội triển khai các dự án tham vọng ở nhiều quốc gia, tiếp xúc với những công nghệ mới nhất và học hỏi từ những đồng nghiệp giỏi.\r\n- Làm việc trong môi trường trẻ trung, sôi động, hiện đại và đa văn hóa; Các hoạt động truyền thông, sự kiện vào các dịp lễ diễn ra thường xuyên.\r\n- Cơ hội thăng tiến dựa trên năng lực tương ứng với tăng bậc và tăng lương tương xứng.\r\n- Được quyền tham gia các khoá đào tạo kỹ năng mềm (tư duy logic, tư duy sáng tạo, kỹ năng giao tiếp, kỹ năng quản trị dự án, kỹ năng đàm phán…) và các lớp học tiếng Nhật, tiếng Anh.\r\n- Và rất nhiều quyền lợi hấp dẫn khác...', '- Chịu trách nhiệm hoàn thành công việc, đảm bảo chất lượng.\r\n- Làm việc, phối hợp công việc theo nhóm dưới sự phân công công việc của cấp trên.\r\n- Công việc sẽ được trao đổi cụ thể hơn trong quá trình phỏng vấn.', '- Có ít nhất 5 năm kinh nghiệm làm việc về kinh nghiệm phát triển Full-Stack, đặc biệt với .Net và Angular.\r\n- Tiếng Anh giao tiếp tốt.\r\n- Có kinh nghiệm vững chắc về: C#, .Net Core, ASP.NET MVC, Entity Framework, REST, Angular 4+, JavaScript, HTML5/CSS3.\r\n- Có kinh nghiệm với DevExpress, NodeJS, Telerik, CI/CD sử dụng Azure DevOps là một lợi thế.\r\n- Ưu tiên ứng viên có kinh nghiệm về Service bus, Event Hub, Azure SQL, Azure Search.', 'Remote', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2022-12-29 07:36:56', '2023-01-10 08:13:41', 1),
(22, '382dad3c-26ee-4128-a87a-958d2e2eb721', 'full-time', 11, '[\"23\"]', '2023-01-17', '18', 'no-exp', 'cao-dang', 1, '- Attractive income up to 40,000,000 VND.\r\n- Connection bonus: 1~10 million bonus for anyone who introduces friends and acquaintances to the company.\r\n- Working with large and advanced systems, have the opportunity to develop comprehensive technical skills with complex problems, requiring high accuracy.\r\n- Become one of the influential KeyPerson in the project, high chance to become Leader, Project Manager.\r\n- Participating in personnel engagement activities: Weekend Online Game prizes (Half-Life, AOE, Dota2, LOL, Pubg...), Team Building by week, by month, by project.\r\n- Opportunity for advancement based on ability with corresponding increase in rank and salary increase.\r\n- Opportunity to implement ambitious projects in many countries, exposure to the latest technologies and learn from good colleagues.\r\n- Participate in skills training courses: AWS, Microservices, foreign languages ​​(English, Japanese)...', '- Design, develop and maintain software that powers complex operations.\r\n- Help design the architecture, own development of major initiatives and modules.\r\n- Work in cross functional teams with product managers, UX designers and QA engineers to ship software on-quality and on-time.\r\n- Help level up the team; share knowledge, conduct code and design review; drive code quality and process improvement initiatives.\r\n- Write code that you are proud of, with extensibility, scalability and performance in mind: 250 million API requests & 3TB of data daily.', '- Solid Computer Science fundamentals, excellent problem-solving skills and a strong understanding of distributed computing principles.\r\n- Excellent verbal and written communication skills in English.\r\n- 3-6 years of backend or frontend development in a product company, with a proven track record of shipping successful software products.\r\n- Deep experience in Go (Golang)or similar technologies.\r\n- Able to work within the GMT+8 time zone', 'Hà Nội', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2022-12-29 07:47:46', '2023-01-10 08:13:33', 1),
(23, '8285edcd-644e-465b-8963-b47a1f16a6da', 'full-time', 13, '[\"29\"]', '2023-12-31', '19', 'no-exp', 'khac', 8, '- Attractive income up to 40,000,000 VND.\r\n- Connection bonus: 1~10 million bonus for anyone who introduces friends and acquaintances to the company.\r\n- Working with large and advanced systems, have the opportunity to develop comprehensive technical skills with complex problems, requiring high accuracy.\r\n- Become one of the influential KeyPerson in the project, high chance to become Leader, Project Manager.\r\n- Participating in personnel engagement activities: Weekend Online Game prizes (Half-Life, AOE, Dota2, LOL, Pubg...), Team Building by week, by month, by project.\r\n- Opportunity for advancement based on ability with corresponding increase in rank and salary increase.\r\n- Opportunity to implement ambitious projects in many countries, exposure to the latest technologies and learn from good colleagues.\r\n- Participate in skills training courses: AWS, Microservices, foreign languages ​​(English, Japanese)...', '- Buildingand setting up new development tools and infrastructure\r\n- Understanding the needs of stakeholders and conveying this to developers\r\n- Working on ways to automate and improve development and release processes\r\n- Testing and examining code written by others and analyzing results\r\n- Ensuring that systems are safe and secure against cybersecurity threats\r\n- Identifying technical problems and developing software updates and ‘fixes’\r\n- Working with software developers and software engineers to ensure that development follows established processes and works as intended\r\n- Planning out projects and being involved in project management decisions', '- Have 5+ years of experience\r\n- Have experience on how to apply modern methodologies like Continuous Delivery, Zero Downtime, High Availability, Geolocation and principles like Software-as-a-Service\r\n- Experienced in establishing CI/CD pipeline\r\n- Expert in Amazon and Azure cloud platforms\r\n- Familiar with and able to manage DevOps toolchain, such as Jenkins, Sonar, Nexus, Artifactory, Portainer, Terraform\r\n- Well-versed in general-purpose scripting programming languages such as Shell, Python, Groovy\r\n- Familiar with network concepts, virtualization and containerization with VMWare, Docker, Kubernetes\r\n- Development experience in Java platform, Maven, JUnit, Microservices\r\n- Ability to set up and configure Apache Tomcat application server\r\n- Experience with collaboration tools, such as JIRA and Git\r\n- Familiar with release management process\r\n- Knowledge of fundamentals of cybersecurity', 'Remote', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2023-01-03 08:21:44', '2023-01-10 08:13:23', 1),
(24, '2cc9438e-f8ef-40a5-a73a-be9428849eb2', 'freelancer', 4, '[\"25\"]', '2023-12-31', '19', 'no-exp', 'tat-cat-trinh-do', 5, '- Attractive income up to 40,000,000 VND.\r\n- Connection bonus: 1~10 million bonus for anyone who introduces friends and acquaintances to the company.\r\n- Working with large and advanced systems, have the opportunity to develop comprehensive technical skills with complex problems, requiring high accuracy.\r\n- Become one of the influential KeyPerson in the project, high chance to become Leader, Project Manager.\r\n- Participating in personnel engagement activities: Weekend Online Game prizes (Half-Life, AOE, Dota2, LOL, Pubg...), Team Building by week, by month, by project.\r\n- Opportunity for advancement based on ability with corresponding increase in rank and salary increase.\r\n- Opportunity to implement ambitious projects in many countries, exposure to the latest technologies and learn from good colleagues.\r\n- Participate in skills training courses: AWS, Microservices, foreign languages ​​(English, Japanese)...', '- Collaborate with cross-functional engineering and product teams to design and develop products.\r\n- Take an active responsibility in creating highly scalable products for our scaling needs.\r\n- Write highly performant codethat isclean, simple, maintainable, and battle-tested with test-driven development (TDD) for maximum test coverage automated from pull request all the way to release.\r\n- Wearthe polyglot hat and deep-diveinto multiple codebases (C/C++/JS/TS/Rust) to contribute and understand how things work under the hood.', '- Solid Computer Science fundamentals, excellent problem-solving skills.\r\n- 2+ years of hands-on experience with C++\r\n- Ability to take full ownership and work independently while collaborating with others in a fast-paced agile and async team.\r\n- Willingness to pick up new and emerging bleeding edge technologies\r\n- Experience participating or maintaining in open-sourcesoftware developmentis preferred\r\n- Good communication skills (written and verbal), proven team player, rolling up the sleeves and getting involved in the nitty gritty.\r\n- Bachelor’s or Master’s degree in Computer Science or related field from a top university.\r\n- Able to work within the GMT+8 time zone', 'Remote', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2023-01-03 08:23:19', '2023-01-10 08:14:16', 1),
(25, 'be269dda-94da-4c21-944c-5d80c542578f', 'full-time', 4, '[\"28\"]', '2023-02-12', '19', 'no-exp', 'cao-dang', 1, '- Mức thu nhập hấp dẫn lên đến 35,000,000 VNĐ.\r\n- Thưởng kết nối: thưởng 1~10 triệu cho bất cứ ai khi giới thiệu được bạn bè, người quen vào công ty.\r\n- Làm việc với hệ thống lớn và tiên tiến, có cơ hội phát triển technical skills toàn diện với các bài toán phức tạp, yêu cầu độ chính xác cao.\r\n- Trở thành một trong những KeyPerson có sức ảnh hưởng trong dự án, cơ hội cao trở thành Leader, Project Manager.\r\n- Tham gia các hoạt động gắn kết nhân sự: các giải Game Online cuối tuần (Half-Life, AOE, Dota2, LOL, Pubg...), Team Building theo tuần, theo tháng, theo dự án.\r\n- Cơ hội thăng tiến dựa trên năng lực tương ứng với tăng bậc và tăng lương tương xứng.\r\n- Cơ hội triển khai các dự án tham vọng ở nhiều quốc gia, tiếp xúc với những công nghệ mới nhất và học hỏi từ những đồng nghiệp giỏi.\r\n- Được tham gia các khoá đào tạo kỹ năng: AWS, Microservice, ngoại ngữ (tiếng Anh, tiếng Nhật)...', '- Thiết kế, phát triển và tối ưu hóa hiệu suất của các sản phẩm Mobile Apps trên nền tảng Flutter.\r\n- Tham gia vào tất cả các khâu trong quá trình phát triển phần mềm, bao gồm: tìm hiểu yêu cầu, phân tích, thiết kế, nghiên cứu công nghệ mới…\r\n- Đưa ra các best practices trong quy trình phát triển phần mềm để liên tục nâng cao hiệu suất và cải thiện kĩ năng của các thành viên trong dự án và trong công ty\r\n- Giải quyết issues, thực hiện công việc theo kế hoạch, báo cáo tiến độ công việc cho Leader phụ trách.\r\n- Công việc sẽ được trao đổi cụ thể hơn trong quá trình phỏng vấn.', '- Tối thiểu 5 năm kinh nghiệm làm Flutter.\r\n- Tiếng Anh giao tiếp tốt\r\n- Yêu cầu sống ở Hà Nội (để sử dụng laptop công ty cung cấp (vì lý do bảo mật))\r\n- Có kiến thức tốt về OOP, Data Structures, Algorithm, UI/UX.\r\n- Có kinh nghiệm làm việc với backend Restful API.\r\n- Có kinh nghiệm làm việc trên hệ thống quản lý source code Git.\r\n- Kỹ năng phân tích tình huống và xử lý vấn đề.\r\n- Yêu cầu khả năng làm việc nhóm và tinh thần chịu trách nhiệm cao.\r\n- Khả năng tự học hỏi và tính tự giác cao.', 'Remote', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2023-01-03 08:24:40', '2023-01-10 08:13:12', 1),
(26, 'fa5b5751-bd85-433a-a606-ab847bc07f76', 'part-time', 11, '[\"24\"]', '2023-12-31', '18', 'duoi-1-nam', 'dai-hoc', 5, '- Attractive income up to 40,000,000 VND.\r\n- Connection bonus: 1~10 million bonus for anyone who introduces friends and acquaintances to the company.\r\n- Working with large and advanced systems, have the opportunity to develop comprehensive technical skills with complex problems, requiring high accuracy.\r\n- Become one of the influential KeyPerson in the project, high chance to become Leader, Project Manager.\r\n- Participating in personnel engagement activities: Weekend Online Game prizes (Half-Life, AOE, Dota2, LOL, Pubg...), Team Building by week, by month, by project.\r\n- Opportunity for advancement based on ability with corresponding increase in rank and salary increase.\r\n- Opportunity to implement ambitious projects in many countries, exposure to the latest technologies and learn from good colleagues.\r\n- Participate in skills training courses: AWS, Microservices, foreign languages ​​(English, Japanese)...', '- Maintaina positive, empathetic, and professional attitude toward customers at all times\r\n- Resolve customers’ concerns and answer customers’ questions to the best of your best ability\r\n- Attract customers by promoting the product and company positively, answering questions and addressing concerns as they arise\r\n- Prepare product and customer reports by gathering data collected during customer interactions\r\n- Providefeedback on the efficiency of the customer service process\r\n- Managea team of junior customer service representatives\r\n- Ensure customer satisfaction and provide professional customer support', '- High school diploma, general education degree, or equivalent\r\n- Personable,attentiveand assertive\r\n- Strong interpersonal skills\r\n- Strong verbal communicator\r\n- Familiar with the software used to connect with customers and gather their information\r\n- Knowledge of applicable products and markets', 'Remote', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2023-01-03 10:43:50', '2023-01-16 16:47:54', 6),
(27, 'faf21ba5-74bc-4deb-b3ad-d4e83ba9cf91', 'full-time', 11, '[\"24\"]', '2023-03-31', '19', 'no-exp', 'dai-hoc', 1, '- Attractive income up to 40,000,000 VND.\r\n- Connection bonus: 1~10 million bonus for anyone who introduces friends and acquaintances to the company.\r\n- Working with large and advanced systems, have the opportunity to develop comprehensive technical skills with complex problems, requiring high accuracy.\r\n- Become one of the influential KeyPerson in the project, high chance to become Leader, Project Manager.\r\n- Participating in personnel engagement activities: Weekend Online Game prizes (Half-Life, AOE, Dota2, LOL, Pubg...), Team Building by week, by month, by project.\r\n- Opportunity for advancement based on ability with corresponding increase in rank and salary increase.\r\n- Opportunity to implement ambitious projects in many countries, exposure to the latest technologies and learn from good colleagues.\r\n- Participate in skills training courses: AWS, Microservices, foreign languages ​​(English, Japanese)...', '- Outreach:helpexecute anoutreach strategy, especially forvendors\r\n- Partnerships: listen attentively, communicate effectively, and present confidently the benefits of partnerships\r\n- Onboarding: welcome and onboard users to the organization\r\n- Account management: act as a Single Point of Contact foryourassigned businessesthroughout thejourney; manage the overall relationship through proactive, open, and responsive communication', '- 3+years of experience as a BDM or SDR•Strong desire and ability to realizelarger and more strategic roles within a sales organization\r\n- Experience in doing sales in the APAC/US/EMEA region\r\n- Able to work with minimal supervision\r\n- Comfortable in speaking with C-Level professionals\r\n- Excellent listening, negotiation and presentation skills\r\n- High energy and activity levels•Excellent organization and time-management skills-a structured way of working, but flexible enough to cater tocontinuous change\r\n- Ability to work in a multiculturaland remotesetting\r\n- Excellent English written and verbal communication skills', 'Remote', '\"[{\\\"start_date\\\":\\\"09:30\\\"},{\\\"end_date\\\":\\\"18:30\\\"}]\"', '2023-01-03 10:45:38', '2023-01-11 04:38:57', 2),
(31, '307ef0d5-4ca4-4baa-9020-e44942d05887', 'part-time', 11, '[\"17\",\"27\"]', '2023-02-06', '18', '1-nam', 'dai-hoc', 1, 'qew', 'we', 'qwe', '3333', '\"[{\\\"start_date\\\":\\\"22:50\\\"},{\\\"end_date\\\":\\\"00:49\\\"}]\"', '2023-02-02 03:49:27', '2023-02-02 03:49:27', 31),
(32, '314bc343-ab11-4821-baf8-2486654652cb', 'full-time', 14, '[\"17\",\"28\"]', '2023-02-07', '19', 'duoi-1-nam', 'dai-hoc', 1, '23', '23', '42', '1234', '\"[{\\\"start_date\\\":\\\"18:11\\\"},{\\\"end_date\\\":\\\"19:11\\\"}]\"', '2023-02-06 11:11:43', '2023-02-06 11:11:43', 2),
(33, '6b67cee5-b3c3-4c44-8467-c48f465bfa04', 'thuc-tap', 12, '[\"16\",\"29\"]', '2023-02-28', '19', 'duoi-1-nam', 'dai-hoc', 2, '33', '3', '3', '333', '\"[{\\\"start_date\\\":\\\"18:32\\\"},{\\\"end_date\\\":\\\"19:32\\\"}]\"', '2023-02-06 11:33:01', '2023-02-06 11:33:01', 8),
(35, '9025f8a1-5dc9-43f7-b320-6c6e33d3e3de', 'freelancer', 12, '[\"17\"]', '2023-02-09', '18', '2-nam', 'khac', 1, 'ewf', 'gr', 'few', 'eqwewq', '\"[{\\\"start_date\\\":\\\"10:29\\\"},{\\\"end_date\\\":\\\"10:28\\\"}]\"', '2023-02-08 03:30:31', '2023-02-08 03:30:31', 27),
(36, 'ecfd659e-1736-4146-958a-090eef75048a', 'part-time', 11, '[\"16\"]', '2023-02-09', '20', '1-nam', 'cao-dang', 3, 'ư', 'e', 'ư', '1', '\"[{\\\"start_date\\\":\\\"10:33\\\"},{\\\"end_date\\\":\\\"10:33\\\"}]\"', '2023-02-08 03:34:21', '2023-02-08 04:05:02', 64),
(37, '55126ccd-b0d1-4888-8e26-9e4023c757f9', 'freelancer', 13, '[\"16\"]', '2023-02-02', '19', '2-nam', 'dai-hoc', 2, 'd', 'ds', 'd', 'fdf', '\"[{\\\"start_date\\\":\\\"11:16\\\"},{\\\"end_date\\\":\\\"01:14\\\"}]\"', '2023-02-08 04:14:30', '2023-02-08 04:14:30', 25),
(38, '0da1dd50-8922-4160-97af-9ad3d5429ef3', 'freelancer', 12, '[\"16\"]', '2023-02-16', '19', 'duoi-1-nam', 'dai-hoc', 2, 're', 'rew', 're', '32q3wtrytfugyhui', '\"[{\\\"start_date\\\":\\\"13:34\\\"},{\\\"end_date\\\":\\\"13:34\\\"}]\"', '2023-02-08 06:34:42', '2023-02-08 06:34:42', 27),
(39, '21c2c591-3250-45be-9a06-3f550bf1ea65', 'freelancer', 11, '[\"16\"]', '2023-02-10', '20', 'no-exp', 'khac', 1, 'fa', 'afs', 'af', 'rgff', '\"[{\\\"start_date\\\":\\\"04:36\\\"},{\\\"end_date\\\":\\\"04:36\\\"}]\"', '2023-02-08 09:36:49', '2023-02-08 09:36:49', 2),
(40, '3c24cc9d-ccc0-4d6d-a1c6-35dccb0dac27', 'freelancer', 4, '[\"16\"]', '2023-02-09', '19', '1-nam', 'khac', 2, 'qew', 'qew', 'qew', 'wqeqw', '\"[{\\\"start_date\\\":\\\"05:05\\\"},{\\\"end_date\\\":\\\"17:04\\\"}]\"', '2023-02-08 10:05:07', '2023-02-08 10:05:07', 2),
(41, 'f2a77e41-1d0d-4dd0-9ba6-9bd757caa0c7', 'part-time', 11, '[\"16\"]', '2023-02-09', '20', '1-nam', 'dai-hoc', 3, 'eqw', 'qwe', 'eqw', 'ewq', '\"[{\\\"start_date\\\":\\\"16:22\\\"},{\\\"end_date\\\":\\\"16:22\\\"}]\"', '2023-02-09 09:22:55', '2023-02-09 09:22:55', 4);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(2, '2022_11_15_085123_create_roles_table', 1),
(3, '2022_11_16_041758_create_users_table', 1),
(4, '2022_11_16_041822_create_jobs_table', 1),
(5, '2022_11_16_041844_create_job_details_table', 1),
(6, '2022_11_16_041922_create_candidates_table', 1),
(7, '2022_11_16_041938_create_applies_table', 1),
(8, '2022_11_21_084751_create_setting_jobs_table', 1),
(9, '2022_11_23_085013_create_permissions_table', 1),
(10, '2022_11_23_085206_create_roles_permissions_table', 1),
(11, '2022_11_23_085208_create_setting_kpis_table', 1),
(12, '2022_11_24_083931_create_categories_table', 1),
(13, '2022_11_29_020921_change_column_role_id_to_nullable_in_users_table', 1),
(16, '2022_11_30_174944_create_short_urls_table', 2),
(17, '2022_12_01_111441_create_bonuses_table', 2),
(21, '2022_12_05_100545_drop_column_file_to_candidates_table', 5),
(22, '2022_12_05_132516_create_sources_table', 5),
(23, '2022_12_05_132529_add_columns_to_applies_table', 5),
(24, '2022_12_05_140127_drop_column_status_to_applies_table', 5),
(25, '2022_12_05_140143_add_column_status_to_applies_table', 5),
(26, '2022_12_06_111203_drop_column_skills_to_candidates_table', 6),
(27, '2022_12_06_111710_add_column_skills_to_applies_table', 6),
(28, '2022_12_07_105210_add_column_viewer_to_blogs_table', 7),
(29, '2022_12_07_092543_add_column_view_count_to_jobs_table', 8),
(30, '2022_12_08_170417_create_candidates_customers_table', 9),
(31, '2022_12_08_171318_create_users_bonus_table', 9),
(32, '2022_12_09_153515_add_column_reason_to_applies_table', 9),
(33, '2022_12_12_120356_add_column_parent_id_to_users_table', 9),
(34, '2022_12_12_181050_change_columns_in_candidates_customers_table', 9),
(35, '2022_12_13_115523_change_column_password_and_phone_nullable_in_users_table', 10),
(37, '2022_12_13_144938_create_blogs_table', 11),
(38, '2022_12_13_113216_change_columns_to_users_bonus_table', 12),
(39, '2022_12_15_140156_add_column_month_week_to_users_bonus_table', 12),
(40, '2022_12_20_133749_change_column_status_in_users_table', 13),
(41, '2022_12_21_095513_add_columns_to_candidates_customers_table', 14),
(42, '2022_12_21_153514_add_column_income_to_candidates_customers_table', 14),
(43, '2022_12_23_143621_add_column_location_to_jobs_table', 15),
(44, '2022_12_28_145222_add_column_interview_date_to_applies_table', 16),
(45, '2022_12_28_154137_create_contacts_table', 17),
(46, '2022_12_29_094425_add_column_image_to_sources_table', 18),
(47, '2023_01_03_151730_change_column_location_to_jobs_table', 18),
(48, '2023_01_09_152012_create_provinces_table', 19),
(49, '2023_01_09_153125_add_column_province_id_to_job_details_table', 20),
(54, '2023_02_02_095043_create_notifies_table', 21),
(55, '2023_02_07_134047_add_column_content_to_notifies_table', 22),
(57, '2023_02_09_141414_drop_column_actision_to_notifies_table', 23),
(61, '2023_02_09_141538_create_notify_actisions_table', 24);

-- --------------------------------------------------------

--
-- Table structure for table `notifies`
--

CREATE TABLE `notifies` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('job','apply','blog') COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` enum('normal','high') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifies`
--

INSERT INTO `notifies` (`id`, `title`, `type`, `user_id`, `type_id`, `priority`, `created_at`, `updated_at`, `content`) VALUES
(63, 'Đã có cv ứng tuyển mới', 'apply', NULL, '7be9bafd-31d8-4c7d-8f6a-28bf56cd7e7e', 'normal', '2023-02-09 09:21:42', '2023-02-09 09:21:42', 'CV ReactJS Developer (Onsite Đê La Thành) vừa thêm chưa có HR PIC.'),
(64, 'Đã thêm một job mới', 'job', '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'f2a77e41-1d0d-4dd0-9ba6-9bd757caa0c7', 'normal', '2023-02-09 09:22:55', '2023-02-09 09:22:55', 'Công việc qweeqrqwtgtqqt3 đã được Leader Division A thêm.'),
(65, 'Đã thêm một tin tức mới', 'blog', '013db4b6-92a7-4cbd-a1d4-fce422684e70', '066fe7cb-d83a-422d-8845-849c715bf2b1', 'normal', '2023-02-09 09:23:26', '2023-02-09 09:23:26', 'Tin tức khjhjmad đã được Leader Division A thêm.'),
(66, 'Đã có cv ứng tuyển mới', 'apply', '013db4b6-92a7-4cbd-a1d4-fce422684e70', '89a1d5c1-9935-4971-9ace-8a2bd0912983', 'high', '2023-02-09 09:26:21', '2023-02-09 09:26:21', 'Leader Division A đã có ứng viên ứng tuyển vị trí ewqrerkjh.'),
(67, 'Đã thay đổi cv cập nhật mới', 'apply', '013db4b6-92a7-4cbd-a1d4-fce422684e70', '89a1d5c1-9935-4971-9ace-8a2bd0912983', 'normal', '2023-02-09 09:27:44', '2023-02-09 09:27:44', 'Leader Division A đã thay đổi trạng thái cv ứng viên ui,umrymrym thành interview.'),
(68, 'Đã thay đổi cv cập nhật mới', 'apply', '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 'b0eadd83-366b-4bc9-9b5f-a33bbc1e88cf', 'normal', '2023-02-09 10:59:17', '2023-02-09 10:59:17', 'Lê Văn  Phong đã thay đổi trạng thái cv ứng viên sdgwseg thành interview.'),
(69, 'Đã có cv ứng tuyển mới', 'apply', '013db4b6-92a7-4cbd-a1d4-fce422684e70', '7cae553b-1845-429a-a31c-812408ea3bed', 'high', '2023-02-14 10:12:51', '2023-02-14 10:12:51', 'Leader Division A đã có ứng viên ứng tuyển vị trí eweqeqqqweqwtrjtyktyk.'),
(70, 'Đã có cv ứng tuyển mới', 'apply', NULL, 'd08761b6-eb74-4caa-9911-41bb4cbd9b64', 'normal', '2023-03-13 10:56:34', '2023-03-13 10:56:34', 'CV Business Development Manager (Remote) vừa thêm chưa có HR PIC.');

-- --------------------------------------------------------

--
-- Table structure for table `notify_actisions`
--

CREATE TABLE `notify_actisions` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify_id` bigint UNSIGNED NOT NULL,
  `actision` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notify_actisions`
--

INSERT INTO `notify_actisions` (`id`, `user_id`, `notify_id`, `actision`, `created_at`, `updated_at`) VALUES
(44, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 67, 'active', '2023-02-09 09:58:14', '2023-02-09 09:58:14'),
(45, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 66, 'active', '2023-02-09 09:58:14', '2023-02-09 09:58:31'),
(46, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 65, 'inactive', '2023-02-09 09:58:14', '2023-02-09 09:58:14'),
(47, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 64, 'inactive', '2023-02-09 09:58:14', '2023-02-09 09:58:14'),
(48, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 63, 'inactive', '2023-02-09 09:58:14', '2023-02-09 09:58:14'),
(49, '24ed8b74-b209-4458-a67e-a92a761437b3', 67, 'active', '2023-02-09 09:59:07', '2023-02-09 09:59:07'),
(50, '24ed8b74-b209-4458-a67e-a92a761437b3', 66, 'inactive', '2023-02-09 09:59:07', '2023-02-09 09:59:07'),
(51, '24ed8b74-b209-4458-a67e-a92a761437b3', 65, 'inactive', '2023-02-09 09:59:07', '2023-02-09 09:59:07'),
(52, '24ed8b74-b209-4458-a67e-a92a761437b3', 64, 'inactive', '2023-02-09 09:59:07', '2023-02-09 09:59:07'),
(53, '24ed8b74-b209-4458-a67e-a92a761437b3', 63, 'inactive', '2023-02-09 09:59:07', '2023-02-09 09:59:07'),
(54, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 65, 'active', '2023-02-09 10:20:48', '2023-02-09 10:20:48'),
(55, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 64, 'inactive', '2023-02-09 10:20:48', '2023-02-09 10:20:48'),
(56, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 63, 'active', '2023-02-09 10:20:48', '2023-02-09 10:21:13'),
(57, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', 68, 'active', '2023-02-10 02:22:34', '2023-02-10 02:22:34'),
(58, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 69, 'active', '2023-02-14 10:12:55', '2023-02-14 10:12:55'),
(59, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 68, 'inactive', '2023-02-14 10:12:55', '2023-02-14 10:12:55'),
(60, '24ed8b74-b209-4458-a67e-a92a761437b3', 69, 'active', '2023-03-13 10:38:16', '2023-03-13 10:38:16'),
(61, '24ed8b74-b209-4458-a67e-a92a761437b3', 68, 'inactive', '2023-03-13 10:38:16', '2023-03-13 10:38:16');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('active','inactive') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Phân quyền', 'phan-quyen', 'Có chức năng Phân quyền cho các vai trò.', 'active', '2022-11-30 08:28:54', NULL),
(2, 'Cấu hình KPI', 'cau-hinh-kpi', 'Có chức năng cấu hình thông số KPI cho các vai trò.', 'active', '2022-11-30 08:28:54', NULL),
(3, 'Bonus', 'bonus', 'Có chức năng Cấu hình thông số Bonus cho các vai trò.', 'active', '2022-11-30 08:28:54', NULL),
(4, 'Cấu hình Công việc', 'cau-hinh-cong-viec', 'Có chức năng Cấu hình thông số cho công việc', 'active', '2022-11-30 08:28:54', NULL),
(5, 'Thêm/Sửa/Xóa Công việc', 'them-sua-xoa-cong-viec', 'Có chức năng Thêm/Sửa/Xóa công việc.', 'active', '2022-11-30 08:28:54', NULL),
(6, 'Xem danh sách Công việc', 'xem-danh-sach-cong-viec', 'Có chức Xem danh sách công việc.', 'active', '2022-11-30 08:28:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `id` bigint NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `name`, `slug`) VALUES
(1, 'Hà Nội', 'ha-noi'),
(2, 'Hà Giang', 'ha-giang'),
(4, 'Cao Bằng', 'cao-bang'),
(6, 'Bắc Kạn', 'bac-kan'),
(8, 'Tuyên Quang', 'tuyen-quang'),
(10, 'Lào Cai', 'lao-cai'),
(11, 'Điện Biên', 'dien-bien'),
(12, 'Lai Châu', 'lai-chau'),
(14, 'Sơn La', 'son-la'),
(15, 'Yên Bái', 'yen-bai'),
(17, 'Hoà Bình', 'hoa-binh'),
(19, 'Thái Nguyên', 'thai-nguyen'),
(20, 'Lạng Sơn', 'lang-son'),
(22, 'Quảng Ninh', 'quang-ninh'),
(24, 'Bắc Giang', 'bac-giang'),
(25, 'Phú Thọ', 'phu-tho'),
(26, 'Vĩnh Phúc', 'vinh-phuc'),
(27, 'Bắc Ninh', 'bac-ninh'),
(30, 'Hải Dương', 'hai-duong'),
(31, 'Hải Phòng', 'hai-phong'),
(33, 'Hưng Yên', 'hung-yen'),
(34, 'Thái Bình', 'thai-binh'),
(35, 'Hà Nam', 'ha-nam'),
(36, 'Nam Định', 'nam-dinh'),
(37, 'Ninh Bình', 'ninh-binh'),
(38, 'Thanh Hóa', 'thanh-hoa'),
(40, 'Nghệ An', 'nghe-an'),
(42, 'Hà Tĩnh', 'ha-tinh'),
(44, 'Quảng Bình', 'quang-binh'),
(45, 'Quảng Trị', 'quang-tri'),
(46, 'Thừa Thiên Huế', 'thua-thien-hue'),
(48, 'Đà Nẵng', 'da-nang'),
(49, 'Quảng Nam', 'quang-nam'),
(51, 'Quảng Ngãi', 'quang-ngai'),
(52, 'Bình Định', 'binh-dinh'),
(54, 'Phú Yên', 'phu-yen'),
(56, 'Khánh Hòa', 'khanh-hoa'),
(58, 'Ninh Thuận', 'ninh-thuan'),
(60, 'Bình Thuận', 'binh-thuan'),
(62, 'Kon Tum', 'kon-tum'),
(64, 'Gia Lai', 'gia-lai'),
(66, 'Đắk Lắk', 'dak-lak'),
(67, 'Đắk Nông', 'dak-nong'),
(68, 'Lâm Đồng', 'lam-dong'),
(70, 'Bình Phước', 'binh-phuoc'),
(72, 'Tây Ninh', 'tay-ninh'),
(74, 'Bình Dương', 'binh-duong'),
(75, 'Đồng Nai', 'dong-nai'),
(77, 'Bà Rịa - Vũng Tàu', 'ba-ria-vung-tau'),
(79, 'Hồ Chí Minh', 'ho-chi-minh'),
(80, 'Long An', 'long-an'),
(82, 'Tiền Giang', 'tien-giang'),
(83, 'Bến Tre', 'ben-tre'),
(84, 'Trà Vinh', 'tra-vinh'),
(86, 'Vĩnh Long', 'vinh-long'),
(87, 'Đồng Tháp', 'dong-thap'),
(89, 'An Giang', 'an-giang'),
(91, 'Kiên Giang', 'kien-giang'),
(92, 'Cần Thơ', 'can-tho'),
(93, 'Hậu Giang', 'hau-giang'),
(94, 'Sóc Trăng', 'soc-trang'),
(95, 'Bạc Liêu', 'bac-lieu'),
(96, 'Cà Mau', 'ca-mau');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Lead Division', 'active', '2022-11-30 08:28:54', NULL),
(2, 'Leader', 'active', '2022-11-30 08:28:54', NULL),
(3, 'HR', 'active', '2022-11-30 08:28:54', NULL),
(4, 'Sale', 'active', '2022-11-30 08:28:54', NULL),
(5, 'Khác', 'active', '2022-11-30 08:28:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles_permissions`
--

CREATE TABLE `roles_permissions` (
  `id` bigint UNSIGNED NOT NULL,
  `role_id` bigint UNSIGNED NOT NULL,
  `permission_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles_permissions`
--

INSERT INTO `roles_permissions` (`id`, `role_id`, `permission_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 2, 2),
(8, 2, 3),
(9, 2, 4),
(10, 2, 5),
(11, 2, 6),
(12, 3, 2),
(13, 3, 5),
(14, 3, 6),
(15, 4, 3),
(16, 4, 6),
(17, 3, 1),
(18, 3, 3),
(19, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `setting_jobs`
--

CREATE TABLE `setting_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('salaries','levels','skills') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image_path` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `setting_jobs`
--

INSERT INTO `setting_jobs` (`id`, `name`, `type`, `slug`, `description`, `image_path`, `created_at`, `updated_at`) VALUES
(4, '15 - 20 triệu', 'salaries', '15-20-trieu', 'tttt', NULL, '2022-12-02 02:54:00', '2023-01-04 09:09:35'),
(11, '10 - 15 triệu', 'salaries', '10-15-trieu', 'Ngon bổ rẻ', NULL, '2022-12-15 03:16:42', '2023-01-04 09:09:46'),
(12, '5 - 10 triệu', 'salaries', '5-10-trieu', 'ngon bổ rẻ', NULL, '2022-12-15 03:16:58', '2023-01-04 09:10:12'),
(13, '20 - 25 triệu', 'salaries', '20-25-trieu', 'ngon bổ rẻ', NULL, '2022-12-15 03:18:04', '2023-01-04 09:10:28'),
(14, '5 triệu', 'salaries', '5-trieu', 'ggggg', NULL, '2022-12-15 03:18:30', '2023-01-04 09:10:45'),
(16, 'java', 'skills', 'java', 'java', 'imageSkill/YVf8YQbakHZX2CnW78oKPX0EIxAlZ4m2QB5ufaH9.png', '2022-12-19 08:53:52', '2023-02-06 11:32:21'),
(17, 'mobile', 'skills', 'mobile', 'mobile', 'imageSkill/jBh4cDJo3xV7FTVXWTiGf0FtfkrPK3mJ8CFMnhT7.png', '2022-12-19 08:54:05', '2023-02-08 03:32:05'),
(18, 'Middle', 'levels', 'middle', 'Middle', NULL, '2022-12-19 08:54:46', '2022-12-29 07:19:44'),
(19, 'Senior', 'levels', 'senior', 'Senior', NULL, '2022-12-19 08:54:54', '2022-12-29 07:19:23'),
(20, 'Junior', 'levels', 'junior', 'JUNIOR', NULL, '2022-12-19 08:55:03', '2022-12-29 07:19:33'),
(21, 'ReactJS', 'skills', 'reactjs', 'ReactJS', 'imageSkill/vDAHEToo29VifDsey8nd6FMbOFPz0hrgiD97hpSa.png', '2022-12-21 06:48:56', '2022-12-21 06:48:56'),
(22, 'Net', 'skills', 'net', 'Net', 'imageSkill/pZKsVWjct27NJa1DQg3bTzcZpISRmyt4hMkb58Ws.png', '2022-12-21 06:49:17', '2022-12-21 06:49:17'),
(23, 'Golang', 'skills', 'golang', 'Golang', 'imageSkill/wgpA56KUo8LU51iS4rmLkB9jMlFLs6mmgw6BgG0U.png', '2022-12-29 07:43:19', '2022-12-29 07:43:19'),
(24, 'Sale', 'skills', 'sale', 'Sale', NULL, '2022-12-29 07:44:25', '2022-12-29 07:44:25'),
(25, 'C++', 'skills', 'c', 'C++', 'imageSkill/ONqLX6DyOoJuLfnhZ56dDWjkmedSjKnA2vQHydLA.png', '2023-01-03 08:37:10', '2023-01-03 08:37:10'),
(26, 'Python', 'skills', 'python', 'Python', 'imageSkill/uqxYeyKWVXQZsq6hILmUZih2hVGjgIh1GpjrPkVS.png', '2023-01-03 08:37:29', '2023-01-03 08:37:29'),
(27, 'Admin IT', 'skills', 'admin-it', 'Admin IT', 'imageSkill/xnAsIu6VPJwDab8nSwo0VKpkp8hnPqPXF77omoW9.png', '2023-01-03 08:37:47', '2023-01-03 08:37:47'),
(28, 'Flutter', 'skills', 'flutter', 'Flutter', 'imageSkill/vnG5OdKy8qcYYsprgAU5rjS0SP1aZQn0wCerrHAz.png', '2023-01-03 08:38:06', '2023-01-03 08:38:19'),
(29, 'DevOps', 'skills', 'devops', 'DevOps', 'imageSkill/6GolqAo5XeodRBHwz6mCNTgn5Qu9cosapI1fpnf3.jpg', '2023-01-03 08:39:26', '2023-01-03 08:39:26'),
(30, 'PHP', 'skills', 'php', 'PHP', 'imageSkill/vAWkTi7uMzSyyFdmjzGKOYhumWiY8hVIf00tseLK.jpg', '2023-01-04 02:43:08', '2023-01-11 04:49:29');

-- --------------------------------------------------------

--
-- Table structure for table `setting_kpis`
--

CREATE TABLE `setting_kpis` (
  `id` bigint UNSIGNED NOT NULL,
  `week` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `month` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `role_id` bigint UNSIGNED NOT NULL,
  `status` enum('active','inactive') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `setting_kpis`
--

INSERT INTO `setting_kpis` (`id`, `week`, `month`, `role_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '{\"candidates\":\"0\",\"cv_interview\":\"0\",\"cv_customer\":\"0\"}', '{\"candidates\":\"0\",\"cv_interview\":\"0\",\"cv_customer\":\"0\"}', 1, 'active', '2022-12-13 04:51:29', '2022-12-13 04:51:29'),
(2, '{\"candidates\":\"0\",\"cv_interview\":\"0\",\"cv_customer\":\"0\"}', '{\"candidates\":\"0\",\"cv_interview\":\"0\",\"cv_customer\":\"0\"}', 2, 'active', '2022-12-13 04:51:38', '2022-12-13 04:51:38'),
(3, '{\"candidates\":\"0\",\"cv_interview\":\"0\",\"cv_customer\":\"0\"}', '{\"candidates\":\"0\",\"cv_interview\":\"0\",\"cv_customer\":\"0\"}', 3, 'active', '2022-12-13 04:51:52', '2022-12-13 04:51:52'),
(4, '{\"candidates\":\"0\",\"cv_interview\":\"0\",\"cv_customer\":\"0\"}', '{\"candidates\":\"0\",\"cv_interview\":\"0\",\"cv_customer\":\"0\"}', 4, 'active', '2022-12-13 04:51:59', '2022-12-13 04:51:59');

-- --------------------------------------------------------

--
-- Table structure for table `short_urls`
--

CREATE TABLE `short_urls` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `destination_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `key_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activated_at` datetime NOT NULL DEFAULT '2022-12-02 10:02:47',
  `deactivated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `short_urls`
--

INSERT INTO `short_urls` (`id`, `user_id`, `destination_url`, `short_url`, `key_code`, `ip_address`, `browser`, `device_type`, `activated_at`, `deactivated_at`, `created_at`, `updated_at`) VALUES
(2, '24ed8b74-b209-4458-a67e-a92a761437b3', 'http://localhost:8000/tuyen-dung/business-development-manager-remote?source=google&id=24ed8b74-b209-4458-a67e-a92a761437b3', 'http://localhost:8000/tuyen-dung-business-development-manager-remote-lxa8jg', 'lxa8jg', '127.0.0.1', 'Chrome', 'Windows 10', '2022-12-02 10:02:47', NULL, '2023-01-11 04:41:44', '2023-01-11 04:41:44'),
(4, '24ed8b74-b209-4458-a67e-a92a761437b3', 'http://localhost:8000/tuyen-dung/customer-service-specialist-remote?source=google&id=24ed8b74-b209-4458-a67e-a92a761437b3', 'http://localhost:8000/tuyen-dung-customer-service-specialist-remote-tdyyzu', 'tdyyzu', '127.0.0.1', 'Chrome', 'Windows 10', '2022-12-02 10:02:47', NULL, '2023-01-16 16:50:31', '2023-01-16 16:50:31'),
(6, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'http://localhost:8000/tuyen-dung/business-development-manager-remote?source=13r&id=013db4b6-92a7-4cbd-a1d4-fce422684e70', 'http://localhost:8000/tuyen-dung-business-development-manager-remote-dwlrpr', 'dwlrpr', '127.0.0.1', 'Chrome', 'Windows 10', '2022-12-02 10:02:47', NULL, '2023-02-02 04:26:07', '2023-02-02 04:26:07'),
(7, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'http://localhost:8000/tuyen-dung/htq?source=13r&id=013db4b6-92a7-4cbd-a1d4-fce422684e70', 'http://localhost:8000/tuyen-dung-htq-5a1zge', '5a1zge', '127.0.0.1', 'Chrome', 'Windows 10', '2022-12-02 10:02:47', NULL, '2023-02-08 06:31:40', '2023-02-08 06:31:40'),
(8, '013db4b6-92a7-4cbd-a1d4-fce422684e70', 'http://localhost:8000/tuyen-dung/reactjs-developer-onsite-de-la-thanh-2?source=13r&id=013db4b6-92a7-4cbd-a1d4-fce422684e70', 'http://localhost:8000/tuyen-dung-reactjs-developer-onsite-de-la-thanh-2-ijwyik', 'ijwyik', '127.0.0.1', 'Chrome', 'Windows 10', '2022-12-02 10:02:47', NULL, '2023-02-08 06:31:44', '2023-02-08 06:31:44');

-- --------------------------------------------------------

--
-- Table structure for table `sources`
--

CREATE TABLE `sources` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` bigint UNSIGNED DEFAULT NULL,
  `url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sources`
--

INSERT INTO `sources` (`id`, `name`, `slug`, `parent_id`, `url`, `created_at`, `updated_at`, `image`) VALUES
(2, '13r', '13r', NULL, 'r2', '2023-02-01 04:31:51', '2023-02-01 04:31:51', 'imageblogs/UhUHJEDYvOpdt9iG3fzezodPdhMMvD23441vzy0S.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'uuid gen auto in code',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `role_id` bigint UNSIGNED DEFAULT NULL,
  `status` enum('active','inactive','pending') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` int NOT NULL,
  `parent_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `phone`, `avatar`, `role_id`, `status`, `remember_token`, `created_at`, `updated_at`, `code`, `parent_id`) VALUES
('013db4b6-92a7-4cbd-a1d4-fce422684e70', 'leaderdivision@takumi.com', '$2y$10$59ugfO9kIkHlP6c9121Zx.Dsp1SgzCwDNC3yz1N6Kv20VsxixA6oy', 'Leader Division A', '0942389599', NULL, 1, 'active', NULL, '2022-11-30 08:28:54', '2023-02-14 06:46:01', 2, NULL),
('09d0e76d-8f0e-4826-9288-8b0379c17dc3', 'phonglv@hachinet.com', '$2y$10$tc3wMbx/YNHdsUW3Cq6Ri.sA1W5mqclzJthLqP947LNCv3B2Fswqm', 'Lê Văn  Phong', NULL, 'https://secure.gravatar.com/avatar/4b2968687acd275da43f8de459cbd4bb?s=80&d=identicon', 2, 'active', 'Dczv8jZXBropWket8rBb225MYbv13P394iNK2ACCcbKbqBamu2nXYPWlp9e0', '2023-01-31 07:34:21', '2023-01-31 07:34:51', 11, NULL),
('1979c566-9db7-4114-b949-7982572fe7b7', 'hoangdv@hachinet.com', '$2y$10$VLWSsAWhN94cC/hSgYyvuuUocfrhBDkH92RbBVuWZIOSf/vR4qDFG', 'Đinh Việt Hoàng', NULL, 'https://git.hachinet.com/uploads/-/system/user/avatar/438/avatar.png', 3, 'active', 'OXJaopER3O5BWi4C6wH4TlsR22DQJqLXy103hIxamk8tVDZe7tDieCyzcLv4', '2022-12-13 07:00:26', '2022-12-13 07:01:12', 7, NULL),
('24ed8b74-b209-4458-a67e-a92a761437b3', 'admin@takumi.com', '$2y$10$hWHC5HDDCP7ii8ARjZRCzu4hHTB/kskVQQs0CNYWylfulaONcLA3i', 'Admin', '0942389598', NULL, NULL, 'active', 'ges7hUr2pbcNp5d6f9vZjXxuqqi46DLyiHvetOVhRmAIGOr8VAelx8PELuBj', '2022-11-30 08:28:54', NULL, 1, NULL),
('3d139d09-7552-47ed-b68d-0f4dae9e01aa', 'phantv2k3@gmail.com', '$2y$10$6inhi6VEU2KiGxHoRHZjG.SK3YAwmUvRYXxmhVt7KOUpcrIG9gO.K', 'Leader B', '0942389597', NULL, 2, 'active', NULL, '2022-11-30 08:28:54', NULL, 3, NULL),
('49a6da64-36a9-4580-b4fb-74c40cd95d9f', 'khac@takumi.com', '$2y$10$64VvM2UoNhGTi2xYht2qlue1qRrpnqFzQYR1uxcylXh8xZTiRHH.G', 'trong ngu', '0942389594', NULL, 5, 'active', NULL, '2022-11-30 08:28:54', '2022-12-19 07:36:24', 6, NULL),
('4b9fadba-2b78-420d-bb5a-14a47901b72d', 'sale@takumi.com', '$2y$10$g2u9cWclC6c6AEYaa3UPTe/4HP45CLI2XJaP6PXCPFQNt/TTMz3a6', 'Sale D', '0942389595', NULL, 4, 'active', NULL, '2022-11-30 08:28:54', NULL, 5, NULL),
('4d69b2b9-6b9a-4052-821b-b6b5c02cd46b', 'hr@takumi.com', '$2y$10$CZ.pMyFX.FqTplrSzAF3meqR/1S4HcQ8vJaSwH2D4ltZPW/DqL.JW', 'HR C', '0942389596', NULL, 3, 'active', NULL, '2022-11-30 08:28:54', NULL, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_bonus`
--

CREATE TABLE `users_bonus` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `level_bonus` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_bonus` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `apply_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `month` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `week` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_bonus`
--

INSERT INTO `users_bonus` (`id`, `user_id`, `level_bonus`, `role_bonus`, `apply_id`, `created_at`, `updated_at`, `month`, `week`) VALUES
(1, 'ba3e1018-4b76-44ed-bb33-114dd17deb08', '0', '0', 'a97c6066-25e9-401d-964a-3796cd8d5517', '2023-01-13 03:18:06', '2023-01-16 15:11:22', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"1\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(2, 'ba3e1018-4b76-44ed-bb33-114dd17deb08', '0', '0', '59bcf0f6-99c5-48c2-b37f-bdde4546c5ab', '2023-01-17 02:54:48', '2023-01-17 03:03:32', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"2\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(3, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', 'd4105d69-2dca-408a-91c5-4c5a63a179a5', '2023-01-17 03:20:13', '2023-01-17 03:20:13', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(4, '49a6da64-36a9-4580-b4fb-74c40cd95d9f', '0', '0', 'd4105d69-2dca-408a-91c5-4c5a63a179a5', '2023-01-17 03:20:13', '2023-01-17 03:20:13', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(5, '3d139d09-7552-47ed-b68d-0f4dae9e01aa', '0', '0', 'd4105d69-2dca-408a-91c5-4c5a63a179a5', '2023-01-17 03:20:13', '2023-01-17 03:20:13', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(6, '1979c566-9db7-4114-b949-7982572fe7b7', '0', '0', 'd4105d69-2dca-408a-91c5-4c5a63a179a5', '2023-01-17 03:20:13', '2023-01-17 03:20:13', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(7, '4d69b2b9-6b9a-4052-821b-b6b5c02cd46b', '0', '0', 'd4105d69-2dca-408a-91c5-4c5a63a179a5', '2023-01-17 03:20:13', '2023-01-17 03:20:13', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(8, 'ba3e1018-4b76-44ed-bb33-114dd17deb08', '0', '0', 'd4105d69-2dca-408a-91c5-4c5a63a179a5', '2023-01-17 03:20:14', '2023-01-17 03:20:14', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"2\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(9, '4b9fadba-2b78-420d-bb5a-14a47901b72d', '0', '0', 'd4105d69-2dca-408a-91c5-4c5a63a179a5', '2023-01-17 03:20:14', '2023-01-17 03:20:14', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(10, 'ba3e1018-4b76-44ed-bb33-114dd17deb08', '0', '0', '891934e7-898f-4d5e-8d4b-5479a41c7c63', '2023-01-17 03:27:47', '2023-01-17 03:29:50', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"3\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(11, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '891934e7-898f-4d5e-8d4b-5479a41c7c63', '2023-01-17 03:29:50', '2023-01-17 03:29:50', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(12, '49a6da64-36a9-4580-b4fb-74c40cd95d9f', '0', '0', '891934e7-898f-4d5e-8d4b-5479a41c7c63', '2023-01-17 03:29:50', '2023-01-17 03:29:50', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(13, '3d139d09-7552-47ed-b68d-0f4dae9e01aa', '0', '0', '891934e7-898f-4d5e-8d4b-5479a41c7c63', '2023-01-17 03:29:50', '2023-01-17 03:29:50', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(14, '1979c566-9db7-4114-b949-7982572fe7b7', '0', '0', '891934e7-898f-4d5e-8d4b-5479a41c7c63', '2023-01-17 03:29:50', '2023-01-17 03:29:50', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(15, '4d69b2b9-6b9a-4052-821b-b6b5c02cd46b', '0', '0', '891934e7-898f-4d5e-8d4b-5479a41c7c63', '2023-01-17 03:29:50', '2023-01-17 03:29:50', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(16, '4b9fadba-2b78-420d-bb5a-14a47901b72d', '0', '0', '891934e7-898f-4d5e-8d4b-5479a41c7c63', '2023-01-17 03:29:50', '2023-01-17 03:29:50', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(17, 'ba3e1018-4b76-44ed-bb33-114dd17deb08', '0', '0', '1923ff8c-e1a1-4b1a-83db-27b08c1976df', '2023-01-17 04:10:45', '2023-01-17 04:11:02', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"4\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(18, 'ba3e1018-4b76-44ed-bb33-114dd17deb08', '0', '0', '9a00662e-dca6-4d4f-9dec-ea203ca379a1', '2023-01-17 09:07:29', '2023-01-17 09:07:52', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"5\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(19, 'ba3e1018-4b76-44ed-bb33-114dd17deb08', '0', '0', '7466ced3-328a-4a35-8d13-fc4443c9ee25', '2023-01-17 09:09:09', '2023-01-17 09:09:18', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"6\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(20, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '676b1798-d951-4b4a-81b9-db0b2c1a65d6', '2023-01-31 07:38:17', '2023-01-31 07:38:27', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"1\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(21, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '87520a08-f494-4a2a-ac1d-04b892adbf5e', '2023-01-31 07:46:49', '2023-01-31 08:07:46', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"2\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(22, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '8cf2d3b0-2bbb-41ab-8fef-2801416546b1', '2023-02-01 10:11:32', '2023-02-01 10:43:47', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"1\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(23, '49a6da64-36a9-4580-b4fb-74c40cd95d9f', '0', '0', '8cf2d3b0-2bbb-41ab-8fef-2801416546b1', '2023-02-01 10:43:47', '2023-02-01 10:43:47', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(24, '3d139d09-7552-47ed-b68d-0f4dae9e01aa', '0', '0', '8cf2d3b0-2bbb-41ab-8fef-2801416546b1', '2023-02-01 10:43:47', '2023-02-01 10:43:47', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(25, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '8cf2d3b0-2bbb-41ab-8fef-2801416546b1', '2023-02-01 10:43:47', '2023-02-01 10:43:47', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(26, '1979c566-9db7-4114-b949-7982572fe7b7', '0', '0', '8cf2d3b0-2bbb-41ab-8fef-2801416546b1', '2023-02-01 10:43:47', '2023-02-01 10:43:47', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(27, '4d69b2b9-6b9a-4052-821b-b6b5c02cd46b', '0', '0', '8cf2d3b0-2bbb-41ab-8fef-2801416546b1', '2023-02-01 10:43:47', '2023-02-01 10:43:47', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(28, '4b9fadba-2b78-420d-bb5a-14a47901b72d', '0', '0', '8cf2d3b0-2bbb-41ab-8fef-2801416546b1', '2023-02-01 10:43:47', '2023-02-01 10:43:47', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(29, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', 'b596c876-b923-44a3-b7ee-a1a0d51f787c', '2023-02-01 10:47:00', '2023-02-01 10:47:00', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"1\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(30, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '23521601-b76e-400e-8ed8-b8bdee555387', '2023-02-01 10:47:24', '2023-02-02 04:17:42', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"3\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(31, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '180d3768-50cf-471c-82c0-b7716269c71c', '2023-02-01 10:47:58', '2023-02-02 04:20:47', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"4\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(32, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '31518d04-78fd-48c2-9942-ec10ea4b9b41', '2023-02-02 04:26:51', '2023-02-02 04:26:51', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"4\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(33, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', 'fe849def-90bf-4875-840e-eb5528396207', '2023-02-02 04:29:46', '2023-02-03 03:18:21', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"5\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(34, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '7b847bd0-000c-4740-8a6c-58419b75e37d', '2023-02-02 04:39:27', '2023-02-06 11:36:28', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"5\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(35, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '9d0ddff9-b72f-4103-9772-b8297700dbac', '2023-02-06 11:41:49', '2023-02-06 11:42:07', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"6\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(36, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '05572a21-9211-49ae-b65a-49dd93569301', '2023-02-07 03:07:58', '2023-02-07 03:08:05', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"7\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(37, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '0af6b45d-748a-4024-9b09-11f91458d0eb', '2023-02-07 03:09:24', '2023-02-07 03:09:31', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"1\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(38, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '2614a011-f179-4537-81a4-5d4e719741a9', '2023-02-07 03:10:57', '2023-02-07 03:10:57', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"7\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(39, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '4a400a85-5d3a-41c7-94ab-b1b7968956cd', '2023-02-07 03:12:05', '2023-02-07 03:12:05', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"7\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(40, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', 'c24d12bb-c2fd-4393-ad9b-09644b36e718', '2023-02-07 03:56:02', '2023-02-07 03:56:02', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"7\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(41, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', 'a67b290e-86d7-490a-b5f0-fa6aad443c9b', '2023-02-07 03:58:53', '2023-02-07 04:12:39', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"8\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(42, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '01f331c4-820b-4aca-891f-e059e1e39e25', '2023-02-07 04:31:22', '2023-02-07 04:31:22', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"8\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(43, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '34f82794-9829-4273-8768-979fe2d06b60', '2023-02-07 04:34:32', '2023-02-07 04:34:32', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"8\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(44, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '55f2d471-f59e-4d81-879d-e14ac89fbd93', '2023-02-07 04:35:30', '2023-02-07 04:35:46', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"2\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(45, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', 'd1ecf15e-454d-49cb-83ea-a7756cc3a508', '2023-02-07 06:58:10', '2023-02-07 06:58:10', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"8\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(46, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '4834d8be-9edf-4744-afb9-d298b94ac7b1', '2023-02-07 06:58:58', '2023-02-07 06:58:58', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"8\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(47, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '32603b01-39bb-452e-b660-d55c81e9d78c', '2023-02-07 07:00:16', '2023-02-07 07:00:16', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"8\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(48, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '882998c0-b055-4fb5-a5f0-c264f1f14540', '2023-02-07 07:02:01', '2023-02-07 08:48:00', '{\"candidates\":\"1\\/0\",\"cv_interview\":\"9\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(49, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', 'a3362844-6924-488a-93a1-2c77c62b48e2', '2023-02-08 02:09:39', '2023-02-08 02:09:46', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"3\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(50, '4b9fadba-2b78-420d-bb5a-14a47901b72d', '0', '0', '1c087bf7-2138-4342-9d25-b9e1f590cf59', '2023-02-08 02:16:52', '2023-02-08 02:16:52', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(51, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '7d35cf2e-3a85-4fb7-ad51-cfae075de436', '2023-02-08 02:18:58', '2023-02-08 02:18:58', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"3\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(52, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '4f5d9dd1-75bf-4b0b-af6a-89da02613173', '2023-02-08 03:53:39', '2023-02-08 05:29:27', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"4\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(53, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '3b32a9ab-23a6-4241-9640-11e92183a19d', '2023-02-08 04:09:20', '2023-02-08 05:38:20', '{\"candidates\":\"2\\/0\",\"cv_interview\":\"10\\/0\",\"cv_customer\":\"2\\/0\"}', NULL),
(54, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '49b71eff-56b1-4293-9780-3a62c8f3bfab', '2023-02-08 04:21:39', '2023-02-08 05:25:33', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"4\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(55, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '2213fba0-3dfd-4a2a-b7fe-525abfbddbfa', '2023-02-08 04:22:20', '2023-02-08 05:31:36', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"4\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(56, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', 'b0eadd83-366b-4bc9-9b5f-a33bbc1e88cf', '2023-02-08 04:22:32', '2023-02-09 10:59:12', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"4\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(57, '49a6da64-36a9-4580-b4fb-74c40cd95d9f', '0', '0', '3b32a9ab-23a6-4241-9640-11e92183a19d', '2023-02-08 05:38:20', '2023-02-08 05:38:20', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(58, '3d139d09-7552-47ed-b68d-0f4dae9e01aa', '0', '0', '3b32a9ab-23a6-4241-9640-11e92183a19d', '2023-02-08 05:38:20', '2023-02-08 05:38:20', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(59, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '3b32a9ab-23a6-4241-9640-11e92183a19d', '2023-02-08 05:38:20', '2023-02-08 05:38:20', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"4\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(60, '1979c566-9db7-4114-b949-7982572fe7b7', '0', '0', '3b32a9ab-23a6-4241-9640-11e92183a19d', '2023-02-08 05:38:20', '2023-02-08 05:38:20', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(61, '4d69b2b9-6b9a-4052-821b-b6b5c02cd46b', '0', '0', '3b32a9ab-23a6-4241-9640-11e92183a19d', '2023-02-08 05:38:20', '2023-02-08 05:38:20', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"0\\/0\"}', NULL),
(62, '4b9fadba-2b78-420d-bb5a-14a47901b72d', '0', '0', '3b32a9ab-23a6-4241-9640-11e92183a19d', '2023-02-08 05:38:20', '2023-02-08 05:38:20', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"0\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(63, '4d69b2b9-6b9a-4052-821b-b6b5c02cd46b', '0', '0', 'ce8285e3-8754-475b-b7c3-91eb044033d7', '2023-02-08 09:45:03', '2023-02-09 07:40:07', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"1\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(64, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '1db60f0b-797c-4792-b6b5-7e664ef07ec2', '2023-02-08 09:50:23', '2023-02-08 09:50:23', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"4\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(65, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '01f331c4-820b-4aca-891f-e059e1e39e25', '2023-02-08 09:56:26', '2023-02-08 09:56:26', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"4\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(66, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '89a1d5c1-9935-4971-9ace-8a2bd0912983', '2023-02-09 09:26:21', '2023-02-09 09:27:39', '{\"candidates\":\"2\\/0\",\"cv_interview\":\"11\\/0\",\"cv_customer\":\"3\\/0\"}', NULL),
(67, '09d0e76d-8f0e-4826-9288-8b0379c17dc3', '0', '0', '7be9bafd-31d8-4c7d-8f6a-28bf56cd7e7e', '2023-02-14 08:05:10', '2023-02-14 08:05:10', '{\"candidates\":\"0\\/0\",\"cv_interview\":\"4\\/0\",\"cv_customer\":\"1\\/0\"}', NULL),
(68, '013db4b6-92a7-4cbd-a1d4-fce422684e70', '0', '0', '7cae553b-1845-429a-a31c-812408ea3bed', '2023-02-14 10:12:51', '2023-02-14 10:12:51', '{\"candidates\":\"2\\/0\",\"cv_interview\":\"11\\/0\",\"cv_customer\":\"3\\/0\"}', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applies`
--
ALTER TABLE `applies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `applies_candidate_id_foreign` (`candidate_id`),
  ADD KEY `applies_job_id_foreign` (`job_id`),
  ADD KEY `applies_source_id_foreign` (`source_id`),
  ADD KEY `applies_user_id_foreign` (`user_id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blogs_title_unique` (`title`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `blogs_category_id_foreign` (`category_id`);

--
-- Indexes for table `bonuses`
--
ALTER TABLE `bonuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `candidates_email_unique` (`email`),
  ADD UNIQUE KEY `candidates_phone_unique` (`phone`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `candidates_customers`
--
ALTER TABLE `candidates_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `jobs_user_id_foreign` (`user_id`);

--
-- Indexes for table `job_details`
--
ALTER TABLE `job_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_details_job_id_foreign` (`job_id`),
  ADD KEY `job_details_province_id_foreign` (`province_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifies`
--
ALTER TABLE `notifies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifies_user_id_foreign` (`user_id`);

--
-- Indexes for table `notify_actisions`
--
ALTER TABLE `notify_actisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notify_actisions_user_id_foreign` (`user_id`),
  ADD KEY `notify_actisions_notify_id_foreign` (`notify_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_permissions_role_id_foreign` (`role_id`),
  ADD KEY `roles_permissions_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `setting_jobs`
--
ALTER TABLE `setting_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_kpis`
--
ALTER TABLE `setting_kpis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `setting_kpis_role_id_foreign` (`role_id`);

--
-- Indexes for table `short_urls`
--
ALTER TABLE `short_urls`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `short_urls_key_code_unique` (`key_code`),
  ADD KEY `short_urls_user_id_foreign` (`user_id`);

--
-- Indexes for table `sources`
--
ALTER TABLE `sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `code` (`code`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `users_bonus`
--
ALTER TABLE `users_bonus`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applies`
--
ALTER TABLE `applies`
  MODIFY `code` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `code` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `bonuses`
--
ALTER TABLE `bonuses`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `code` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `candidates_customers`
--
ALTER TABLE `candidates_customers`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `code` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `job_details`
--
ALTER TABLE `job_details`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `notifies`
--
ALTER TABLE `notifies`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `notify_actisions`
--
ALTER TABLE `notify_actisions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `setting_jobs`
--
ALTER TABLE `setting_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `setting_kpis`
--
ALTER TABLE `setting_kpis`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `short_urls`
--
ALTER TABLE `short_urls`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sources`
--
ALTER TABLE `sources`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `code` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users_bonus`
--
ALTER TABLE `users_bonus`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `applies`
--
ALTER TABLE `applies`
  ADD CONSTRAINT `applies_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `candidates` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `applies_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `applies_source_id_foreign` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `applies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `jobs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `job_details`
--
ALTER TABLE `job_details`
  ADD CONSTRAINT `job_details_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `job_details_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifies`
--
ALTER TABLE `notifies`
  ADD CONSTRAINT `notifies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notify_actisions`
--
ALTER TABLE `notify_actisions`
  ADD CONSTRAINT `notify_actisions_notify_id_foreign` FOREIGN KEY (`notify_id`) REFERENCES `notifies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notify_actisions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD CONSTRAINT `roles_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `roles_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `setting_kpis`
--
ALTER TABLE `setting_kpis`
  ADD CONSTRAINT `setting_kpis_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `short_urls`
--
ALTER TABLE `short_urls`
  ADD CONSTRAINT `short_urls_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
